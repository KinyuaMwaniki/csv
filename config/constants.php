<?php

return [
    'permissions' => [
        [
            'name' =>          'view all leads',
            'model' =>          'Lead'
        ],
        [
            'name' =>          'view own leads',
            'model' =>          'Lead'
        ],
        [
            'name' =>          'create leads',
            'model' =>          'Lead'
        ],
        [
            'name' =>          'edit leads',
            'model' =>          'Lead'
        ],
        [
            'name' =>          'delete leads',
            'model' =>          'Lead'
        ],
    ],
    'settings' => [
        'date_format' => 'd-m-y',
    ]
];
