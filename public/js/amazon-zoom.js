function amazonZoom(imgID, mainID, rectID, zoomID, imgSrc) {
    var picture = null;
    var zoom = null;
    picture = document.getElementById(imgID);
    var mainContainer = document.getElementById(mainID);
    var rect = document.getElementById(rectID);
    zoom = document.getElementById(zoomID);
    zoom.style.backgroundImage = 'none';

    let w1 = mainContainer.offsetWidth;
    let h1 = mainContainer.offsetHeight;

    let ratio = 3;

    zoom.style.backgroundSize = w1 * ratio + "px " + h1 * ratio + "px";

    let x, y, xx, yy;

    let w2 = rect.offsetWidth;
    let h2 = rect.offsetHeight;

    zoom.style.backgroundImage = "url('" + imgSrc + "')";

    w2 = w2 / 2;
    h2 = h2 / 2;

    function move(event) {
        x = event.offsetX;
        y = event.offsetY;

        xx = x - w2;
        yy = y - h2;
        if (x < w2) {
            x = w2;
            xx = 0;
        }
        if (x > w1 - w2) {
            x = w1 - w2;
            xx = x - w2;
        }
        if (y < h2) {
            y = h2;
            yy = 0;
        }
        if (y > h1 - h2) {
            y = h1 - h2;
        }

        xx = xx * ratio;
        yy = yy * ratio;
        rect.style.left = x + "px";
        rect.style.top = y + "px";
        zoom.style.backgroundPosition = "-" + xx + "px " + "-" + yy + "px";
    }

    mainContainer.addEventListener("mousemove", function() {
        move(event);
        addOpacity();
    });

    function addOpacity() {
        rect.classList.add("rect-active");
        zoom.classList.add("rect-active");
    }

    function removeOpacity() {
        zoom.classList.remove("rect-active");
    }

    mainContainer.addEventListener("mouseout", function() {
        removeOpacity();
    });
}
