var placeSearch, autocomplete, autocomplete_billing;

const componentForm = {
    street_number: "short_name",
    sublocality_level_1: "long_name",
    locality: "long_name",
    administrative_area_level_1: "short_name",
    country: "long_name",
    postal_code: "short_name",
};

const billingComponentForm = {
    street_number: "short_name",
    sublocality_level_1: "long_name",
    locality: "long_name",
    administrative_area_level_1: "short_name",
    country: "long_name",
    postal_code: "short_name",
};
