<?php

namespace Tests\Feature;

use Faker\Factory;
use Tests\TestCase;
use App\Models\Role;
use App\Models\User;
use App\Models\Company;
use App\Models\Country;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CountryTest extends TestCase
{
    use RefreshDatabase;

    // php artisan test --filter CountryTest

    public function createUser()
    {
        $country = Country::factory()->create();
        $company = Company::factory()->create([
            'country_id' => $country->id
        ]);
        $role = Role::factory()->create([
            'company_id' => $company->id,
        ]);
        $user = Sanctum::actingAs(
            User::factory()->create([
                'company_id' => $company->id
            ]),
            ['view-tasks']
        );
        return $user;
    }

    /**
     * @test
     */
    public function non_authenticated_users_cannot_access_the_following_endpoints_for_the_countries_api()
    {
        $index = $this->json('POST', '/api/v1/countries');
        $index1 = $this->json('GET', '/api/v1/countries');
        $index2 = $this->json('PUT', '/api/v1/countries/-1');
        $index3 = $this->json('GET', '/api/v1/countries/-1');
        $index4 = $this->json('DELETE', '/api/v1/countries/-1');

        // Unauthorized means status 401
        $index->assertUnauthorized();
        $index1->assertUnauthorized();
        $index2->assertUnauthorized();
        $index3->assertUnauthorized();
        $index4->assertUnauthorized();
    }

    /**
     * @test
     */
    public function can_return_a_collection_of_non_paginated_countries()
    {
        $user = $this->createUser();

        Country::factory()->count(3)->create();

        $response = $this->actingAs($user)->json('GET', '/api/v1/countries');

        $response->assertOk()
            ->assertJsonStructure([
                'countries' => [
                    '*' => [
                        'id', 'name'
                    ]
                ],

            ])->assertJsonCount(4, 'countries');
    }

    /**
     * @test
     */
    public function will_fail_with_validation_errors_when_creating_a_country_with_wrong_inputs()
    {
        $faker = Factory::create();

        $user = $this->createUser();

        $response = $this->actingAs($user)->json('POST', '/api/v1/countries', [
            'iso' => $iso = strtoupper($faker->lexify('??')),
            'nicename' => $nicename = $faker->name,
            'iso3' => $iso3 = strtoupper($faker->lexify('???')),
            'numcode' => $numcode = $faker->randomNumber($nbDigits = 4, $strict = false),
            'phonecode' => $phonecode = $faker->areaCode,
            'status' => $status = $faker->randomElement([0, 1]),
        ]);

        $response->assertStatus(422)
            ->assertJsonValidationErrors(['name']);

        $response = $this->actingAs($user)->json('POST', '/api/v1/countries', [
            'iso' => $iso = strtoupper($faker->lexify('??')),
            'name' => $name = $faker->name,
            'nicename' => $nicename = $faker->name,
            'iso3' => $iso3 = strtoupper($faker->lexify('???')),
            'phonecode' => $phonecode = $faker->areaCode,
            'status' => $status = $faker->randomElement([0, 1]),
        ]);

        $response->assertStatus(422)
            ->assertJsonValidationErrors(['numcode']);
    }

    /**
     * @test
     */
    public function can_create_a_country()
    {

        $faker = Factory::create();

        $user = $this->createUser();

        $response = $this->actingAs($user)->json('POST', '/api/v1/countries', [
            'iso' => $iso = strtoupper($faker->lexify('??')),
            'name' => $name = $faker->name,
            'nicename' => $nicename = $faker->name,
            'iso3' => $iso3 = strtoupper($faker->lexify('???')),
            'numcode' => $numcode = $faker->randomNumber($nbDigits = 4, $strict = false),
            'phonecode' => $phonecode = $faker->areaCode,
            'status' => $status = $faker->randomElement([0, 1]),
        ]);

        $response->assertJsonStructure([
            'id', 'iso', 'name', 'nicename', 'iso3', 'numcode', 'phonecode', 'status'
        ])
            ->assertJson([
                'iso' => $iso,
                'name' => $name,
                'nicename' => $nicename,
                'iso3' => $iso3,
                'numcode' => $numcode,
                'phonecode' => $phonecode,
                'status' => $status
            ])
            ->assertStatus(201);

        $this->assertDatabaseHas('tbl_country', [
                'iso' => $iso,
                'name' => $name,
                'nicename' => $nicename,
                'iso3' => $iso3,
                'numcode' => $numcode,
                'phonecode' => $phonecode,
                'status' => $status
        ]);
    }

    /**
     * @test
     */
    public function will_fail_with_a_404_if_country_is_not_found()
    {
        $user = $this->createUser();
        $response = $this->actingAs($user)->json('GET', 'api/v1/countries/-1');
        $response->assertNotFound();
    }

    /**
     * @test
     */
    public function can_return_a_budget_item()
    {
        // Given
        $user = $this->createUser();
        $country = Country::factory()->create();


        // When
        $response = $this->actingAs($user)->json('GET', "api/v1/countries/$country->id");

        // Then
        $response->assertOk()
            ->assertExactJson([
                'id' => $country->id,
                'iso' => $country->iso,
                'name' => $country->name,
                'nicename' => $country->nicename,
                'iso3' => $country->iso3,
                'numcode' => $country->numcode,
                'phonecode' => $country->phonecode,
                'status' => $country->status,
                'created_at' => (string)$country->created_at
            ]);
    }

    /**
     * @test
     */
    public function will_fail_with_a_404_if_a_country_we_want_to_update_is_not_found()
    {
        $user = $this->createUser();
        $response = $this->actingAs($user)->json('PUT', 'api/v1/countries/-1', [
            'name' => 'Updated name',
        ]);

        $response->assertNotFound();
    }

    /**
     * @test
     */
    public function can_update_a_country()
    {
        // Given
        $faker = Factory::create();

        $user = $this->createUser();

        $country = Country::factory()->create();

        // When
        $response = $this->actingAs($user)->json('PUT', "api/v1/countries/$country->id", [
            'iso' => $iso = strtoupper($faker->lexify('??')),
            'name' => $name = $faker->name,
            'nicename' => $nicename = $faker->name,
            'iso3' => $iso3 = strtoupper($faker->lexify('???')),
            'numcode' => $numcode = $faker->randomNumber($nbDigits = 4, $strict = false),
            'phonecode' => $phonecode = $faker->areaCode,
            'status' => $status = $faker->randomElement([0, 1]),
        ]);

        // Then
        $response->assertJsonStructure([
            'id', 'iso', 'name', 'nicename', 'iso3', 'numcode', 'phonecode', 'status'
        ])
            ->assertJson([
                'iso' => $iso,
                'name' => $name,
                'nicename' => $nicename,
                'iso3' => $iso3,
                'numcode' => $numcode,
                'phonecode' => $phonecode,
                'status' => $status
            ])
            ->assertOk();

        $this->assertDatabaseHas('tbl_country', [
                'iso' => $iso,
                'name' => $name,
                'nicename' => $nicename,
                'iso3' => $iso3,
                'numcode' => $numcode,
                'phonecode' => $phonecode,
                'status' => $status
        ]);
    }


    /**
     * @test
     */
    public function will_fail_with_a_404_if_a_country_we_want_to_delete_is_not_found()
    {

        $user = $this->createUser();

        $response = $this->actingAs($user)->json('DELETE', 'api/v1/countries/-1');
        $response->assertNotFound();
    }

    /**
     * @test
     */
    public function can_delete_a_country()
    {
        $user = $this->createUser();

        $country = Country::factory()->create();
        $response = $this->actingAs($user)->json('DELETE', "api/v1/countries/$country->id");

        $response->assertStatus(204)
            ->assertSee(null);

        $this->assertDatabaseMissing('tbl_country', ['id' => $country->id]);
    }
}
