-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 03, 2021 at 11:08 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `crm`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_city`
--

CREATE TABLE `tbl_city` (
  `id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `code` varchar(10) NOT NULL,
  `name` varchar(50) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_city`
--

INSERT INTO `tbl_city` (`id`, `state_id`, `code`, `name`, `status`) VALUES
(1, 26, 'LKO', 'Lucknow', 1),
(2, 26, 'KNP', 'Kanpur', 1),
(3, 26, 'LMP', 'Lakhimpur Kheri', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_country`
--

CREATE TABLE `tbl_country` (
  `id` int(11) NOT NULL,
  `iso` char(2) NOT NULL,
  `name` varchar(80) NOT NULL,
  `nicename` varchar(80) NOT NULL,
  `iso3` char(3) DEFAULT NULL,
  `numcode` smallint(6) DEFAULT NULL,
  `phonecode` int(5) NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_country`
--

INSERT INTO `tbl_country` (`id`, `iso`, `name`, `nicename`, `iso3`, `numcode`, `phonecode`, `status`) VALUES
(1, 'AF', 'AFGHANISTAN', 'Afghanistan', 'AFG', 4, 93, 1),
(2, 'AL', 'ALBANIA', 'Albania', 'ALB', 8, 355, 1),
(3, 'DZ', 'ALGERIA', 'Algeria', 'DZA', 12, 213, 1),
(4, 'AS', 'AMERICAN SAMOA', 'American Samoa', 'ASM', 16, 1684, 1),
(5, 'AD', 'ANDORRA', 'Andorra', 'AND', 20, 376, 1),
(6, 'AO', 'ANGOLA', 'Angola', 'AGO', 24, 244, 1),
(7, 'AI', 'ANGUILLA', 'Anguilla', 'AIA', 660, 1264, 1),
(8, 'AQ', 'ANTARCTICA', 'Antarctica', NULL, NULL, 0, 1),
(9, 'AG', 'ANTIGUA AND BARBUDA', 'Antigua and Barbuda', 'ATG', 28, 1268, 1),
(10, 'AR', 'ARGENTINA', 'Argentina', 'ARG', 32, 54, 1),
(11, 'AM', 'ARMENIA', 'Armenia', 'ARM', 51, 374, 1),
(12, 'AW', 'ARUBA', 'Aruba', 'ABW', 533, 297, 1),
(13, 'AU', 'AUSTRALIA', 'Australia', 'AUS', 36, 61, 1),
(14, 'AT', 'AUSTRIA', 'Austria', 'AUT', 40, 43, 1),
(15, 'AZ', 'AZERBAIJAN', 'Azerbaijan', 'AZE', 31, 994, 1),
(16, 'BS', 'BAHAMAS', 'Bahamas', 'BHS', 44, 1242, 1),
(17, 'BH', 'BAHRAIN', 'Bahrain', 'BHR', 48, 973, 1),
(18, 'BD', 'BANGLADESH', 'Bangladesh', 'BGD', 50, 880, 1),
(19, 'BB', 'BARBADOS', 'Barbados', 'BRB', 52, 1246, 1),
(20, 'BY', 'BELARUS', 'Belarus', 'BLR', 112, 375, 1),
(21, 'BE', 'BELGIUM', 'Belgium', 'BEL', 56, 32, 1),
(22, 'BZ', 'BELIZE', 'Belize', 'BLZ', 84, 501, 1),
(23, 'BJ', 'BENIN', 'Benin', 'BEN', 204, 229, 1),
(24, 'BM', 'BERMUDA', 'Bermuda', 'BMU', 60, 1441, 1),
(25, 'BT', 'BHUTAN', 'Bhutan', 'BTN', 64, 975, 1),
(26, 'BO', 'BOLIVIA', 'Bolivia', 'BOL', 68, 591, 1),
(27, 'BA', 'BOSNIA AND HERZEGOVINA', 'Bosnia and Herzegovina', 'BIH', 70, 387, 1),
(28, 'BW', 'BOTSWANA', 'Botswana', 'BWA', 72, 267, 1),
(29, 'BV', 'BOUVET ISLAND', 'Bouvet Island', NULL, NULL, 0, 1),
(30, 'BR', 'BRAZIL', 'Brazil', 'BRA', 76, 55, 1),
(31, 'IO', 'BRITISH INDIAN OCEAN TERRITORY', 'British Indian Ocean Territory', NULL, NULL, 246, 1),
(32, 'BN', 'BRUNEI DARUSSALAM', 'Brunei Darussalam', 'BRN', 96, 673, 1),
(33, 'BG', 'BULGARIA', 'Bulgaria', 'BGR', 100, 359, 1),
(34, 'BF', 'BURKINA FASO', 'Burkina Faso', 'BFA', 854, 226, 1),
(35, 'BI', 'BURUNDI', 'Burundi', 'BDI', 108, 257, 1),
(36, 'KH', 'CAMBODIA', 'Cambodia', 'KHM', 116, 855, 1),
(37, 'CM', 'CAMEROON', 'Cameroon', 'CMR', 120, 237, 1),
(38, 'CA', 'CANADA', 'Canada', 'CAN', 124, 1, 1),
(39, 'CV', 'CAPE VERDE', 'Cape Verde', 'CPV', 132, 238, 1),
(40, 'KY', 'CAYMAN ISLANDS', 'Cayman Islands', 'CYM', 136, 1345, 1),
(41, 'CF', 'CENTRAL AFRICAN REPUBLIC', 'Central African Republic', 'CAF', 140, 236, 1),
(42, 'TD', 'CHAD', 'Chad', 'TCD', 148, 235, 1),
(43, 'CL', 'CHILE', 'Chile', 'CHL', 152, 56, 1),
(44, 'CN', 'CHINA', 'China', 'CHN', 156, 86, 1),
(45, 'CX', 'CHRISTMAS ISLAND', 'Christmas Island', NULL, NULL, 61, 1),
(46, 'CC', 'COCOS (KEELING) ISLANDS', 'Cocos (Keeling) Islands', NULL, NULL, 672, 1),
(47, 'CO', 'COLOMBIA', 'Colombia', 'COL', 170, 57, 1),
(48, 'KM', 'COMOROS', 'Comoros', 'COM', 174, 269, 1),
(49, 'CG', 'CONGO', 'Congo', 'COG', 178, 242, 1),
(50, 'CD', 'CONGO, THE DEMOCRATIC REPUBLIC OF THE', 'Congo, the Democratic Republic of the', 'COD', 180, 242, 1),
(51, 'CK', 'COOK ISLANDS', 'Cook Islands', 'COK', 184, 682, 1),
(52, 'CR', 'COSTA RICA', 'Costa Rica', 'CRI', 188, 506, 1),
(53, 'CI', 'COTE D\'IVOIRE', 'Cote D\'Ivoire', 'CIV', 384, 225, 1),
(54, 'HR', 'CROATIA', 'Croatia', 'HRV', 191, 385, 1),
(55, 'CU', 'CUBA', 'Cuba', 'CUB', 192, 53, 1),
(56, 'CY', 'CYPRUS', 'Cyprus', 'CYP', 196, 357, 1),
(57, 'CZ', 'CZECH REPUBLIC', 'Czech Republic', 'CZE', 203, 420, 1),
(58, 'DK', 'DENMARK', 'Denmark', 'DNK', 208, 45, 1),
(59, 'DJ', 'DJIBOUTI', 'Djibouti', 'DJI', 262, 253, 1),
(60, 'DM', 'DOMINICA', 'Dominica', 'DMA', 212, 1767, 1),
(61, 'DO', 'DOMINICAN REPUBLIC', 'Dominican Republic', 'DOM', 214, 1809, 1),
(62, 'EC', 'ECUADOR', 'Ecuador', 'ECU', 218, 593, 1),
(63, 'EG', 'EGYPT', 'Egypt', 'EGY', 818, 20, 1),
(64, 'SV', 'EL SALVADOR', 'El Salvador', 'SLV', 222, 503, 1),
(65, 'GQ', 'EQUATORIAL GUINEA', 'Equatorial Guinea', 'GNQ', 226, 240, 1),
(66, 'ER', 'ERITREA', 'Eritrea', 'ERI', 232, 291, 1),
(67, 'EE', 'ESTONIA', 'Estonia', 'EST', 233, 372, 1),
(68, 'ET', 'ETHIOPIA', 'Ethiopia', 'ETH', 231, 251, 1),
(69, 'FK', 'FALKLAND ISLANDS (MALVINAS)', 'Falkland Islands (Malvinas)', 'FLK', 238, 500, 1),
(70, 'FO', 'FAROE ISLANDS', 'Faroe Islands', 'FRO', 234, 298, 1),
(71, 'FJ', 'FIJI', 'Fiji', 'FJI', 242, 679, 1),
(72, 'FI', 'FINLAND', 'Finland', 'FIN', 246, 358, 1),
(73, 'FR', 'FRANCE', 'France', 'FRA', 250, 33, 1),
(74, 'GF', 'FRENCH GUIANA', 'French Guiana', 'GUF', 254, 594, 1),
(75, 'PF', 'FRENCH POLYNESIA', 'French Polynesia', 'PYF', 258, 689, 1),
(76, 'TF', 'FRENCH SOUTHERN TERRITORIES', 'French Southern Territories', NULL, NULL, 0, 1),
(77, 'GA', 'GABON', 'Gabon', 'GAB', 266, 241, 1),
(78, 'GM', 'GAMBIA', 'Gambia', 'GMB', 270, 220, 1),
(79, 'GE', 'GEORGIA', 'Georgia', 'GEO', 268, 995, 1),
(80, 'DE', 'GERMANY', 'Germany', 'DEU', 276, 49, 1),
(81, 'GH', 'GHANA', 'Ghana', 'GHA', 288, 233, 1),
(82, 'GI', 'GIBRALTAR', 'Gibraltar', 'GIB', 292, 350, 1),
(83, 'GR', 'GREECE', 'Greece', 'GRC', 300, 30, 1),
(84, 'GL', 'GREENLAND', 'Greenland', 'GRL', 304, 299, 1),
(85, 'GD', 'GRENADA', 'Grenada', 'GRD', 308, 1473, 1),
(86, 'GP', 'GUADELOUPE', 'Guadeloupe', 'GLP', 312, 590, 1),
(87, 'GU', 'GUAM', 'Guam', 'GUM', 316, 1671, 1),
(88, 'GT', 'GUATEMALA', 'Guatemala', 'GTM', 320, 502, 1),
(89, 'GN', 'GUINEA', 'Guinea', 'GIN', 324, 224, 1),
(90, 'GW', 'GUINEA-BISSAU', 'Guinea-Bissau', 'GNB', 624, 245, 1),
(91, 'GY', 'GUYANA', 'Guyana', 'GUY', 328, 592, 1),
(92, 'HT', 'HAITI', 'Haiti', 'HTI', 332, 509, 1),
(93, 'HM', 'HEARD ISLAND AND MCDONALD ISLANDS', 'Heard Island and Mcdonald Islands', NULL, NULL, 0, 1),
(94, 'VA', 'HOLY SEE (VATICAN CITY STATE)', 'Holy See (Vatican City State)', 'VAT', 336, 39, 1),
(95, 'HN', 'HONDURAS', 'Honduras', 'HND', 340, 504, 1),
(96, 'HK', 'HONG KONG', 'Hong Kong', 'HKG', 344, 852, 1),
(97, 'HU', 'HUNGARY', 'Hungary', 'HUN', 348, 36, 1),
(98, 'IS', 'ICELAND', 'Iceland', 'ISL', 352, 354, 1),
(99, 'IN', 'INDIA', 'India', 'IND', 356, 91, 1),
(100, 'ID', 'INDONESIA', 'Indonesia', 'IDN', 360, 62, 1),
(101, 'IR', 'IRAN, ISLAMIC REPUBLIC OF', 'Iran, Islamic Republic of', 'IRN', 364, 98, 1),
(102, 'IQ', 'IRAQ', 'Iraq', 'IRQ', 368, 964, 1),
(103, 'IE', 'IRELAND', 'Ireland', 'IRL', 372, 353, 1),
(104, 'IL', 'ISRAEL', 'Israel', 'ISR', 376, 972, 1),
(105, 'IT', 'ITALY', 'Italy', 'ITA', 380, 39, 1),
(106, 'JM', 'JAMAICA', 'Jamaica', 'JAM', 388, 1876, 1),
(107, 'JP', 'JAPAN', 'Japan', 'JPN', 392, 81, 1),
(108, 'JO', 'JORDAN', 'Jordan', 'JOR', 400, 962, 1),
(109, 'KZ', 'KAZAKHSTAN', 'Kazakhstan', 'KAZ', 398, 7, 1),
(110, 'KE', 'KENYA', 'Kenya', 'KEN', 404, 254, 1),
(111, 'KI', 'KIRIBATI', 'Kiribati', 'KIR', 296, 686, 1),
(112, 'KP', 'KOREA, DEMOCRATIC PEOPLE\'S REPUBLIC OF', 'Korea, Democratic People\'s Republic of', 'PRK', 408, 850, 1),
(113, 'KR', 'KOREA, REPUBLIC OF', 'Korea, Republic of', 'KOR', 410, 82, 1),
(114, 'KW', 'KUWAIT', 'Kuwait', 'KWT', 414, 965, 1),
(115, 'KG', 'KYRGYZSTAN', 'Kyrgyzstan', 'KGZ', 417, 996, 1),
(116, 'LA', 'LAO PEOPLE\'S DEMOCRATIC REPUBLIC', 'Lao People\'s Democratic Republic', 'LAO', 418, 856, 1),
(117, 'LV', 'LATVIA', 'Latvia', 'LVA', 428, 371, 1),
(118, 'LB', 'LEBANON', 'Lebanon', 'LBN', 422, 961, 1),
(119, 'LS', 'LESOTHO', 'Lesotho', 'LSO', 426, 266, 1),
(120, 'LR', 'LIBERIA', 'Liberia', 'LBR', 430, 231, 1),
(121, 'LY', 'LIBYAN ARAB JAMAHIRIYA', 'Libyan Arab Jamahiriya', 'LBY', 434, 218, 1),
(122, 'LI', 'LIECHTENSTEIN', 'Liechtenstein', 'LIE', 438, 423, 1),
(123, 'LT', 'LITHUANIA', 'Lithuania', 'LTU', 440, 370, 1),
(124, 'LU', 'LUXEMBOURG', 'Luxembourg', 'LUX', 442, 352, 1),
(125, 'MO', 'MACAO', 'Macao', 'MAC', 446, 853, 1),
(126, 'MK', 'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF', 'Macedonia, the Former Yugoslav Republic of', 'MKD', 807, 389, 1),
(127, 'MG', 'MADAGASCAR', 'Madagascar', 'MDG', 450, 261, 1),
(128, 'MW', 'MALAWI', 'Malawi', 'MWI', 454, 265, 1),
(129, 'MY', 'MALAYSIA', 'Malaysia', 'MYS', 458, 60, 1),
(130, 'MV', 'MALDIVES', 'Maldives', 'MDV', 462, 960, 1),
(131, 'ML', 'MALI', 'Mali', 'MLI', 466, 223, 1),
(132, 'MT', 'MALTA', 'Malta', 'MLT', 470, 356, 1),
(133, 'MH', 'MARSHALL ISLANDS', 'Marshall Islands', 'MHL', 584, 692, 1),
(134, 'MQ', 'MARTINIQUE', 'Martinique', 'MTQ', 474, 596, 1),
(135, 'MR', 'MAURITANIA', 'Mauritania', 'MRT', 478, 222, 1),
(136, 'MU', 'MAURITIUS', 'Mauritius', 'MUS', 480, 230, 1),
(137, 'YT', 'MAYOTTE', 'Mayotte', NULL, NULL, 269, 1),
(138, 'MX', 'MEXICO', 'Mexico', 'MEX', 484, 52, 1),
(139, 'FM', 'MICRONESIA, FEDERATED STATES OF', 'Micronesia, Federated States of', 'FSM', 583, 691, 1),
(140, 'MD', 'MOLDOVA, REPUBLIC OF', 'Moldova, Republic of', 'MDA', 498, 373, 1),
(141, 'MC', 'MONACO', 'Monaco', 'MCO', 492, 377, 1),
(142, 'MN', 'MONGOLIA', 'Mongolia', 'MNG', 496, 976, 1),
(143, 'MS', 'MONTSERRAT', 'Montserrat', 'MSR', 500, 1664, 1),
(144, 'MA', 'MOROCCO', 'Morocco', 'MAR', 504, 212, 1),
(145, 'MZ', 'MOZAMBIQUE', 'Mozambique', 'MOZ', 508, 258, 1),
(146, 'MM', 'MYANMAR', 'Myanmar', 'MMR', 104, 95, 1),
(147, 'NA', 'NAMIBIA', 'Namibia', 'NAM', 516, 264, 1),
(148, 'NR', 'NAURU', 'Nauru', 'NRU', 520, 674, 1),
(149, 'NP', 'NEPAL', 'Nepal', 'NPL', 524, 977, 1),
(150, 'NL', 'NETHERLANDS', 'Netherlands', 'NLD', 528, 31, 1),
(151, 'AN', 'NETHERLANDS ANTILLES', 'Netherlands Antilles', 'ANT', 530, 599, 1),
(152, 'NC', 'NEW CALEDONIA', 'New Caledonia', 'NCL', 540, 687, 1),
(153, 'NZ', 'NEW ZEALAND', 'New Zealand', 'NZL', 554, 64, 1),
(154, 'NI', 'NICARAGUA', 'Nicaragua', 'NIC', 558, 505, 1),
(155, 'NE', 'NIGER', 'Niger', 'NER', 562, 227, 1),
(156, 'NG', 'NIGERIA', 'Nigeria', 'NGA', 566, 234, 1),
(157, 'NU', 'NIUE', 'Niue', 'NIU', 570, 683, 1),
(158, 'NF', 'NORFOLK ISLAND', 'Norfolk Island', 'NFK', 574, 672, 1),
(159, 'MP', 'NORTHERN MARIANA ISLANDS', 'Northern Mariana Islands', 'MNP', 580, 1670, 1),
(160, 'NO', 'NORWAY', 'Norway', 'NOR', 578, 47, 1),
(161, 'OM', 'OMAN', 'Oman', 'OMN', 512, 968, 1),
(162, 'PK', 'PAKISTAN', 'Pakistan', 'PAK', 586, 92, 1),
(163, 'PW', 'PALAU', 'Palau', 'PLW', 585, 680, 1),
(164, 'PS', 'PALESTINIAN TERRITORY, OCCUPIED', 'Palestinian Territory, Occupied', NULL, NULL, 970, 1),
(165, 'PA', 'PANAMA', 'Panama', 'PAN', 591, 507, 1),
(166, 'PG', 'PAPUA NEW GUINEA', 'Papua New Guinea', 'PNG', 598, 675, 1),
(167, 'PY', 'PARAGUAY', 'Paraguay', 'PRY', 600, 595, 1),
(168, 'PE', 'PERU', 'Peru', 'PER', 604, 51, 1),
(169, 'PH', 'PHILIPPINES', 'Philippines', 'PHL', 608, 63, 1),
(170, 'PN', 'PITCAIRN', 'Pitcairn', 'PCN', 612, 0, 1),
(171, 'PL', 'POLAND', 'Poland', 'POL', 616, 48, 1),
(172, 'PT', 'PORTUGAL', 'Portugal', 'PRT', 620, 351, 1),
(173, 'PR', 'PUERTO RICO', 'Puerto Rico', 'PRI', 630, 1787, 1),
(174, 'QA', 'QATAR', 'Qatar', 'QAT', 634, 974, 1),
(175, 'RE', 'REUNION', 'Reunion', 'REU', 638, 262, 1),
(176, 'RO', 'ROMANIA', 'Romania', 'ROM', 642, 40, 1),
(177, 'RU', 'RUSSIAN FEDERATION', 'Russian Federation', 'RUS', 643, 70, 1),
(178, 'RW', 'RWANDA', 'Rwanda', 'RWA', 646, 250, 1),
(179, 'SH', 'SAINT HELENA', 'Saint Helena', 'SHN', 654, 290, 1),
(180, 'KN', 'SAINT KITTS AND NEVIS', 'Saint Kitts and Nevis', 'KNA', 659, 1869, 1),
(181, 'LC', 'SAINT LUCIA', 'Saint Lucia', 'LCA', 662, 1758, 1),
(182, 'PM', 'SAINT PIERRE AND MIQUELON', 'Saint Pierre and Miquelon', 'SPM', 666, 508, 1),
(183, 'VC', 'SAINT VINCENT AND THE GRENADINES', 'Saint Vincent and the Grenadines', 'VCT', 670, 1784, 1),
(184, 'WS', 'SAMOA', 'Samoa', 'WSM', 882, 684, 1),
(185, 'SM', 'SAN MARINO', 'San Marino', 'SMR', 674, 378, 1),
(186, 'ST', 'SAO TOME AND PRINCIPE', 'Sao Tome and Principe', 'STP', 678, 239, 1),
(187, 'SA', 'SAUDI ARABIA', 'Saudi Arabia', 'SAU', 682, 966, 1),
(188, 'SN', 'SENEGAL', 'Senegal', 'SEN', 686, 221, 1),
(189, 'CS', 'SERBIA AND MONTENEGRO', 'Serbia and Montenegro', NULL, NULL, 381, 1),
(190, 'SC', 'SEYCHELLES', 'Seychelles', 'SYC', 690, 248, 1),
(191, 'SL', 'SIERRA LEONE', 'Sierra Leone', 'SLE', 694, 232, 1),
(192, 'SG', 'SINGAPORE', 'Singapore', 'SGP', 702, 65, 1),
(193, 'SK', 'SLOVAKIA', 'Slovakia', 'SVK', 703, 421, 1),
(194, 'SI', 'SLOVENIA', 'Slovenia', 'SVN', 705, 386, 1),
(195, 'SB', 'SOLOMON ISLANDS', 'Solomon Islands', 'SLB', 90, 677, 1),
(196, 'SO', 'SOMALIA', 'Somalia', 'SOM', 706, 252, 1),
(197, 'ZA', 'SOUTH AFRICA', 'South Africa', 'ZAF', 710, 27, 1),
(198, 'GS', 'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS', 'South Georgia and the South Sandwich Islands', NULL, NULL, 0, 1),
(199, 'ES', 'SPAIN', 'Spain', 'ESP', 724, 34, 1),
(200, 'LK', 'SRI LANKA', 'Sri Lanka', 'LKA', 144, 94, 1),
(201, 'SD', 'SUDAN', 'Sudan', 'SDN', 736, 249, 1),
(202, 'SR', 'SURINAME', 'Suriname', 'SUR', 740, 597, 1),
(203, 'SJ', 'SVALBARD AND JAN MAYEN', 'Svalbard and Jan Mayen', 'SJM', 744, 47, 1),
(204, 'SZ', 'SWAZILAND', 'Swaziland', 'SWZ', 748, 268, 1),
(205, 'SE', 'SWEDEN', 'Sweden', 'SWE', 752, 46, 1),
(206, 'CH', 'SWITZERLAND', 'Switzerland', 'CHE', 756, 41, 1),
(207, 'SY', 'SYRIAN ARAB REPUBLIC', 'Syrian Arab Republic', 'SYR', 760, 963, 1),
(208, 'TW', 'TAIWAN, PROVINCE OF CHINA', 'Taiwan, Province of China', 'TWN', 158, 886, 1),
(209, 'TJ', 'TAJIKISTAN', 'Tajikistan', 'TJK', 762, 992, 1),
(210, 'TZ', 'TANZANIA, UNITED REPUBLIC OF', 'Tanzania, United Republic of', 'TZA', 834, 255, 1),
(211, 'TH', 'THAILAND', 'Thailand', 'THA', 764, 66, 1),
(212, 'TL', 'TIMOR-LESTE', 'Timor-Leste', NULL, NULL, 670, 1),
(213, 'TG', 'TOGO', 'Togo', 'TGO', 768, 228, 1),
(214, 'TK', 'TOKELAU', 'Tokelau', 'TKL', 772, 690, 1),
(215, 'TO', 'TONGA', 'Tonga', 'TON', 776, 676, 1),
(216, 'TT', 'TRINIDAD AND TOBAGO', 'Trinidad and Tobago', 'TTO', 780, 1868, 1),
(217, 'TN', 'TUNISIA', 'Tunisia', 'TUN', 788, 216, 1),
(218, 'TR', 'TURKEY', 'Turkey', 'TUR', 792, 90, 1),
(219, 'TM', 'TURKMENISTAN', 'Turkmenistan', 'TKM', 795, 7370, 1),
(220, 'TC', 'TURKS AND CAICOS ISLANDS', 'Turks and Caicos Islands', 'TCA', 796, 1649, 1),
(221, 'TV', 'TUVALU', 'Tuvalu', 'TUV', 798, 688, 1),
(222, 'UG', 'UGANDA', 'Uganda', 'UGA', 800, 256, 1),
(223, 'UA', 'UKRAINE', 'Ukraine', 'UKR', 804, 380, 1),
(224, 'AE', 'UNITED ARAB EMIRATES', 'United Arab Emirates', 'ARE', 784, 971, 1),
(225, 'GB', 'UNITED KINGDOM', 'United Kingdom', 'GBR', 826, 44, 1),
(226, 'US', 'UNITED STATES', 'United States', 'USA', 840, 1, 1),
(227, 'UM', 'UNITED STATES MINOR OUTLYING ISLANDS', 'United States Minor Outlying Islands', NULL, NULL, 1, 1),
(228, 'UY', 'URUGUAY', 'Uruguay', 'URY', 858, 598, 1),
(229, 'UZ', 'UZBEKISTAN', 'Uzbekistan', 'UZB', 860, 998, 1),
(230, 'VU', 'VANUATU', 'Vanuatu', 'VUT', 548, 678, 1),
(231, 'VE', 'VENEZUELA', 'Venezuela', 'VEN', 862, 58, 1),
(232, 'VN', 'VIET NAM', 'Viet Nam', 'VNM', 704, 84, 1),
(233, 'VG', 'VIRGIN ISLANDS, BRITISH', 'Virgin Islands, British', 'VGB', 92, 1284, 1),
(234, 'VI', 'VIRGIN ISLANDS, U.S.', 'Virgin Islands, U.s.', 'VIR', 850, 1340, 1),
(235, 'WF', 'WALLIS AND FUTUNA', 'Wallis and Futuna', 'WLF', 876, 681, 1),
(236, 'EH', 'WESTERN SAHARA', 'Western Sahara', 'ESH', 732, 212, 1),
(237, 'YE', 'YEMEN', 'Yemen', 'YEM', 887, 967, 1),
(238, 'ZM', 'ZAMBIA', 'Zambia', 'ZMB', 894, 260, 1),
(239, 'ZW', 'ZIMBABWE', 'Zimbabwe', 'ZWE', 716, 263, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_images`
--

CREATE TABLE `tbl_images` (
  `id` int(11) NOT NULL,
  `reference_type` varchar(100) NOT NULL,
  `reference_id` int(11) NOT NULL,
  `original_name` varchar(255) NOT NULL,
  `name` varchar(100) NOT NULL,
  `file_ext` varchar(10) NOT NULL,
  `force_download` tinyint(1) NOT NULL DEFAULT 0,
  `file_path` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `uploaded_on` timestamp NOT NULL DEFAULT current_timestamp(),
  `uploaded_by` int(11) NOT NULL,
  `updated_on` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_images`
--

INSERT INTO `tbl_images` (`id`, `reference_type`, `reference_id`, `original_name`, `name`, `file_ext`, `force_download`, `file_path`, `status`, `uploaded_on`, `uploaded_by`, `updated_on`, `updated_by`) VALUES
(133, 'property', 9261608, 'udhav city21 - Copy - Copy.jpeg', '9261608-19022108383829.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-02-19 08:38:38', 1, NULL, NULL),
(134, 'property', 9261608, 'udhav city22 - Copy - Copy.jpeg', '9261608-190221083838911.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-02-19 08:38:38', 1, NULL, NULL),
(135, 'property', 9261608, 'udhav city23.jpeg', '9261608-190221083838498.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-02-19 08:38:38', 1, NULL, NULL),
(136, 'property', 9261608, 'udhav city24.jpeg', '9261608-19022108383819.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-02-19 08:38:38', 1, NULL, NULL),
(137, 'property', 9261608, 'udhav city25.jpeg', '9261608-190221083838919.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-02-19 08:38:38', 1, NULL, NULL),
(138, 'property', 9261608, 'udhav city26.jpeg', '9261608-190221083839753.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-02-19 08:38:39', 1, NULL, NULL),
(139, 'property', 9261608, 'udhav city27.jpeg', '9261608-190221083839999.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-02-19 08:38:39', 1, NULL, NULL),
(147, 'property', 1523513, 'WhatsApp Image 2020-11-02 at 11.54.42.jpeg', '1523513-200221123319186.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-02-20 12:33:19', 1, NULL, NULL),
(148, 'property', 1523513, 'WhatsApp Image 2020-11-02 at 11.30.47.jpeg', '1523513-200221123319905.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-02-20 12:33:19', 1, NULL, NULL),
(171, 'property', 1114135, '66648aa4dab0e847.webp', '1114135-04032108595830.webp', 'webp', 0, 'uploads/property/', 1, '2021-03-04 08:59:58', 1, NULL, NULL),
(172, 'property', 1211732, 'madhav green.jpeg', '1211732-040321091627700.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-03-04 09:16:27', 1, NULL, NULL),
(173, 'property', 1211732, 'madhav green1.jpeg', '1211732-040321091627842.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-03-04 09:16:27', 1, NULL, NULL),
(174, 'property', 1211732, 'madhav green2.jpeg', '1211732-040321091627120.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-03-04 09:16:27', 1, NULL, NULL),
(175, 'property', 1211732, 'madhav green3.jpeg', '1211732-040321091627257.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-03-04 09:16:27', 1, NULL, NULL),
(176, 'property', 1211732, 'madhav green4.jpeg', '1211732-040321091628415.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-03-04 09:16:28', 1, NULL, NULL),
(177, 'property', 1950333, 'IMG_20210305_113936.jpg', '1950333-0503210618201.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-05 06:18:20', 1, NULL, NULL),
(178, 'property', 1950333, 'IMG_20210305_113934.jpg', '1950333-0503210618271.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-05 06:18:27', 1, NULL, NULL),
(179, 'property', 1950333, 'IMG_20210305_113931.jpg', '1950333-050321061850848.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-05 06:18:50', 1, NULL, NULL),
(180, 'property', 1950333, 'IMG_20210305_113858.jpg', '1950333-05032106185537.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-05 06:18:55', 1, NULL, NULL),
(181, 'property', 1950333, 'IMG_20210305_113855.jpg', '1950333-050321061901497.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-05 06:19:01', 1, NULL, NULL),
(182, 'property', 1950333, 'IMG_20210305_113854.jpg', '1950333-050321061902618.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-05 06:19:02', 1, NULL, NULL),
(183, 'property', 1950333, 'IMG_20210305_113852.jpg', '1950333-050321061906638.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-05 06:19:06', 1, NULL, NULL),
(184, 'property', 1950333, 'IMG_20210305_113850.jpg', '1950333-050321061907158.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-05 06:19:07', 1, NULL, NULL),
(185, 'property', 1950333, 'IMG_20210305_113849.jpg', '1950333-050321061910730.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-05 06:19:10', 1, NULL, NULL),
(186, 'property', 1950333, 'IMG_20210305_113847.jpg', '1950333-050321061914506.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-05 06:19:14', 1, NULL, NULL),
(187, 'property', 1950333, 'IMG-20210305-WA0004.jpg', '1950333-050321061914842.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-05 06:19:14', 1, NULL, NULL),
(188, 'property', 1950333, 'IMG-20210305-WA0000.jpg', '1950333-050321061915731.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-05 06:19:15', 1, NULL, NULL),
(189, 'property', 1950333, 'IMG-20210305-WA0001.jpg', '1950333-050321061915241.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-05 06:19:15', 1, NULL, NULL),
(190, 'property', 1950333, 'IMG-20210305-WA0002.jpg', '1950333-050321061916702.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-05 06:19:16', 1, NULL, NULL),
(191, 'property', 1950333, 'IMG_20210305_113844.jpg', '1950333-050321061916817.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-05 06:19:16', 1, NULL, NULL),
(192, 'property', 1950333, 'IMG-20210305-WA0003.jpg', '1950333-050321061916396.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-05 06:19:16', 1, NULL, NULL),
(193, 'property', 2058067, '275EE21D-0208-4D76-B960-6DA9051C30D0.jpeg', '2058067-050321065012621.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-03-05 06:50:12', 1, NULL, NULL),
(194, 'property', 2058067, 'F44E2421-7E1E-45F4-9D06-8C29AEE64836.jpeg', '2058067-050321065012379.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-03-05 06:50:12', 1, NULL, NULL),
(195, 'property', 2058067, '2801F841-875B-4714-BF63-09A97EA6FDDC.jpeg', '2058067-050321065013256.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-03-05 06:50:13', 1, NULL, NULL),
(196, 'property', 2058067, '1703A084-FC88-48CC-A1AE-351A22FE0E86.jpeg', '2058067-050321065013792.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-03-05 06:50:13', 1, NULL, NULL),
(197, 'property', 2058067, 'AB7F809A-97D3-4282-9F9D-7C249934F3BF.jpeg', '2058067-050321065013264.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-03-05 06:50:13', 1, NULL, NULL),
(198, 'property', 2058067, '34DD7343-392C-4316-ABF1-AFEC49DDD1B6.jpeg', '2058067-050321065013329.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-03-05 06:50:13', 1, NULL, NULL),
(199, 'property', 8054063, '705DE8B6-F7AB-4754-BA9C-1B4E2DF479C9.jpeg', '8054063-050321114105150.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-03-05 11:41:05', 1, NULL, NULL),
(200, 'property', 8054063, '7C7AFFF4-1A15-4728-ADE2-D0A537F6039D.jpeg', '8054063-050321114105177.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-03-05 11:41:05', 1, NULL, NULL),
(201, 'property', 8054063, 'C622F439-5FDE-4657-AF9A-1F0899EC7086.jpeg', '8054063-050321114105418.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-03-05 11:41:05', 1, NULL, NULL),
(202, 'property', 8054063, 'FF4A0CFD-7792-4D36-A7E9-1D02E80E47B2.jpeg', '8054063-050321114105322.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-03-05 11:41:05', 1, NULL, NULL),
(203, 'property', 8054063, 'E671FB9E-3571-461F-BC1E-A9E8422C7D1C.jpeg', '8054063-050321114106156.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-03-05 11:41:06', 1, NULL, NULL),
(204, 'property', 8054063, '40168282-358E-484C-9E81-E02D0586E3D1.jpeg', '8054063-050321114106302.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-03-05 11:41:06', 1, NULL, NULL),
(205, 'property', 3862796, '4bhk_house_row_house_gomti_nagar_lucknow_lucknow_1010007582724048587.jpg', '3862796-050321122931938.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-05 12:29:31', 1, NULL, NULL),
(206, 'property', 7333853, 'D8A6219B-83A3-4565-8F87-D9834D35F13B.jpeg', '7333853-060321062220746.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-03-06 06:22:20', 1, NULL, NULL),
(207, 'property', 7333853, '514CF555-7103-49CE-A2ED-F7488EA7F982.jpeg', '7333853-060321062221889.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-03-06 06:22:21', 1, NULL, NULL),
(208, 'property', 7333853, '995AD822-8FE9-49D8-BD17-A8172E2E56E2.jpeg', '7333853-060321062221928.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-03-06 06:22:21', 1, NULL, NULL),
(209, 'property', 7333853, '121B5447-E571-4D4F-A608-406BE56C5A5F.jpeg', '7333853-060321062222628.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-03-06 06:22:22', 1, NULL, NULL),
(210, 'property', 7333853, '9B8AC2D5-6244-453A-96B7-D8A51A41AC17.jpeg', '7333853-06032106222499.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-03-06 06:22:24', 1, NULL, NULL),
(211, 'property', 7333853, '2BE26722-FC05-47CB-AE93-A4AA5CD030CA.jpeg', '7333853-060321062224535.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-03-06 06:22:24', 1, NULL, NULL),
(212, 'property', 7333853, 'AD98310B-8B3E-4D55-94AD-0F6457DC9D9F.jpeg', '7333853-060321062225279.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-03-06 06:22:25', 1, NULL, NULL),
(213, 'property', 7333853, 'D2B53557-358A-4DB1-8D55-A2E4A6694ACA.jpeg', '7333853-060321062228301.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-03-06 06:22:28', 1, NULL, NULL),
(214, 'property', 7333853, 'F6E8475E-81F3-4FF0-BCC6-1DC62FDD818D.jpeg', '7333853-060321062228838.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-03-06 06:22:28', 1, NULL, NULL),
(215, 'property', 7333853, 'D3044245-3579-446F-B57C-D94EEEC25479.jpeg', '7333853-060321062230841.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-03-06 06:22:30', 1, NULL, NULL),
(216, 'property', 7333853, 'D2A3B8B4-B7C0-4DF3-BD54-01380B5ABEA5.jpeg', '7333853-06032106223150.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-03-06 06:22:31', 1, NULL, NULL),
(217, 'property', 7333853, 'DFBFA5D0-6BE5-41A6-968F-B38D0366B45C.jpeg', '7333853-060321062231380.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-03-06 06:22:31', 1, NULL, NULL),
(218, 'property', 7333853, '7C3D265D-BC4A-4365-B913-1635BD966804.jpeg', '7333853-060321062232356.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-03-06 06:22:32', 1, NULL, NULL),
(219, 'property', 7333853, 'AE9FC6D8-DECC-4C13-AF17-3B110DC91F51.jpeg', '7333853-060321062233599.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-03-06 06:22:33', 1, NULL, NULL),
(220, 'property', 7333853, 'C6D48A1A-0287-4149-BA33-8FD3139C7DCA.jpeg', '7333853-06032106223379.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-03-06 06:22:33', 1, NULL, NULL),
(221, 'property', 7333853, '2379F9E4-3F3F-4764-9E83-54573F29AA8C.jpeg', '7333853-060321062235402.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-03-06 06:22:35', 1, NULL, NULL),
(222, 'property', 9598982, 'udhav city.jpeg', '3369921-060321062612894.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-03-06 06:26:12', 1, '2021-03-06 06:38:18', NULL),
(223, 'property', 9598982, 'udhav city1.jpeg', '3369921-060321062612856.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-03-06 06:26:12', 1, '2021-03-06 06:38:19', NULL),
(224, 'property', 9598982, 'udhav city2.jpeg', '3369921-060321062612344.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-03-06 06:26:12', 1, '2021-03-06 06:38:20', NULL),
(225, 'property', 9598982, 'udhav city4.jpeg', '3369921-060321062612236.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-03-06 06:26:12', 1, '2021-03-06 06:38:22', NULL),
(226, 'property', 9598982, 'udhav city3.jpeg', '3369921-060321062612161.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-03-06 06:26:12', 1, '2021-03-06 06:38:23', NULL),
(227, 'property', 9598982, 'udhav city10.jpeg', '3369921-060321062612701.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-03-06 06:26:12', 1, '2021-03-06 06:38:24', NULL),
(228, 'property', 9598982, 'udhav city11.jpeg', '3369921-06032106261252.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-03-06 06:26:12', 1, '2021-03-06 06:38:25', NULL),
(229, 'property', 9598982, 'udhav city12 - Copy.jpeg', '3369921-060321062613529.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-03-06 06:26:13', 1, '2021-03-06 06:38:27', NULL),
(230, 'property', 9598982, 'udhav city13.jpeg', '3369921-060321062613489.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-03-06 06:26:13', 1, '2021-03-06 06:38:28', NULL),
(231, 'property', 9598982, 'udhav city23.jpeg', '3369921-060321062613148.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-03-06 06:26:13', 1, '2021-03-06 06:38:29', NULL),
(232, 'property', 9598982, 'udhav city24.jpeg', '3369921-060321062613245.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-03-06 06:26:13', 1, '2021-03-06 06:38:30', NULL),
(233, 'property', 2868099, 'IMG-20210306-WA0011.jpg', '2868099-060321064806718.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 06:48:06', 1, NULL, NULL),
(234, 'property', 2868099, 'IMG-20210306-WA0015.jpg', '2868099-060321064806894.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 06:48:06', 1, NULL, NULL),
(235, 'property', 2868099, 'IMG-20210306-WA0012.jpg', '2868099-060321064807857.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 06:48:07', 1, NULL, NULL),
(236, 'property', 2868099, 'IMG-20210306-WA0013.jpg', '2868099-060321064807302.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 06:48:07', 1, NULL, NULL),
(237, 'property', 2868099, 'IMG-20210306-WA0014.jpg', '2868099-060321064808692.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 06:48:08', 1, NULL, NULL),
(238, 'property', 2868099, 'IMG_20210305_113936.jpg', '2868099-060321064810617.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 06:48:10', 1, NULL, NULL),
(239, 'property', 2868099, 'IMG_20210305_113931.jpg', '2868099-060321064813856.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 06:48:13', 1, NULL, NULL),
(240, 'property', 2868099, 'IMG_20210305_113934.jpg', '2868099-060321064814248.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 06:48:14', 1, NULL, NULL),
(241, 'property', 2868099, 'IMG_20210305_113858.jpg', '2868099-060321064816124.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 06:48:16', 1, NULL, NULL),
(242, 'property', 2868099, 'IMG_20210305_113854.jpg', '2868099-060321064818445.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 06:48:18', 1, NULL, NULL),
(243, 'property', 2868099, 'IMG_20210305_113855.jpg', '2868099-060321064819354.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 06:48:19', 1, NULL, NULL),
(244, 'property', 2868099, 'IMG_20210305_113852.jpg', '2868099-060321064820965.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 06:48:20', 1, NULL, NULL),
(245, 'property', 2868099, 'IMG_20210305_113849.jpg', '2868099-060321064822217.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 06:48:22', 1, NULL, NULL),
(246, 'property', 2868099, 'IMG_20210305_113850.jpg', '2868099-06032106482382.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 06:48:23', 1, NULL, NULL),
(247, 'property', 2868099, 'IMG_20210305_113847.jpg', '2868099-060321064825153.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 06:48:25', 1, NULL, NULL),
(248, 'property', 2868099, 'IMG-20210305-WA0004.jpg', '2868099-060321064825958.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 06:48:25', 1, NULL, NULL),
(249, 'property', 2868099, 'IMG-20210305-WA0000.jpg', '2868099-060321064825825.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 06:48:25', 1, NULL, NULL),
(250, 'property', 2868099, 'IMG-20210305-WA0001.jpg', '2868099-06032106482646.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 06:48:26', 1, NULL, NULL),
(251, 'property', 2868099, 'IMG_20210305_113844.jpg', '2868099-060321064827241.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 06:48:27', 1, NULL, NULL),
(252, 'property', 6688699, 'IMG-20210306-WA0015.jpg', '6688699-060321065630115.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 06:56:30', 1, NULL, NULL),
(253, 'property', 6688699, 'IMG-20210306-WA0012.jpg', '6688699-060321065631145.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 06:56:31', 1, NULL, NULL),
(254, 'property', 6688699, 'IMG-20210306-WA0011.jpg', '6688699-060321065631834.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 06:56:31', 1, NULL, NULL),
(255, 'property', 2962806, 'IMG-20210306-WA0012.jpg', '2962806-060321065826255.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 06:58:26', 1, NULL, NULL),
(256, 'property', 2962806, 'IMG-20210306-WA0015.jpg', '2962806-060321065826548.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 06:58:26', 1, NULL, NULL),
(257, 'property', 2962806, 'IMG-20210306-WA0011.jpg', '2962806-060321065826713.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 06:58:26', 1, NULL, NULL),
(258, 'property', 2962806, 'IMG-20210306-WA0014.jpg', '2962806-060321065826955.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 06:58:26', 1, NULL, NULL),
(259, 'property', 7593631, 'pexels-photo-106399.jpeg', '7593631-06032107082552.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-03-06 07:08:25', 1, NULL, NULL),
(260, 'property', 7593631, '1590612065-2208.jpg', '7593631-060321070826101.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 07:08:26', 1, NULL, NULL),
(261, 'property', 3129471, 'IMG-20210306-WA0000.jpg', '3129471-060321084145200.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:41:45', 1, NULL, NULL),
(262, 'property', 3129471, 'IMG-20210306-WA0011.jpg', '3129471-06032108421558.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:42:15', 1, NULL, NULL),
(263, 'property', 3629702, 'IMG-20210306-WA0012.jpg', '3629702-060321084246313.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:42:46', 1, NULL, NULL),
(264, 'property', 3629702, 'IMG-20210306-WA0000.jpg', '3629702-06032108424614.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:42:46', 1, NULL, NULL),
(265, 'property', 3629702, 'IMG-20210306-WA0001.jpg', '3629702-060321084246265.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:42:46', 1, NULL, NULL),
(266, 'property', 3629702, 'IMG-20210306-WA0003.jpg', '3629702-060321084246745.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:42:46', 1, NULL, NULL),
(267, 'property', 3629702, 'IMG-20210306-WA0002.jpg', '3629702-060321084247748.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:42:47', 1, NULL, NULL),
(268, 'property', 3629702, 'IMG-20210306-WA0004.jpg', '3629702-060321084247918.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:42:47', 1, NULL, NULL),
(269, 'property', 3629702, 'IMG-20210306-WA0005.jpg', '3629702-060321084247275.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:42:47', 1, NULL, NULL),
(270, 'property', 3629702, 'IMG-20210306-WA0006.jpg', '3629702-06032108424731.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:42:47', 1, NULL, NULL),
(271, 'property', 9446819, 'IMG-20210306-WA0011.jpg', '9446819-060321084250265.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:42:50', 1, NULL, NULL),
(272, 'property', 9446819, 'IMG-20210306-WA0015.jpg', '9446819-060321084251966.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:42:51', 1, NULL, NULL),
(273, 'property', 9446819, 'IMG-20210306-WA0012.jpg', '9446819-060321084251595.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:42:51', 1, NULL, NULL),
(274, 'property', 9446819, 'IMG-20210306-WA0013.jpg', '9446819-06032108425182.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:42:51', 1, NULL, NULL),
(275, 'property', 9446819, 'IMG-20210306-WA0014.jpg', '9446819-060321084251536.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:42:51', 1, NULL, NULL),
(276, 'property', 9446819, 'IMG_20210305_113934.jpg', '9446819-060321084256972.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:42:56', 1, NULL, NULL),
(277, 'property', 9446819, 'IMG_20210305_113936.jpg', '9446819-060321084256798.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:42:56', 1, NULL, NULL),
(278, 'property', 2943018, 'IMG-20210306-WA0015.jpg', '2943018-060321084342202.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:43:42', 1, NULL, NULL),
(279, 'property', 3657051, 'IMG-20210306-WA0015.jpg', '3657051-060321084402790.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:44:02', 1, NULL, NULL),
(280, 'property', 3657051, 'IMG-20210306-WA0016.jpg', '3657051-060321084402156.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:44:02', 1, NULL, NULL),
(281, 'property', 3657051, 'IMG-20210306-WA0014.jpg', '3657051-060321084403395.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:44:03', 1, NULL, NULL),
(282, 'property', 3657051, 'IMG-20210306-WA0013.jpg', '3657051-06032108440374.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:44:03', 1, NULL, NULL),
(283, 'property', 3657051, 'IMG-20210306-WA0012.jpg', '3657051-060321084403732.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:44:03', 1, NULL, NULL),
(284, 'property', 3657051, 'IMG-20210306-WA0011.jpg', '3657051-060321084403682.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:44:03', 1, NULL, NULL),
(285, 'property', 3657051, 'IMG-20210306-WA0010.jpg', '3657051-060321084403548.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:44:03', 1, NULL, NULL),
(286, 'property', 3657051, 'IMG-20210306-WA0009.jpg', '3657051-060321084403915.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:44:03', 1, NULL, NULL),
(287, 'property', 3657051, 'IMG-20210306-WA0008.jpg', '3657051-060321084404431.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:44:04', 1, NULL, NULL),
(288, 'property', 3657051, 'IMG-20210306-WA0007.jpg', '3657051-060321084404845.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:44:04', 1, NULL, NULL),
(289, 'property', 3657051, 'IMG-20210306-WA0006.jpg', '3657051-060321084404467.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:44:04', 1, NULL, NULL),
(290, 'property', 3657051, 'IMG-20210306-WA0005.jpg', '3657051-060321084404949.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:44:04', 1, NULL, NULL),
(291, 'property', 3657051, 'IMG-20210306-WA0004.jpg', '3657051-060321084405862.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:44:05', 1, NULL, NULL),
(292, 'property', 3657051, 'IMG-20210306-WA0003.jpg', '3657051-060321084405600.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:44:05', 1, NULL, NULL),
(293, 'property', 3657051, 'IMG-20210306-WA0002.jpg', '3657051-060321084405523.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:44:05', 1, NULL, NULL),
(294, 'property', 3657051, 'IMG-20210306-WA0001.jpg', '3657051-060321084406489.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:44:06', 1, NULL, NULL),
(295, 'property', 5817774, 'IMG-20210306-WA0014.jpg', '5817774-060321084415971.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:44:15', 1, NULL, NULL),
(296, 'property', 5817774, 'IMG-20210306-WA0013.jpg', '5817774-060321084415502.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:44:15', 1, NULL, NULL),
(297, 'property', 5817774, 'IMG-20210306-WA0008.jpg', '5817774-060321084416599.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:44:16', 1, NULL, NULL),
(298, 'property', 5817774, 'IMG-20210306-WA0009.jpg', '5817774-06032108441645.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:44:16', 1, NULL, NULL),
(299, 'property', 5817774, 'IMG-20210306-WA0010.jpg', '5817774-060321084416593.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:44:16', 1, NULL, NULL),
(300, 'property', 5817774, 'IMG-20210306-WA0011.jpg', '5817774-060321084416431.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:44:16', 1, NULL, NULL),
(301, 'property', 5817774, 'IMG-20210306-WA0007.jpg', '5817774-060321084417531.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:44:17', 1, NULL, NULL),
(302, 'property', 5817774, 'IMG-20210306-WA0005.jpg', '5817774-060321084417453.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:44:17', 1, NULL, NULL),
(303, 'property', 5817774, 'IMG-20210306-WA0004.jpg', '5817774-060321084417988.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:44:17', 1, NULL, NULL),
(304, 'property', 5817774, 'IMG-20210306-WA0002.jpg', '5817774-060321084417502.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:44:17', 1, NULL, NULL),
(305, 'property', 9533768, 'IMG-20210306-WA0015.jpg', '9533768-060321084544974.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:45:44', 1, NULL, NULL),
(306, 'property', 9533768, 'IMG-20210306-WA0016.jpg', '9533768-060321084544558.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:45:44', 1, NULL, NULL),
(307, 'property', 9533768, 'IMG-20210306-WA0014.jpg', '9533768-060321084544453.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:45:44', 1, NULL, NULL),
(308, 'property', 9533768, 'IMG-20210306-WA0013.jpg', '9533768-060321084544171.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:45:44', 1, NULL, NULL),
(309, 'property', 9533768, 'IMG-20210306-WA0012.jpg', '9533768-060321084544414.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:45:44', 1, NULL, NULL),
(310, 'property', 9533768, 'IMG-20210306-WA0011.jpg', '9533768-060321084545900.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:45:45', 1, NULL, NULL),
(311, 'property', 9533768, 'IMG-20210306-WA0010.jpg', '9533768-060321084545490.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:45:45', 1, NULL, NULL),
(312, 'property', 9533768, 'IMG-20210306-WA0009.jpg', '9533768-060321084545903.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:45:45', 1, NULL, NULL),
(313, 'property', 9533768, 'IMG-20210306-WA0008.jpg', '9533768-060321084545945.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:45:45', 1, NULL, NULL),
(314, 'property', 9533768, 'IMG-20210306-WA0007.jpg', '9533768-060321084546604.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:45:46', 1, NULL, NULL),
(315, 'property', 9533768, 'IMG-20210306-WA0006.jpg', '9533768-060321084546733.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:45:46', 1, NULL, NULL),
(316, 'property', 9533768, 'IMG-20210306-WA0005.jpg', '9533768-060321084546987.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:45:46', 1, NULL, NULL),
(317, 'property', 9533768, 'IMG-20210306-WA0004.jpg', '9533768-060321084546512.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:45:46', 1, NULL, NULL),
(318, 'property', 9533768, 'IMG-20210306-WA0003.jpg', '9533768-060321084546121.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:45:46', 1, NULL, NULL),
(319, 'property', 9533768, 'IMG-20210306-WA0002.jpg', '9533768-060321084546781.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:45:46', 1, NULL, NULL),
(320, 'property', 9533768, 'IMG-20210306-WA0001.jpg', '9533768-06032108454789.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:45:47', 1, NULL, NULL),
(321, 'property', 2299864, 'IMG-20210306-WA0015.jpg', '2299864-060321084703202.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:47:03', 1, NULL, NULL),
(322, 'property', 2299864, 'IMG-20210306-WA0016.jpg', '2299864-060321084703619.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:47:03', 1, NULL, NULL),
(323, 'property', 2299864, 'IMG-20210306-WA0014.jpg', '2299864-06032108470340.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:47:03', 1, NULL, NULL),
(324, 'property', 2299864, 'IMG-20210306-WA0013.jpg', '2299864-06032108470460.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:47:04', 1, NULL, NULL),
(325, 'property', 2299864, 'IMG-20210306-WA0012.jpg', '2299864-060321084704287.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:47:04', 1, NULL, NULL),
(326, 'property', 2299864, 'IMG-20210306-WA0011.jpg', '2299864-060321084704851.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:47:04', 1, NULL, NULL),
(327, 'property', 2299864, 'IMG-20210306-WA0010.jpg', '2299864-06032108470463.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:47:04', 1, NULL, NULL),
(328, 'property', 2299864, 'IMG-20210306-WA0009.jpg', '2299864-060321084705817.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-06 08:47:05', 1, NULL, NULL),
(329, 'property', 5479542, 'IMG_20210219_134748145.jpg', '5479542-080321093842485.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-08 09:38:42', 1, NULL, NULL),
(330, 'property', 4064219, 'IMG-20210306-WA0002.jpg', '4064219-080321094128172.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-08 09:41:28', 1, NULL, NULL),
(331, 'property', 4064219, 'IMG-20210306-WA0004.jpg', '4064219-08032109412840.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-08 09:41:28', 1, NULL, NULL),
(332, 'property', 4064219, 'IMG-20210306-WA0006.jpg', '4064219-080321094128580.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-08 09:41:28', 1, NULL, NULL),
(333, 'property', 4064219, 'IMG-20210306-WA0007.jpg', '4064219-080321094129175.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-08 09:41:29', 1, NULL, NULL),
(334, 'property', 4064219, 'IMG-20210306-WA0005.jpg', '4064219-080321094129811.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-08 09:41:29', 1, NULL, NULL),
(335, 'property', 7020335, 'IMG-20210308-WA0020.jpg', '7020335-080321094314280.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-08 09:43:14', 1, NULL, NULL),
(336, 'property', 7020335, 'IMG-20210308-WA0019.jpg', '7020335-080321094314819.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-08 09:43:14', 1, NULL, NULL),
(337, 'property', 7020335, 'IMG-20210308-WA0018.jpg', '7020335-080321094314208.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-08 09:43:14', 1, NULL, NULL),
(338, 'property', 7020335, 'IMG-20210308-WA0017.jpg', '7020335-080321094314157.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-08 09:43:14', 1, NULL, NULL),
(339, 'property', 7020335, 'IMG-20210308-WA0015.jpg', '7020335-080321094314464.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-08 09:43:14', 1, NULL, NULL),
(340, 'property', 7020335, 'IMG-20210308-WA0016.jpg', '7020335-080321094314639.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-08 09:43:14', 1, NULL, NULL),
(341, 'property', 7020335, 'IMG-20210308-WA0014.jpg', '7020335-080321094314494.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-08 09:43:14', 1, NULL, NULL),
(342, 'property', 7020335, 'IMG-20210308-WA0012.jpg', '7020335-080321094315167.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-08 09:43:15', 1, NULL, NULL),
(343, 'property', 7020335, 'IMG-20210308-WA0009.jpg', '7020335-080321094315866.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-08 09:43:15', 1, NULL, NULL),
(344, 'property', 7020335, 'IMG-20210308-WA0011.jpg', '7020335-080321094315759.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-08 09:43:15', 1, NULL, NULL),
(345, 'property', 7020335, 'IMG-20210308-WA0010.jpg', '7020335-080321094315788.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-08 09:43:15', 1, NULL, NULL),
(346, 'property', 7020335, 'IMG-20210308-WA0008.jpg', '7020335-080321094315264.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-08 09:43:15', 1, NULL, NULL),
(347, 'property', 7020335, 'IMG-20210308-WA0005.jpg', '7020335-080321094316851.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-08 09:43:16', 1, NULL, NULL),
(348, 'property', 7020335, 'IMG-20210308-WA0004.jpg', '7020335-080321094316964.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-08 09:43:16', 1, NULL, NULL),
(349, 'property', 7020335, 'IMG-20210308-WA0003.jpg', '7020335-080321094316260.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-08 09:43:16', 1, NULL, NULL),
(350, 'property', 7020335, 'IMG-20210308-WA0002.jpg', '7020335-080321094316218.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-08 09:43:16', 1, NULL, NULL),
(351, 'property', 7020335, 'IMG-20210308-WA0006.jpg', '7020335-080321094316872.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-08 09:43:16', 1, NULL, NULL),
(352, 'property', 7020335, 'IMG-20210308-WA0007.jpg', '7020335-080321094317379.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-08 09:43:17', 1, NULL, NULL),
(353, 'property', 6368047, 'IMG-20210131-WA0021.jpg', '6368047-090321090118622.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-09 09:01:18', 1, NULL, NULL),
(354, 'property', 8855711, 'IMG-20210302-WA0019.jpg', '8855711-09032109234097.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-09 09:23:40', 1, NULL, NULL),
(355, 'property', 8855711, 'WhatsApp Image 2021-01-30 at 22.36.08 (1) - Copy - Copy - Copy - Copy - Copy.jpeg', '8855711-090321092423136.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-03-09 09:24:23', 1, NULL, NULL),
(356, 'property', 8855711, 'WhatsApp Image 2021-01-30 at 22.36.09 (1).jpeg', '8855711-090321092423258.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-03-09 09:24:23', 1, NULL, NULL),
(357, 'property', 8855711, 'WhatsApp Image 2021-01-30 at 22.36.09.jpeg', '8855711-090321092423677.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-03-09 09:24:23', 1, NULL, NULL),
(358, 'property', 8855711, 'WhatsApp Image 2021-01-30 at 22.36.10 - Copy.jpeg', '8855711-090321092423491.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-03-09 09:24:23', 1, NULL, NULL),
(359, 'property', 8855711, 'WhatsApp Image 2021-01-30 at 22.36.11.jpeg', '8855711-09032109242356.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-03-09 09:24:23', 1, NULL, NULL),
(360, 'property', 8855711, 'WhatsApp Image 2021-01-30 at 22.36.19 (1) - Copy.jpeg', '8855711-090321092424278.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-03-09 09:24:24', 1, NULL, NULL),
(361, 'property', 8855711, 'WhatsApp Image 2021-01-30 at 22.36.20 - Copy - Copy.jpeg', '8855711-090321092424232.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-03-09 09:24:24', 1, NULL, NULL),
(362, 'property', 8855711, 'WhatsApp Image 2021-01-30 at 22.36.20 (1) - Copy - Copy.jpeg', '8855711-090321092424700.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-03-09 09:24:24', 1, NULL, NULL),
(363, 'property', 8855711, 'WhatsApp Image 2021-01-30 at 22.36.22 (1).jpeg', '8855711-090321092424579.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-03-09 09:24:24', 1, NULL, NULL),
(364, 'property', 8855711, 'WhatsApp Image 2021-01-30 at 22.36.22.jpeg', '8855711-090321092424491.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-03-09 09:24:24', 1, NULL, NULL),
(365, 'property', 8855711, 'WhatsApp Image 2021-01-30 at 22.36.19.jpeg', '8855711-090321092425286.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-03-09 09:24:25', 1, NULL, NULL),
(366, 'property', 9998544, 'pexels-photo-106399.jpeg', '9998544-090321115012148.jpeg', 'jpeg', 0, 'uploads/property/', 1, '2021-03-09 11:50:12', 1, NULL, NULL),
(367, 'property', 9998544, '1590612065-2208.jpg', '9998544-090321115012854.jpg', 'jpg', 0, 'uploads/property/', 1, '2021-03-09 11:50:12', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_masters`
--

CREATE TABLE `tbl_masters` (
  `id` int(11) NOT NULL,
  `type` varchar(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_masters`
--

INSERT INTO `tbl_masters` (`id`, `type`, `name`, `status`, `created_by`, `created_on`, `updated_by`, `updated_on`) VALUES
(2, 'flooring', 'Marbel', 1, 1, '2021-02-09 13:10:19', NULL, '0000-00-00 00:00:00'),
(3, 'furnishing', 'Furnished', 1, 1, '2021-02-09 18:00:26', NULL, '0000-00-00 00:00:00'),
(4, 'furnishing', 'Semi Furnished', 1, 1, '2021-02-09 18:00:37', NULL, '0000-00-00 00:00:00'),
(5, 'furnishing', 'UnFurnished', 1, 1, '2021-02-09 18:00:55', NULL, '0000-00-00 00:00:00'),
(6, 'unit-type', 'Sq. Ft', 1, 1, '2021-02-09 18:06:21', NULL, '0000-00-00 00:00:00'),
(7, 'unit-type', 'Sq. Mtr', 1, 1, '2021-02-09 18:06:37', NULL, '0000-00-00 00:00:00'),
(8, 'construction-status', 'Ready to move', 1, 1, '2021-02-09 18:14:43', NULL, '0000-00-00 00:00:00'),
(9, 'construction-status', 'Under Construction', 1, 1, '2021-02-09 18:15:12', NULL, '0000-00-00 00:00:00'),
(10, 'facing', 'North', 1, 1, '2021-02-10 10:02:40', NULL, '0000-00-00 00:00:00'),
(11, 'facing', 'South', 1, 1, '2021-02-10 10:02:48', NULL, '0000-00-00 00:00:00'),
(12, 'facing', 'East', 1, 1, '2021-02-10 10:02:58', NULL, '0000-00-00 00:00:00'),
(13, 'facing', 'West', 1, 1, '2021-02-10 10:03:06', NULL, '0000-00-00 00:00:00'),
(14, 'facing', 'North East', 1, 1, '2021-02-10 10:03:20', NULL, '0000-00-00 00:00:00'),
(15, 'flooring', 'Tiles', 1, 1, '2021-02-15 12:01:24', NULL, '0000-00-00 00:00:00'),
(16, 'locality', 'Vikas Nagar', 1, 1, '2021-02-17 06:22:28', NULL, '0000-00-00 00:00:00'),
(17, 'locality', 'Indira Nagar', 1, 1, '2021-02-16 12:00:00', NULL, '0000-00-00 00:00:00'),
(18, 'locality', 'Gomati nagar', 1, 1, '2021-02-16 12:00:11', NULL, '0000-00-00 00:00:00'),
(19, 'locality', 'telibag', 1, 1, '2021-02-17 01:54:49', NULL, '0000-00-00 00:00:00'),
(20, 'locality', 'Gulistan Colony', 1, 1, '2021-02-24 02:15:12', NULL, '0000-00-00 00:00:00'),
(21, 'locality', 'Gomti Nagar', 1, 1, '2021-03-05 06:51:43', NULL, '0000-00-00 00:00:00'),
(22, 'unit-type', 'ds', 1, 1, '2021-03-18 06:06:55', NULL, '0000-00-00 00:00:00'),
(23, 'unit-type', 'dsw', 1, 1, '2021-03-18 06:06:58', NULL, '0000-00-00 00:00:00'),
(24, 'unit-type', 'dsw', 1, 1, '2021-03-18 06:07:00', NULL, '0000-00-00 00:00:00'),
(25, 'unit-type', 'dsw', 1, 1, '2021-03-18 06:07:04', NULL, '0000-00-00 00:00:00'),
(26, 'unit-type', 'dsw', 1, 1, '2021-03-18 06:07:07', NULL, '0000-00-00 00:00:00'),
(27, 'unit-type', 'dsw', 1, 1, '2021-03-18 06:07:10', NULL, '0000-00-00 00:00:00'),
(28, 'unit-type', 'dsw', 1, 1, '2021-03-18 06:07:13', NULL, '0000-00-00 00:00:00'),
(29, 'unit-type', 'dsw', 1, 1, '2021-03-18 06:07:16', NULL, '0000-00-00 00:00:00'),
(30, 'unit-type', 'dsw', 1, 1, '2021-03-18 06:07:20', NULL, '0000-00-00 00:00:00'),
(31, 'unit-type', 'dsw', 1, 1, '2021-03-18 06:07:23', NULL, '0000-00-00 00:00:00'),
(32, 'unit-type', 'dsw', 1, 1, '2021-03-18 06:07:27', NULL, '0000-00-00 00:00:00'),
(33, 'unit-type', 'dsw', 1, 1, '2021-03-18 06:07:31', NULL, '0000-00-00 00:00:00'),
(34, 'unit-type', 'dsw', 1, 1, '2021-03-18 06:07:34', NULL, '0000-00-00 00:00:00'),
(35, 'unit-type', 'dsw', 1, 1, '2021-03-18 06:07:38', NULL, '0000-00-00 00:00:00'),
(36, 'unit-type', 'dsw', 1, 1, '2021-03-18 06:07:41', NULL, '0000-00-00 00:00:00'),
(37, 'unit-type', 'dsw', 1, 1, '2021-03-18 06:07:44', NULL, '0000-00-00 00:00:00'),
(38, 'unit-type', 'dsw', 1, 1, '2021-03-18 06:07:47', NULL, '0000-00-00 00:00:00'),
(39, 'unit-type', 'dsw', 1, 1, '2021-03-18 06:07:50', NULL, '0000-00-00 00:00:00'),
(40, 'unit-type', 'dsw', 1, 1, '2021-03-18 06:07:52', NULL, '0000-00-00 00:00:00'),
(41, 'unit-type', 'dsw', 1, 1, '2021-03-18 06:07:57', NULL, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_menus`
--

CREATE TABLE `tbl_menus` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT 0,
  `name` varchar(50) NOT NULL,
  `url` varchar(50) NOT NULL,
  `icon` varchar(50) DEFAULT NULL,
  `menu_order` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_on` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_menus`
--

INSERT INTO `tbl_menus` (`id`, `parent_id`, `name`, `url`, `icon`, `menu_order`, `created_by`, `created_on`, `updated_by`, `updated_on`, `status`) VALUES
(1, 0, 'Dashboard', '/site/index', 'fa fa-home', 0, 1, '2020-05-21 20:14:07', 1, '2020-07-13 06:37:00', 1),
(2, 0, 'User Management', '#', 'fa fa-users	', 1, 1, '2020-05-21 20:16:24', 1, '2020-12-21 21:46:43', 1),
(3, 0, 'Masters', '#', 'fa fa-tags', 2, 1, '2020-05-21 20:17:33', 1, '2020-12-21 21:47:07', 1),
(4, 0, 'Products', '#', 'fa fa-product-hunt	', 3, 1, '2020-05-21 20:18:36', NULL, '2020-06-09 13:39:48', 1),
(5, 0, 'Clinics', '#', 'fa fa-h-square	', 4, 1, '2020-05-21 20:19:24', NULL, '2020-06-09 13:39:51', 1),
(6, 0, 'Orders', '#', 'fa fa-shopping-cart	', 7, 1, '2020-05-21 20:20:18', 1, '2020-12-24 19:50:01', 1),
(7, 0, 'Settings', '#', 'fa fa-cogs', 12, 1, '2020-05-21 20:21:46', 1, '2020-12-24 19:50:37', 1),
(9, 2, 'Roles', '/roles/index', '', 1, 1, '2020-05-21 20:29:45', 1, '2020-06-09 13:42:34', 1),
(10, 2, 'Permission Group', '/permission-group/index', '', 2, 1, '2020-05-21 20:33:08', 1, '2020-06-09 13:45:10', 1),
(11, 2, 'Permission', '/permission/index', '', 3, 1, '2020-05-21 20:34:09', 1, '2020-06-09 13:45:10', 1),
(12, 2, 'Role Permission', '/role-permission/create', '', 4, 1, '2020-05-21 20:34:59', 1, '2020-06-09 13:45:10', 1),
(13, 2, 'User', '/user/index', '', 5, 1, '2020-05-21 20:37:32', 1, '2020-06-09 13:45:40', 1),
(14, 2, 'Add User	', '/user/create', '', 6, 1, '2020-05-21 20:43:12', 1, '2020-06-09 13:45:43', 1),
(15, 3, 'Unit Type	', '/unit-type/index', '', NULL, 1, '2020-05-21 20:44:05', 1, '2020-06-09 13:41:52', 1),
(18, 4, 'Products List', '/products/index', '', NULL, 1, '2020-05-21 20:47:32', NULL, '2020-06-09 13:41:13', 1),
(19, 5, 'Clinic List', '/clinic/index', '', NULL, 1, '2020-05-21 20:48:34', NULL, '2020-06-09 13:41:15', 1),
(20, 6, 'Create Orders', '/clinic-orders/create', '', 1, 1, '2020-05-21 20:52:31', 1, '2020-09-05 09:56:48', 1),
(21, 7, 'Menus Management', '/menus/index', '', 2, 1, '2020-05-21 20:53:54', 1, '2020-12-24 18:40:23', 1),
(30, 3, 'State', '/state/index', '', 1, 1, '2020-12-21 21:12:25', NULL, NULL, 1),
(31, 3, 'Category', '/category/index', '', 2, 1, '2020-12-21 21:13:49', 1, '2020-12-21 21:17:04', 1),
(32, 3, 'Insurance', '/insurance/index', '', 3, 1, '2020-12-21 21:14:42', NULL, NULL, 1),
(33, 0, 'Physician', '/physician/index', 'fa fa-user-md', 5, 1, '2020-12-21 21:19:26', 1, '2020-12-21 21:48:06', 1),
(34, 0, 'Customers', '/customers/index', 'fa fa-user-circle', 6, 1, '2020-12-21 21:21:54', 1, '2020-12-21 21:48:32', 1),
(35, 6, 'All Orders', '/orders/index', 'fa fa-shopping-cart', 7, 1, '2020-12-21 21:50:24', 1, '2020-12-24 21:11:11', 1),
(36, 4, 'Products XL Imports', '/products/products-xl-upload', '', 2, 1, '2020-12-29 23:35:56', 1, '2020-12-30 00:15:33', 1),
(38, 6, 'At Warehouse', '/orders/at-warehouse', '', 1, 1, '2021-01-02 22:17:57', NULL, NULL, 1),
(39, 6, 'Shipped', '/orders/shipped', '', 2, 1, '2021-01-02 22:18:58', NULL, NULL, 1),
(40, 6, 'Delivered', '/orders/delivered', '', 3, 1, '2021-01-02 22:19:28', 1, '2021-01-02 22:20:00', 1),
(41, 6, 'Holded', '/orders/hold', '', 4, 1, '2021-01-02 22:21:14', NULL, NULL, 1),
(42, 6, 'Cancelled', '/orders/cancelled', '', 5, 1, '2021-01-02 22:21:47', NULL, NULL, 1),
(43, 0, 'Inbox', '#', 'fa fa-download', 1, 1, '2021-01-04 03:47:49', 1, '2021-01-04 03:51:46', 1),
(44, 43, 'Customer Inbox', '/assignment/index', '', 1, 1, '2021-01-04 03:48:31', 1, '2021-01-05 00:56:17', 1),
(45, 6, 'Label Generated', '/orders/label-generated', '', 2, 1, '2021-01-07 00:55:49', 1, '2021-01-07 04:56:34', 1),
(46, 0, 'Order Process Steps', '#', 'fa fa-list-ul', 10, 1, '2021-01-07 11:21:56', NULL, NULL, 1),
(47, 46, 'Order Process History', '/process-history/index', '', 3, 1, '2021-01-07 11:23:49', 1, '2021-01-07 23:32:29', 1),
(48, 46, 'Process Step Mapping', '/process-step-mapping/index', '', 2, 1, '2021-01-07 11:24:51', 1, '2021-01-07 23:32:19', 1),
(49, 46, 'Order Process List', '/process-step/index', '', 1, 1, '2021-01-07 11:26:01', 1, '2021-01-07 23:32:11', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_process_history`
--

CREATE TABLE `tbl_process_history` (
  `id` int(11) NOT NULL,
  `reference_type` varchar(50) NOT NULL,
  `reference_id` int(11) NOT NULL,
  `step_id` int(11) NOT NULL,
  `next_step_id` int(11) DEFAULT NULL,
  `submission_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `processed_date` datetime DEFAULT NULL,
  `processed_by` int(11) DEFAULT NULL,
  `process_remark` text DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_ip` varchar(20) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_on` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `status` smallint(2) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_process_history`
--

INSERT INTO `tbl_process_history` (`id`, `reference_type`, `reference_id`, `step_id`, `next_step_id`, `submission_date`, `processed_date`, `processed_by`, `process_remark`, `created_by`, `created_ip`, `updated_by`, `updated_on`, `status`) VALUES
(22, 'property', 1, 20, NULL, '2021-02-17 10:45:58', NULL, NULL, NULL, 1, '49.36.167.3', NULL, NULL, 1),
(23, 'property', 2, 20, NULL, '2021-02-19 07:26:43', NULL, NULL, NULL, 1, '49.36.163.25', NULL, NULL, 1),
(24, 'property', 3, 20, NULL, '2021-02-19 08:08:22', NULL, NULL, NULL, 1, '49.36.163.25', NULL, NULL, 1),
(25, 'property', 4, 20, NULL, '2021-02-19 08:11:37', NULL, NULL, NULL, 1, '49.36.163.25', NULL, NULL, 1),
(26, 'property', 5, 20, NULL, '2021-02-19 08:38:44', NULL, NULL, NULL, 1, '49.36.165.60', NULL, NULL, 1),
(27, 'property', 6, 20, NULL, '2021-02-20 12:33:21', NULL, NULL, NULL, 1, '49.36.165.60', NULL, NULL, 1),
(28, 'property', 6, 21, NULL, '2021-03-04 08:31:03', NULL, NULL, NULL, 1, '49.36.165.60', NULL, NULL, 1),
(29, 'property', 7, 20, NULL, '2021-03-04 09:00:00', NULL, NULL, NULL, 1, '49.36.165.69', NULL, NULL, 1),
(30, 'property', 8, 20, NULL, '2021-03-04 09:16:29', NULL, NULL, NULL, 1, '49.36.165.60', NULL, NULL, 1),
(31, 'property', 9, 20, NULL, '2021-03-05 12:29:36', NULL, NULL, NULL, 1, '49.36.161.119', NULL, NULL, 1),
(32, 'property', 10, 20, NULL, '2021-03-05 12:31:15', NULL, NULL, NULL, 1, '49.36.161.119', NULL, NULL, 1),
(33, 'property', 11, 20, NULL, '2021-03-05 12:31:33', NULL, NULL, NULL, 1, '49.36.161.119', NULL, NULL, 1),
(34, 'property', 12, 20, NULL, '2021-03-06 06:37:44', NULL, NULL, NULL, 1, '49.36.165.60', NULL, NULL, 1),
(35, 'property', 13, 20, 36, '2021-03-06 06:48:41', '2021-03-06 00:00:00', 41, 'P K', 41, '49.36.165.60', 41, '2021-03-06 06:49:27', 1),
(36, 'property', 13, 21, 37, '2021-03-06 06:49:27', '2021-03-06 00:00:00', 41, 'P K', 41, '49.36.165.60', 41, '2021-03-06 06:49:27', 1),
(37, 'property', 13, 21, NULL, '2021-03-06 06:49:27', NULL, NULL, NULL, 41, '49.36.165.60', NULL, NULL, 1),
(38, 'property', 14, 20, NULL, '2021-03-06 06:56:48', NULL, NULL, NULL, 41, '49.36.165.60', NULL, NULL, 1),
(39, 'property', 15, 20, NULL, '2021-03-06 07:08:29', NULL, NULL, NULL, 1, '49.36.165.60', NULL, NULL, 1),
(40, 'property', 16, 20, 49, '2021-03-06 08:40:49', '2021-03-06 00:00:00', 41, 'Shailendra', 51, '49.36.165.60', 41, '2021-03-06 08:55:40', 1),
(41, 'property', 17, 20, 45, '2021-03-06 08:42:06', '2021-03-06 00:00:00', 41, 'Manisha verma', 41, '49.36.165.60', 41, '2021-03-06 08:43:41', 1),
(42, 'property', 18, 20, NULL, '2021-03-06 08:42:37', NULL, NULL, NULL, 55, '49.36.165.60', NULL, NULL, 1),
(43, 'property', 19, 20, NULL, '2021-03-06 08:42:54', NULL, NULL, NULL, 51, '49.36.165.60', NULL, NULL, 1),
(44, 'property', 20, 20, NULL, '2021-03-06 08:43:05', NULL, NULL, NULL, 53, '49.36.165.60', NULL, NULL, 1),
(45, 'property', 17, 21, NULL, '2021-03-06 08:43:41', NULL, NULL, NULL, 41, '49.36.165.60', NULL, NULL, 1),
(46, 'property', 21, 20, NULL, '2021-03-06 08:44:31', NULL, NULL, NULL, 51, '49.36.165.60', NULL, NULL, 1),
(47, 'property', 22, 20, 48, '2021-03-06 08:47:12', '2021-03-06 00:00:00', 41, 'Ravi Rawat', 52, '49.36.165.60', 41, '2021-03-06 08:49:59', 1),
(48, 'property', 22, 21, NULL, '2021-03-06 08:49:59', NULL, NULL, NULL, 41, '49.36.165.60', NULL, NULL, 1),
(49, 'property', 16, 21, NULL, '2021-03-06 08:55:40', NULL, NULL, NULL, 41, '49.36.165.60', NULL, NULL, 1),
(50, 'property', 23, 20, NULL, '2021-03-08 09:38:48', NULL, NULL, NULL, 55, '49.36.165.60', NULL, NULL, 1),
(51, 'property', 24, 20, NULL, '2021-03-08 09:40:36', NULL, NULL, NULL, 53, '49.36.165.60', NULL, NULL, 1),
(52, 'property', 25, 20, NULL, '2021-03-08 09:42:23', NULL, NULL, NULL, 51, '49.36.165.60', NULL, NULL, 1),
(53, 'property', 26, 20, NULL, '2021-03-08 09:44:07', NULL, NULL, NULL, 53, '49.36.165.60', NULL, NULL, 1),
(54, 'property', 27, 20, 57, '2021-03-09 09:01:24', '2021-03-10 00:00:00', 1, NULL, 55, '49.36.165.60', 1, '2021-03-10 14:46:27', 1),
(55, 'property', 28, 20, NULL, '2021-03-09 09:26:06', NULL, NULL, NULL, 52, '49.36.165.60', NULL, NULL, 1),
(56, 'property', 29, 20, NULL, '2021-03-09 11:50:14', NULL, NULL, NULL, 1, '49.36.169.123', NULL, NULL, 1),
(57, 'property', 27, 21, 58, '2021-03-10 14:46:27', '2021-03-10 00:00:00', 1, NULL, 1, '112.79.132.190', 1, '2021-03-10 14:46:28', 1),
(58, 'property', 27, 21, 59, '2021-03-10 14:46:28', '2021-03-10 00:00:00', 1, NULL, 1, '112.79.132.190', 1, '2021-03-10 14:46:30', 1),
(59, 'property', 27, 21, 60, '2021-03-10 14:46:30', '2021-03-10 00:00:00', 1, NULL, 1, '112.79.132.190', 1, '2021-03-10 14:46:31', 1),
(60, 'property', 27, 21, NULL, '2021-03-10 14:46:31', NULL, NULL, NULL, 1, '112.79.132.190', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_process_step`
--

CREATE TABLE `tbl_process_step` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `reference_type` varchar(20) DEFAULT NULL,
  `step_label` varchar(255) NOT NULL,
  `history_label` varchar(255) NOT NULL,
  `allowed_days` int(11) DEFAULT 0,
  `ref_link` varchar(255) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_on` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `status` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_process_step`
--

INSERT INTO `tbl_process_step` (`id`, `name`, `reference_type`, `step_label`, `history_label`, `allowed_days`, `ref_link`, `created_by`, `created_on`, `updated_by`, `updated_on`, `status`) VALUES
(20, 'List Property', 'Property', 'List Property', 'Pending', 0, NULL, 1, '2021-02-16 00:30:23', 1, '2021-02-16 00:31:37', 1),
(21, 'Approve Property', 'Property', 'Approve', 'Approved', 0, NULL, 1, '2021-02-16 00:31:30', NULL, NULL, 1),
(22, 'Reject', 'Property', 'Reject', 'Rejected', 0, NULL, 1, '2021-02-16 00:31:51', NULL, NULL, 1),
(23, 'Hold', 'Property', 'Hold', 'On Hold', 0, NULL, 1, '2021-02-16 00:32:03', NULL, NULL, 1),
(24, 'Book', 'Property', 'Book', 'Booked', 0, NULL, 1, '2021-02-16 00:32:47', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_process_step_mapping`
--

CREATE TABLE `tbl_process_step_mapping` (
  `id` int(11) NOT NULL,
  `from_step` int(11) NOT NULL,
  `to_step` int(11) NOT NULL,
  `display_label` varchar(255) NOT NULL,
  `to_role` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_on` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_process_step_mapping`
--

INSERT INTO `tbl_process_step_mapping` (`id`, `from_step`, `to_step`, `display_label`, `to_role`, `created_by`, `created_on`, `updated_by`, `updated_on`, `status`) VALUES
(1, 20, 21, 'Created', 1, 1, '2021-02-16 00:40:11', NULL, NULL, 1),
(2, 20, 21, 'Created', 2, 1, '2021-02-16 00:40:11', NULL, NULL, 1),
(3, 20, 22, 'Created', 1, 1, '2021-02-16 00:40:11', NULL, NULL, 1),
(4, 20, 22, 'Created', 2, 1, '2021-02-16 00:40:11', NULL, NULL, 1),
(5, 20, 23, 'Created', 1, 1, '2021-02-16 00:40:11', NULL, NULL, 1),
(6, 20, 23, 'Created', 2, 1, '2021-02-16 00:40:11', NULL, NULL, 1),
(7, 21, 22, 'Approved', 1, 1, '2021-02-16 00:40:21', NULL, NULL, 1),
(8, 21, 22, 'Approved', 2, 1, '2021-02-16 00:40:21', NULL, NULL, 1),
(9, 21, 23, 'Approved', 1, 1, '2021-02-16 00:40:21', NULL, NULL, 1),
(10, 21, 23, 'Approved', 2, 1, '2021-02-16 00:40:21', NULL, NULL, 1),
(11, 21, 24, 'Approved', 1, 1, '2021-02-16 00:40:21', NULL, NULL, 1),
(12, 21, 24, 'Approved', 2, 1, '2021-02-16 00:40:21', NULL, NULL, 1),
(13, 23, 21, 'Approved', 1, 1, '2021-02-16 02:28:18', NULL, NULL, 1),
(14, 23, 21, 'Approved', 2, 1, '2021-02-16 02:28:18', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_state`
--

CREATE TABLE `tbl_state` (
  `id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL DEFAULT 1,
  `code` char(2) NOT NULL,
  `name` varchar(50) NOT NULL,
  `status` tinyint(1) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_state`
--

INSERT INTO `tbl_state` (`id`, `country_id`, `code`, `name`, `status`) VALUES
(1, 1, 'AP', 'Andhra Pradesh', 1),
(2, 1, 'AR', 'Arunachal Pradesh', 1),
(3, 1, 'AS', 'Assam', 1),
(4, 1, 'BR', 'Bihar', 1),
(5, 1, 'CG', 'Chhattisgarh', 1),
(6, 1, 'GA', 'Goa', 1),
(7, 1, 'GJ', 'Gujarat', 1),
(8, 1, 'HR', 'Haryana', 1),
(9, 1, 'HP', 'Himachal Pradesh', 1),
(10, 1, 'JH', 'Jharkhand', 1),
(11, 1, 'KA', 'Karnataka', 1),
(12, 1, 'KL', 'Kerala', 1),
(13, 1, 'MP', 'Madhya Pradesh', 1),
(14, 1, 'MH', 'Maharashtra', 1),
(15, 1, 'MN', 'Manipur', 1),
(16, 1, 'ML', 'Meghalaya', 1),
(17, 1, 'MZ', 'Mizoram', 1),
(18, 1, 'NL', 'Nagaland', 1),
(19, 1, 'OD', 'Odisha', 1),
(20, 1, 'PB', 'Punjab', 1),
(21, 1, 'RJ', 'Rajasthan', 1),
(22, 1, 'SK', 'Sikkim', 1),
(23, 1, 'TN', 'Tamil Nadu', 1),
(24, 1, 'TS', 'Telangana', 1),
(25, 1, 'TR', 'Tripura', 1),
(26, 1, 'UP', 'Uttar Pradesh', 1),
(27, 1, 'UK', 'Uttarakhand', 1),
(28, 1, 'WB', 'West Bengal', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_city`
--
ALTER TABLE `tbl_city`
  ADD PRIMARY KEY (`id`),
  ADD KEY `state_id` (`state_id`);

--
-- Indexes for table `tbl_country`
--
ALTER TABLE `tbl_country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_images`
--
ALTER TABLE `tbl_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reference_id` (`reference_id`),
  ADD KEY `uploaded_by` (`uploaded_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `tbl_masters`
--
ALTER TABLE `tbl_masters`
  ADD PRIMARY KEY (`id`,`updated_on`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `tbl_menus`
--
ALTER TABLE `tbl_menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name_2` (`name`),
  ADD KEY `parent_id` (`parent_id`),
  ADD KEY `name` (`name`),
  ADD KEY `created_by` (`created_by`,`created_on`,`updated_by`,`updated_on`,`status`);

--
-- Indexes for table `tbl_process_history`
--
ALTER TABLE `tbl_process_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_id` (`reference_type`),
  ADD KEY `from_step_id` (`step_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `processed_by` (`processed_by`),
  ADD KEY `reference_id` (`reference_id`);

--
-- Indexes for table `tbl_process_step`
--
ALTER TABLE `tbl_process_step`
  ADD PRIMARY KEY (`id`),
  ADD KEY `allowed_days` (`allowed_days`),
  ADD KEY `step_label_2` (`step_label`),
  ADD KEY `name` (`name`),
  ADD KEY `status` (`status`),
  ADD KEY `created_by` (`created_by`,`created_on`,`updated_by`,`updated_on`),
  ADD KEY `history_label` (`history_label`);

--
-- Indexes for table `tbl_process_step_mapping`
--
ALTER TABLE `tbl_process_step_mapping`
  ADD PRIMARY KEY (`id`),
  ADD KEY `from_step` (`from_step`),
  ADD KEY `to_step` (`to_step`),
  ADD KEY `created_by` (`created_by`,`created_on`,`updated_by`,`updated_on`,`status`),
  ADD KEY `to_role` (`to_role`);

--
-- Indexes for table `tbl_state`
--
ALTER TABLE `tbl_state`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`),
  ADD UNIQUE KEY `name` (`name`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_city`
--
ALTER TABLE `tbl_city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_country`
--
ALTER TABLE `tbl_country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=240;

--
-- AUTO_INCREMENT for table `tbl_images`
--
ALTER TABLE `tbl_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=368;

--
-- AUTO_INCREMENT for table `tbl_masters`
--
ALTER TABLE `tbl_masters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `tbl_menus`
--
ALTER TABLE `tbl_menus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `tbl_process_history`
--
ALTER TABLE `tbl_process_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `tbl_process_step`
--
ALTER TABLE `tbl_process_step`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `tbl_process_step_mapping`
--
ALTER TABLE `tbl_process_step_mapping`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `tbl_state`
--
ALTER TABLE `tbl_state`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
