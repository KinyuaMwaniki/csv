# Csv

Commercial Sales View (CSV)

## Deployment

To deploy this project run

```bash
git clone https://gitlab.com/ptindia/csv.git 
cd csv
composer install
npm install
npm run dev
cp .env.example .env
php artisan key:generate
Create db and enter database name, user, and password in .env
php artisan migrate --seed
php artisan storage:link
chmod 777 -R storage
Change the queue connection to database in .env ( QUEUE_CONNECTION=database )
set up your local server
Add your app url to .env APP_URL (e.g. http://csv.test:8080) 
```
  
## Default user

U: admin@crm.test

P: password


## Postman Collection

- [Postman collection](docs/csv.postman_collection.json)

# API Documentation

- [Authentication](docs/authentication.md)
- [States](docs/states.md)
- [Countries](docs/countries.md)
- [Catalogs](docs/catalogs.md)
- [Clients](docs/clients.md)
- [Reports](docs/reports.md)

## Deposit
- [Create](docs/users/deposit/create.md)
- [Update](docs/users/deposit/update.md)
- [Delete](docs/users/deposit/delete.md)

## Deposit With Base64 Image
- [Create](docs/users/deposit/mobile/create.md)
- [Update](docs/users/deposit/mobile/update.md)

## Orders
- [Get Pdf](docs/users/orders/get_pdf.md)
- [Update](docs/users/order/update.md)

## Dashboard
- [Get Numbers](docs/users/dashboard/numbers.md)
- [Recent Orders](docs/users/dashboard/recent_orders.md)
- [Daily Collections](docs/users/dashboard/daily_collections.md)
- [Daily Monthly Collection](docs/users/dashboard/monthly_collections.md)

## Users
- [Search By name or phone](docs/users/search_by_name_or_phone.md)

## Currencies
- [Get All](docs/currencies/get_all.md)
