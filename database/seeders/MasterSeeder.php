<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Master;
use App\Models\Company;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;

class MasterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Master::count() === 0 && App::environment('local')) {
            DB::statement('SET FOREIGN_KEY_CHECKS=0');

            DB::table('tbl_masters')->truncate();

            $company = Company::find(1);
            $company2 = Company::find(2);

            $masters = [
                ['name' => 'Mr', 'type' => 'title', 'status' => 1, 'company_id' => $company->id],
                ['name' => 'Mrs', 'type' => 'title', 'status' => 1, 'company_id' => $company->id],
                ['name' => 'Dr', 'type' => 'title', 'status' => 1, 'company_id' => $company->id],
                ['name' => 'Miss', 'type' => 'title', 'status' => 1, 'company_id' => $company->id],
                ['name' => 'Cold Call', 'type' => 'lead_source', 'status' => 1, 'company_id' => $company->id],
                ['name' => 'Existing Customer', 'type' => 'lead_source', 'status' => 1, 'company_id' => $company->id],
                ['name' => 'Self Generated', 'type' => 'lead_source', 'status' => 1, 'company_id' => $company->id],
                ['name' => 'Employee', 'type' => 'lead_source', 'status' => 1, 'company_id' => $company->id],
                ['name' => 'Banking', 'type' => 'industry', 'status' => 1, 'company_id' => $company->id],
                ['name' => 'Apparel', 'type' => 'industry', 'status' => 1, 'company_id' => $company->id],
                ['name' => 'Chemicals', 'type' => 'industry', 'status' => 1, 'company_id' => $company->id],
                ['name' => 'Lead Status', 'type' => 'lead_status', 'status' => 1, 'company_id' => $company->id],
                ['name' => 'Cold', 'type' => 'lead_status', 'status' => 1, 'company_id' => $company->id],
                ['name' => 'Contacted', 'type' => 'lead_status', 'status' => 1, 'company_id' => $company->id],
                ['name' => 'Acquired', 'type' => 'rating', 'status' => 1, 'company_id' => $company->id],
                ['name' => 'Active', 'type' => 'rating', 'status' => 1, 'company_id' => $company->id],
                ['name' => 'Market Failed', 'type' => 'rating', 'status' => 1, 'company_id' => $company->id],
            ];

            foreach ($masters as $master) {
                Master::create($master);
            }

            foreach($masters as $master) {
                Master::create(array_merge($master, ['company_id' => $company2->id]));
            }

            DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        }
    }
}
