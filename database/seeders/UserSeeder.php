<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use App\Models\Master;
use App\Models\Company;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (User::count() === 0 && App::environment('local')) {
            $admin_one = User::where('email', 'admin@crm.test')->first();
            $password = 'password';
            $company_one = Company::find(1);

            if (!$admin_one) {
                $admin_one = User::create([
                    'title' => 'Mr',
                    'first_name' => 'Crm',
                    'last_name' => 'Administrator',
                    'username' => 'admin',
                    'phone' => '+2551234566',
                    'email' => 'admin@crm.test',
                    'user_role' => Role::where('name', 'Administrator')->where('company_id', $company_one->id)->first()->id,
                    'email_verified_at' => now(),
                    'active' => 1,
                    'password' => Hash::make($password),
                    'company_id' => $company_one->id
                ]);
                $this->command->info("Company Name => {$company_one->name}, Username => {$admin_one->email}, Password => {$password}");

                foreach (Master::where('company_id', $company_one->id)->get() as $master) {
                    $master->update([
                        'created_by' => $admin_one->id,
                        'updated_by' => $admin_one->id
                    ]);
                }
            } else {
                $this->command->info("admin@crm.test already exists, password is $password");
            }




            $admin_two = User::where('email', 'admin2@crm.test')->first();
            $company_two = Company::find(2);

            if (!$admin_two) {
                $admin_two = User::create([
                    'title' => 'Mr',
                    'first_name' => 'Second',
                    'last_name' => 'Administrator',
                    'username' => 'admin2',
                    'phone' => '+2551234567',
                    'email' => 'admin2@crm.test',
                    'user_role' => Role::where('name', 'Administrator')->where('company_id', $company_two->id)->first()->id,
                    'email_verified_at' => now(),
                    'active' => 1,
                    'password' => Hash::make($password),
                    'company_id' => $company_two->id
                ]);
                $this->command->info("Company Name => {$company_two->name}, Username => {$admin_two->email}, Password => {$password}");

                foreach (Master::where('company_id', $company_two->id)->get() as $master) {
                    $master->update([
                        'created_by' => $admin_two->id,
                        'updated_by' => $admin_two->id
                    ]);
                }
            } else {
                $this->command->info("admin2@crm.test already exists, password is $password");
            }

            User::factory()->count(10)->create();
        }
    }
}
