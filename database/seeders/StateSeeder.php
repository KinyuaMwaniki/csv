<?php

namespace Database\Seeders;

use App\Models\State;
use App\Models\Country;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;

class StateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (State::count() === 0 && App::environment('local')) {
            DB::statement('SET FOREIGN_KEY_CHECKS=0');

            DB::table('tbl_state')->truncate();

            $india = Country::where('iso', 'IN')->first();

            $states = [
                [$india->id, 'AP', 'Andhra Pradesh', 1],
                [$india->id, 'AR', 'Arunachal Pradesh', 1],
                [$india->id, 'AS', 'Assam', 1],
                [$india->id, 'BR', 'Bihar', 1],
                [$india->id, 'CG', 'Chhattisgarh', 1],
                [$india->id, 'GA', 'Goa', 1],
                [$india->id, 'GJ', 'Gujarat', 1],
                [$india->id, 'HR', 'Haryana', 1],
                [$india->id, 'HP', 'Himachal Pradesh', 1],
                [$india->id, 'JH', 'Jharkhand', 1],
                [$india->id, 'KA', 'Karnataka', 1],
                [$india->id, 'KL', 'Kerala', 1],
                [$india->id, 'MP', 'Madhya Pradesh', 1],
                [$india->id, 'MH', 'Maharashtra', 1],
                [$india->id, 'MN', 'Manipur', 1],
                [$india->id, 'ML', 'Meghalaya', 1],
                [$india->id, 'MZ', 'Mizoram', 1],
                [$india->id, 'NL', 'Nagaland', 1],
                [$india->id, 'OD', 'Odisha', 1],
                [$india->id, 'PB', 'Punjab', 1],
                [$india->id, 'RJ', 'Rajasthan', 1],
                [$india->id, 'SK', 'Sikkim', 1],
                [$india->id, 'TN', 'Tamil Nadu', 1],
                [$india->id, 'TS', 'Telangana', 1],
                [$india->id, 'TR', 'Tripura', 1],
                [$india->id, 'UP', 'Uttar Pradesh', 1],
                [$india->id, 'UK', 'Uttarakhand', 1],
                [$india->id, 'WB', 'West Bengal', 1]
            ];

            foreach ($states as $state) {
                DB::insert('insert into tbl_state (country_id, code, name, status) values (?, ?, ?, ?)', $state);
            }

            DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        }
    }
}
