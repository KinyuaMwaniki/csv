<?php

namespace Database\Seeders;

use App\Models\Company;
use App\Models\Currency;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;

class CurrencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $company1 = Company::find(1);

        if ($company1->currencies()->count() === 0 && App::environment('local')) {
            $currencies = [
                ['name' => 'Afghani', 'code' => 'AFN', 'symbol' => '؋', 'company_id' => $company1->id],
                ['name' => 'Euro', 'code' => 'EUR', 'symbol' => '€', 'company_id' => $company1->id],
                ['name' => 'Lek', 'code' => 'ALL', 'symbol' => 'Lek', 'company_id' => $company1->id],
                ['name' => 'Algerian Dinar', 'code' => 'DZD', 'symbol' => null, 'company_id' => $company1->id],
                ['name' => 'US Dollar', 'code' => 'USD', 'symbol' => '$', 'company_id' => $company1->id],
            ];
            
            foreach ($currencies as $currency) {
                Currency::create($currency);
            }
        };
        
        
        $company2 = Company::find(2);
        
        if ($company2->currencies()->count() === 0 && App::environment('local')) {
            $currencies = [
                ['name' => 'Afghani', 'code' => 'AFN', 'symbol' => '؋', 'company_id' => $company2->id],
                ['name' => 'Euro', 'code' => 'EUR', 'symbol' => '€', 'company_id' => $company2->id],
                ['name' => 'Lek', 'code' => 'ALL', 'symbol' => 'Lek', 'company_id' => $company2->id],
                ['name' => 'Algerian Dinar', 'code' => 'DZD', 'symbol' => null, 'company_id' => $company2->id],
                ['name' => 'US Dollar', 'code' => 'USD', 'symbol' => '$', 'company_id' => $company2->id],
            ];

            foreach ($currencies as $currency) {
                Currency::create($currency);
            }
        };
    }
}
