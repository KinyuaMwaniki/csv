<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\Company;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Role::count() === 0 && App::environment('local')) {
            DB::statement('SET FOREIGN_KEY_CHECKS=0');

            DB::table('roles')->truncate();

            $company1 = Company::find(1);
            $company2 = Company::find(2);
            
            $roles = [
                ['name' => 'Administrator', 'guard_name' => 'web', 'company_id' => $company1->id],
                ['name' => 'Staff', 'guard_name' => 'web', 'company_id' => $company1->id],
                ['name' => 'Customer', 'guard_name' => 'web', 'company_id' => $company1->id],
                ['name' => 'Administrator', 'guard_name' => 'web', 'company_id' => $company2->id],
                ['name' => 'Staff', 'guard_name' => 'web', 'company_id' => $company2->id],
                ['name' => 'Customer', 'guard_name' => 'web', 'company_id' => $company2->id],
            ];

            foreach ($roles as $role) {
                Role::create($role);
            }

            DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        }
    }
}
