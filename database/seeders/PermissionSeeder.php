<?php

namespace Database\Seeders;

use App\Models\Role;
// use Spatie\Permission\Models\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Artisan;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        if (Permission::count() === 0 && App::environment('local')) {
            Artisan::call('cache:forget spatie.permission.cache');

            $admin = Role::where('name', 'Administrator')->first();

            $permissions = Config::get('constants.permissions');

            $this->command->info('Updating permissions');
            foreach ($permissions as $single) {
                Permission::updateOrCreate(
                    [
                        'name' => $single['name'],
                    ],
                    [
                        'name' => $single['name'],
                        'guard_name' => 'web',
                        'model' => $single['model']
                    ],
                );
            }
            $this->command->info('Updated permissions');
            $permissions = Permission::all();

            if (isset($admin)) {
                $admin->givePermissionTo($permissions);
                $this->command->info("All permissions granted to Admin");
            } else {
                $this->command->info("Role with 'Administrator' name not found");
            }
        }
    }
}
