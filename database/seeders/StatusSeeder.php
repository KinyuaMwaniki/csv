<?php

namespace Database\Seeders;

use App\Models\Status;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Status::count() === 0 && App::environment('local')) {
            DB::statement('SET FOREIGN_KEY_CHECKS=0');

            DB::table('statuses')->truncate();

            Status::create(
                ['name' => 'Active'],
            );

            Status::create(
                ['name' => 'Inactive']
            );

            DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        }
    }
}
