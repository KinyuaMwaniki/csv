<?php

namespace Database\Factories;

use App\Models\Lead;
use App\Models\User;
use App\Models\Master;
use App\Models\Country;
use Illuminate\Database\Eloquent\Factories\Factory;

class LeadFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Lead::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {

        $lead_source = Master::where('type', 'lead_source')
            ->inRandomOrder()
            ->first();

        $industry = Master::where('type', 'industry')
            ->inRandomOrder()
            ->first();

        $rating = Master::where('type', 'rating')
            ->inRandomOrder()
            ->first();


        $status = Master::where('type', 'lead_status')
            ->inRandomOrder()
            ->first();

        $assigned_to = User::inRandomOrder()
            ->first();


        $user = User::where('user_role', 1)
            ->inRandomOrder()
            ->first();

        $country = Country::inRandomOrder()
            ->first();

        if ($country->states()->exists()) {
            $state = $country->states->random();
        }

        return [
            'title' => $this->faker->randomElement($array = array ('Mr','Mrs','Dr', 'Miss')),
            'first_name' =>  $this->faker->firstName(),
            'last_name' => $this->faker->lastName(),
            'phone' => $this->faker->unique()->e164PhoneNumber(),
            'company' =>  $this->faker->company(),
            'mobile' => $this->faker->unique()->e164PhoneNumber(),
            'designation' => $this->faker->jobTitle(),
            'fax' => $this->faker->e164PhoneNumber(),
            'source' => $lead_source->name,
            'email' => $this->faker->unique()->safeEmail(),
            'industry' => $industry->name,
            'website' => $this->faker->domainName(),
            'annual_revenue' => $this->faker->numberBetween($min = 1000, $max = 1000000),
            'status' => $status->name,
            'no_of_employees' => $this->faker->numberBetween($min = 10, $max = 1000),
            'rating' => $rating->name,
            'secondary_email' => $this->faker->unique()->safeEmail(),
            'assigned_to' => $assigned_to->id,
            'created_by' => $user->id,
            'email_opt_out' => $this->faker->numberBetween($min = 0, $max = 1),
            'lane' => $this->faker->streetName(),
            'pobox' => $this->faker->streetAddress(),
            'code' => $this->faker->buildingNumber(),
            'city' => $this->faker->city(),
            'state' => isset($state) ? $state->id : null,
            'country' => $country->id,
            'description' => $this->faker->text($maxNbChars = 200),
        ];
    }
}
