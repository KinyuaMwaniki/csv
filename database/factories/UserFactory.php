<?php

namespace Database\Factories;

use App\Models\Role;
use App\Models\User;
use App\Models\Master;
use App\Models\Company;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {

        $role = Role::inRandomOrder()->first();
        $company = Company::inRandomOrder()->first();

        return [
            'title' => $this->faker->randomElement($array = array ('Mr','Mrs','Dr', 'Miss')),
            'first_name' =>  $this->faker->firstName(),
            'last_name' => $this->faker->lastName(),
            'email' => $this->faker->unique()->safeEmail(),
            'username' => $this->faker->unique()->name(),
            'phone' => $this->faker->unique()->e164PhoneNumber(),
            'user_role' => $role->id, 
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'active' => true,
            'company_id' => $company->id,
            'force_pw_change' => false,
            'remember_token' => Str::random(10),
        ];
    }

    /**
     * Indicate that the model's email address should be unverified.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function unverified()
    {
        return $this->state(function (array $attributes) {
            return [
                'email_verified_at' => null,
            ];
        });
    }
}
