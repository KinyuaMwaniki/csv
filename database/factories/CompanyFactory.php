<?php

namespace Database\Factories;

use App\Models\User;
use App\Models\Master;
use App\Models\Company;
use App\Models\Country;
use Illuminate\Database\Eloquent\Factories\Factory;

class CompanyFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Company::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $country = Country::inRandomOrder()
            ->first();

        if (isset($country) && $country->states()->exists()) {
            $state = $country->states->random();
        }

        return [
            'name' =>  $this->faker->company(),
            'industry' => 'Apparel',
            'mobile' => $this->faker->unique()->e164PhoneNumber(),
            'fax' => $this->faker->e164PhoneNumber(),
            'email' => $this->faker->unique()->safeEmail(),
            'address' => $this->faker->streetAddress(),
            'address_2' => $this->faker->streetAddress(),
            'city' => $this->faker->city(),
            'state_id' => isset($state) ? $state->id : null,
            'country_id' => $country->id,
            'zip_code' => $this->faker->postcode(),
            'status' => true,
        ];
    }
}
