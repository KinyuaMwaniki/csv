<?php

namespace Database\Factories;

use App\Models\Country;
use Illuminate\Database\Eloquent\Factories\Factory;

class CountryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Country::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'iso' => strtoupper($this->faker->lexify('??')),
            'name' => $this->faker->name,
            'nicename' => $this->faker->name,
            'iso3' => strtoupper($this->faker->lexify('???')),
            'numcode' => $this->faker->randomNumber($nbDigits = 4, $strict = false),
            'phonecode' => $this->faker->areaCode,
            'status' => $this->faker->randomElement([0, 1]),
        ];
    }
}
