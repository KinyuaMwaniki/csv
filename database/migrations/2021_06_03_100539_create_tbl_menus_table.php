<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_menus', function (Blueprint $table) {
            $table->id();
            $table->integer('parent_id')->nullable()->default(0)->index('parent_id');
            $table->string('name', 50)->index('name');
            $table->string('url', 50);
            $table->string('icon', 50)->nullable();
            $table->integer('menu_order')->nullable();
            $table->integer('created_by');
            $table->timestamp('created_on')->useCurrent();
            $table->integer('updated_by')->nullable();
            $table->timestamp('updated_on')->nullable();
            $table->boolean('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_menus');
    }
}
