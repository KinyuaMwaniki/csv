<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_images', function (Blueprint $table) {
            $table->id();
            $table->string('reference_type', 100);
            $table->integer('reference_id')->index('reference_id');
            $table->string('original_name');
            $table->string('name', 100);
            $table->string('file_ext', 10);
            $table->boolean('force_download')->default(0);
            $table->string('file_path');
            $table->unsignedBigInteger('company_id');
            $table->boolean('status')->default(1);
            $table->timestamp('uploaded_on')->useCurrent();
            $table->integer('uploaded_by')->index('uploaded_by');
            $table->integer('updated_by')->nullable()->index('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_images');
    }
}
