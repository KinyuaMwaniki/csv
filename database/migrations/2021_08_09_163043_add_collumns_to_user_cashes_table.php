<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCollumnsToUserCashesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_cashes', function (Blueprint $table) {
            $table->boolean('denied')->after('approved')->default(0);
            $table->string('reason_denied')->after('denied')->nullable();
            $table->string('status')->after('reason_denied')->default('Pending');
            $table->unsignedBigInteger('user_id')->after('company_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_cashes', function (Blueprint $table) {
            $table->dropColumn(['denied']);
            $table->dropColumn(['reason_denied']);
            $table->dropColumn(['status']);
            $table->dropColumn(['user_id']);
        });
    }
}
