<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('description')->nullable();
            $table->string('meta_title');
            $table->text('meta_description')->nullable();
            $table->text('meta_keyword')->nullable();
            $table->string('tag')->nullable();
            $table->string('model');
            $table->string('sku')->nullable();
            $table->string('upc')->nullable();
            $table->string('ean')->nullable();
            $table->string('jan')->nullable();
            $table->string('isbn')->nullable();
            $table->string('mpn')->nullable();
            $table->string('location')->nullable();
            $table->decimal('price', $precision = 15, $scale = 2)->nullable();
            $table->integer('quantity')->nullable();
            $table->integer('minimum')->nullable();
            $table->integer('subtract')->nullable();
            $table->integer('stock_status_id')->nullable();
            $table->boolean('shipping')->nullable();
            $table->date('date_available')->nullable();
            $table->boolean('status')->default(true);
            $table->integer('sort_order')->nullable();
            $table->unsignedBigInteger('company_id');
            $table->unsignedBigInteger('manufacturer_id')->nullable();
            $table->unsignedBigInteger('brand_id')->nullable();
            $table->unsignedBigInteger('created_by');
            $table->unsignedBigInteger('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
