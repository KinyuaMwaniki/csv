<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leads', function (Blueprint $table) {
            $table->id();
            $table->string('title')->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name');
            $table->string('phone')->nullable();
            $table->string('company')->nullable();
            $table->string('mobile')->nullable();
            $table->string('designation')->nullable();
            $table->string('fax')->nullable();
            $table->string('source')->default('crm');
            $table->string('email')->nullable();
            $table->string('industry')->nullable();
            $table->string('website')->nullable();
            $table->string('annual_revenue')->nullable();
            $table->string('status')->nullable();
            $table->string('no_of_employees')->nullable();
            $table->string('rating')->nullable();
            $table->string('secondary_email')->nullable();
            $table->unsignedBigInteger('company_id');
            $table->unsignedBigInteger('assigned_to');
            $table->unsignedBigInteger('created_by');
            $table->boolean('email_opt_out')->default(0);
            $table->string('lane')->nullable();
            $table->string('pobox')->nullable();
            $table->string('code')->nullable();
            $table->string('city')->nullable();
            $table->unsignedBigInteger('country')->nullable();
            $table->unsignedBigInteger('state')->nullable();
            $table->text('description')->nullable();
            $table->timestamps();

            $table->foreign('assigned_to')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('created_by')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
                
            $table->foreign('country')
                ->references('id')
                ->on('tbl_country')
                ->onDelete('cascade');

            $table->foreign('state')
                ->references('id')
                ->on('tbl_state')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leads');
    }
}
