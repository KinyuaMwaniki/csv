<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblProcessHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_process_history', function (Blueprint $table) {
            $table->id();
            $table->string('reference_type', 50)->index('order_id');
            $table->integer('reference_id')->index('reference_id');
            $table->integer('step_id')->index('from_step_id');
            $table->integer('next_step_id')->nullable();
            $table->timestamp('submission_date')->useCurrent();
            $table->dateTime('processed_date')->nullable();
            $table->integer('processed_by')->nullable()->index('processed_by');
            $table->text('process_remark')->nullable();
            $table->integer('created_by')->index('created_by');
            $table->string('created_ip', 20);
            $table->integer('updated_by')->nullable()->index('updated_by');
            $table->smallInteger('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_process_history');
    }
}
