<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateUsersTableTwo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('title')->after('id');
            $table->string('first_name')->after('title');
            $table->string('last_name')->after('first_name');
            $table->string('username')->after('email')->unique();
            $table->string('phone')->after('username')->unique();
            $table->unsignedBigInteger('role')->after('phone')->nullable();
            $table->string('photo')->after('role')->nullable();
            $table->dropColumn('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            // $table->dropForeign(['role']);

            $table->dropColumn(['title', 'first_name', 'last_name', 'username', 'phone', 'role', 'photo']);
            $table->string('name')->after('id');
        });
    }
}
