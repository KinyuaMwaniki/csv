<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCreditLimitStartDateAndCreditLimitEndDateToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->date('credit_limit_start_date')->after('credit')->nullable();
            $table->date('credit_limit_end_date')->after('credit_limit_start_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['credit_limit_start_date']);
            $table->dropColumn(['credit_limit_end_date']);
        });
    }
}
