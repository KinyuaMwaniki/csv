<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->id();
            $table->string('external_id');
            $table->string('payment_method');
            $table->string('type_of_wallet')->nullable();
            $table->string('transaction_id')->nullable();
            $table->decimal('amount_paid', $precision = 15, $scale = 2)->nullable();
            $table->decimal('amount_remaining', $precision = 15, $scale = 2)->nullable();
            $table->unsignedBigInteger('order_id');
            $table->unsignedBigInteger('company_id');
            $table->unsignedBigInteger('created_by');
            $table->unsignedBigInteger('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
