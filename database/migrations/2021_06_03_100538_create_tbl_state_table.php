<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblStateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_state', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('country_id');
            $table->char('code', 2)->unique('code');
            $table->string('name', 50)->unique('name');
            $table->boolean('status')->nullable()->default(1);
            $table->timestamps();

            $table->foreign('country_id')
                ->references('id')
                ->on('tbl_country')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_state');
    }
}
