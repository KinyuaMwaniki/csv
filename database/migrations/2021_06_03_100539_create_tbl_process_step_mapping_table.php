<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblProcessStepMappingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_process_step_mapping', function (Blueprint $table) {
            $table->id();
            $table->integer('from_step')->index('from_step');
            $table->integer('to_step')->index('to_step');
            $table->string('display_label');
            $table->integer('to_role')->index('to_role');
            $table->integer('created_by');
            $table->integer('updated_by')->nullable();
            $table->boolean('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_process_step_mapping');
    }
}
