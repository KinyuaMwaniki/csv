<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblProcessStepTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_process_step', function (Blueprint $table) {
            $table->id();
            $table->string('name')->index('name');
            $table->string('reference_type', 20)->nullable();
            $table->string('step_label')->index('step_label_2');
            $table->string('history_label')->index('history_label');
            $table->integer('allowed_days')->nullable()->default(0)->index('allowed_days');
            $table->string('ref_link')->nullable();
            $table->integer('created_by');
            $table->integer('updated_by')->nullable();
            $table->boolean('status')->default(1)->index('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_process_step');
    }
}
