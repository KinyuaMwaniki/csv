<?php

namespace App\Exports;

use App\Models\Lead;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class LeadsExport implements FromCollection, WithMapping, WithHeadings
{
    public function collection()
    {
        return Lead::with(['assignee', 'fromCountry'])->get();
    }

    public function map($lead): array
    {
        return [
            $lead->title,
            $lead->first_name,
            $lead->last_name,
            $lead->id,
            $lead->phone,
            $lead->company,
            $lead->mobile,
            $lead->designation,
            $lead->fax,
            $lead->source,
            $lead->email,
            $lead->industry,
            $lead->website,
            $lead->annual_revenue,
            $lead->status,
            $lead->no_of_employees,
            $lead->rating,
            $lead->secondary_email,
            $lead->assignee->first_name,
            $lead->created_at,
            $lead->updated_at,
            $lead->email_opt_out,
            $lead->creator->first_name . ' ' . $lead->creator->last_name,
            $lead->lane,
            $lead->pobox,
            $lead->code,
            $lead->city,
            $lead->fromCountry ? $lead->fromCountry->name : '',
            $lead->fromState ? $lead->fromState->name : '',
            $lead->description
        ];
    }

    public function headings(): array
    {
        return [
            'Salutation',
            'First Name',
            'Last Name',
            'Lead Number',
            'Primary Phone',
            'Company',
            'Mobile Phone',
            'Designation',
            'Fax',
            'Lead Source',
            'Primary Email',
            'Industry',
            'Website',
            'Annual Revenue',
            'Lead Status',
            'Number of Employees',
            'Rating',
            'Secondary Email',
            'Assigned To',
            'Created At',
            'Updated At',
            'Email Opt Out',
            'Created By',
            'Street',
            'PO Box',
            'Postal Code',
            'City',
            'Country',
            'State',
            'Description'
        ];
    }
}
