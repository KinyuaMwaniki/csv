<?php

namespace App\Exports;

use Carbon\Carbon;
use App\Models\Order;
use Illuminate\Support\Facades\Artisan;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;

class OrdersExport implements FromCollection, WithMapping, WithHeadings
{

    protected $request;
    protected $user;

    function __construct($user, $request)
    {
        $this->user = $user;
        $this->request = $request;
    }


    public function collection()
    {
        $created_at = $this->request['created_at'];
        $order_number = $this->request['order_number'];
        $name = $this->request['name'];
        $total = array_key_exists('total', $this->request) ? $this->request['total'] : null;
        $amount_remaining = $this->request['amount_remaining'];
        $amount_paid = array_key_exists('amount_paid', $this->request) ? $this->request['amount_paid'] : null;
        $to = $this->request['to'];
        $from = $this->request['from'];
        $onlyDebts = array_key_exists('onlyDebts', $this->request) ? $this->request['onlyDebts'] : null;

        $columns = $this->request['tableColumns'];
        $column = $this->request['column'];
        $dir = $this->request['dir'];

        $query = Order::select('id', 'order_number', 'created_at', 'total', 'amount_paid', 'amount_remaining', 'client_id', 'status')
            ->where('company_id', $this->user->company_id)
            ->with(['client'])
            ->orderBy($columns[$column], $dir);


        if ($this->user->theirRole->name === "Commercial") {
            $query->where('created_by', $this->user->id);
        }

        if ($onlyDebts === 1) {
            $query->where('amount_remaining', '>', 0);
        }

        if ($created_at) {
            $query->whereDate('created_at', '=', Carbon::parse($created_at));
        }

        if ($order_number) {
            $query->where('order_number', 'like', '%' . $order_number . '%');
        }

        if ($name) {
            $query->whereHas('client', function ($query) use ($name) {
                $query->where('first_name', $name)
                    ->orWhere('last_name', $name)
                    ->orWhereRaw(
                        "concat(first_name, ' ', last_name) like '%" . $name . "%' "
                    );
            });
        }

        if ($total) {
            $query->where('total', 'like', '%' . $total . '%');
        }

        if ($amount_remaining) {
            $query->where('amount_remaining', 'like', '%' . $amount_remaining . '%');
        }

        if ($amount_paid) {
            $query->where('amount_paid', 'like', '%' . $amount_paid . '%');
        }

        if ($to && $from) {
            $query->whereBetween('created_at', [Carbon::parse($from), Carbon::parse($to)]);
        }

        $orders = $query->get();

        return $orders;
    }

    public function map($order): array
    {
        return [
            $order->created_at->format('d-m-Y'),
            $order->order_number,
            $order->client->first_name . " " . $order->client->last_name,
            $order->total,
            $order->amount_paid,
            $order->amount_remaining
        ];
    }

    public function headings(): array
    {
        return [
            __('invoice.createdAt'),
            __('invoice.orderNumber'),
            __('invoice.clientName'),
            __('invoice.totalAmount'),
            __('invoice.paidAmount'),
            __('invoice.remainingAmount'),
        ];
    }
}
