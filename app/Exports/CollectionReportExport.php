<?php

namespace App\Exports;

use Carbon\Carbon;
use App\Models\Payment;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;

class CollectionReportExport implements FromCollection, WithMapping, WithHeadings
{

    protected $request;
    protected $user;

    function __construct($user, $request)
    {
        $this->user = $user;
        $this->request = $request;
    }
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $created_at = $this->request['created_at'];
        $orderNumber = $this->request['orderNumber'];
        $name = $this->request['name'];
        $status = $this->request['status'];
        $payment_method = $this->request['payment_method'];
        $total = array_key_exists('total', $this->request) ? $this->request['total'] : null;
        $amount_paid = array_key_exists('amount_paid', $this->request) ? $this->request['amount_paid'] : null;
        $to = $this->request['to'];
        $from = $this->request['from'];
        $pendingWallet = array_key_exists('pendingWallet', $this->request) ? $this->request['amount_paid'] : null;

        $columns = $this->request['tableColumns'];
        $column = $this->request['column'];
        $dir = $this->request['dir'];

        $query = Payment::select('id', 'created_at', 'amount_paid', 'amount_remaining', 'payment_method', 'order_id', 'status')
            ->where('company_id', $this->user->company_id)
            ->whereIn('payment_method', ['cash', 'wallet'])
            ->with(['order', 'order.client'])
            ->orderBy($columns[$column], $dir);

        if ($this->user->theirRole->name === "Commercial") {
            $query->where('created_by', $this->user->id);
        }

        if ($created_at) {
            $query->whereDate('created_at', '=', Carbon::parse($created_at));
        }

        if ($orderNumber) {
            $query->whereHas('order', function ($query) use ($orderNumber) {
                $query->where('order_number', 'like', '%' . $orderNumber . '%');
            });
        }

        if ($name) {
            $query->whereHas('order', function ($query) use ($name) {
                $query->whereHas('client', function ($query) use ($name) {
                    $query->where('first_name', $name)
                        ->orWhere('last_name', $name)
                        ->orWhereRaw(
                            "concat(first_name, ' ', last_name) like '%" . $name . "%' "
                        );
                });
            });
        }

        if ($payment_method) {
            $query->where('payment_method', 'like', '%' . $payment_method . '%');
        }

        if ($total) {
            $query->where('total', 'like', '%' . $total . '%');
        }

        if ($amount_paid) {
            $query->where('amount_paid', 'like', '%' . $amount_paid . '%');
        }

        if ($status) {
            $query->where('status', $status);
        }

        if ($to && $from) {
            $query->whereBetween('created_at', [Carbon::parse($from), Carbon::parse($to)]);
        }

        if ($pendingWallet === 1) {
            $query->where('payment_method', 'wallet')
                ->where('status', 'pending');
        }

        $payments = $query->get();

        return $payments;
    }

    public function map($payment): array
    {
        return [
            $payment->created_at->format('d-m-Y'),
            $payment->payment_method,
            $payment->order->order_number ? $payment->order->order_number : "",
            $payment->order->client->first_name . " " . $payment->order->client->last_name,
            $payment->order->total,
            $payment->amount_paid,
            $payment->order->amount_remaining,
            $payment->status
        ];
    }

    public function headings(): array
    {
        return [
            __('invoice.paymentDate'),
            __('invoice.paymentMethod'),
            __('invoice.orderNumber'),
            __('invoice.clientName'),
            __('invoice.totalAmount'),
            __('invoice.paidAmount'),
            __('invoice.remainingAmount'),
            __('invoice.status')
        ];
    }
}
