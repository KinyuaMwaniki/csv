<?php

namespace App\Models;

use App\Models\CategoryAttribute;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Category extends Model
{
    use HasFactory;

    protected $guarded = [];
    private $descendants = [];

    protected $appends = [
        'full_name',
    ];

    public function parentCategory()
    {
        return $this->belongsTo(Category::class, 'parent_id');
    }

    public function childrenCategories()
    {
        return $this->hasMany(Category::class, 'parent_id')
            ->select(['id', 'name', 'slug']);
    }

    public function children()
    {
        return $this->childrenCategories()->with('children');
    }

    public function hasChildren()
    {
        if ($this->children->count()) {
            return true;
        }
        return false;
    }

    public function findDescendants(Category $category)
    {
        $this->descendants[] = $category->id;

        if ($category->hasChildren()) {
            foreach ($category->children as $child) {
                $this->findDescendants($child);
            }
        }
    }

    public function getDescendants(Category $category)
    {
        $this->findDescendants($category);
        return $this->descendants;
    }

    public function company()
    {
        return $this->belongsTo(Company::class)->select(['id', 'name']);
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'created_by')->select(['id', 'first_name', 'last_name']);
    }

    public function updater()
    {
        return $this->belongsTo(User::class, 'updated_by')->select(['id', 'first_name', 'last_name']);
    }

    public function attributes()
    {
        return $this->belongsToMany(AttributeType::class, 'category_attributes', 'category_id', 'attribute_id')
            ->withPivot('value', 'unit_id', 'unit_name')
            ->using(CategoryAttribute::class)
            ->orderBy('name');
    }

    public function getFullNameAttribute()
    {
        $full_name = $this->name;
        $parent = $this->parentCategory;
        while (!is_null($parent)) {
            $full_name = "{$parent->name} > {$full_name}";
            $parent = $parent->parentCategory;
        }
        return $full_name;
    }
}
