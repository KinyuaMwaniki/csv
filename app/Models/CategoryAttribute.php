<?php

namespace App\Models;

use App\Models\Unit;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class CategoryAttribute extends Pivot
{
    use HasFactory;

    protected $table = "category_attributes";

    public function units()
    {
        return $this->belongsTo(Unit::class, 'unit_id');
    }
}
