<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Master extends Model
{
    use HasFactory;

    protected $table = 'tbl_masters';
    protected $guarded = [];


    public function creator()
    {
        return $this->belongsTo(User::class, 'created_by')->select(['id', 'first_name', 'last_name']);
    }
    public function updater()
    {
        return $this->belongsTo(User::class, 'updated_by')->select(['id', 'first_name', 'last_name']);
    }
}
