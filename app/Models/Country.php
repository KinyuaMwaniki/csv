<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    use HasFactory;

    protected $primaryKey = 'id';
    protected $table = 'tbl_country';
    protected $guarded = [];

    public function states()
    {
        return $this->hasMany(State::class)->select(['id', 'country_id', 'name']);
    }
}
