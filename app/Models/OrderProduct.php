<?php

namespace App\Models;

use App\Models\ProductAttribute;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\JsonEncodingException;

class OrderProduct extends Model
{
    use HasFactory;

    protected $fillable = [
        'order_id',
        'product_id',
        'quantity',
        'price',
        'total',
        'attributes',
        'company_id',
        'created_by',
        'updated_by'
    ];

    protected $casts = [
        'attributes' => 'array',
    ];

    protected $appends = [
        'product_attributes',
        'order_product_attrs'
    ];

    public function company()
    {
        return $this->belongsTo(Company::class)->select(['id', 'name']);
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'created_by')->select(['id', 'first_name', 'last_name']);
    }

    public function updater()
    {
        return $this->belongsTo(User::class, 'updated_by')->select(['id', 'first_name', 'last_name']);
    }

    public function order()
    {
        return $this->belongsTo(Order::class)->select(['id']);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function getOrderProductAttrsAttribute()
    {
        $prdAttrs = [];
        if (!isset($this['attributes']) || count($this['attributes']) === 0) {
            return [];
        }
        foreach ($this['attributes'] as $attribute) {
            $prdAt = ProductAttribute::with(['attributes'])->where('id', $attribute)->first();
            if (empty($prdAt)) {
                continue;
            }
            array_push($prdAttrs, $prdAt);
        }
        return $prdAttrs;
    }

    public function getProductAttributesAttribute()
    {
        $prdAttrs = [];
        if (!isset($this['attributes']) || count($this['attributes']) === 0) {
            return [];
        }
        foreach ($this['attributes'] as $attribute) {
            $prdAt = ProductAttribute::with(['attributes'])->where('id', $attribute)->first();
            if (empty($prdAt)) {
                continue;
            }
            $single = $this->getAttributeName($prdAt);
            array_push($prdAttrs, $single);
        }
        return $prdAttrs;
    }

    private function getAttributeName($attribute)
    {
        info($attribute);
        $single = [];
        $single['id'] = $attribute['id'];
        $single['name'] = $attribute['attributes']['name'];
        $value = $attribute['unit_name'] !== null ? "{$attribute['value']} {$attribute['unit_name']}" : $attribute['value'];
        $single['value'] = $value;
        return $single;
    }
}
