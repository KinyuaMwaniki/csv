<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory;

    protected $fillable = [
        'first_name',
        'last_name',
        'company_name',
        'mobile',
        'email',
        'street_address',
        'zip_code',
        'locality',
        'city',
        'state',
        'country',
        'created_by',
        'updated_by',
        'company_id',
        'status',
        'is_premium'
    ];


    protected $appends = [
        'debts',
    ];

    public function balance()
    {
        return $this->hasOne(ClientBalance::class, 'client_id');
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function photo()
    {
        return $this->hasOne(File::class, 'reference_id')
            ->where('reference_type', get_class($this))
            ->where('type', 'photo')
            ->select(['id', 'reference_id', 'reference_type', 'name', 'force_download', 'file_path', 'created_at']);
    }

    public function documents()
    {
        return $this->hasMany(File::class, 'reference_id')
            ->where('reference_type', get_class($this))
            ->where('type', 'document')
            ->select(['id', 'reference_id', 'reference_type', 'original_name', 'name', 'force_download', 'file_path', 'created_at']);
    }


    public function shippingAddresses()
    {
        return $this->hasMany(Address::class, 'client_id')
            ->where('is_billing', 0)
            ->select(['id', 'client_id', 'street_address', 'zip_code', 'locality', 'city', 'state', 'country', 'is_default', 'is_billing', 'company_id'])
            ->orderBy('is_default', 'DESC');
    }

    public function billingAddresses()
    {
        return $this->hasMany(Address::class, 'client_id')
            ->where('is_billing', 1)
            ->select(['id', 'client_id', 'street_address', 'zip_code', 'locality', 'city', 'state', 'country', 'is_default', 'is_billing', 'company_id']);
    }

    public function addresses()
    {
        return $this->hasMany(Address::class, 'client_id')
            ->select(['id', 'client_id', 'street_address', 'zip_code', 'locality', 'city', 'state', 'country', 'is_default', 'is_billing', 'company_id']);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function getDebtsAttribute()
    {
        return $this->orders->sum('amount_remaining');
    }
}
