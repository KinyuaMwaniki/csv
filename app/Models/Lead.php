<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Lead extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function assignee()
    {
        return $this->belongsTo(User::class, 'assigned_to');
    }

    public function fromCountry()
    {
        return $this->belongsTo(Country::class, 'country')->select(['id', 'name']);
    }

    public function fromState()
    {
        return $this->belongsTo(State::class, 'state')->select(['id', 'name']);
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'created_by')->select(['id', 'first_name']);
    }
}
