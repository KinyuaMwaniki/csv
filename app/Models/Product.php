<?php

namespace App\Models;

use App\Models\File;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'model',
        'created_by',
        'updated_by',
        'company_id',
        'sku',
        'price',
        'quantity',
        'minimum',
        'subtract',
        'stock_status_id',
        'shipping',
        'date_available',
        'length',
        'width',
        'height',
        'status',
        'sort_order',
        'manufacturer_id',
        'currency_id'
    ];

    const PRODUCT_STOCK_STATUS = [
        1 => '2-3 Days',
        2 => 'In Stock',
        3 => 'Out Of Stock',
        4 => 'Pre-Order',
    ];

    public function creator()
    {
        return $this->belongsTo(User::class, 'created_by')->select(['id', 'first_name', 'last_name']);
    }

    public function updater()
    {
        return $this->belongsTo(User::class, 'updated_by')->select(['id', 'first_name', 'last_name']);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'product_categories', 'product_id', 'category_id')
            ->withTimestamps();
    }

    public function attributes()
    {
        return $this->belongsToMany(AttributeType::class, 'product_attributes', 'product_id', 'attribute_id')
            ->withPivot('value', 'unit_id', 'unit_name', 'id')
            ->withTimestamps();
    }

    public function brand() {
        return $this->belongsTo(Brand::class, 'manufacturer_id');
    }

    public function photos()
    {
        return $this->hasMany(File::class, 'reference_id')
                    ->where('reference_type', get_class($this))
                    ->select(['id', 'reference_id', 'reference_type', 'name', 'force_download', 'file_path', 'created_at']);
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }
}
