<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Address extends Model
{
    use HasFactory;

    protected $fillable = [
        'street_address',
        'zip_code',
        'locality',
        'city',
        'state',
        'country',
        'is_default',
        'is_billing',
        'client_id',
        'company_id'
    ];


    public function orders()
    {
        return $this->hasMany(Order::class);
    }
}
