<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClientBalanceActivity extends Model
{
    use HasFactory;

    protected $fillable = [
        'balance_id',
        'client_id',
        'amount',
        'payment_type',
        'company_id',
        'created_by',
        'updated_by',
    ];
}
