<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserCash extends Model
{
    use HasFactory;

    protected $fillable = [
        'date',
        'amount',
        'approved',
        'denied',
        'reason_denied',
        'company_id',
        'status',
        'user_id',
        'created_by',
        'updated_by'
    ];

    public function photo()
    {
        return $this->hasOne(File::class, 'reference_id')
            ->where('reference_type', get_class($this))
            ->select(['id', 'reference_id', 'reference_type', 'name', 'force_download', 'file_path']);
    }
}
