<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'company_id'];

    public function logo()
    {
        return $this->hasOne(File::class, 'reference_id')
                    ->where('reference_type', get_class($this))
                    ->select(['id', 'reference_id', 'reference_type', 'name', 'force_download', 'file_path']);
    }
}
