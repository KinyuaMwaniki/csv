<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'industry',
        'mobile',
        'fax',
        'email',
        'currency_id',
        'address',
        'zip_code',
        'city',
        'state',
        'country',
        // 'period_of_activity',
        // 'start_activity'
    ];

    public function creator()
    {
        return $this->belongsTo(User::class, 'created_by')->select(['id', 'first_name', 'last_name']);
    }

    public function updater()
    {
        return $this->belongsTo(User::class, 'updated_by')->select(['id', 'first_name', 'last_name']);
    }

    public function currencies()
    {
        return $this->hasMany(Currency::class);
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }

    public function getFullOrderNumberAttribute()
    {
        return str_pad($this->current_order_number, 5, '0', STR_PAD_LEFT);
    }
}
