<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClientBalance extends Model
{
    use HasFactory;

    protected $fillable = [
        'client_id',
        'balance',
        'company_id',
        'created_by',
        'updated_by',
    ];

    public function activities()
    {
        return $this->hasMany(ClientBalanceActivity::class, 'client_id');
    }
}
