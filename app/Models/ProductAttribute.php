<?php

namespace App\Models;

use App\Models\Product;
use App\Models\AttributeType;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ProductAttribute extends Model
{
    use HasFactory;

    public function attributes()
    {
        return $this->belongsTo(AttributeType::class, 'attribute_id')->select(['id', 'name']);
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }
}
