<?php

namespace App\Models;

use Illuminate\Support\Str;
use App\Events\UserPasswordReset;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Facades\Cache;
use Spatie\Permission\Traits\HasRoles;
use App\Notifications\Auth\VerifyEmail;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * Send the email verification notification.
     *
     * @return void
     */
    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyEmail());
    }


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'first_name',
        'last_name',
        'username',
        'country_code',
        'phone',
        'user_role',
        'email',
        'password',
        'active',
        'force_pw_change',
        'company_id',
        'locale',
        'credit',
        // 'credit_limit_start_date',
        // 'credit_limit_end_date'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function theirRole()
    {
        return $this->belongsTo(Role::class, 'user_role')->select(['id', 'name']);
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function userPhoto()
    {
        return $this->hasOne(File::class, 'reference_id')
            ->where('reference_type', get_class($this))
            ->select(['id', 'reference_id', 'reference_type', 'name', 'force_download', 'file_path', 'created_at']);
    }

    public function OTP()
    {
        return Cache::get($this->OTPKey());
    }

    public function cacheTheOTP()
    {
        $OTP = rand(1000, 9999);
        Cache::put([$this->OTPKey() => $OTP], now()->addSeconds(10800)); // 3 hours
        return $OTP;
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }

    public function sendOTP()
    {
        $OTP = $this->cacheTheOTP();
        event(new UserPasswordReset($this, $OTP));
    }

    public function OTPKey()
    {
        return "OTP_for_{$this->id}";
    }
}
