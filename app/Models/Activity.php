<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    use HasFactory;

    protected $fillable = [
        'log_name',
        'causer_id',
        'external_id',
        'causer_type',
        'text',
        'source_type',
        'source_id',
        'company_id'
    ];
    protected $guarded = ['id'];
}
