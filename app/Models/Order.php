<?php

namespace App\Models;

use Illuminate\Support\Facades\Config;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Order extends Model
{
    use HasFactory;

    protected $fillable = [
        'order_number',
        'total',
        'payment_when',
        'payment_method',
        'type_of_wallet',
        'transaction_id',
        'amount_paid',
        'amount_remaining',
        'status',
        'requires_approval',
        'address_id',
        'client_id',
        'company_id',
        'created_by',
        'updated_by'
    ];

    protected $casts = [
        'created_at' => "datetime:d-m-y",
    ];

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'created_by')->select(['id', 'first_name', 'last_name']);
    }

    public function updater()
    {
        return $this->belongsTo(User::class, 'updated_by')->select(['id', 'first_name', 'last_name']);
    }

    public function client()
    {
        return $this->belongsTo(Client::class)->select(['id', 'first_name', 'last_name', 'company_name', 'mobile', 'email']);
    }

    public function address()
    {
        return $this->belongsTo(Address::class);
    }

    public function products()
    {
        return $this->hasMany(OrderProduct::class, 'order_id');
    }

    public function payments()
    {
        return $this->hasMany(Payment::class, 'order_id');
    }
}
