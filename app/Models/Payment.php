<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use HasFactory;

    protected $fillable = [
        'external_id',
        'payment_method',
        'type_of_wallet',
        'transaction_id',
        'amount_paid',
        'amount_remaining',
        'status',
        'reason_denied',
        'order_id',
        'company_id',
        'created_by',
        'updated_by'
    ];

    protected $casts = [
        'created_at' => "datetime:d-m-y",
    ];

    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'created_by');
    }
}
