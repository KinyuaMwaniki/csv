<?php

namespace App\Services;

use App\Models\Company;


class OrderNumberService
{
    private $company;

    public function __construct(Company $company)
    {
        $this->company = $company;
    }

    public function getNextOrderNumber()
    {
        $this->company->current_order_number++;
        $this->company->save();
        $orderNumber = "#".$this->company->full_order_number;
        return $orderNumber;
    }
}
