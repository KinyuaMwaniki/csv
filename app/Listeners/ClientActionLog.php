<?php

namespace App\Listeners;

use App\Models\Activity;
use Illuminate\Support\Str;
use App\Events\ClientLevelUpdated;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ClientActionLog
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ClientLevelUpdated  $event
     * @return void
     */
    public function handle(ClientLevelUpdated $event)
    {
        $client = $event->client;
        $user_id = $event->user_id;
        $new_level = $client->is_premium ? "premium" : "normal";

        switch ($event->action) {
            case 'updated_level':
                Activity::create([
                    'log_name' => 'client',
                    'external_id' => Str::uuid(),
                    'causer_id' => $user_id,
                    'causer_type' => 'App\Models\User',
                    'text' => "Client $client->first_name level updated to $new_level",
                    'source_type' => 'App\Models\Client',
                    'company_id' => $client->company->id,
                    'source_id' => $client->id,
                    'properties' => $event->action
                ]);
                break;

            default:
                break;
        }
    }
}
