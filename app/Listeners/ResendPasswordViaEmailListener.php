<?php

namespace App\Listeners;

use App\Events\UserPasswordReset;
use App\Mail\UserPasswordResetEmail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

// class ResendPasswordViaEmailListener implements ShouldQueue
class ResendPasswordViaEmailListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserPasswordReset  $event
     * @return void
     */
    public function handle(UserPasswordReset $event)
    {
        Mail::to($event->user->email)->send(new UserPasswordResetEmail($event->user, $event->OTP));
    }
}
