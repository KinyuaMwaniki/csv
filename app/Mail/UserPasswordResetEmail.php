<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Artisan;

class UserPasswordResetEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $OTP;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public function __construct(User $user, $OTP)
    {
        $this->user = $user;
        $this->OTP = $OTP;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.users.reset')
            ->with([
                'user' => $this->user,
                'OTP' => $this->OTP
            ]);
    }
}
