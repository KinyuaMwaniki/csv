<?php

namespace App\Console\Commands;

use App\Models\Role;
// use Spatie\Permission\Models\Role;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Artisan;
use Spatie\Permission\Models\Permission;

class PermissionsUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'permissions:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update the project permissions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Artisan::call('cache:forget spatie.permission.cache');

        $admin = Role::where('name', 'Administrator')->first();

        $permissions = Config::get('constants.permissions');

        $this->info('Updating permissions');
        foreach ($permissions as $single) {
            Permission::updateOrCreate(
                [
                    'name' => $single['name'],
                ],
                [
                    'name' => $single['name'],
                    'guard_name' => 'web',
                    'model' => $single['model']
                ],

            );
        }
        $this->info('Updated permissions');

        $permissions = Permission::all();

        if (isset($admin)) {
            $admin->givePermissionTo($permissions);
            $this->info("All permissions granted to Admin");
        } else {

            $this->info("Role with 'Administrator' name not found");
        }
    }
}
