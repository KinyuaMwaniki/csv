<?php

namespace App\Imports;

use Throwable;
use App\Models\Lead;
use App\Models\Master;
use App\Models\Country;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\WithBatchInserts;

class LeadsImport implements ToModel, WithHeadingRow, WithValidation, SkipsOnFailure, WithBatchInserts
{
    use Importable, SkipsFailures;
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        $country = Country::where('name', $row['country'])->first();
        $country_id = isset($country) ? $country->id : null;

        $lead_status = Master::where('type', 'lead_status')
            ->where('name', $row['lead_status'])
            ->first();
        $lead_status_name = isset($lead_status) ? $lead_status->name : null;


        $rating = Master::where('type', 'rating')
            ->where('name', $row['rating'])
            ->first();
        $rating_name = isset($rating) ? $rating->name : null;

        return new Lead([
            'title' => $row['salutation'],
            'first_name' => $row['first_name'],
            'last_name' => $row['last_name'],
            'phone' => $row['primary_phone'],
            'company' => $row['company'],
            'mobile' => $row['mobile_phone'],
            'designation' => $row['designation'],
            'fax' => $row['fax'],
            'source' => $row['lead_source'],
            'email' => $row['primary_email'],
            'industry' => $row['industry'],
            'website' => $row['website'],
            'annual_revenue' => $row['annual_revenue'],
            'status' => $lead_status_name,
            'no_of_employees' => $row['number_of_employees'],
            'rating' => $rating_name,
            'secondary_email' => $row['secondary_email'],
            'assigned_to' => 1,
            'created_by' => 1,
            'email_opt_out' => $row['email_opt_out'],
            'lane' => $row['street'],
            'pobox' => $row['po_box'],
            'code' => $row['postal_code'],
            'city' => $row['city'],
            'country' => $country_id,
            // 'state' => $row['state'],
            'description' => $row['description'],
        ]);
    }

    public function rules(): array
    {
        return [
            'last_name' => ['bail', 'required', 'string'],
        ];
    }

    public function batchSize(): int
    {
        return 500;
    }
}
