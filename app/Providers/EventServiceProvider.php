<?php

namespace App\Providers;

use App\Events\UserRegistered;
use App\Events\UserPasswordReset;
use App\Events\ClientLevelUpdated;
use App\Listeners\ClientActionLog;
use Illuminate\Support\Facades\Event;
use App\Listeners\SendActivationEmail;
use Illuminate\Auth\Events\Registered;
use App\Listeners\ResendPasswordViaEmailListener;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        UserRegistered::class => [
            SendActivationEmail::class,
        ],
        ClientLevelUpdated::class => [
            ClientActionLog::class
        ],
        UserPasswordReset::class => [
            ResendPasswordViaEmailListener::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
