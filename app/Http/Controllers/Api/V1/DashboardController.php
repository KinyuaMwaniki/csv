<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Order;
use App\Models\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index(Request $request)
    {
        $user = $request->user();
        $companyId = $user->company_id;

        $month = date('m');
        $year = date("Y");

        $clientsCountQuery = Client::where('company_id', $companyId);
        if ($user->theirRole->name === "Commercial") {
            $clientsCountQuery->where('created_by', $user->id);
        }
        $clientsCount = $clientsCountQuery->count();

        $clientsThisMonthQuery = Client::where('company_id', $companyId)
            ->whereYear('created_at', $year)
            ->whereMonth('created_at', $month);
        if ($user->theirRole->name === "Commercial") {
            $clientsCountQuery->where('created_by', $user->id);
        }
        $clientsThisMonth = $clientsThisMonthQuery->count();

        $totalDebtQuery = Order::where('company_id', $companyId)
            ->where('amount_remaining', '>', 0);
        if ($user->theirRole->name === "Commercial") {
            $totalDebtQuery->where('created_by', $user->id);
        }
        $totalDebt = $totalDebtQuery->sum('amount_remaining');

        $totalCashPaymentQuery = Order::where('company_id', $companyId)
            ->where('payment_method', 'cash');
        if ($user->theirRole->name === "Commercial") {
            $totalCashPaymentQuery->where('created_by', $user->id);
        }
        $totalCashPayment = $totalCashPaymentQuery->sum('amount_paid');

        $totalWalletPaymentQuery = Order::where('company_id', $companyId)
            ->where('payment_method', 'wallet');
        if ($user->theirRole->name === "Commercial") {
            $totalWalletPaymentQuery->where('created_by', $user->id);
        }
        $totalWalletPayment = $totalWalletPaymentQuery->sum('amount_paid');

        return response()->json([
            'clientsCount' => $clientsCount,
            'clientsThisMonth' => $clientsThisMonth,
            'month' => $month,
            'totalDebt' => (float) $totalDebt,
            'totalPayment' => $totalCashPayment + $totalWalletPayment,
        ], 200);
    }
}
