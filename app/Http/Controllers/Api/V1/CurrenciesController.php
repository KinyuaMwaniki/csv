<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateCurrencyRequest;
use App\Http\Requests\UpdateCurrencyRequest;
use App\Http\Resources\CurrencyResource;
use App\Models\Currency;
use Illuminate\Http\Request;

class CurrenciesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return response()->json([
            'currencies' => Currency::select('id', 'name', 'code')->where('company_id', $request->user()->company_id)->get()
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCurrencyRequest $request)
    {
        $currency = Currency::create(array_merge(
            $request->validated(),
            [
                'company_id' => $request->user()->company_id
            ]
        ));

        $currency = $currency->refresh();
        return response()->json(new CurrencyResource($currency), 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function showCompanyCurrency(Request $request)
    {
        $user = $request->user();
        $company = $user->company;
        return response()->json(new CurrencyResource($company->currency), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCurrencyRequest $request, Currency $currency)
    {
        $currency->update($request->validated());
        return response()->json(new CurrencyResource($currency));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Currency $currency)
    {
        $currency->delete();
        return response()->json(null, 204);
    }

    public function indexPaginated(Request $request)
    {
        $columns = ['name', 'code', 'symbol', 'status', 'actions'];

        $length = $request->length;
        $column = $request->column;
        $dir = $request->dir;
        $searchValue = $request->search;
        $draw = $request->draw;

        $query = Currency::select('id', 'name', 'code', 'symbol', 'status', 'company_id')
            ->where('company_id', $request->user()->company_id)
            ->orderBy($columns[$column], $dir);

        if ($searchValue) {
            $query->where(function ($query) use ($searchValue) {
                $query->where('name', 'like', '%' . $searchValue . '%')
                    ->orWhere('code', 'like', '%' . $searchValue . '%');
            });
        }

        $currencies = $query->paginate($length);

        return response()->json([
            'currencies' => $currencies,
            'draw' => $draw
        ], 200);
    }
}
