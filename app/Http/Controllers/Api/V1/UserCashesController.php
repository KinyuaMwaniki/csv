<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\File;
use App\Models\User;
use App\Models\UserCash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Storage;
use App\Http\Resources\UserCashResource;
use App\Http\Requests\CreateUserCashRequest;
use App\Http\Requests\UpdateUserCashRequest;

class UserCashesController extends Controller
{
     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateUserCashRequest $request)
    {
        $user = $request->user();
        DB::beginTransaction();
        try {
            $cash = UserCash::create(array_merge(
                $request->validated(),
                [
                    'company_id' => $user->company_id,
                    'user_id' => $user->id,
                    'created_by' => $user->id,
                    'updated_by' => $user->id,
                ]
            ));

            $fileNameWithExt = $request->file('uploaded_photo')->getClientOriginalName();
            $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('uploaded_photo')->getClientOriginalExtension();
            $fileNameToStore = $fileName . '_' . time() . '.' . $extension;
            $request->file('uploaded_photo')->storeAs('public/user_cash', $fileNameToStore);
            $file_path = 'user_cash/' . $fileNameToStore;

            File::create(
                [
                    'reference_type' => UserCash::class,
                    'reference_id' => $cash->id,
                    'original_name' => $fileNameWithExt,
                    'name' => $fileName,
                    'file_ext' => $extension,
                    'force_download' => true,
                    'file_path' => $file_path,
                    'status' => true,
                    'uploaded_by' => $user->id,
                    'updated_by' => $user->id,
                    'company_id' => $user->company_id,
                ]
            );
            DB::commit();
            return response()->json(new UserCashResource($cash), 201);
        } catch (\Exception $e) {
            DB::rollback();
            info('User cash controller error');
            info($e);
            return response()->json(['error' => 'Unable to save'], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserCashRequest $request, UserCash $userCash)
    {
        DB::beginTransaction();
        try {
            $userCash->update(
                array_merge(
                    $request->validated(),
                    [
                        'status' => 'Pending',
                        'updated_by' => $request->user()->id
                    ]
                )
            );

            if ($request->hasFile('uploaded_photo')) {
                if ($userCash->photo()->exists()) {
                    Storage::delete('//public//' . $userCash->photo->file_path);
                    $userCash->photo()->delete();
                }

                $fileNameWithExt = $request->file('uploaded_photo')->getClientOriginalName();
                $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
                $extension = $request->file('uploaded_photo')->getClientOriginalExtension();
                $fileNameToStore = $fileName . '_' . time() . '.' . $extension;
                $request->file('uploaded_photo')->storeAs('public/user_cash', $fileNameToStore);
                $file_path = 'user_cash/' . $fileNameToStore;

                File::create(
                    [
                        'reference_type' => UserCash::class,
                        'reference_id' => $userCash->id,
                        'original_name' => $fileNameWithExt,
                        'name' => $fileName,
                        'file_ext' => $extension,
                        'force_download' => true,
                        'file_path' => $file_path,
                        'status' => true,
                        'uploaded_by' => $request->user()->id,
                        'updated_by' => $request->user()->id,
                        'company_id' => $request->user()->company_id,
                    ]
                );
            }
            DB::commit();
            return response()->json(new UserCashResource($userCash), 200);
        } catch (\Exception $e) {
            DB::rollback();
            info('Usercash controller error');
            info($e);
            return response()->json(['error' => 'Unable to save'], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Usercash $userCash)
    {
        DB::beginTransaction();
        try {
            if ($userCash->photo()->exists()) {
                Storage::delete('//public//' . $userCash->photo->file_path);
                $userCash->photo()->delete();
            }
            $userCash->delete();

            DB::commit();
            return response()->json(null, 204);
        } catch (\Exception $e) {
            DB::rollback();
            info('User cash controller error');
            info($e);
            return response()->json(['error' => 'Unable to delete'], 500);
        }
    }

    public function indexPaginated(Request $request)
    {
        $user = $request->user();

        $columns = ['date', 'amount', 'actions', 'photo', 'status', 'reason_denied', 'actions'];

        $length = $request->length;
        $column = $request->column;
        $dir = $request->dir;
        $searchValue = $request->search;
        $draw = $request->draw;

        $query = UserCash::select('id', 'date', 'amount', 'approved', 'denied', 'reason_denied', 'status')->where('company_id', $request->user()->company_id)->with('photo')->orderBy($columns[$column], $dir);

        if ($searchValue) {
            $query->where(function ($query) use ($searchValue) {
                $query->where('date', 'like', '%' . $searchValue . '%')
                    ->orWhere('amount', 'like', '%' . $searchValue . '%');
            });
        }

        if ($user->theirRole->name === "Commercial") {
            $query->where('created_by', $request->user()->id);
        }

        $collections = $query->paginate($length);

        return response()->json([
            'collections' => $collections,
            'draw' => $draw
        ], 200);
    }

    public function approve(Request $request, UserCash $userCash)
    {
        if (!Gate::allows('approve-order')) {
            abort(403);
        }

        DB::beginTransaction();
        try {
            if ($userCash->approved === 0) {

                $userCash->update([
                    'approved' => 1,
                    'denied' => 0,
                    'reason_denied' => null,
                    'status' => 'Approved',
                    'updated_by' => $request->user()->id
                ]);

                $user = User::find($userCash->created_by);
                $user->update([
                    'credit' => $user->credit + $userCash->amount
                ]);

                DB::commit();
            }
            return response()->json(new UserCashResource($userCash), 200);
        } catch (\Exception $e) {
            DB::rollback();
            info('Approve usercash error');
            info($e);
            return response()->json(['error' => 'Unable to approve'], 500);
        }
    }

    public function deny(Request $request, UserCash $userCash)
    {
        if (!Gate::allows('approve-order')) {
            abort(403);
        }

        $userCash->update([
            'approved' => 0,
            'denied' => 1,
            'status' => 'Denied',
            'reason_denied' => $request->reason_denied,
            'updated_by' => $request->user()->id
        ]);

        return response()->json(new UserCashResource($userCash), 200);
    }
}
