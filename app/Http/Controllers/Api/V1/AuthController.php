<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Events\UserPasswordReset;
use App\Http\Controllers\Controller;
use App\Http\Requests\ChangePasswordOTPRequest;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\ChangePasswordRequest;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $rules = [
            'email' => ['required'],
            'password' => ['required'],
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $userdata = array(
            'email'     => $request->email,
            'password'  => $request->password
        );


        if (Auth::attempt($userdata)) {
            $user = Auth::user();
            $token = $user->createToken('crm-token', ['admin:view'])->plainTextToken;

            return response()->json([
                'token' => $token,
                'user' => new UserResource(Auth::user()),
            ], 201);
        }

        return response()->json([
            'message' => 'Invalid credentitals'
        ], 401);
    }

    public function getUserInfo(Request $request)
    {
        return response()->json([
            'user' => $request->user()
        ]);
    }

    public function logout(Request $request)
    {
        auth()->user()->currentAccessToken()->delete();;
        return response()->json('Logged out successfully', 200);
    }

    public function forgotPassword(Request $request)
    {
        $rules = [
            'email' => ['required', 'email'],
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $user = User::where('email', $request->email)->first();

        if (empty($user)) {
            return response()->json([
                'message' => "User Not found"
            ], 404);
        }

        $user->sendOTP();

        return response()->json([
            'message' => 'OTP sent, check your email',
        ], 200);
    }

    public function changePasswordWithOTP(ChangePasswordOTPRequest $request)
    {
        $user = User::where('email', $request->email)->first();

        if (empty($user)) {
            return response()->json([
                'message' => "User Not found"
            ], 404);
        }

        if ($request->otp != $user->OTP()) {
            return response()->json([
                'message' => "Wrong OTP"
            ], 401);
        }

        $user->update([
            'password' => Hash::make($request->password),
            'active' => 1,
            'force_pw_change' => 0
        ]);

        $userdata = array(
            'email'     => $request->email,
            'password'  => $request->password
        );

        if (Auth::attempt($userdata)) {
            Auth::user()->createToken('crm-token', ['admin:view'])->plainTextToken;

            return response()->json([
                'message' => "Password changed",
            ], 200);
        }
    }

    public function changePassword(ChangePasswordRequest $request)
    {
        $user = $request->user();

        $rules = [
            'password' => ['required', 'confirmed'],
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $user->update([
            'password' => Hash::make($request->password),
            'active' => 1,
            'force_pw_change' => 0
        ]);

        $token = $user->createToken('myapptoken')->plainTextToken;

        return response()->json([
            'token' => $token,
        ], 200);
    }
}
