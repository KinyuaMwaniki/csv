<?php

namespace App\Http\Controllers\Api\V1;

use DatePeriod;
use DateInterval;
use Carbon\Carbon;
use App\Models\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Artisan;

class ChartsController extends Controller
{
    public function getDailyCollection(Request $request)
    {
        $from = $request->from;
        $to = $request->to;

        $start = Carbon::parse($from);
        $to = Carbon::parse($to)->copy()->endOfDay();

        $days = $this->getDaysListBetweenTwoDays($start, $to);

        $dailyCollectionsQuery = Payment::groupBy('date')
            ->orderBy('date', 'DESC')
            ->whereBetween(DB::raw('date(created_at)'), [$start, $to]);

        $user = $request->user();

        if ($user->theirRole->name === "Commercial") {
            $dailyCollectionsQuery->where('created_by', $user->id);
        }

        $dailyCollections = $dailyCollectionsQuery->get([
            DB::raw('Date(created_at) as date'),
            DB::raw('SUM(amount_paid) as total'),
        ]);

        $collection = collect($days);

        $updatedDailyCollections = $collection->map(function ($item) use ($dailyCollections) {
            $dailyRecord = $dailyCollections->firstWhere('date', $item);
            return [
                'total' => $dailyRecord->total ?? "0.00",
                'date' => $item,
            ];
        });

        return response()->json([
            'dailyCollections' => $updatedDailyCollections->reverse()->values()
        ], 200);
    }

    public function getMonthlyCollection(Request $request)
    {
        $from = $request->from;
        $to = $request->to;

        $start = Carbon::parse($from);
        $end = Carbon::parse($to);

        $startOfMonth = $start->copy()->startOfMonth();
        $endOfMonth = $end->copy()->endOfMonth();

        $months = $this->getMonthListFromDate($startOfMonth, $endOfMonth);

        $monthlyCollectionsQuery = Payment::select(
            DB::raw('sum(amount_paid) as total'),
            DB::raw("DATE_FORMAT(created_at,'%M %Y') as months"),
            DB::raw("DATE_FORMAT(created_at,'%m') as monthKey"),
            DB::raw('max(created_at) as createdAt')

        )
            ->orderBy('createdAt', 'desc')
            ->whereBetween(DB::raw('date(created_at)'), [$start, $end])
            ->groupBy('months', 'monthKey');


        $user = $request->user();

        if ($user->theirRole->name === "Commercial") {
            $monthlyCollectionsQuery->where('created_by', $user->id);
        }

        $monthlyCollections = $monthlyCollectionsQuery->get();

        $collection = collect($months);

        $updatedMonthlyCollections = $collection->map(function ($item, $key) use ($monthlyCollections) {
            $monthlyRecord = $monthlyCollections->firstWhere('months', $item);
            return [
                'total' => $monthlyRecord->total ?? "0.00",
                'months' => $item,
                'monthKey' => Carbon::createFromFormat("F Y", $item)->format('m')
            ];
        });

        return response()->json([
            'monthlyCollections' => $updatedMonthlyCollections,
        ], 200);
    }

    private function getMonthListFromDate($startOfMonth, $endOfMonth)
    {
        $interval = DateInterval::createFromDateString('1 month');
        $period   = new DatePeriod($startOfMonth, $interval, $endOfMonth);

        $months = array();

        foreach ($period as $dt) {
            $months[] = $dt->format("F Y");
        }

        return $months;
    }

    private function getDaysListBetweenTwoDays($start, $to)
    {
        $daterange =  new DatePeriod(
            $start,
            new DateInterval('P1D'),
            $to
        );

        foreach ($daterange as $date) {
            $dates[] = $date->format("Y-m-d");
        }
        return $dates;
    }
}
