<?php

namespace App\Http\Controllers\Api\V1;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Order;
use App\Models\Product;
use Illuminate\Support\Str;
use App\Models\OrderProduct;
use Illuminate\Http\Request;
use App\Exports\OrdersExport;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use App\Http\Controllers\Controller;
use App\Services\OrderNumberService;
use Illuminate\Support\Facades\Gate;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Resources\OrderResource;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\CreateOrderRequest;
use App\Http\Requests\UpdateOrderRequest;
use Illuminate\Validation\ValidationException;
use App\Http\Requests\UpdateOrderStatusRequest;
use App\Http\Requests\UpdateOrderPaymentRequest;

class OrdersController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateOrderRequest $request)
    {
        Artisan::call('log:clear');
        info($request->all());

        $amount_remaining = $request->when === 'later' ? $request->total : $request->amount_remaining;
        $user = $request->user();

        if ($user->theirRole->name === "Commercial" && $amount_remaining > $user->credit) {
            return response()->json([
                'message' => 'The remaining amount is greater than your available credit limit'
            ], 400);
        }

        DB::beginTransaction();
        try {
            $order = Order::create(array_merge(
                $request->validated(),
                [
                    'order_number' => app(OrderNumberService::class, ['company' => $user->company])->getNextOrderNumber(),
                    'payment_method' => $request->payment_when === 'now' ? $request->payment_method : NULL,
                    'type_of_wallet' => $request->payment_method === 'wallet' && $request->payment_when === 'now' ? $request->type_of_wallet : NULL,
                    'amount_paid' => $request->amount_paid,
                    'amount_remaining' => $amount_remaining,
                    'created_by' => $user->id,
                    'updated_by' => $user->id,
                    'company_id' => $user->company_id,
                ]
            ));

            if ($request->payment_when !== 'later') {
                $payment = $order->payments()->create([
                    'external_id' => Str::uuid()->toString(),
                    'payment_method' => $request->payment_when === 'now' ? $request->payment_method : NULL,
                    'type_of_wallet' => $request->payment_method === 'wallet' && $request->payment_when === 'now' ? $request->type_of_wallet : NULL,
                    'transaction_id' => $request->transaction_id,
                    'amount_paid' => $request->amount_paid,
                    'amount_remaining' => $request->when === 'later' ? $request->total : $request->amount_remaining,
                    'company_id' => $user->company_id,
                    'created_by' => $user->id,
                    'updated_by' => $user->id
                ]);

                if ($payment->payment_method === "wallet" && $user->theirRole->name === "Commercial") {
                    $payment->update([
                        'status' => 'Pending',
                    ]);

                    // $user->credit = $user->credit - $order->amount_paid;
                    // $user->save();

                    $order->requires_approval = 1;
                    $order->save();
                }


                if ($payment->payment_method === "wallet" && $user->theirRole->name === "Administrator") {
                    $payment->update([
                        'status' => 'Approved',
                    ]);
                }


                if ($payment->payment_method === "cash") {
                    $payment->update([
                        'status' => 'Cash Paid',
                    ]);
                }
            }

            if ($user->theirRole->name === "Commercial") {
                $user->credit = $user->credit - $order->total;
                $user->save();
            }

            foreach ($request->cart as $single) {
                $product = Product::find($single['id']);
                OrderProduct::create([
                    'order_id' => $order->id,
                    'product_id' => $single['id'],
                    'quantity' => $single['amountInCart'],
                    'price' => $product->price,
                    'total' => $product->price * $single['amountInCart'],
                    'attributes' => $single['selectedAttributes'],
                    'company_id' => $user->company_id,
                    'created_by' => $user->id,
                    'updated_by' => $user->id
                ]);
                
                $product->update([
                    'quantity' => $product->quantity - $single['amountInCart']
                ]);
            }

            info((float) $order->total);
            info((float) $order->products->sum("total"));

            if ((float) $order->total !== (float) $order->products->sum("total")) {
                return response()->json([
                    'error' => 'The total order amount is not equal to the sum of cart item totals'
                ], 422);
            }

            DB::commit();
            return response()->json(new OrderResource($order), 201);
        } catch (\Exception $e) {
            DB::rollback();
            info('Order controller error');
            info($e);
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        $this->authorize('view', $order);
        return response()->json(new OrderResource($order), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateOrderRequest $request, Order $order)
    {

        Artisan::call('log:clear');
        info($request->all());

        if ($order->status !== 'processing') {
            return response()->json(['error' => 'You cannot only update an order with processing status'], 403);
        }

        $user = $request->user();

        if ($user->theirRole->name === "Commercial" && $request->amount_remaining > $user->credit) {
            return response()->json([
                'message' => 'The remaining amount is greater than your available credit limit'
            ], 400);
        }

        DB::beginTransaction();
        try {
            $order->update(array_merge(
                $request->validated(),
                [
                    'payment_method' => $request->payment_method,
                    'transaction_id' => $request->transaction_id,
                    'amount_paid' => $request->amount_paid + $order->amount_paid,
                    'amount_remaining' => $request->total - ($request->amount_paid + $order->amount_paid),
                    'type_of_wallet' => $request->payment_method === 'cash' ? NULL : $request->type_of_wallet,
                    'updated_by' => $user->id,
                    'company_id' => $user->company_id,
                ]
            ));

            $orderProductIds = $order->products()->pluck('id')->toArray();
            $idsInCart = $this->searchForId($request->cart);

            $payment = $order->payments()->create([
                'external_id' => Str::uuid()->toString(),
                'payment_method' => $request->payment_method,
                'type_of_wallet' => $request->payment_method === 'cash' ? NULL : $request->type_of_wallet,
                'transaction_id' => $request->transaction_id,
                'amount_paid' => $request->amount_paid,
                'amount_remaining' => $request->amount_remaining,
                'company_id' => $user->company_id,
                'created_by' => $user->id,
                'updated_by' => $user->id
            ]);

            if ($payment->payment_method === "wallet" && $user->theirRole->name === "Commercial") {
                $payment->update([
                    'status' => 'pending',
                ]);

                // $user->credit = $user->credit - $request->amount_paid;
                // $user->save();
            }

            if ($payment->payment_method === "wallet" && $user->theirRole->name === "Administrator") {
                $payment->update([
                    'status' => 'Approved',
                ]);
            }

            if ($payment->payment_method === "cash") {
                $payment->update([
                    'status' => 'Cash Paid',
                ]);
            }

            // Remove order products that have been removed from the order
            foreach ($orderProductIds as $single) {
                if (!in_array($single, $idsInCart)) {
                    OrderProduct::find($single)->delete();
                }
            }

            foreach ($request->cart as $single) {
                $orderProduct = OrderProduct::find($single['id']);
                $product = Product::find($single['product_id']);
                if ($single['id'] === -1 && empty($orderProduct)) {
                    OrderProduct::create([
                        'order_id' => $order->id,
                        'product_id' => $single['product_id'],
                        'quantity' => $single['quantity'],
                        'price' => $product->price,
                        'total' => $product->price * $single['quantity'],
                        'attributes' => $single['selectedAttributes'],
                        'company_id' => $user->company_id,
                        'created_by' => $user->id,
                        'updated_by' => $user->id
                    ]);
                } else {
                    $orderProduct->update([
                        'total' => $product->price * $single['quantity'],
                        'quantity' => $single['quantity'],
                        'updated_by' => $user->id
                    ]);
                }
            }

            info((float) $order->total);
            info((float) $order->products->sum("total"));

            if ((float) $order->total !== (float) $order->products->sum("total")) {
                return response()->json([
                    'error' => 'The total order amount is not equal to the sum of cart item totals'
                ], 422);
            }

            DB::commit();
            return response()->json(new OrderResource($order), 200);
        } catch (\Exception $e) {
            DB::rollback();
            info('Order controller error');
            info($e);
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    public function updatePayment(UpdateOrderPaymentRequest $request, Order $order)
    {
        DB::beginTransaction();
        try {
            $user = $request->user();

            $order->update([
                'payment_method' => $request->payment_method,
                'type_of_wallet' => $request->payment_method === 'wallet' ? $request->type_of_wallet : NULL,
                'transaction_id' => $request->transaction_id,
                'amount_paid' => $request->amount_paid + $order->amount_paid,
                'amount_remaining' => $order->amount_remaining - $request->amount_paid,
                'updated_by' => $user->id
            ]);

            $payment = $order->payments()->create([
                'external_id' => Str::uuid()->toString(),
                'payment_method' => $request->payment_method,
                'type_of_wallet' => $request->payment_method === 'wallet' ? $request->type_of_wallet : NULL,
                'transaction_id' => $request->transaction_id,
                'amount_paid' => $request->amount_paid,
                'amount_remaining' => $request->amount_remaining,
                'company_id' => $user->company_id,
                'created_by' => $user->id,
                'updated_by' => $user->id
            ]);

            if ($payment->payment_method === "wallet" && $user->theirRole->name === "Commercial") {
                $payment->update([
                    'status' => 'pending',
                ]);

                // $user->credit = $user->credit - $request->amount_paid;
                // $user->save();
            }

            if ($payment->payment_method === "wallet" && $user->theirRole->name === "Administrator") {
                $payment->update([
                    'status' => 'Approved',
                ]);
            }

            if ($payment->payment_method === "cash") {
                $payment->update([
                    'status' => 'Cash Paid',
                ]);
            }

            DB::commit();
            $order = $order->refresh();
            return response()->json(new OrderResource($order), 200);
        } catch (\Exception $e) {
            DB::rollback();
            info('Order controller error');
            info($e);
            return response()->json(['error' => 'Unable to save'], 500);
        }
    }

    public function downloadInvoice(Request $request)
    {
        App::setlocale($request->locale);

        $order = Order::where('id', $request->id)
            ->with(['products.product', 'products.product.currency', 'client', 'address'])
            ->firstOrFail();
        return view('downloads.invoice')->with('order', $order);
    }

    public function getPdf(Request $request)
    {
        $order = Order::where('id', $request->id)
            ->with(['products.product', 'products.product.currency', 'client', 'address'])
            ->firstOrFail();
        $data = [
            'order' => $order,
        ];
        $fileNameToStore = 'invoice' . '_' . time() . '.pdf';
        $pdf = app('dompdf.wrapper');
        $pdf->loadView('downloads.pdf', $data);
        $content = $pdf->output();
        Storage::put("/public/invoices/$fileNameToStore", $content);
        return response()->json([
            'url' => "/storage/invoices/$fileNameToStore",
        ], 200);
    }

    public function allPaginated(Request $request)
    {        
        $columns = $request->tableColumns;

        $length = $request->length;
        $column = $request->column;
        $dir = $request->dir;
        $searchValue = $request->search;
        $searchField = $request->field;
        $relationship = $request->relationship;
        $relationship_field = $request->relationship_field;
        $draw = $request->draw;
        $pending = $request->pending;

        $query = Order::select('id', 'created_at', 'total', 'amount_paid', 'order_number', 'amount_remaining', 'status', 'client_id')
            ->where('company_id', $request->user()->company_id)
            ->with(['client', 'products'])
            ->orderBy($columns[$column], $dir);

        if ($pending === 1) {
            $query->where('requires_approval', 1);
        }

        $user = $request->user();

        if ($user->theirRole->name === "Commercial") {
            $query->where('created_by', $user->id);
        }

        if ($searchValue && $searchField) {
            if ($relationship) {
                if ($searchField === 'client') {
                    $query->whereHas($searchField, function ($query) use ($relationship_field, $searchValue) {
                        $query->whereRaw(
                            "concat(first_name, ' ', last_name) like '%" . $searchValue . "%' "
                        );
                    });
                } else {
                    $query->whereHas($searchField, function ($query) use ($relationship_field, $searchValue) {
                        $query->where($relationship_field, 'like', '%' . $searchValue . '%');
                    });
                }
            } else {
                $query->where(function ($query) use ($searchField, $searchValue) {
                    $query->where($searchField, 'like', '%' . $searchValue . '%');
                });
            }
        }

        $orders = $query->paginate($length);

        return response()->json([
            'orders' => $orders,
            'draw' => $draw
        ], 200);
    }

    public function debtsPaginated(Request $request)
    {
        $columns = $request->tableColumns;
        $length = $request->length;
        $column = $request->column;
        $dir = $request->dir;
        $searchValue = $request->search;
        $searchField = $request->field;
        $relationship = $request->relationship;
        $relationship_field = $request->relationship_field;
        $draw = $request->draw;

        $query = Order::select('id', 'created_at', 'order_number', 'total', 'amount_paid', 'amount_remaining', 'client_id', 'status')
            ->where('company_id', $request->user()->company_id)
            ->where('amount_remaining', '>', 0)
            ->with(['client'])
            ->orderBy($columns[$column], $dir);

        $user = $request->user();

        if ($user->theirRole->name === "Commercial") {
            $query->where('created_by', $user->id);
        }

        if ($searchValue && $searchField) {
            if ($relationship) {
                if ($searchField === 'client') {
                    $query->whereHas($searchField, function ($query) use ($relationship_field, $searchValue) {
                        $query->whereRaw(
                            "concat(first_name, ' ', last_name) like '%" . $searchValue . "%' "
                        );
                    });
                } else {
                    $query->whereHas($searchField, function ($query) use ($relationship_field, $searchValue) {
                        $query->where($relationship_field, 'like', '%' . $searchValue . '%');
                    });
                }
            } else {
                $query->where(function ($query) use ($searchField, $searchValue) {
                    $query->where($searchField, 'like', '%' . $searchValue . '%');
                });
            }
        }

        $orders = $query->paginate($length);

        return response()->json([
            'orders' => $orders,
            'draw' => $draw
        ], 200);
    }

    public function indexPaginated(Request $request)
    {
        $columns = $request->tableColumns;
        $length = $request->length;
        $column = $request->column;
        $dir = $request->dir;
        $searchValue = $request->search;
        $draw = $request->draw;
        $id = $request->id;
        $isDebt = $request->isDebt;

        $query = Order::select('id', 'created_at', 'total', 'amount_paid', 'amount_remaining', 'client_id', 'status', 'order_number')
            ->with(['client'])
            ->where('company_id', $request->user()->company_id)
            ->orderBy($columns[$column], $dir);


        $user = $request->user();

        if ($user->theirRole->name === "Commercial") {
            $query->where('created_by', $user->id);
        }

        if ($id) {
            $query->where('client_id', $id);
        }

        if ($isDebt) {
            $query->where('amount_remaining', '>', 0);
        }

        if ($searchValue) {
            $query->where(function ($query) use ($searchValue) {
                $query->where('id', 'like', '%' . $searchValue . '%')
                    ->orWhere('order_number', 'like', '%' . $searchValue . '%')
                    ->orWhere('total', 'like', '%' . $searchValue . '%')
                    ->orWhere('amount_paid', 'like', '%' . $searchValue . '%')
                    ->orWhere('amount_remaining', 'like', '%' . $searchValue . '%');
            });
        }

        $orders = $query->paginate($length);

        return response()->json([
            'orders' => $orders,
            'draw' => $draw
        ], 200);
    }

    public function updateStatus(UpdateOrderStatusRequest $request, Order $order)
    {
        $order->update([
            'status' => $request->status,
            'updated_by' => $request->user()->id
        ]);
        return response()->json(new OrderResource($order), 200);
    }

    public function approve(Request $request, Order $order)
    {
        if (!Gate::allows('approve-order')) {
            abort(403);
        }

        DB::beginTransaction();
        try {
            $order->update([
                'requires_approval' => 0,
                'updated_by' => $request->user()->id
            ]);

            $user = User::find($order->created_by);
            $user->update([
                'credit' => $user->credit + $order->total
            ]);

            DB::commit();
            return response()->json(new OrderResource($order), 200);
        } catch (\Exception $e) {
            DB::rollback();
            info('Approve order error');
            info($e);
            return response()->json(['error' => 'Unable to approve'], 500);
        }
    }

    function searchForId($array)
    {
        $idsInCart = [];
        foreach ($array as $key => $val) {
            foreach ($val as $keyIn => $valIn) {
                if ($keyIn === 'id') {
                    array_push($idsInCart, $valIn);
                }
            }
        }
        return $idsInCart;
    }

    public function salesReport(Request $request)
    {
        $created_at = $request->created_at;
        $order_number = $request->order_number;
        $name = $request->name;
        $total = $request->total;
        $amount_remaining = $request->amount_remaining;
        $amount_paid = $request->amount_paid;
        $to = $request->to;
        $from = $request->from;
        $onlyDebts = $request->onlyDebts;

        $columns = $request->tableColumns;
        $length = $request->length;
        $column = $request->column;
        $dir = $request->dir;
        $draw = $request->draw;

        $query = Order::select('id', 'order_number', 'created_at', 'total', 'amount_paid', 'amount_remaining', 'client_id', 'status')
            ->where('company_id', $request->user()->company_id)
            ->with(['client'])
            ->orderBy($columns[$column], $dir);

        $user = $request->user();

        if ($user->theirRole->name === "Commercial") {
            $query->where('created_by', $user->id);
        }

        if ($onlyDebts === 1) {
            $query->where('amount_remaining', '>', 0);
        }

        if ($created_at) {
            $query->whereDate('created_at', '=', Carbon::parse($created_at));
        }

        if ($order_number) {
            $query->where('order_number', 'like', '%' . $order_number . '%');
        }

        if ($name) {
            $query->whereHas('client', function ($query) use ($name) {
                $query->where('first_name', $name)
                    ->orWhere('last_name', $name)
                    ->orWhereRaw(
                        "concat(first_name, ' ', last_name) like '%" . $name . "%' "
                    );
            });
        }

        if ($total) {
            $query->where('total', 'like', '%' . $total . '%');
        }

        if ($amount_remaining) {
            $query->where('amount_remaining', 'like', '%' . $amount_remaining . '%');
        }

        if ($amount_paid) {
            $query->where('amount_paid', 'like', '%' . $amount_paid . '%');
        }

        if ($to && $from) {
            $query->whereBetween('created_at', [Carbon::parse($from)->toDateString(), Carbon::parse($to)->copy()->endOfDay()]);
        }

        $orders = $query->paginate($length);

        return response()->json([
            'orders' => $orders,
            'draw' => $draw
        ], 200);
    }

    public function export(Request $request)
    {
        $locale = $request->user()->locale;
        App::setlocale($locale);

        $file_name = 'orders' . time() . '.xlsx';
        Excel::store(new OrdersExport($request->user(), $request->all()), $file_name, 'excel_downloads');
        $url = Storage::disk('excel_downloads')->url($file_name);
        return response()->json([
            'url' => $url,
        ], 200);
    }
}
