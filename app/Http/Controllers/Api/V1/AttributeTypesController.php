<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Models\AttributeType;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Resources\AttributeTypeResource;
use App\Http\Requests\CreateAttributeTypeRequest;
use App\Http\Requests\UpdateAttributeTypeRequest;

class AttributeTypesController extends Controller
{

    public function index(Request $request)
    {
        $attribute_types = AttributeType::where('company_id', $request->user()->company_id)->get();
        return response()->json([
            'attribute_types' => AttributeTypeResource::collection($attribute_types)
        ], 200);
    }

    public function store(CreateAttributeTypeRequest $request)
    {
        DB::beginTransaction();
        try {
            $attribute_type = AttributeType::create(array_merge(
                $request->validated(),
                [
                    'company_id' => $request->user()->company_id,
                ]
            ));

            if (count($request->units) > 0) {
                foreach ($request->units as $unit) {
                    $attribute_type->units()->create([
                        'name' => $unit,
                        'company_id' => $request->user()->company_id
                    ]);
                }
            }
            
            DB::commit();
            return response()->json(new AttributeTypeResource($attribute_type), 201);
        } catch (\Exception $e) {
            DB::rollback();
            info('Attribute type controller error');
            info($e);
            return response()->json(['error' => 'Unable to save'], 500);
        }
    }


    public function show($id)
    {
        //
    }

    public function update(UpdateAttributeTypeRequest $request, AttributeType $attribute_type)
    {
        DB::beginTransaction();
        try {
            $attribute_type->update(
                $request->validated(),
            );

            if ($attribute_type->units()->exists()) {
                $attribute_type->units()->delete();
            }

            if (count($request->units) > 0) {
                foreach ($request->units as $unit) {
                    $attribute_type->units()->create([
                        'name' => $unit,
                        'company_id' => $request->user()->company_id
                    ]);
                }
            }
            DB::commit();
            return response()->json(new AttributeTypeResource($attribute_type), 200);
        } catch (\Exception $e) {
            DB::rollback();
            info('Attribute type controller error');
            info($e);
            return response()->json(['error' => 'Unable to save'], 500);
        }
    }


    public function destroy(AttributeType $attribute_type)
    {
        $attribute_type->delete();
        return response()->json(null, 204);
    }

    public function indexPaginated(Request $request)
    {
        $columns = ['name', 'units', 'actions'];

        $length = $request->length;
        $column = $request->column;
        $dir = $request->dir;
        $searchValue = $request->search;
        $draw = $request->draw;

        $query = AttributeType::select('id', 'name')->where('company_id', $request->user()->company_id)->with('units')->orderBy($columns[$column], $dir);

        if ($searchValue) {
            $query->where(function ($query) use ($searchValue) {
                $query->where('name', 'like', '%' . $searchValue . '%');
            });
        }

        $attribute_types = $query->paginate($length);

        return response()->json([
            'attribute_types' => $attribute_types,
            'draw' => $draw
        ], 200);
    }
}
