<?php

namespace App\Http\Controllers\Api\V1;

use Attribute;
use App\Models\File;
use App\Models\Unit;
use App\Models\Product;
use App\Models\Category;
use PhpParser\JsonDecoder;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Resources\ProductResource;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Storage;
use SebastianBergmann\Environment\Console;
use App\Http\Requests\CreateProductRequest;
use App\Http\Requests\UpdateProductRequest;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateProductRequest $request)
    {
        DB::beginTransaction();
        try {
            $user = $request->user();
            $product = Product::create(array_merge(
                $request->except('categories'),
                [
                    'created_by' => $user->id,
                    'updated_by' => $user->id,
                    'company_id' => $request->user()->company_id
                ]
            ));

            if ($request->categories) { // Save product categories
                foreach ($request->categories as $category) {
                    $product->categories()->attach(
                        $category,
                        [
                            'company_id' => $request->user()->company_id,
                        ]
                    );
                }
            }

            if ($request['attributes']) { // Save product attributes
                foreach ($request['attributes'] as $single) {
                    $attribute = json_decode($single);
                    $product->attributes()->attach(
                        $attribute->id,
                        [
                            'company_id' => $request->user()->company_id,
                            'value' => $attribute->value,
                            'unit_id' => isset($attribute->unit) && $attribute->unit !== "" ? $attribute->unit : null,
                            'unit_name' => isset($attribute->unit) && $attribute->unit !== "" ? Unit::find($attribute->unit)->name : null,
                        ]
                    );
                }
            }

            if ($request['images']) { // Save product images
                foreach ($request['images'] as $image) {
                    $fileNameWithExt = $image->getClientOriginalName();
                    $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
                    $extension = $image->getClientOriginalExtension();
                    $fileNameToStore = $fileName . '_' . time() . '.' . $extension;
                    $image->storeAs('public/product_images', $fileNameToStore);
                    $file_path = 'product_images/' . $fileNameToStore;

                    File::create(
                        [
                            'reference_type' => Product::class,
                            'reference_id' => $product->id,
                            'original_name' => $fileNameWithExt,
                            'name' => $fileName,
                            'file_ext' => $extension,
                            'force_download' => false,
                            'file_path' => $file_path,
                            'status' => true,
                            'uploaded_by' => $request->user()->id,
                            'updated_by' => $request->user()->id,
                            'company_id' => $request->user()->company_id,
                        ]
                    );
                }
            }


            DB::commit();
            return response()->json(new ProductResource($product), 201);
        } catch (\Exception $e) {
            DB::rollback();
            info('Product Controller error');
            info($e);
            return response()->json(['error' => 'Unable to save'], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        $this->authorize('view', $product);
        return response()->json(new ProductResource($product), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProductRequest $request, Product $product)
    {
        DB::beginTransaction();
        try {
            $user = $request->user();
            $product->update(
                array_merge(
                    $request->except(['categories', 'attributes', 'current_images', 'images']),
                    [
                        'updated_by' => $request->user()->id,
                    ]
                )
            );

            if ($request->categories) { // Update product categories
                if ($product->categories()->exists()) {
                    $product->categories()->detach();
                }
                foreach ($request->categories as $category) {
                    $product->categories()->attach(
                        $category,
                        [
                            'company_id' => $user->company_id,
                        ]
                    );
                }
            }

            if ($request['attributes']) { // Update product attributes
                if ($product->attributes()->exists()) {
                    $product->attributes()->detach();
                }
                foreach ($request['attributes'] as $single) {
                    $attribute = json_decode($single);
                    $product->attributes()->attach(
                        $attribute->id,
                        [
                            'company_id' => $request->user()->company_id,
                            'value' => $attribute->value,
                            'unit_id' => isset($attribute->unit) && $attribute->unit !== "" ? $attribute->unit : null,
                            'unit_name' => isset($attribute->unit) && $attribute->unit !== "" ? Unit::find($attribute->unit)->name : null,
                        ]
                    );
                }
            }

            if ($request['current_images']) { // Delete any removed image
                $current_images = $request['current_images'];

                foreach ($product->photos as $photo) {
                    if (!in_array($photo->id, $current_images)) {
                        Storage::delete('//public//' . $photo->file_path);
                        $photo->delete();
                    }
                }
            } else {
                if ($product->photos()->exists()) { // If all current images have been removed, delete from db and storage
                    foreach ($product->photos as $photo) {
                        Storage::delete('//public//' . $photo->file_path);
                        $photo->delete();
                    }
                }
            }

            if ($request['images']) { // Save product images
                foreach ($request['images'] as $image) {
                    $fileNameWithExt = $image->getClientOriginalName();
                    $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
                    $extension = $image->getClientOriginalExtension();
                    $fileNameToStore = $fileName . '_' . time() . '.' . $extension;
                    $image->storeAs('public/product_images', $fileNameToStore);
                    $file_path = 'product_images/' . $fileNameToStore;

                    File::create(
                        [
                            'reference_type' => Product::class,
                            'reference_id' => $product->id,
                            'original_name' => $fileNameWithExt,
                            'name' => $fileName,
                            'file_ext' => $extension,
                            'force_download' => false,
                            'file_path' => $file_path,
                            'status' => true,
                            'uploaded_by' => $request->user()->id,
                            'updated_by' => $request->user()->id,
                            'company_id' => $request->user()->company_id,
                        ]
                    );
                }
            }

            DB::commit();
            $product = $product->refresh();
            return response()->json(new ProductResource($product), 200);
        } catch (\Exception $e) {
            DB::rollback();
            info('Product Controller error');
            info($e);
            return response()->json(['error' => 'Unable to update'], 500);
        }
    }

    public function updateStatus(Request $request, Product $product)
    {
        $user = $request->user();
        $product->update(
            [
                'status' => $request->status,
                'updated_by' => $user->id
            ]
        );
        return response()->json(new ProductResource($product), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        DB::beginTransaction();
        try {
            if ($product->photos()->exists()) {
                foreach ($product->photos as $photo) {
                    Storage::delete('//public//' . $photo->file_path);
                    $photo->delete();
                }
            }
            $product->delete();

            DB::commit();
            return response()->json(null, 204);
        } catch (\Exception $e) {
            DB::rollback();
            info('Product controller error');
            info($e);
            return response()->json(['error' => 'Unable to delete'], 500);
        }
    }

    public function indexPaginated(Request $request)
    {
        $columns = ['id', 'name', 'model', 'price', 'quantity', 'status', 'actions'];


        $length = $request->length;
        $column = $request->column;
        $dir = $request->dir;
        $searchValue = $request->search;
        $draw = $request->draw;

        $query = Product::select('id', 'name', 'description', 'model', 'price', 'quantity', 'manufacturer_id', 'status', 'currency_id')
            ->with(['brand', 'photos', 'currency'])
            ->where('company_id', $request->user()->company_id)
            ->orderBy($columns[$column], $dir);

        if ($searchValue) {
            $query->where(function ($query) use ($searchValue) {
                $query->where('name', 'like', '%' . $searchValue . '%')
                    ->orWhere('model', 'like', '%' . $searchValue . '%')
                    ->orWhere('price', 'like', '%' . $searchValue . '%')
                    ->orWhere('quantity', 'like', '%' . $searchValue . '%');
            });
        }

        $products = $query->paginate($length);

        return response()->json([
            'products' => $products,
            'draw' => $draw
        ], 200);
    }

    public function catalogPaginated(Request $request)
    {
        $columns = ['name', 'model', 'price', 'quantity', 'status', 'actions'];

        $length = $request->length;
        $column = $request->column;
        $dir = $request->dir;
        $searchValue = $request->search;
        $draw = $request->draw;
        $categoryId = $request->categoryId;



        $query = Product::select('id', 'name', 'model', 'price', 'quantity', 'manufacturer_id', 'status', 'currency_id')
            ->with(['brand', 'photos', 'currency', 'categories', 'attributes'])
            ->where('company_id', $request->user()->company_id)
            ->orderBy($columns[$column], $dir);

        if (isset($categoryId)) {
            $category = Category::find($categoryId);
            $categoryIds = $category->getDescendants($category);

            $query = $query->whereHas('categories', function ($query) use ($categoryIds) {
                $query->whereIn("categories.id", $categoryIds);
            });
        }

        if ($searchValue) {
            $query->where(function ($query) use ($searchValue) {
                $query->where('name', 'like', '%' . $searchValue . '%')
                    ->orWhere('model', 'like', '%' . $searchValue . '%')
                    ->orWhere('price', 'like', '%' . $searchValue . '%')
                    ->orWhere('quantity', 'like', '%' . $searchValue . '%');
            });
        }

        $products = $query->paginate($length);

        return response()->json([
            'products' => $products,
            'draw' => $draw
        ], 200);
    }
}
