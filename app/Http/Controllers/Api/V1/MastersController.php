<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Master;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\MasterResource;
use App\Http\Requests\CreateMasterRequest;
use App\Http\Requests\UpdateMasterRequest;

class MastersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateMasterRequest $request)
    {
        $user = $request->user();

        $master = Master::create(array_merge(
            $request->validated(),
            [
                'created_by' => $user->id,
                'updated_by' => $user->id,
                'company_id' => $user->company_id
            ]
        ));
        return response()->json(new MasterResource($master), 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Master $master)
    {
        return response()->json(new MasterResource($master), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateMasterRequest $request, Master $master)
    {
        $user = $request->user();

        $master->update(array_merge(
            $request->validated(),
            [
                'updated_by' => $user->id
            ]
        ));
        return response()->json(new MasterResource($master));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Master $master)
    {
        $master->delete();
        return response()->json(null, 204);
    }

    public function indexPaginated(Request $request)
    {
        $columns = ['name', 'created_by', 'updated_by', 'status', 'actions'];

        $length = $request->length;
        $column = $request->column;
        $dir = $request->dir;
        $searchValue = $request->search;
        $draw = $request->draw;
        $type = $request->type;

        $query = Master::select('id', 'type', 'name', 'status', 'created_by', 'updated_by')->where('company_id', $request->user()->company_id)->with(['creator', 'updater'])->orderBy($columns[$column], $dir);

        if ($type !== 'all') {
            $query->where('type', $type);
        }

        if ($searchValue) {
            $query->where(function ($query) use ($searchValue) {
                $query->where('type', 'like', '%' . $searchValue . '%')
                    ->orWhere('name', 'like', '%' . $searchValue . '%')
                    ->orWhereHas('creator', function ($query) use ($searchValue) {
                        $query->where('first_name', 'like', '%' . $searchValue . '%')
                            ->orWhere('last_name', 'like', '%' . $searchValue . '%');
                    })
                    ->orWhereHas('updater', function ($query) use ($searchValue) {
                        $query->where('first_name', 'like', '%' . $searchValue . '%')
                            ->orWhere('last_name', 'like', '%' . $searchValue . '%');
                    });
            });
        }

        $masters = $query->paginate($length);

        return response()->json([
            'masters' => $masters,
            'draw' => $draw
        ], 200);
    }

    public function indexByType(Request $request, $type)
    {
        $masters = $type === 'all' ? Master::where('company_id', $request->user()->company_id)->get() : Master::where('type', $type)->where('company_id', $request->user()->company_id)->get();
        $types = Master::select('type')->where('company_id', $request->user()->company_id)->distinct()->get()->map(function ($item) {
            return $item->type;
        });;
        return response()->json([
            'masters' => $masters,
            'types' => $types
        ], 200);
    }
}
