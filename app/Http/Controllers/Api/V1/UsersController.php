<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\File;
use App\Models\Role;
use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Hash;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Requests\UpdateUserCurrencyRequest;

class UsersController extends Controller
{
    public function index(Request $request)
    {
        return response()->json([
            'users' => User::where('company_id', $request->user()->company_id)->get()
        ], 200);
    }

    public function store(CreateUserRequest $request)
    {
        $this->authorize('create', $request->user());

        DB::beginTransaction();
        try {
            $user = User::create(array_merge(
                $request->validated(),
                [
                    'password' => Hash::make(Str::random(8)),
                    'company_id' => $request->user()->company->id
                ]
            ));

            if ($request->hasFile('uploaded_photo')) {
                $fileNameWithExt = $request->file('uploaded_photo')->getClientOriginalName();
                $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
                $extension = $request->file('uploaded_photo')->getClientOriginalExtension();
                $fileNameToStore = $fileName . '_' . time() . '.' . $extension;
                $request->file('uploaded_photo')->storeAs('public/user_photos', $fileNameToStore);
                $file_path = 'user_photos/' . $fileNameToStore;

                File::create(
                    [
                        'reference_type' => User::class,
                        'reference_id' => $user->id,
                        'original_name' => $fileNameWithExt,
                        'name' => $fileName,
                        'file_ext' => $extension,
                        'force_download' => false,
                        'file_path' => $file_path,
                        'status' => true,
                        'uploaded_by' => $request->user()->id,
                        'updated_by' => $request->user()->id,
                        'company_id' => $request->user()->company_id,
                    ]
                );
            }

            DB::commit();
            event(new Registered($user));
            return response()->json(new UserResource($user), 201);
        } catch (\Exception $e) {
            DB::rollback();
            info('User controller error');
            info($e);
            return response()->json(['error' => 'Unable to save'], 500);
        }
    }

    public function show(User $user)
    {
        $this->authorize('view', $user);
        return response()->json(new UserResource($user), 200);
    }

    public function update(UpdateUserRequest $request, User $user)
    {
        $this->authorize('update', $user);

        DB::beginTransaction();
        try {
            $user->update(
                $request->validated(),
            );

            if ($request->hasFile('uploaded_photo')) {
                if ($user->userPhoto()->exists()) {
                    Storage::delete('//public//' . $user->userPhoto->file_path);
                    $user->userPhoto()->delete();
                }

                $fileNameWithExt = $request->file('uploaded_photo')->getClientOriginalName();
                $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
                $extension = $request->file('uploaded_photo')->getClientOriginalExtension();
                $fileNameToStore = $fileName . '_' . time() . '.' . $extension;
                $request->file('uploaded_photo')->storeAs('public/user_photos', $fileNameToStore);
                $file_path = 'user_photos/' . $fileNameToStore;

                File::create(
                    [
                        'reference_type' => User::class,
                        'reference_id' => $user->id,
                        'original_name' => $fileNameWithExt,
                        'name' => $fileName,
                        'file_ext' => $extension,
                        'force_download' => false,
                        'file_path' => $file_path,
                        'status' => true,
                        'uploaded_by' => $request->user()->id,
                        'updated_by' => $request->user()->id,
                        'company_id' => $request->user()->company_id,
                    ]
                );
            }

            DB::commit();
            $user = $user->refresh();
            return response()->json(new UserResource($user), 200);
        } catch (\Exception $e) {
            DB::rollback();
            info('User controller error');
            info($e);
            return response()->json(['error' => 'Unable to save'], 500);
        }
    }

    public function destroy(User $user)
    {
        $this->authorize('delete', $user);

        DB::beginTransaction();
        try {
            if ($user->userPhoto()->exists()) {
                Storage::delete('//public//' . $user->userPhoto->file_path);
                $user->userPhoto()->delete();
            }
            $user->delete();

            DB::commit();
            return response()->json(null, 204);
        } catch (\Exception $e) {
            DB::rollback();
            info('User controller error');
            info($e);
            return response()->json(['error' => 'Unable to delete'], 500);
        }
    }

    public function indexPaginated(Request $request)
    {
        $this->authorize('viewAny', $request->user());

        $columns = ['first_name', 'phone', 'email', 'their_role', 'status', 'actions'];

        $length = $request->length;
        $column = $request->column;
        $dir = $request->dir;
        $searchValue = $request->search;
        $searchField = $request->field;
        $relationship = $request->relationship;
        $relationship_field = $request->relationship_field;
        $draw = $request->draw;

        $query = User::select('id', 'title', 'first_name', 'last_name', 'username', 'phone', 'email', 'active', 'force_pw_change', 'photo', 'user_role')
            ->where('company_id', $request->user()->company_id)
            ->with('theirRole')
            ->orderBy($columns[$column], $dir);

        if ($searchValue && $searchField) {
            if ($relationship) {
                $query->whereHas($searchField, function ($query) use ($relationship_field, $searchValue) {
                    $query->where($relationship_field, 'like', '%' . $searchValue . '%');
                });
            } else {
                $query->where(function ($query) use ($searchField, $searchValue) {
                    $query->where($searchField, 'like', '%' . $searchValue . '%');
                });
            }
        }

        $users = $query->paginate($length);

        return response()->json([
            'users' => $users,
            'draw' => $draw
        ], 200);
    }

    public function searchUpdateCredit(Request $request)
    {
        $draw = $request->draw;
        $searchValue = $request->search;
        $user = $request->user();
        $commercialId = Role::where('name', 'Commercial')->first()->id;

        $users = User::where(function ($q) use ($searchValue) {
            $q->where('phone', 'like',  '%' . $searchValue . '%')->orWhereRaw(
                "concat(first_name, ' ', last_name) like '%" . $searchValue . "%' "
            );
        })
            ->where('user_role', $commercialId)
            ->where('company_id', $user->company_id)
            ->get();

        if ($users->count() === 0) {
            return response()->json(null, 404);
        }

        $users = UserResource::collection($users);
        return response()->json([
            'users' => $users,
            'draw' => $draw
        ], 200);
    }
}
