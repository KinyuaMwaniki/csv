<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Company;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Http\Resources\CompanyResource;
use Illuminate\Support\Facades\Artisan;
use App\Http\Requests\CreateCompanyRequest;
use App\Http\Requests\UpdateActivityPeriod;
use App\Http\Requests\UpdateCompanyRequest;
use App\Http\Requests\UpdateUserCompanyRequest;

class CompaniesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'companies' => Company::select('id', 'name')->get()
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCompanyRequest $request)
    {
        $user = $request->user();
        $company = Company::create(array_merge(
            $request->validated(),
            [
                'created_by' => $user->id,
                'updated_by' => $user->id
            ]
        ));

        return response()->json(new CompanyResource($company), 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showUserCompany(Request $request)
    {
        $user = $request->user();
        $company = $user->company;
        return response()->json(new CompanyResource($company), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCompanyRequest $request, Company $company)
    {
        $user = $request->user();
        $company->update(array_merge(
            $request->validated(),
            [
                'updated_by' => $user->id
            ]
        ));
        return response()->json(new CompanyResource($company), 200);
    }


    public function updateUserCompany(UpdateUserCompanyRequest $request)
    {
        $user = $request->user();
        $company = $user->company;
        $company->update(
            [
                'name' => $request->name,
                'industry' => $request->industry,
                'mobile' => $request->mobile,
                'fax' => $request->fax,
                'email' => $request->email,
                'currency_id' => $request->currency_id,
                'address' => $request->address,
                'zip_code' => $request->postal_code,
                'city' => $request->locality,
                'state' => $request->administrative_area_level_1,
                'country' => $request->country,
                'updated_by' => $user->id
            ]
        );

        return response()->json(new UserResource($user), 200);
    }

    // public function updateActivityPeriod(UpdateActivityPeriod $request)
    // {
    //     $user = $request->user();
    //     $company = $user->company;
    //     $company->update(
    //         [
    //             'period_of_activity' => $request->periodOfActivity,
    //             'start_activity' => $request->startActivity,
    //         ]
    //     );

    //     return response()->json(new UserResource($user), 200);
    // }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        $company->delete();
        return response()->json(null, 204);
    }

    public function indexPaginated(Request $request)
    {
        $columns = ['actions', 'status', 'name', 'industry', 'mobile', 'fax', 'email', 'address', 'address_2', 'city', 'country_id', 'state_id', 'zip_code'];

        $length = $request->length;
        $column = $request->column;
        $dir = $request->dir;
        $searchValue = $request->search;
        $draw = $request->draw;

        $query = Company::select('id', 'status', 'name', 'industry', 'mobile', 'fax', 'email', 'address', 'address_2', 'city', 'country_id', 'state_id', 'zip_code')->with(['state', 'country', 'creator', 'updater'])->orderBy($columns[$column], $dir);

        if ($searchValue) {
            $query->where(function ($query) use ($searchValue) {
                $query->where('name', 'like', '%' . $searchValue . '%')
                    ->orWhere('industry', 'like', '%' . $searchValue . '%')
                    ->orWhere('mobile', 'like', '%' . $searchValue . '%')
                    ->orWhere('fax', 'like', '%' . $searchValue . '%')
                    ->orWhere('email', 'like', '%' . $searchValue . '%')
                    ->orWhere('address', 'like', '%' . $searchValue . '%')
                    ->orWhere('address_2', 'like', '%' . $searchValue . '%')
                    ->orWhere('city', 'like', '%' . $searchValue . '%')
                    ->orWhere('zip_code', 'like', '%' . $searchValue . '%')
                    ->orWhereHas('country', function ($query) use ($searchValue) {
                        $query->where('name', 'like', '%' . $searchValue . '%');
                    })
                    ->orWhereHas('creator', function ($query) use ($searchValue) {
                        $query->where('first_name', 'like', '%' . $searchValue . '%')
                            ->orWhere('last_name', 'like', '%' . $searchValue . '%');
                    })
                    ->orWhereHas('updater', function ($query) use ($searchValue) {
                        $query->where('first_name', 'like', '%' . $searchValue . '%')
                            ->orWhere('last_name', 'like', '%' . $searchValue . '%');
                    });
            });
        }

        $companies = $query->paginate($length);

        return response()->json([
            'companies' => $companies,
            'draw' => $draw
        ], 200);
    }
}
