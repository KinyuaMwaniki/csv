<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Lead;
use App\Models\User;
use App\Exports\LeadsExport;
use App\Imports\LeadsImport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Resources\LeadResource;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\CreateLeadRequest;
use App\Http\Requests\UpdateLeadRequest;
use App\Http\Requests\ImportLeadsRequest;

class LeadsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateLeadRequest $request)
    {
        $lead_id = Lead::create($request->validated())->id;
        return response()->json($lead_id, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Lead $lead)
    {
        return response()->json(new LeadResource($lead), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateLeadRequest $request, Lead $lead)
    {
        $lead->update($request->validated());
        return response()->json($lead->id, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Lead $lead)
    {
        $lead->delete();
        return response()->json(null, 204);
    }

    public function indexPaginated(Request $request)
    {
        
        $columns = ['action', 'last_name', 'phone', 'mobile', 'company', 'source', 'email', 'website', 'assigned_to', 'city'];

        $length = $request->length;
        $column = $request->column;
        $dir = $request->dir;
        $searchValue = $request->search;
        $draw = $request->draw;

        $query = Lead::select('id', 'last_name', 'phone', 'mobile', 'company', 'source', 'email', 'website', 'assigned_to', 'city')->with('assignee')->orderBy($columns[$column], $dir);

        if ($searchValue) {
            $query->where(function ($query) use ($searchValue) {
                $query->where('city', 'like', '%' . $searchValue . '%')
                    ->orWhere('last_name', 'like', '%' . $searchValue . '%')
                    ->orWhere('phone', 'like', '%' . $searchValue . '%')
                    ->orWhere('mobile', 'like', '%' . $searchValue . '%')
                    ->orWhere('company', 'like', '%' . $searchValue . '%')
                    ->orWhere('source', 'like', '%' . $searchValue . '%')
                    ->orWhere('email', 'like', '%' . $searchValue . '%')
                    ->orWhere('website', 'like', '%' . $searchValue . '%')
                    ->orWhereHas('assignee', function ($query) use ($searchValue) {
                        $query->where('first_name', 'like', '%' . $searchValue . '%');
                    });
            });
        }

        $leads = $query->paginate($length);

        return response()->json([
            'leads' => $leads,
            'draw' => $draw
        ], 200);
    }

    public function export()
    {
        $file_name = 'leads' . time() . '.xlsx';
        Excel::store(new LeadsExport(), $file_name, 'excel_downloads');
        $url = Storage::disk('excel_downloads')->url($file_name);
        return response()->json([
            'url' => $url,
        ], 200);
    }

    public function deleteChecked(Request $request)
    {
        DB::beginTransaction();

        try {
            foreach ($request->all() as $lead_id) {
                $lead = Lead::findOrFail($lead_id);
                $lead->delete();
            }
            DB::commit();
            return response()->json(null, 204);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'Unable to Delete, rolled back',
                'error' => $e
            ], 500);
        }
    }

    public function import(ImportLeadsRequest $request)
    {
        $file = $request->file('import');
        $import = new LeadsImport;

        try {
            $import->import($file);
            if ($import->failures()->isNotEmpty()) {
                return response()->json([
                    'failures' => $import->failures(),
                ], 422);
            }
            return response()->json(null, 201);
        } catch (\Exception $e) {
            return response()->json([
                'error' => $e->getMessage(),
            ], 500);
        }
    }
}
