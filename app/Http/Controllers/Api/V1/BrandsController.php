<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\File;
use App\Models\Brand;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Resources\BrandResource;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\CreateBrandRequest;
use App\Http\Requests\UpdateBrandRequest;

class BrandsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return response()->json([
            'brands' => Brand::select('id', 'name')->where('company_id', $request->user()->company_id)->get()
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateBrandRequest $request)
    {
        DB::beginTransaction();
        try {
            $brand = Brand::create(array_merge(
                $request->validated(),
                [
                    'company_id' => $request->user()->company_id,
                ]
            ));

            $fileNameWithExt = $request->file('uploaded_photo')->getClientOriginalName();
            $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('uploaded_photo')->getClientOriginalExtension();
            $fileNameToStore = $fileName . '_' . time() . '.' . $extension;
            $request->file('uploaded_photo')->storeAs('public/brand_logos', $fileNameToStore);
            $file_path = 'brand_logos/' . $fileNameToStore;

            File::create(
                [
                    'reference_type' => Brand::class,
                    'reference_id' => $brand->id,
                    'original_name' => $fileNameWithExt,
                    'name' => $fileName,
                    'file_ext' => $extension,
                    'force_download' => true,
                    'file_path' => $file_path,
                    'status' => true,
                    'uploaded_by' => $request->user()->id,
                    'updated_by' => $request->user()->id,
                    'company_id' => $request->user()->company_id,
                ]
            );
            DB::commit();
            return response()->json(new BrandResource($brand), 201);
        } catch (\Exception $e) {
            DB::rollback();
            info('Brand controller error');
            info($e);
            return response()->json(['error' => 'Unable to save'], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBrandRequest $request, Brand $brand)
    {
        DB::beginTransaction();
        try {
            $brand->update(
                $request->validated(),
            );

            if ($request->hasFile('uploaded_photo')) {
                if ($brand->logo()->exists()) {
                    Storage::delete('//public//' . $brand->logo->file_path);
                    $brand->logo()->delete();
                }

                $fileNameWithExt = $request->file('uploaded_photo')->getClientOriginalName();
                $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
                $extension = $request->file('uploaded_photo')->getClientOriginalExtension();
                $fileNameToStore = $fileName . '_' . time() . '.' . $extension;
                $request->file('uploaded_photo')->storeAs('public/brand_logos', $fileNameToStore);
                $file_path = 'brand_logos/' . $fileNameToStore;

                File::create(
                    [
                        'reference_type' => Brand::class,
                        'reference_id' => $brand->id,
                        'original_name' => $fileNameWithExt,
                        'name' => $fileName,
                        'file_ext' => $extension,
                        'force_download' => true,
                        'file_path' => $file_path,
                        'status' => true,
                        'uploaded_by' => $request->user()->id,
                        'updated_by' => $request->user()->id,
                        'company_id' => $request->user()->company_id,
                    ]
                );
            }
            DB::commit();
            return response()->json(new BrandResource($brand), 200);
        } catch (\Exception $e) {
            DB::rollback();
            info('Brand controller error');
            info($e);
            return response()->json(['error' => 'Unable to save'], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Brand $brand)
    {
        DB::beginTransaction();
        try {
            if ($brand->logo()->exists()) {
                Storage::delete('//public//' . $brand->logo->file_path);
                $brand->logo()->delete();
            }
            $brand->delete();

            DB::commit();
            return response()->json(null, 204);
        } catch (\Exception $e) {
            DB::rollback();
            info('Brand controller error');
            info($e);
            return response()->json(['error' => 'Unable to delete'], 500);
        }
    }

    public function indexPaginated(Request $request)
    {
        $columns = ['name', 'logo', 'actions'];

        $length = $request->length;
        $column = $request->column;
        $dir = $request->dir;
        $searchValue = $request->search;
        $draw = $request->draw;

        $query = Brand::select('id', 'name')->where('company_id', $request->user()->company_id)->with('logo')->orderBy($columns[$column], $dir);

        if ($searchValue) {
            $query->where(function ($query) use ($searchValue) {
                $query->where('name', 'like', '%' . $searchValue . '%');
            });
        }

        $brands = $query->paginate($length);

        return response()->json([
            'brands' => $brands,
            'draw' => $draw
        ], 200);
    }
}
