<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Role;
// use Spatie\Permission\Models\Role;
use Illuminate\Http\Request;
use App\Http\Requests\RolesRequest;
use App\Http\Controllers\Controller;
use App\Http\Resources\RoleResource;
use App\Http\Requests\RoleUpdateRequest;

class RolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return response()->json([
            'roles' => Role::where('company_id', $request->user()->company_id)->get()
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RolesRequest $request)
    {
        $role = Role::create(
            array_merge(
                $request->validated(),
                [
                    'guard_name' => 'web',
                    'company_id' => $request->user()->company->id
                ]
            )
        );
        return response()->json(new RoleResource($role), 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        return response()->json(new RoleResource($role), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RoleUpdateRequest $request, Role $role)
    {
        if ($role->name === "Administrator" || $role->name === "Commercial") {
            return response()->json([
                'message' => 'This role cannot be deleted'
            ], 400);
        }
        $role->update($request->validated());
        return response()->json(new RoleResource($role));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        if ($role->name === "Administrator" || $role->name === "Commercial") {
            return response()->json([
                'message' => 'This role cannot be deleted'
            ], 400);
        }
        $role->delete();
        return response()->json(null, 204);
    }

    public function indexPaginated(Request $request)
    {
        $columns = ['name', 'actions'];

        $length = $request->length;
        $column = $request->column;
        $dir = $request->dir;
        $searchValue = $request->search;
        $draw = $request->draw;

        $query = Role::select('id', 'name')->where('company_id', $request->user()->company_id)->orderBy($columns[$column], $dir);

        if ($searchValue) {
            $query->where(function ($query) use ($searchValue) {
                $query->where('name', 'like', '%' . $searchValue . '%');
            });
        }

        $roles = $query->paginate($length);

        return response()->json([
            'roles' => $roles,
            'draw' => $draw
        ], 200);
    }
}
