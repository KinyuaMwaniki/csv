<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Client;
use Illuminate\Http\Request;
use App\Models\ClientBalance;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Artisan;
use App\Http\Requests\CreateClientBalancesRequest;

class ClientBalancesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateClientBalancesRequest $request)
    {
        $client = Client::find($request->client_id);
        DB::beginTransaction();
        try {
            $user = $request->user();
            if ($client->balance()->exists()) {
                $balance = $client->balance;
                $client_balance = $client->balance->balance;
                $client->balance()->update([
                    'balance' => $client_balance + $request->amount,
                    'updated_by' => $user->id,
                ]);
            } else {
                $balance = ClientBalance::create(array_merge(
                    [
                        'client_id' => $request->client_id,
                        'balance' => $request->amount,
                        'created_by' => $user->id,
                        'updated_by' => $user->id,
                        'company_id' => $user->company_id,
                    ]
                ));
            }

            $balance->activities()->create([
                'balance_id' => $balance->id,
                'client_id' => $balance->client_id,
                'amount' => $request->amount,
                'payment_type' => 'credit',
                'company_id' => $user->company_id,
                'created_by' => $user->id,
                'updated_by' => $user->id
            ]);

            DB::commit();
            return response()->json($request->client_id, 200);
        } catch (\Exception $e) {
            DB::rollback();
            info('Client Balance controller error');
            info($e);
            return response()->json(['error' => 'Unable to save'], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
