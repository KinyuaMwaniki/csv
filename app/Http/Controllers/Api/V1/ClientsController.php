<?php

namespace App\Http\Controllers\Api\V1;

use App\Events\ClientLevelUpdated;
use App\Models\File;
use App\Models\User;
use App\Models\Client;
use App\Models\Address;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Resources\ClientResource;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\CreateClientRequest;
use App\Http\Requests\UpdateClientRequest;

use function PHPUnit\Framework\isNull;

class ClientsController extends Controller
{
    const UPDATED_LEVEL = 'updated_level';

    public function index(Request $request)
    {
        $user = $request->user();
        if ($user->theirRole->name === 'Administrator') {
            return response()->json([
                'clients' => Client::where('company_id', $user->company_id)->get()
            ], 200);
        }
        return response()->json([
            'clients' => Client::where('company_id', $user->company_id)
                ->where('created_by', $user->id)
                ->orWhere('updated_by', $user->id)
                ->get()
        ], 200);
    }

    public function store(CreateClientRequest $request)
    {
        $user = $request->user();

        DB::beginTransaction();
        try {
            $client = Client::create(array_merge(
                $request->validated(),
                [
                    'created_by' => $user->id,
                    'updated_by' => $user->id,
                    'company_id' => $user->company->id
                ]
            ));

            if ($request->addresses) { // Save client addresses
                foreach ($request->addresses as $single) {
                    $address = json_decode($single);
                    Address::create([
                        'street_address' => $address->street_number,
                        'zip_code' => $address->postal_code,
                        'locality' => $address->sublocality_level_1,
                        'city' => $address->locality,
                        'state' => $address->administrative_area_level_1,
                        'country' => $address->country,
                        'client_id' => $client->id,
                        'company_id' => $user->company->id,
                        'is_default' => $address->is_default,
                        'is_billing' => $address->is_billing,
                    ]);
                }
            }

            if ($request['uploaded_photo']) { // Save photo
                $fileNameWithExt = $request->file('uploaded_photo')->getClientOriginalName();
                $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
                $extension = $request->file('uploaded_photo')->getClientOriginalExtension();
                $fileNameToStore = $fileName . '_' . time() . '.' . $extension;
                $request->file('uploaded_photo')->storeAs('public/client_photos', $fileNameToStore);
                $file_path = 'client_photos/' . $fileNameToStore;

                File::create(
                    [
                        'reference_type' => Client::class,
                        'reference_id' => $client->id,
                        'original_name' => $fileNameWithExt,
                        'name' => $fileName,
                        'file_ext' => $extension,
                        'type' => 'photo',
                        'force_download' => false,
                        'file_path' => $file_path,
                        'status' => true,
                        'uploaded_by' => $user->id,
                        'updated_by' => $user->id,
                        'company_id' => $user->company_id,
                    ]
                );
            }

            if ($request['documents']) { // Save client documents
                foreach ($request['documents'] as $name => $file) {
                    $fileNameWithExt = $file->getClientOriginalName();
                    $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
                    $extension = $file->getClientOriginalExtension();
                    $fileNameToStore = $fileName . '_' . time() . '.' . $extension;
                    $file->storeAs('public/client_documents', $fileNameToStore);
                    $file_path = 'client_documents/' . $fileNameToStore;

                    File::create(
                        [
                            'reference_type' => Client::class,
                            'reference_id' => $client->id,
                            'original_name' => $name,
                            'name' => $fileName,
                            'file_ext' => $extension,
                            'type' => 'document',
                            'force_download' => true,
                            'file_path' => $file_path,
                            'status' => true,
                            'uploaded_by' => $user->id,
                            'updated_by' => $user->id,
                            'company_id' => $user->company_id,
                        ]
                    );
                }
            }

            DB::commit();
            $client = $client->fresh();
            return response()->json(new ClientResource($client), 201);
        } catch (\Exception $e) {
            DB::rollback();
            info('Clients controller error');
            info($e);
            return response()->json(['error' => 'Unable to save'], 500);
        }
    }

    public function show(Client $client)
    {

        $this->authorize('view', $client);
        return response()->json(new ClientResource($client), 200);
    }

    public function update(UpdateClientRequest $request, Client $client)
    {
        $user = $request->user();

        DB::beginTransaction();
        try {

            $client->update(array_merge(
                $request->validated(),
                [
                    'updated_by' => $user->id,
                ]
            ));


            if ($request->addresses) { // Save client addresses
                $clientAddresses = $client->addresses()->pluck('id')->toArray();
                $idsInAddresses = $this->searchForId($request->addresses);

                // If an address has been removed from the client and it is not attached to any order, delete it
                foreach ($clientAddresses as $single) {
                    if (!in_array($single, $idsInAddresses) && Address::find($single)->orders()->count() === 0) {
                        Address::find($single)->delete();
                    }
                }

                foreach ($request->addresses as $single) {
                    $address = json_decode($single);

                    if (isset($address->id) && $address->id !== "") {
                        $toUpdate = Address::find($address->id);
                        $toUpdate->update([
                            'street_address' => $address->street_number,
                            'zip_code' => $address->postal_code,
                            'locality' => $address->sublocality_level_1,
                            'city' => $address->locality,
                            'state' => $address->administrative_area_level_1,
                            'country' => $address->country,
                            'is_default' => $address->is_default,
                            'is_billing' => $address->is_billing,
                        ]);
                    } else {
                        Address::create([
                            'street_address' => $address->street_number,
                            'zip_code' => $address->postal_code,
                            'locality' => $address->sublocality_level_1,
                            'city' => $address->locality,
                            'state' => $address->administrative_area_level_1,
                            'country' => $address->country,
                            'client_id' => $client->id,
                            'company_id' => $user->company->id,
                            'is_default' => $address->is_default,
                            'is_billing' => $address->is_billing,
                        ]);
                    }
                }
            }

            if ($request->hasFile('uploaded_photo')) { // Save photo
                if ($client->photo()->exists()) {
                    Storage::delete('//public//' . $client->photo->file_path);
                    $client->photo()->delete();
                }

                $fileNameWithExt = $request->file('uploaded_photo')->getClientOriginalName();
                $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
                $extension = $request->file('uploaded_photo')->getClientOriginalExtension();
                $fileNameToStore = $fileName . '_' . time() . '.' . $extension;
                $request->file('uploaded_photo')->storeAs('public/client_photos', $fileNameToStore);
                $file_path = 'client_photos/' . $fileNameToStore;

                File::create(
                    [
                        'reference_type' => Client::class,
                        'reference_id' => $client->id,
                        'original_name' => $fileNameWithExt,
                        'name' => $fileName,
                        'file_ext' => $extension,
                        'type' => 'photo',
                        'force_download' => false,
                        'file_path' => $file_path,
                        'status' => true,
                        'uploaded_by' => $user->id,
                        'updated_by' => $user->id,
                        'company_id' => $user->company_id,
                    ]
                );
            }

            if ($request['current_documents']) { // Delete any removed document
                $current_document = $request['current_documents'];



                foreach ($client->documents as $document) {
                    if (!in_array($document->id, $current_document)) {
                        Storage::delete('//public//' . $document->file_path);
                        $document->delete();
                    }
                }
            } else {
                if ($client->documents()->exists()) { // If all current document have been removed, delete from db and storage
                    foreach ($client->documents as $document) {
                        Storage::delete('//public//' . $document->file_path);
                        $document->delete();
                    }
                }
            }

            if ($request['documents']) { // Save client documents
                foreach ($request['documents'] as $name => $file) {
                    $fileNameWithExt = $file->getClientOriginalName();
                    $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
                    $extension = $file->getClientOriginalExtension();
                    $fileNameToStore = $fileName . '_' . time() . '.' . $extension;
                    $file->storeAs('public/client_documents', $fileNameToStore);
                    $file_path = 'client_documents/' . $fileNameToStore;

                    File::create(
                        [
                            'reference_type' => Client::class,
                            'reference_id' => $client->id,
                            'original_name' => $name,
                            'name' => $fileName,
                            'file_ext' => $extension,
                            'type' => 'document',
                            'force_download' => true,
                            'file_path' => $file_path,
                            'status' => true,
                            'uploaded_by' => $user->id,
                            'updated_by' => $user->id,
                            'company_id' => $user->company_id,
                        ]
                    );
                }
            }

            DB::commit();
            $client = $client->refresh();
            return response()->json(new ClientResource($client), 200);
        } catch (\Exception $e) {
            DB::rollback();
            info('Clients controller error');
            info($e);
            return response()->json(['error' => 'Unable to save'], 500);
        }
    }

    function searchForId($array)
    {
        $ids = [];
        foreach ($array as $val) {
            $decodedVal = json_decode($val);
            foreach ($decodedVal as $keyIn => $valIn) {
                if ($keyIn === 'id') {
                    array_push($ids, $valIn);
                }
            }
        }
        return $ids;
    }

    public function updateLevel(UpdateClientRequest $request, Client $client)
    {
        $user = $request->user();

        DB::beginTransaction();
        try {
            $client->update(array_merge(
                $request->validated(),
                [
                    'updated_by' => $user->id,
                ]
            ));

            event(new ClientLevelUpdated($client, self::UPDATED_LEVEL, $user->id));

            DB::commit();
            $client = $client->refresh();
            return response()->json(new ClientResource($client), 200);
        } catch (\Exception $e) {
            DB::rollback();
            info('Update client level error');
            info($e);
            return response()->json(['error' => 'Unable to update'], 500);
        }
    }

    public function destroy(Client $client)
    {
        DB::beginTransaction();
        try {
            if ($client->photo()->exists()) {
                Storage::delete('//public//' . $client->photo->file_path);
                $client->photo()->delete();
            }

            if ($client->documents()->exists()) {
                foreach ($client->documents as $document) {
                    Storage::delete('//public//' . $document->file_path);
                    $document->delete();
                }
            }

            $client->delete();

            DB::commit();
            return response()->json(null, 204);
        } catch (\Exception $e) {
            DB::rollback();
            info('Clients controller error');
            info($e);
            return response()->json(['error' => 'Unable to delete'], 500);
        }
    }

    public function indexPaginated(Request $request)
    {
        $columns = ['first_name', 'mobile', 'email', 'status', 'actions'];

        $length = $request->length;
        $column = $request->column;
        $dir = $request->dir;
        $searchValue = $request->search;
        $searchField = $request->field;
        $relationship = $request->relationship;
        $relationship_field = $request->relationship_field;
        $draw = $request->draw;


        $query = Client::select('id', 'first_name', 'last_name', 'company_name',  'mobile', 'email', 'created_by', 'updated_by', 'company_id', 'status')
            ->with(['addresses', 'documents', 'photo'])
            ->where('company_id', $request->user()->company_id)
            ->orderBy($columns[$column], $dir);


        $user = $request->user();

        if ($user->theirRole->name === "Commercial") {
            $query->where('created_by', $user->id);
        }

        // Used when we are at viewing another users profile
        if ($request->filled('user_id')) {
            $user = User::find($request->user_id);
            if ($user->theirRole  !== 'Administrator') {
                $query->where(function ($query) use ($user) {
                    $query->where('created_by', $user->id)
                        ->orWhere('updated_by', $user->id);
                });
            }
        }

        if ($searchValue && $searchField) {
            if ($searchField === 'first_name') {
                $query->whereRaw(
                    "concat(first_name, ' ', last_name) like '%" . $searchValue . "%' "
                );
            } else {
                if ($relationship) {
                    $query->whereHas($searchField, function ($query) use ($relationship_field, $searchValue) { // searchField is the relationship name defined in the model. relationship_field is the field from the relatied model that will be returned.
                        $query->where($relationship_field, 'like', '%' . $searchValue . '%');
                    });
                } else {
                    $query->where(function ($query) use ($searchField, $searchValue) {
                        $query->where($searchField, 'like', '%' . $searchValue . '%');
                    });
                }
            }
        }

        $clients = $query->paginate($length);

        return response()->json([
            'clients' => $clients,
            'draw' => $draw
        ], 200);
    }

    public function debtsPaginated(Request $request)
    {
        $columns = ['first_name'];

        $length = $request->length;
        $column = $request->column;
        $dir = $request->dir;
        $searchValue = $request->search;
        $searchField = $request->field;
        $relationship = $request->relationship;
        $relationship_field = $request->relationship_field;
        $draw = $request->draw;

        $query = Client::select('id', 'first_name', 'last_name', 'mobile', 'email', 'street_address', 'zip_code', 'locality', 'city', 'state', 'country', 'created_by', 'updated_by', 'company_id', 'status')
            ->where('company_id', $request->user()->company_id)
            ->whereHas('orders', function ($query) {
                return $query->where('amount_remaining', '>', 0);
            })
            ->orderBy($columns[$column], $dir);

        $user = $request->user();
        if ($user->theirRole->name  !== 'Administrator') {
            $query->where(function ($query) use ($user) {
                $query->where('created_by', $user->id)
                    ->orWhere('updated_by', $user->id);
            });
        }

        if ($searchValue && $searchField) {
            if ($searchField === 'first_name') {
                $query->whereRaw(
                    "concat(first_name, ' ', last_name) like '%" . $searchValue . "%' "
                );
            } else {
                if ($relationship) {
                    $query->whereHas($searchField, function ($query) use ($relationship_field, $searchValue) { // searchField is the relationship name defined in the model. relationship_field is the field from the relatied model that will be returned.
                        $query->where($relationship_field, 'like', '%' . $searchValue . '%');
                    });
                } else {
                    $query->where(function ($query) use ($searchField, $searchValue) {
                        $query->where($searchField, 'like', '%' . $searchValue . '%');
                    });
                }
            }
        }

        $clients = $query->paginate($length);

        return response()->json([
            'clients' => $clients,
            'draw' => $draw
        ], 200);
    }

    public function searchClients(Request $request)
    {
        $searchValue = $request->search;
        $user = $request->user();
        $query = Client::where('mobile', $searchValue)
            ->orWhereRaw(
                "concat(first_name, ' ', last_name) like '%" . $searchValue . "%' "
            )
            ->where('company_id', $user->company_id);

        if ($user->theirRole->name !== "Administrator") {
            $query->where('created_by', $user->id);
        }

        $client = $query->first();

        if (!$client) {
            return response()->json(null, 404);
        }

        $client = new ClientResource($client);
        return response()->json([
            'client' => $client,
        ], 200);
    }

    public function updateStatus(Request $request, Client $client)
    {
        $user = $request->user();
        $client->update(
            [
                'status' => $request->status,
                'updated_by' => $user->id
            ]
        );
        return response()->json(new ClientResource($client), 200);
    }

    public function checkEmail(Request $request)
    {
        $rules = [
            'email' => [
                'email',
                'required',
                Rule::unique('clients')->where(function ($query) use ($request) {
                    return $query
                        ->where('company_id', $request->user()->company_id);
                }),
            ],
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        return response()->json(NULL, 200);
    }

    public function searchAddMoney(Request $request)
    {
        $searchValue = $request->search;
        $user = $request->user();

        $query = Client::where('company_id', $user->company_id);

        $query->where(function ($query) use ($searchValue) {
            $query->where('mobile', 'like', '%' . $searchValue . '%')
                ->orWhere('first_name', $searchValue)
                ->orWhere('last_name', $searchValue)
                ->orWhereRaw(
                    "concat(first_name, ' ', last_name) like '%" . $searchValue . "%' "
                );
        });

        if ($user->theirRole->name  !== 'Administrator') {
            $query->where(function ($query) use ($user) {
                $query->where('created_by', $user->id);
            });
        }

        $clients = $query->get();

        if ($clients->count() === 0) {
            return response()->json(null, 404);
        }

        return response()->json([
            'clients' => ClientResource::collection($clients),
            'draw' => $request->draw
        ], 200);
    }
}
