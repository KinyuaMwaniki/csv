<?php

namespace App\Http\Controllers\Api\V1;

use Carbon\Carbon;
use App\Models\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Resources\OrderResource;
use App\Exports\CollectionReportExport;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Storage;

class PaymentsController extends Controller
{
    public function collectionReport(Request $request)
    {
        $created_at = $request->created_at;
        $name = $request->name;
        $total = $request->total;
        $amount_paid = $request->amount_paid;
        $payment_method = $request->payment_method;
        $to = $request->to;
        $from = $request->from;
        $status = $request->status;
        $pendingWallet = $request->pendingWallet;

        $orderNumber = $request->orderNumber;

        $length = $request->length;
        $draw = $request->draw;

        $query = Payment::select('id', 'created_at', 'amount_paid', 'amount_remaining', 'payment_method', 'order_id', 'status', 'reason_denied')
            ->where('company_id', $request->user()->company_id)
            ->whereIn('payment_method', ['cash', 'wallet'])
            ->with(['order', 'order.client'])
            ->orderBy('created_at', 'desc');

        $user = $request->user();

        if ($user->theirRole->name === "Commercial") {
            $query->where('created_by', $user->id);
        }

        if ($created_at) {
            $query->whereDate('created_at', '=', Carbon::parse($created_at));
        }

        if ($orderNumber) {
            $query->whereHas('order', function ($query) use ($orderNumber) {
                $query->where('order_number', 'like', '%' . $orderNumber . '%');
            });
        }

        if ($name) {
            $query->whereHas('order', function ($query) use ($name) {
                $query->whereHas('client', function ($query) use ($name) {
                    $query->where('first_name', $name)
                        ->orWhere('last_name', $name)
                        ->orWhereRaw(
                            "concat(first_name, ' ', last_name) like '%" . $name . "%' "
                        );
                });
            });
        }

        if ($payment_method) {
            $query->where('payment_method', 'like', '%' . $payment_method . '%');
        }

        if ($total) {
            $query->where('total', 'like', '%' . $total . '%');
        }

        if ($amount_paid) {
            $query->where('amount_paid', 'like', '%' . $amount_paid . '%');
        }

        if ($status) {
            $query->where('status', $status);
        }

        if ($to && $from) {
            $query->whereBetween('created_at', [Carbon::parse($from), Carbon::parse($to)->copy()->endOfDay()]);
        }

        if ($pendingWallet === 1) {
            $query->where('payment_method', 'wallet')
                ->where('status', 'pending');
        }

        $collections = $query->paginate($length);

        return response()->json([
            'collections' => $collections,
            'draw' => $draw
        ], 200);
    }

    public function export(Request $request)
    {
        $locale = $request->user()->locale;
        App::setlocale($locale);

        $file_name = 'collections' . time() . '.xlsx';
        Excel::store(new CollectionReportExport($request->user(), $request->all()), $file_name, 'excel_downloads');
        $url = Storage::disk('excel_downloads')->url($file_name);
        return response()->json([
            'url' => $url,
        ], 200);
    }

    public function approve(Request $request, Payment $payment)
    {
        if (!Gate::allows('approve-order')) {
            abort(403);
        }

        DB::beginTransaction();
        try {
            if ($payment->payment_method === "wallet" && $payment->creator->theirRole->name === "Commercial") {
                $payment->creator->credit = $payment->creator->credit + $payment->amount_paid;
                $payment->creator->save();

                $payment->update([
                    'status' => 'Approved'
                ]);

                DB::commit();
            }
            return response()->json([
                "message", "Approved"
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();
            info('Approve order error');
            info($e);
            return response()->json(['error' => 'Unable to approve'], 500);
        }
    }

    public function deny(Request $request, Payment $payment)
    {
        if (!Gate::allows('approve-order')) {
            abort(403);
        }
        $payment->update([
            'status' => 'Denied',
            'reason_denied' => $request->reason_denied
        ]);

        return response()->json([
            "message", "Approved"
        ], 200);
    }
}
