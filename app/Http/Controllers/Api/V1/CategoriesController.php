<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Unit;
use App\Models\Category;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Resources\CategoryResource;
use App\Http\Requests\CreateCategoryRequest;
use App\Http\Requests\UpdateCategoryRequest;
use Illuminate\Support\Facades\Artisan;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return response()->json([
            'categories' => Category::select('id', 'name', 'parent_id')->with(['attributes'])->where('company_id', $request->user()->company_id)->get()
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCategoryRequest $request)
    {
        DB::beginTransaction();
        try {
            $user = $request->user();
            $category = Category::create(array_merge(
                $request->except('attributes'),
                [
                    'slug' => Str::random(20),
                    'created_by' => $user->id,
                    'updated_by' => $user->id,
                    'company_id' => $request->user()->company_id
                ]
            ));

            $category->slug = Str::slug(($category->id . ' ' . $category->name), '-');
            $category->save();

            if ($request['attributes']) {
                foreach ($request['attributes'] as $attribute) {
                    $category->attributes()->attach(
                        $attribute['id'],
                        [
                            'value' => $attribute['value'],
                            'company_id' => $request->user()->company_id,
                            'unit_id' => isset($attribute['unit']) ? $attribute['unit'] : null,
                            'unit_name' => isset($attribute['unit']) ? Unit::find($attribute['unit'])->name : null,
                        ]
                    );
                }
            }


            DB::commit();
            return response()->json(new CategoryResource($category), 201);
        } catch (\Exception $e) {
            DB::rollback();
            info('Category Controller error');
            info($e);
            return response()->json(['error' => 'Unable to save'], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        $this->authorize('view', $category);
        return response()->json(new CategoryResource($category), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCategoryRequest $request, Category $category)
    {
        DB::beginTransaction();
        try {
            $user = $request->user();
            $category->update(array_merge(
                $request->except('attributes'),
                [
                    'updated_by' => $user->id
                ]
            ));

            $category->attributes()->detach();
            if ($request['attributes']) {
                foreach ($request['attributes'] as $attribute) {
                    $category->attributes()->attach(
                        $attribute['id'],
                        [
                            'value' => $attribute['value'],
                            'company_id' => $request->user()->company_id,
                            'unit_id' => isset($attribute['unit']) ? $attribute['unit'] : null,
                            'unit_name' => isset($attribute['unit']) ? Unit::find($attribute['unit'])->name : null,
                        ]
                    );
                }
            }

            DB::commit();
            return response()->json(new CategoryResource($category), 200);
        } catch (\Exception $e) {
            DB::rollback();
            info('Category Controller error');
            info($e);
            return response()->json(['error' => 'Unable to update'], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $category->delete();
        return response()->json(null, 204);
    }

    public function indexPaginated(Request $request)
    {
        $columns = ['parent', 'name', 'code', 'status', 'actions'];

        $length = $request->length;
        $column = $request->column;
        $dir = $request->dir;
        $searchValue = $request->search;
        $searchField = $request->field;
        $relationship = $request->relationship;
        $relationship_field = $request->relationship_field;
        $draw = $request->draw;

        $query = Category::select('id', 'status', 'parent_id', 'name', 'code', 'slug', 'company_id', 'created_by', 'updated_by')
            ->where('company_id', $request->user()->company_id)
            ->with(['parentCategory', 'company', 'creator', 'updater'])
            ->orderBy($columns[$column], $dir);

        if ($searchValue && $searchField) {
            if ($relationship) {
                $query->whereHas($searchField, function ($query) use ($relationship_field, $searchValue) { // searchField is the relationship name defined in the model. relationship_field is the field from the relatied model that will be returned.
                    $query->where($relationship_field, 'like', '%' . $searchValue . '%');
                });
            } else {
                $query->where(function ($query) use ($searchField, $searchValue) {
                    $query->where($searchField, 'like', '%' . $searchValue . '%');
                });
            }
        }

        $categories = $query->paginate($length);

        return response()->json([
            'categories' => $categories,
            'draw' => $draw
        ], 200);
    }

    public function updateStatus(UpdateCategoryRequest $request, Category $category)
    {
        $user = $request->user();
        $category->update(array_merge(
            $request->except('attributes'),
            [
                'updated_by' => $user->id
            ]
        ));
        return response()->json(new CategoryResource($category), 200);
    }
}
