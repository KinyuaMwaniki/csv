<?php

namespace App\Http\Controllers\Api\V2;

use App\Models\File;
use App\Models\UserCash;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Storage;
use App\Http\Resources\UserCashResource;
use App\Http\Requests\MobileCreateUserCashRequest;
use App\Http\Requests\MobileUpdateUserCashRequest;

class UserCashesController extends Controller
{
    public function store(MobileCreateUserCashRequest $request)
    {
        $user = $request->user();
        DB::beginTransaction();
        try {
            $cash = UserCash::create(array_merge(
                $request->validated(),
                [
                    'company_id' => $user->company_id,
                    'user_id' => $user->id,
                    'created_by' => $user->id,
                    'updated_by' => $user->id,
                ]
            ));

            if ($request->uploaded_photo) {
                $extension = explode('/', mime_content_type($request->uploaded_photo))[1];
                $fileNameToStore = time() . '.' . $extension;
                $image = Image::make($request->uploaded_photo);
                $image->stream();
                Storage::disk('local')->put('public/user_cash' . '/' . $fileNameToStore, $image, 'public');
                $file_path = 'user_cash/' . $fileNameToStore;

                File::create(
                    [
                        'reference_type' => UserCash::class,
                        'reference_id' => $cash->id,
                        'original_name' => $fileNameToStore,
                        'name' => $fileNameToStore,
                        'file_ext' => $extension,
                        'force_download' => false,
                        'file_path' => $file_path,
                        'company_id' => $user->company_id,
                        'uploaded_by' => $user->id,
                        'updated_by' => $user->id
                    ]
                );
            }
            DB::commit();
            return response()->json(new UserCashResource($cash), 201);
        } catch (\Exception $e) {
            DB::rollback();
            info('User cash controller error');
            info($e);
            return response()->json(['error' => 'Unable to save'], 500);
        }
    }

    public function update(MobileUpdateUserCashRequest $request, UserCash $userCash)
    {
        DB::beginTransaction();
        try {
            $userCash->update(
                array_merge(
                    $request->validated(),
                    [
                        'status' => 'Pending',
                        'updated_by' => $request->user()->id
                    ]
                )
            );

            if ($request->uploaded_photo) {
                $userCash->photo()->delete();

                $extension = explode('/', mime_content_type($request->uploaded_photo))[1];
                $fileNameToStore = time() . '.' . $extension;
                $image = Image::make($request->uploaded_photo);
                $image->stream();
                Storage::disk('local')->put('public/user_cash' . '/' . $fileNameToStore, $image, 'public');
                $file_path = 'user_cash/' . $fileNameToStore;

                File::create(
                    [
                        'reference_type' => UserCash::class,
                        'reference_id' => $userCash->id,
                        'original_name' => $fileNameToStore,
                        'name' => $fileNameToStore,
                        'file_ext' => $extension,
                        'force_download' => false,
                        'file_path' => $file_path,
                        'company_id' => $userCash->company_id,
                        'uploaded_by' => $userCash->id,
                        'updated_by' => $userCash->id
                    ]
                );
            }
            DB::commit();
            return response()->json(new UserCashResource($userCash), 200);
        } catch (\Exception $e) {
            DB::rollback();
            info('Usercash controller error');
            info($e);
            return response()->json(['error' => 'Unable to save'], 500);
        }
    }
}
