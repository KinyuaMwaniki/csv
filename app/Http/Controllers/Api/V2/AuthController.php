<?php

namespace App\Http\Controllers\Api\V2;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $rules = [
            'email' => ['required'],
            'password' => ['required'],
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $userdata = array(
            'email'     => $request->email,
            'password'  => $request->password
        );


        if (Auth::attempt($userdata)) {
            $user = Auth::user();
            if ($user->theirRole->name === 'Administrator') {
                return response()->json([
                    'message' => 'An administrator cannot login in the mobile app'
                ], 401);
            }

            // $firstTimeLogin = false;

            // if(!$user->has_logged_in) {
            //     $firstTimeLogin = true;
            //     $user->update([
            //         'has_logged_in' => true,
            //     ]);
            // }

            $token = $user->createToken('crm-token', ['admin:view'])->plainTextToken;

            return response()->json([
                'token' => $token,
                'user' => new UserResource($user),
                // 'firstTimeLogin' => $firstTimeLogin
            ], 201);
        }

        return response()->json([
            'message' => 'Invalid credentitals'
        ], 401);
    }
}
