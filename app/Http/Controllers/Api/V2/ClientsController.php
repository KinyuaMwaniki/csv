<?php

namespace App\Http\Controllers\Api\V2;

use App\Models\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\ClientResource;
use Illuminate\Support\Facades\Artisan;

class ClientsController extends Controller
{
    public function searchClients(Request $request)
    {
        $searchValue = $request->search;
        $user = $request->user();

        $query = Client::where('company_id', $user->company_id);

        $query->where(function ($query) use ($searchValue) {
            $query->where('first_name', $searchValue)
                ->orWhere('mobile', 'like', '%' . $searchValue . '%')
                ->orWhere('last_name', $searchValue)
                ->orWhereRaw(
                    "concat(first_name, ' ', last_name) like '%" . $searchValue . "%' "
                );
        });

        if ($user->theirRole->name === "Commercial") {
            $query->where('created_by', $user->id);
        }

        $clients = $query->get();

        if ($clients->count() === 0) {
            return response()->json(null, 404);
        }

        return response()->json([
            'clients' => ClientResource::collection($clients),
        ], 200);
    }

    public function indexPaginated(Request $request)
    {
        $columns = ['first_name', 'mobile', 'email', 'status', 'actions'];

        $length = $request->length;
        $column = $request->column;
        $dir = $request->dir;
        $searchValue = $request->search;
        $draw = $request->draw;

        $query = Client::select('id', 'first_name', 'last_name', 'company_name', 'mobile', 'email', 'created_by', 'updated_by', 'company_id', 'status')
            ->with(['addresses', 'documents', 'photo'])
            ->where('company_id', $request->user()->company_id)
            ->orderBy($columns[$column], $dir);


        $user = $request->user();

        if ($user->theirRole->name === "Commercial") {
            $query->where('created_by', $user->id);
        }

        // Used when we are at viewing another users profile
        if ($request->filled('user_id')) {
            $user = User::find($request->user_id);
            if ($user->theirRole  !== 'Administrator') {
                $query->where(function ($query) use ($user) {
                    $query->where('created_by', $user->id)
                        ->orWhere('updated_by', $user->id);
                });
            }
        }

        if ($searchValue) {
            $query->where(function ($query) use ($searchValue) {
                $query->whereRaw(
                    "concat(first_name, ' ', last_name) like '%" . $searchValue . "%' "
                )
                    ->orWhere('email', 'like', '%' . $searchValue . '%')
                    ->orWhere('mobile', 'like', '%' . $searchValue . '%');
            });
        }

        $clients = $query->paginate($length);

        return response()->json([
            'clients' => $clients,
            'draw' => $draw
        ], 200);
    }
}
