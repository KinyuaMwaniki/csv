<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AddressResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // $created_at = Carbon::parse((string) $this->created_at);
        // $updated_at = Carbon::parse((string) $this->updated_at);

        // if($updated_at->gt($created_at)) {
        //     $updated = true;
        // } else {
        //     $updated = false;
        // }

        return [
            'id' => $this->id,
            'street_address' => $this->street_address,
            'zip_code' => $this->zip_code,
            'locality' => $this->locality,
            'city' => $this->city,
            'state' => $this->state,
            'country' => $this->country,
            'is_default' => (int) $this->is_default, 
            'is_billing' => (int) $this->is_billing, 
            'company' => new CompanyResource($this->company),
            // 'created_at' => $created_at->format(Config::get('constants.settings.date_format')),
            // 'updated_at' => $updated_at->format(Config::get('constants.settings.date_format')),
            // 'updated' => $updated
        ];
    }
}
