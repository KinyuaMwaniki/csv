<?php

namespace App\Http\Resources;

use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $created_at = Carbon::parse((string) $this->created_at);
        $updated_at = Carbon::parse((string) $this->updated_at);
        $date_available = !is_null($this->date_available) ? Carbon::createFromFormat("Y-m-d", $this->date_available) : null;

        if($updated_at->gt($created_at)) {
            $updated = true;
        } else {
            $updated = false;
        }

        $product_stock_status = Product::PRODUCT_STOCK_STATUS;

        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'model' => $this->model,
            'sku' => $this->sku,
            'price' => $this->price,
            'currency' => new CurrencyResource($this->currency),
            'quantity' => $this->quantity,
            'minimum' => $this->minimum,
            'subtract' => $this->subtract,
            'stock_status' => $product_stock_status[$this->stock_status_id],
            'stock_status_id' => $this->stock_status_id,
            'shipping' => $this->shipping,
            'date_available' => !is_null($date_available) ? $date_available->format('Y-m-d') : null,
            'status' => $this->status,
            'sort_order' => $this->sort_order,
            'manufacturer' => new BrandResource($this->brand),
            'categories' => $this->categories,
            'attributes' => $this->attributes,
            'photos' => FileResource::collection($this->photos),
            'creator' => new UserResource($this->creator),
            'updater' => new UserResource($this->updater),
            'created_at' => $created_at->format(Config::get('constants.settings.date_format')),
            'updated_at' => $updated_at->format(Config::get('constants.settings.date_format')),
            'updated' => $updated
        ];
    }
}
