<?php

namespace App\Http\Resources;

use App\Http\Resources\FileResource;
use App\Http\Resources\RoleResource;
use App\Models\Client;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'email' => $this->email,
            'username' => $this->username,
            'country_code_only' => $this->country_code,
            'phone_only' => $this->phone,
            'phone' => "{$this->country_code}{$this->phone}",
            'role' => new RoleResource($this->theirRole),
            'credit' => $this->credit,
            // 'currency' => new CurrencyResource($this->currency),
            // 'credit_limit_start_date' => $this->credit_limit_start_date,
            // 'credit_limit_end_date' => $this->credit_limit_end_date,
            'company' => new CompanyResource($this->company),
            'photo' => new FileResource($this->userPhoto),
            'locale' => $this->locale,
            'email_verified_at' => $this->email_verified_at,
            'active' => (int) $this->active,
            'force_pw_change' => (int) $this->force_pw_change,
        ];
    }
}
