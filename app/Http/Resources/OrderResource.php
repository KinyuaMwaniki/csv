<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $created_at = Carbon::parse((string) $this->created_at);
        $updated_at = Carbon::parse((string) $this->updated_at);

        return [
            'id' => $this->id,
            'order_number' => $this->order_number,
            'total' => $this->total,
            'payment_method' => $this->payment_method,
            'type_of_wallet' => $this->type_of_wallet,
            'transaction_id' => $this->transaction_id,
            'amount_paid' => $this->amount_paid,
            'amount_remaining' => $this->amount_remaining,
            'status' => $this->status,
            'address' => new AddressResource($this->address), 
            'client' => new OrderClientResource($this->client), 
            'products' => OrderProductResource::collection($this->products),
            'company' => new CompanyResource($this->company), 
            'created_by' => new UserResource($this->creator),
            'updated_by' => new UserResource($this->updator),
            'created_at' => $created_at->format(Config::get('constants.settings.date_format')),
            'updated_at' => $updated_at->format(Config::get('constants.settings.date_format')),
        ];
    }
}
