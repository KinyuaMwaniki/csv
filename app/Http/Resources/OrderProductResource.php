<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $created_at = Carbon::parse((string) $this->created_at);
        $updated_at = Carbon::parse((string) $this->updated_at);

        return [
            'id' => $this->id,
            'order' => $this->order,
            'product' => new ProductResource($this->product),
            'quantity' => $this->quantity,
            'currency' => new CurrencyResource($this->currency),
            'price' => $this->price,
            'total' => $this->total,
            'company' => new CompanyResource($this->company),
            'created_by' => new UserResource($this->creator),
            'updated_by' => new UserResource($this->updator),
            'product_attributes' => $this->product_attributes,
            'created_at' => $created_at->format(Config::get('constants.settings.date_format')),
            'updated_at' => $updated_at->format(Config::get('constants.settings.date_format')),
        ];
    }
}
