<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $created_at = Carbon::parse((string) $this->created_at);
        $updated_at = Carbon::parse((string) $this->updated_at);

        if($updated_at->gt($created_at)) {
            $updated = true;
        } else {
            $updated = false;
        }

        return [
            'id' => $this->id,
            'parent' => new CategoryResource($this->parentCategory),
            'name' => $this->name,
            'full_name' => $this->full_name,
            'code' => $this->code,
            'slug' => $this->slug,
            'status' => $this->status,
            'attributes' => $this->attributes, 
            'company' => new CompanyResource($this->company),
            'creator' => new UserResource($this->creator),
            'updater' => new UserResource($this->updater),
            'created_at' => $created_at->format(Config::get('constants.settings.date_format')),
            'updated_at' => $updated_at->format(Config::get('constants.settings.date_format')),
            'updated' => $updated
        ];
    }
}
