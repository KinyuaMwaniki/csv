<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ClientResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'company_name' => $this->company_name,
            'name' => "{$this->first_name} {$this->last_name}",
            'mobile' => $this->mobile,
            'email' => $this->email,
            'balance' => $this->balance()->exists() ? $this->balance->balance : 0,
            'addresses' => AddressResource::collection($this->addresses),
            'shipping_addresses' => $this->shippingAddresses,
            'billing_addresses' => $this->billingAddresses,
            'addresses' => $this->addresses,
            'orders' => OrderResource::collection($this->orders),
            'is_premium' => (int) $this->is_premium,
            'status' => (int) $this->status,
            'photo' => new FileResource($this->photo),
            'documents' => FileResource::collection($this->documents),
        ];
    }
}
