<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Http\Resources\Json\JsonResource;

class FileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $created_at = Carbon::parse((string) $this->created_at);
        return [
            'id' => $this->id,
            // 'reference_type' => $this->reference_type,
            'original_name' => $this->original_name,
            'name' => $this->name,
            'force_download' => (int) $this->force_download,
            'file_path' => $this->file_path,
            'created_at' => $created_at->format(Config::get('constants.settings.date_format'))
        ];
    }
}
