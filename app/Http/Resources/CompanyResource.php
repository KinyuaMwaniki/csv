<?php

namespace App\Http\Resources;

use App\Http\Resources\UserResource;
use App\Http\Resources\StateResource;
use App\Http\Resources\CountryResource;
use Illuminate\Http\Resources\Json\JsonResource;

class CompanyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'industry' => $this->industry,
            'mobile' => $this->mobile,
            'fax' => $this->fax,
            'email' => $this->email,
            'currency' => new CurrencyResource($this->currency),
            'address' => $this->address,
            'address_2' => $this->address_2,
            'city' => $this->city,
            'state' => $this->state,
            // 'periodOfActivity' => $this->period_of_activity,
            // 'startActivity' => $this->start_activity,
            'country' => $this->country,
            'zip_code' => $this->zip_code,
            'status' => $this->status,
        ];
    }
}
