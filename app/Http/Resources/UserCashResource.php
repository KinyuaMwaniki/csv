<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use App\Http\Resources\FileResource;
use Illuminate\Support\Facades\Config;
use Illuminate\Http\Resources\Json\JsonResource;

class UserCashResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $created_at = Carbon::parse((string) $this->created_at);
        $updated_at = Carbon::parse((string) $this->updated_at);
        return [
            'id' => $this->id,
            'date' => $this->date,
            'amount' => $this->amount,
            'slip' => new FileResource($this->photo),
            'created_at' => $created_at->format(Config::get('constants.settings.date_format')),
            'updated_at' => $updated_at->format(Config::get('constants.settings.date_format')),
        ];
    }
}
