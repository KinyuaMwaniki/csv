<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use App\Http\Resources\UserResource;
use App\Http\Resources\StateResource;
use App\Http\Resources\CountryResource;
use Illuminate\Http\Resources\Json\JsonResource;

class LeadResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $created_at = Carbon::parse((string) $this->created_at);
        $updated_at = Carbon::parse((string) $this->updated_at);
        return [
            'id' => $this->id,
            'title' => $this->title,
            'first_name' => $this->first_name,
            'phone' => $this->phone,
            'mobile' => $this->mobile,
            'fax' => $this->fax,
            'email' => $this->email,
            'website' => $this->website,
            'status' => $this->status,
            'rating' => $this->rating,
            'assignee' => new UserResource($this->assignee),
            'creator' => new UserResource($this->creator),
            'annual_revenue' => $this->annual_revenue,
            'last_name' => $this->last_name,
            'company' => $this->company,
            'designation' => $this->designation,
            'source' => $this->source,
            'industry' => $this->industry,
            'no_of_employees' => $this->no_of_employees,
            'secondary_email' => $this->secondary_email,
            'email_opt_out' => $this->email_opt_out,
            'lane' => $this->lane,
            'pobox' => $this->pobox,
            'code' => $this->code,
            'city' => $this->city,
            'country' => new CountryResource($this->fromCountry),
            'state' => new StateResource($this->fromState),
            'description' => $this->description,
            'created_at' => $created_at->format('d-M-y h:i A'),
            'updated_at' => $updated_at->format('d-M-y h:i A')
        ];
    }
}
