<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class CreateCurrencyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => [
                'bail',
                'required',
                'max:10',
                Rule::unique('currencies')->where(function ($query) {
                    return $query->where('company_id', $this->user()->company_id);
                }),
            ],
            'name' => [
                'bail',
                'required',
                Rule::unique('currencies')->where(function ($query) {
                    return $query->where('company_id', $this->user()->company_id);
                }),
            ],
            'symbol' => ['bail', 'nullable'],
        ];
    }
}
