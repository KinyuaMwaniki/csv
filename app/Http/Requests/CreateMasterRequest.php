<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class CreateMasterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'bail',
                'required',
                Rule::unique('tbl_masters')->where(function ($query) {
                    return $query
                        ->whereName($this->name)
                        ->whereType($this->type);
                })
            ],
            'type' => ['bail', 'required'],
        ];
    }
}
