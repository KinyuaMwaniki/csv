<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class UpdateAttributeTypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('attribute_type');
        
        return [
            'name' => [
                'bail',
                'required',
                Rule::unique('attribute_types')->where(function ($query) {
                    return $query->where('company_id', $this->user()->company_id);
                })->ignore($id)
            ],
            'units' => ['bail', 'nullable', 'array'],
        ];
    }
}
