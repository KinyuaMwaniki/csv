<?php

namespace App\Http\Requests;

use App\Models\Role;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('user');

        return [
            'title' => ['bail', 'nullable'],
            'first_name' => ['bail', 'nullable'],
            'username' => ['bail', 'nullable', Rule::unique('users', 'username')->ignore($id)],
            'last_name' => ['bail', 'nullable'],
            'email' => ['bail', 'nullable', Rule::unique('users', 'email')->ignore($id)],
            'country_code' => ['bail', 'nullable'],
            'phone' => ['bail', 'nullable', Rule::unique('users', 'phone')->ignore($id)],
            'active' => ['bail', 'nullable'],
            'force_pw_change' => ['bail', 'nullable'],
            'user_role' => ['bail', 'nullable'],
            'uploaded_photo' => ['bail', 'nullable', 'mimes:jpeg,jpg,png,gif', 'max:3000'],
            'locale' => ['bail', 'nullable', 'max:2'],
            'credit' => [
                'bail',
                'nullable'
            ],
            // 'credit_limit_start_date' => [
            //     'bail',
            //     'nullable'
            // ],
            // 'credit_limit_end_date' => [
            //     'bail',
            //     'nullable'
            // ],
        ];
    }
}
