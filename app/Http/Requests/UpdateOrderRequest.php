<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'total' => [
                'bail',
                'required',
                'numeric',
                'min:0'
            ],
            'payment_method' => ['bail', 'required'],
            'type_of_wallet' => [
                'bail',
                'required_if:payment_method,wallet'
            ],
            'transaction_id' => [
                'bail',
                'required_if:payment_method,wallet'
            ],
            'amount_paid' => [
                'bail',
                'numeric',
                'required',
                'min:0'
            ],
            'amount_remaining' => [
                'bail',
                'numeric',
                'required',
                'min:0'
            ],
            'address_id' => [
                'bail',
                'required'
            ],
            'client_id' => [
                'bail',
                'required'
            ],
            'cart' => [
                'bail',
                'required'
            ]
        ];
    }
}
