<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class UpdateCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('category');

        return [
            'parent_id' => ['bail', 'nullable'],
            'name' => [
                'bail',
                'nullable',
                Rule::unique('categories')->where(function ($query) {
                    return $query
                        ->where('parent_id', $this->parent_id)
                        ->where('company_id', $this->user()->company_id);
                })->ignore($id)
            ],
            'code' => [
                'bail',
                'nullable',
                'max:10',
                Rule::unique('categories')->where(function ($query) {
                    return $query
                        ->where('parent_id', $this->parent_id)
                        ->where('company_id', $this->user()->company_id);
                })->ignore($id)
            ],
            'status' => ['bail', 'nullable'],
            'attributes' => ['bail', 'nullable'],
        ];
    }
}
