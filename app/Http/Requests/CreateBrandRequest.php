<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class CreateBrandRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'bail',
                'required',
                Rule::unique('brands')->where(function ($query) {
                    return $query->where('company_id', $this->user()->company_id);
                }),
            ],
            'uploaded_photo' => ['bail', 'required', 'mimes:jpeg,jpg,png,gif', 'max:3000'],
        ];
    }
}
