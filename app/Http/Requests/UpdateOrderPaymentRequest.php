<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateOrderPaymentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'payment_method' => ['bail', 'required'],
            'type_of_wallet' => [
                'bail',
                'required_if:payment_method,wallet'
            ],
            'transaction_id' => [
                'bail',
                'required_if:payment_method,wallet'
            ],
            'amount_paid' => [
                'bail',
                'numeric',
                'required',
                'min:0'
            ],
            'amount_remaining' => [
                'bail',
                'numeric',
                'required',
                'min:0'
            ],
        ];
    }
}
