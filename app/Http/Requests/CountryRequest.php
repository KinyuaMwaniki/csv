<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CountryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'iso' => ['required', 'unique:tbl_country', 'max:2' ],
            'name' => ['required', 'unique:tbl_country'],
            'nicename' => ['required', 'unique:tbl_country' ],
            'iso3' => ['required', 'unique:tbl_country', 'max:3' ],
            'numcode' => ['required', 'numeric', 'unique:tbl_country', 'max:32767'],
            'phonecode' => ['required', 'unique:tbl_country' ],
            'status' => ['required'],
        ];
    }
}
