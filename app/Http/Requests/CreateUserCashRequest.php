<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateUserCashRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date' => ['bail', 'required'],
            'amount' => ['bail', 'required'],
            'uploaded_photo' => ['bail', 'required', 'mimes:jpeg,jpg,png,gif', 'max:3000']
        ];
    }
}
