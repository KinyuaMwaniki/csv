<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class UpdateCompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('company');

        return [
            'name' => ['bail', 'nullable',],
            'industry' => ['bail', 'nullable'],
            'mobile' => ['bail', 'nullable', Rule::unique('companies', 'mobile')->ignore($id)],
            'fax' => ['bail', 'nullable', Rule::unique('companies', 'fax')->ignore($id)],
            'email' => ['bail', 'nullable', 'email', Rule::unique('companies', 'email')->ignore($id)],
            'address' => ['bail', 'nullable'],
            'address_2' => ['bail', 'nullable'],
            'city' => ['bail', 'nullable'],
            'state_id' => ['bail', 'nullable'],
            'country_id' => ['bail', 'nullable'],
            'zip_code' => ['bail', 'nullable'],
            'status' => ['bail', 'nullable'],
        ];
    }
}
