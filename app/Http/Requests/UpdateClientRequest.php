<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class UpdateClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('client');

        return [
            'first_name' => ['bail', 'nullable'],
            'last_name' => ['bail', 'nullable'],
            'company_name' => ['bail', 'required'],
            'mobile' => ['bail', 'nullable', Rule::unique('clients', 'mobile')->ignore($id)],
            'email' => ['bail', 'nullable', Rule::unique('clients', 'email')->ignore($id)],
            'street_address' => ['bail', 'nullable'],
            'zip_code' => ['bail', 'nullable'],
            'locality' => ['bail', 'nullable'],
            'city' => ['bail', 'nullable'],
            'state' => ['bail', 'nullable'],
            'country' => ['bail', 'nullable'],
            'is_premium' => ['bail', 'nullable'],
            'uploaded_photo' => ['sometimes', 'mimes:jpeg,jpg,png,gif', 'max:3000'],
            'documents' => ['bail', 'sometimes'],
            'current_documents' => ['bail', 'nullable', 'array'],
            'addresses' => ['bail', 'nullable', 'array'],
        ];
    }
}
