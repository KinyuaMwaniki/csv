<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class UpdateCountryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('country');
        return [
            'iso' => ['bail', 'max:2', 'nullable', Rule::unique('tbl_country')->ignore($id)],
            'name' => ['bail', 'nullable', Rule::unique('tbl_country')->ignore($id)],
            'nicename' => ['bail', 'nullable', Rule::unique('tbl_country')->ignore($id)],
            'iso3' => ['bail', 'max:3', 'nullable', Rule::unique('tbl_country')->ignore($id)],
            'numcode' => ['bail', 'nullable', Rule::unique('tbl_country')->ignore($id)],
            'phonecode' => ['bail', 'nullable', Rule::unique('tbl_country')->ignore($id)],
            'status' => ['bail', 'nullable'],
        ];
    }
}
