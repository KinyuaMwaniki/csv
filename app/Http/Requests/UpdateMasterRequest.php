<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class UpdateMasterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $id = $this->route('master');
        return [
            'name' => [
                'bail',
                'nullable',
                Rule::unique('tbl_masters')->where(function ($query) {
                    return $query
                        ->whereName($this->name)
                        ->whereType($this->type);
                })->ignore($id)
            ],
            'type' => ['bail', 'nullable'],
            'status' => ['bail', 'nullable']
        ];
    }
}
