<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class UpdateUserCompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $company = $this->user()->company;
        info($company);

        return [
            'name' => ['bail', 'nullable',],
            'industry' => ['bail', 'nullable'],
            'mobile' => ['bail', 'nullable', Rule::unique('companies', 'mobile')->ignore($company)],
            'fax' => ['bail', 'nullable', Rule::unique('companies', 'mobile')->ignore($company)],
            'email' => ['bail', 'nullable', 'email', Rule::unique('companies', 'email')->ignore($company)],
            'currency_id' => ['bail', 'nullable'],
            'address' => ['bail', 'nullable'],
            'postal_code' => ['bail', 'nullable'],
            'locality' => ['bail', 'nullable'],
            'administrative_area_level_1' => ['bail', 'nullable'],
            'country' => ['bail', 'nullable'],
        ];
    }
}
