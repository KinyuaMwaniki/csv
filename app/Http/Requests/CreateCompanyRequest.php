<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateCompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['bail', 'required',],
            'industry' => ['bail', 'required'],
            'mobile' => ['bail', 'required', 'unique:companies'],
            'fax' => ['bail', 'nullable', 'unique:companies'],
            'email' => ['bail', 'required', 'email', 'unique:companies'],
            'address' => ['bail', 'required'],
            'address_2' => ['bail', 'nullable'],
            'city' => ['bail', 'required'],
            'state_id' => ['bail', 'nullable'],
            'country_id' => ['bail', 'nullable'],
            'zip_code' => ['bail', 'required'],
        ];
    }
}
