<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateLeadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'assigned_to' => ['bail', 'required'],
            'last_name' => ['bail', 'nullable'],
            'title' => ['bail', 'nullable'],
            'first_name' => ['bail', 'nullable'],
            'phone' => ['bail', 'nullable'],
            'mobile' => ['bail', 'nullable'],
            'fax' => ['bail', 'nullable'],
            'email' => ['bail', 'nullable'],
            'website' => ['bail', 'nullable'],
            'status' => ['bail', 'nullable'],
            'rating' => ['bail', 'nullable'],
            'company' => ['bail', 'nullable'],
            'designation' => ['bail', 'nullable'],
            'source' => ['bail', 'nullable'],
            'industry' => ['bail', 'nullable'],
            'designation' => ['bail', 'nullable'],
            'no_of_employees' => ['bail', 'nullable'],
            'secondary_email' => ['bail', 'nullable'],
            'email_opt_out' => ['bail', 'nullable'],
            'lane' => ['bail', 'nullable'],
            'pobox' => ['bail', 'nullable'],
            'code' => ['bail', 'nullable'],
            'city' => ['bail', 'nullable'],
            'country' => ['bail', 'nullable'],
            'state' => ['bail', 'nullable'],
            'annual_revenue' => ['bail', 'nullable'],
            'description' => ['bail', 'nullable'],
            'created_by' => ['bail', 'required'],
        ];
    }
}
