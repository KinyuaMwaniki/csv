<?php

namespace App\Http\Requests;

use App\Models\Role;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class CreateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['bail', 'required'],
            'first_name' => ['bail', 'required'],
            'last_name' => ['bail', 'required'],
            'email' => ['bail', 'required', 'unique:users'],
            'username' => ['bail', 'required', 'unique:users'],
            'country_code' => ['bail', 'required'],
            'phone' => ['bail', 'required', 'unique:users'],
            'user_role' => ['bail', 'required'],
            'uploaded_photo' => ['sometimes', 'mimes:jpeg,jpg,png,gif', 'max:3000'],
            'username' => ['bail', 'required', 'unique:users'],
            'credit' => [
                'bail',
                Rule::requiredIf(function () {
                    return Role::find($this->user_role)->name === 'Commercial';
                })
            ],
            // 'credit_limit_start_date' => [
            //     'bail',
            //     Rule::requiredIf(function () {
            //         return Role::find($this->user_role)->name === 'Commercial';
            //     })
            // ],
            // 'credit_limit_end_date' => [
            //     'bail',
            //     Rule::requiredIf(function () {
            //         return Role::find($this->user_role)->name === 'Commercial';
            //     })
            // ],
        ];
    }
}
