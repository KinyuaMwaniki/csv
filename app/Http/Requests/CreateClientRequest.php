<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class CreateClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => ['bail', 'required'],
            'last_name' => ['bail', 'required'],
            'company_name' => ['bail', 'required'],
            'mobile' => ['bail', 'required', 'unique:clients'],
            'email' => [
                'bail',
                'required',
                Rule::unique('clients')->where(function ($query) {
                    return $query
                        ->where('company_id', $this->user()->company_id);
                }),
            ],
            'street_address' => ['bail', 'nullable'],
            'zip_code' => ['bail', 'nullable'],
            'locality' => ['bail', 'nullable'],
            'city' => ['bail', 'nullable'],
            'state' => ['bail', 'nullable'],
            'country' => ['bail', 'nullable'],
            'uploaded_photo' => ['sometimes', 'mimes:jpeg,jpg,png,gif', 'max:3000'],
            'documents' => ['bail', 'sometimes'],
            'addresses' => ['bail', 'required', 'array'],
        ];
    }
}
