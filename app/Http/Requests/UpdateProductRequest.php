<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['bail', 'nullable'],
            'description' => ['bail', 'nullable'],
            // 'meta_title' => ['bail', 'required'],
            // 'meta_description' => ['bail', 'nullable'],
            // 'meta_keyword' => ['bail', 'nullable'],
            // 'tag' => ['bail', 'nullable'],
            'model' => ['bail', 'required'],
            'sku' => ['bail', 'nullable'],
            'upc' => ['bail', 'nullable'],
            'ean' => ['bail', 'nullable'],
            'jan' => ['bail', 'nullable'],
            'isbn' => ['bail', 'nullable'],
            'mpn' => ['bail', 'nullable'],
            'location' => ['bail', 'nullable'],
            'price' => ['bail', 'nullable', 'numeric', 'min:0'],
            'currency_id' => ['bail', 'nullable', 'numeric'],
            'quantity' => ['bail', 'nullable', 'numeric', 'min:0'],
            'minimum' => ['bail', 'nullable', 'numeric', 'min:0'],
            'subtract' => ['bail', 'nullable', 'numeric', 'min:0'],
            'stock_status_id' => ['bail', 'nullable', 'numeric'],
            'shipping' => ['bail', 'nullable'],
            'date_available' => ['bail', 'nullable'],
            'status' => ['bail', 'nullable'],
            'sort_order' => ['bail', 'nullable', 'numeric'],
            'manufacturer_id' => ['bail', 'nullable', 'numeric'],
            'categories' => ['bail', 'nullable', 'array'],
            'attributes' => ['bail', 'nullable', 'array'],
            'images' => ['bail', 'nullable', 'array'],
            'current_images' => ['bail', 'nullable', 'array'],
        ];
    }
}
