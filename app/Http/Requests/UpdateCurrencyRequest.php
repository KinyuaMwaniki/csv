<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class UpdateCurrencyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('currency');
        return [
            'code' => [
                'bail',
                'nullable',
                'max:10',
                Rule::unique('currencies')->where(function ($query) {
                    return $query->where('company_id', $this->user()->company_id);
                })->ignore($id),
            ],
            'name' => [
                'bail',
                'nullable',
                Rule::unique('currencies')->where(function ($query) {
                    return $query->where('company_id', $this->user()->company_id);
                })->ignore($id),
            ],
            'symbol' => ['bail', 'nullable'],
            'status' => ['bail', 'nullable']
        ];
    }
}
