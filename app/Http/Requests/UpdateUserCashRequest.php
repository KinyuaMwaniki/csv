<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserCashRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date' => ['bail', 'nullable'],
            'amount' => ['bail', 'nullable'],
            'uploaded_photo' => ['bail', 'nullable', 'mimes:jpeg,jpg,png,gif', 'max:3000']
        ];
    }
}
