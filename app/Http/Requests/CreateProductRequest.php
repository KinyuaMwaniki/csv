<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['bail', 'required'],
            'description' => ['bail', 'nullable'],
            'model' => ['bail', 'required'],
            'sku' => ['bail', 'nullable'],
            'price' => ['bail', 'required', 'numeric', 'min:0'],
            'currency_id' => ['bail', 'required', 'numeric'],
            'quantity' => ['bail', 'required', 'numeric', 'min:0'],
            'minimum' => ['bail', 'nullable', 'numeric', 'min:0'],
            'subtract' => ['bail', 'nullable', 'numeric'],
            'stock_status_id' => ['bail', 'nullable', 'numeric'],
            'shipping' => ['bail', 'nullable'],
            'date_available' => ['bail', 'nullable'],
            'status' => ['bail', 'nullable'],
            'sort_order' => ['bail', 'nullable', 'numeric', 'min:0'],
            'manufacturer_id' => ['bail', 'nullable', 'numeric'],
            'categories' => ['bail', 'nullable', 'array'],
        ];
    }
}
