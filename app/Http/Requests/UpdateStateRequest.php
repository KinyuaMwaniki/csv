<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class UpdateStateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $state = $this->route('state');

        return [
            'name' => ['bail', 'nullable', Rule::unique('tbl_state', 'name')->ignore($state)],
            'country_id' => ['bail', 'nullable'],
            'code' => ['bail', 'nullable', Rule::unique('tbl_state', 'code')->ignore($state)],
            'status' => ['bail', 'nullable']
        ];
    }
}
