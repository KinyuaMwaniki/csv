<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class CreateOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'total' => [
                'bail',
                'required',
                'numeric',
                'min:0'
            ],
            'payment_when' => [
                'bail', 'required',
                Rule::in(['later', 'now']),
            ],
            'payment_method' => [
                'bail', 'required_if:payment_when,now',
                Rule::in(['wallet', 'cash']),
            ],
            'type_of_wallet' => [
                'bail',
                'required_if:payment_method,wallet'
            ],
            'transaction_id' => [
                'bail',
                Rule::requiredIf(function () {
                    return $this->payment_when === 'now' && $this->payment_method === 'wallet';
                })
            ],
            'amount_paid' => [
                'bail',
                'numeric',
                'required',
                'min:0'
            ],
            'amount_remaining' => [
                'bail',
                'numeric',
                'required',
                'min:0'
            ],
            'address_id' => [
                'bail',
                'required'
            ],
            'client_id' => [
                'bail',
                'required'
            ],
            'cart' => [
                'bail',
                'required'
            ]
        ];
    }
}
