# Create

**Endpoint:**  
`/api/v1/countries`

**Method:**  `POST`

**Headers:**
- `Authorization: Bearer {token}`
- `Accept: application/json`

**Payload:**
```json
{
    "iso": "11",
    "name": "What edd",
    "nicename": "abou edd",
    "iso3": "111",
    "numcode": "10002",
    "phonecode": "555",
    "status": 1
}
```

**Validation Rules:**

- `iso`: 'required', 'unique', 'max:2'
- `name`: 'required', 'unique'
- `nicename`: 'required', 'unique'
- `iso3`: 'required', 'unique', 'max:3',
- `numcode`: 'required', 'numeric', 'unique', 'max:32767'
- `phonecode`: 'required', 'unique:tbl_country'
- `status`: 'required',

**Sample Success Response:**
```json
{
    "id": 243,
    "iso": "11",
    "name": "What edd",
    "nicename": "abou edd",
    "iso3": "111",
    "numcode": "10002",
    "phonecode": "555",
    "status": 1,
    "created_at": "2021-07-12 16:24:47"
}
```
**Success Response Status:**
201

# Update

**Endpoint:**  
`/api/v1/countries/243`

**Method:**  `PUT`

**Headers:**
- `Authorization: Bearer {token}`
- `Accept: application/json`

**Payload:**

```json
{
    "iso": "22",
    "name": "asfas editedd",
    "nicename": "asfasd",
    "iso3": "ccc",
    "numcode": 32762,
    "phonecode": "2551",
    "status": 1
}
```
	
**Validation rules:**
- `iso`: 'required', 'unique', 'max:2'
- `name`: 'required', 'unique'
- `nicename`: 'required', 'unique'
- `iso3`: 'required', 'unique', 'max:3',
- `numcode`: 'required', 'numeric', 'unique', 'max:32767'
- `phonecode`: 'required', 'unique:tbl_country'
- `status`: 'required',

**Sample success response:**
```json
{
    "id": 30,
    "code": "SOb",
    "name": "Mombasa - ed",
    "country": {
        "id": 100,
        "iso": "ID",
        "name": "INDONESIA",
        "nicename": "Indonesia",
        "iso3": "IDN",
        "numcode": 360,
        "phonecode": "62",
        "status": 1,
        "created_at": ""
    },
    "status": 1
}
```

# Update Status

**Endpoint:**  
`/api/v1/countries/241`

**Method:**  `PUT`

**Headers:**
- `Authorization: Bearer {token}`
- `Accept: application/json`

**Payload:**
```json
{
    "status": 0
}
```

**Details of payload:**
- `status`: New status of country 

**Sample success response:**
```json
{
    "id": 243,
    "iso": "22",
    "name": "asfas editedd",
    "nicename": "asfasd",
    "iso3": "ccc",
    "numcode": 32762,
    "phonecode": "2551",
    "status": 0,
    "created_at": "2021-07-12 16:24:47"
}
```

# Get Paginated Countries

**Endpoint:**  
`/api/v1/paginated-countries?page=1`

**Method:**  `POST`

**Headers:**
- `Authorization: Bearer {token}`
- `Accept: application/json`

**Payload:**
```json
{
    "draw": 0,
    "length": 10,
    "search": "",
    "column": 1,
    "dir": "asc"
}
```

**Details of payload:**
- `draw`: We use this variable while searching since many requests are sent as user types search string. This variable is returned by the endpoint. We use it to confirm that the reponse is for the current string
- `length`: Length of page
- `search`: String to search
- `column`: Column to order by, from 0 index ['iso', 'name', 'status', 'actions']
- `dir`: Direction to order by

**Sample success response:**
```json
{
    "countries": {
        "current_page": 1,
        "data": [
            {
                "id": 243,
                "status": 0,
                "iso": "22",
                "name": "asfas editedd",
                "nicename": "asfasd",
                "iso3": "ccc",
                "numcode": 32762,
                "phonecode": 2551
            },
            {
                "id": 5,
                "status": 1,
                "iso": "AD",
                "name": "ANDORRA",
                "nicename": "Andorra",
                "iso3": "AND",
                "numcode": 20,
                "phonecode": 376
            },
            {
                "id": 224,
                "status": 1,
                "iso": "AE",
                "name": "UNITED ARAB EMIRATES",
                "nicename": "United Arab Emirates",
                "iso3": "ARE",
                "numcode": 784,
                "phonecode": 971
            },
            {
                "id": 1,
                "status": 1,
                "iso": "AF",
                "name": "AFGHANISTAN",
                "nicename": "Afghanistan",
                "iso3": "AFG",
                "numcode": 32766,
                "phonecode": 93
            },
            {
                "id": 9,
                "status": 1,
                "iso": "AG",
                "name": "ANTIGUA AND BARBUDA",
                "nicename": "Antigua and Barbuda",
                "iso3": "ATG",
                "numcode": 28,
                "phonecode": 1268
            },
            {
                "id": 7,
                "status": 1,
                "iso": "AI",
                "name": "ANGUILLA",
                "nicename": "Anguilla",
                "iso3": "AIA",
                "numcode": 660,
                "phonecode": 51424141
            },
            {
                "id": 2,
                "status": 1,
                "iso": "AL",
                "name": "ALBANIA",
                "nicename": "Albania",
                "iso3": "ALB",
                "numcode": 8,
                "phonecode": 355
            },
            {
                "id": 11,
                "status": 1,
                "iso": "AM",
                "name": "ARMENIA",
                "nicename": "Armenia",
                "iso3": "ARM",
                "numcode": 51,
                "phonecode": 374
            },
            {
                "id": 151,
                "status": 1,
                "iso": "AN",
                "name": "NETHERLANDS ANTILLES",
                "nicename": "Netherlands Antilles",
                "iso3": "ANT",
                "numcode": 530,
                "phonecode": 599
            },
            {
                "id": 6,
                "status": 1,
                "iso": "AO",
                "name": "ANGOLA",
                "nicename": "Angola",
                "iso3": "AGO",
                "numcode": 24,
                "phonecode": 244
            }
        ],
        "first_page_url": "http://csv.test:8080/api/v1/paginated-countries?page=1",
        "from": 1,
        "last_page": 25,
        "last_page_url": "http://csv.test:8080/api/v1/paginated-countries?page=25",
        "links": [
            {
                "url": null,
                "label": "&laquo; Previous",
                "active": false
            },
            {
                "url": "http://csv.test:8080/api/v1/paginated-countries?page=1",
                "label": "1",
                "active": true
            },
            {
                "url": "http://csv.test:8080/api/v1/paginated-countries?page=2",
                "label": "2",
                "active": false
            },
            {
                "url": "http://csv.test:8080/api/v1/paginated-countries?page=3",
                "label": "3",
                "active": false
            },
            {
                "url": "http://csv.test:8080/api/v1/paginated-countries?page=4",
                "label": "4",
                "active": false
            },
            {
                "url": "http://csv.test:8080/api/v1/paginated-countries?page=5",
                "label": "5",
                "active": false
            },
            {
                "url": "http://csv.test:8080/api/v1/paginated-countries?page=6",
                "label": "6",
                "active": false
            },
            {
                "url": "http://csv.test:8080/api/v1/paginated-countries?page=7",
                "label": "7",
                "active": false
            },
            {
                "url": "http://csv.test:8080/api/v1/paginated-countries?page=8",
                "label": "8",
                "active": false
            },
            {
                "url": "http://csv.test:8080/api/v1/paginated-countries?page=9",
                "label": "9",
                "active": false
            },
            {
                "url": "http://csv.test:8080/api/v1/paginated-countries?page=10",
                "label": "10",
                "active": false
            },
            {
                "url": null,
                "label": "...",
                "active": false
            },
            {
                "url": "http://csv.test:8080/api/v1/paginated-countries?page=24",
                "label": "24",
                "active": false
            },
            {
                "url": "http://csv.test:8080/api/v1/paginated-countries?page=25",
                "label": "25",
                "active": false
            },
            {
                "url": "http://csv.test:8080/api/v1/paginated-countries?page=2",
                "label": "Next &raquo;",
                "active": false
            }
        ],
        "next_page_url": "http://csv.test:8080/api/v1/paginated-countries?page=2",
        "path": "http://csv.test:8080/api/v1/paginated-countries",
        "per_page": 10,
        "prev_page_url": null,
        "to": 10,
        "total": 242
    },
    "draw": 0
}
```

# Delete

**Endpoint:**  
`/api/v1/countries/1`

**Method:**  `DELETE`

**Headers:**
- `Authorization: Bearer {token}`
- `Accept: application/json`

**Success Response Status:**
204

