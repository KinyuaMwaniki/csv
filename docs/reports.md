# Orders All Report

**Endpoint:**  
`/api/v1/orders/all`

**Method:**  `POST`

**Headers:**
- `Authorization: Bearer {token}`
- `Accept: application/json`

**Payload:**
```json
{
    "tableColumns": [
        "created_at",
        "order_number",
        "client",
        "total",
        "amount_paid",
        "amount_remaining",
        "actions"
    ],
    "draw": 0,
    "length": 10,
    "search": "1499",
    "column": 1,
    "field": "total",
    "relationship": false,
    "relationship_field": "",
    "dir": "desc"
}
```
**Details of payload:**
- `draw`: If you are sending many requests while searching, increment this by one after every request and then compare the one returned by the API
- `length`: Length of paginated page
- `search`: Search string
- `field`: Field to be searched ['created_at', 'order_number', 'client', 'total', 'amount_paid', 'amount_remaining']
- `relationship`: is the field being searched a relation to another table [set to true if search field is client, otherwise set to false]
- `relationship_field`: If the field being searched is a relationship field, enter the field in another table that will be returned [set to 'first_name' if search field is client, otherwise set to '']
- `dir`: Direction to order by, either asc or desc

**Example Payload : Search for client name**
```json
{
    "tableColumns": [
        "created_at",
        "order_number",
        "client",
        "total",
        "amount_paid",
        "amount_remaining",
        "actions"
    ],
    "draw": 0,
    "length": 10,
    "search": "felix",
    "column": 0,
    "field": "client",
    "relationship": true,
    "relationship_field": "first_name",
    "dir": "desc"
}
```

**Example Payload : Search for order number**
```json
{
    "tableColumns": [
        "created_at",
        "order_number",
        "client",
        "total",
        "amount_paid",
        "amount_remaining",
        "actions"
    ],
    "draw": 0,
    "length": 10,
    "search": "62",
    "column": 0,
    "field": "order_number",
    "relationship": false,
    "relationship_field": "",
    "dir": "desc"
}
```

**Sample response:**
```json
{
    "orders": {
        "current_page": 1,
        "data": [
            {
                "id": 91,
                "created_at": "18-08-21",
                "total": "1499.00",
                "amount_paid": "0.00",
                "order_number": "#00058",
                "amount_remaining": "1499.00",
                "status": "processing",
                "client_id": 196,
                "client": {
                    "id": 196,
                    "first_name": "Walid",
                    "last_name": "ksroo",
                    "mobile": "0101384880",
                    "email": "ksroo@gmail.com",
                    "debts": 42446,
                    "orders": [
                        {
                            "id": 88,
                            "order_number": "#00055",
                            "total": "648.00",
                            "payment_when": "later",
                            "payment_method": null,
                            "type_of_wallet": null,
                            "transaction_id": null,
                            "amount_paid": "0.00",
                            "amount_remaining": "648.00",
                            "status": "processing",
                            "requires_approval": 0,
                            "address_id": 316,
                            "client_id": 196,
                            "company_id": 1,
                            "created_by": 1,
                            "updated_by": 1,
                            "created_at": "18-08-21",
                            "updated_at": "2021-08-18T04:32:58.000000Z"
                        },
                        {
                            "id": 89,
                            "order_number": "#00056",
                            "total": "40000.00",
                            "payment_when": "later",
                            "payment_method": null,
                            "type_of_wallet": null,
                            "transaction_id": null,
                            "amount_paid": "0.00",
                            "amount_remaining": "40000.00",
                            "status": "processing",
                            "requires_approval": 0,
                            "address_id": 316,
                            "client_id": 196,
                            "company_id": 1,
                            "created_by": 1,
                            "updated_by": 1,
                            "created_at": "18-08-21",
                            "updated_at": "2021-08-18T04:34:10.000000Z"
                        },
                        {
                            "id": 90,
                            "order_number": "#00057",
                            "total": "299.00",
                            "payment_when": "later",
                            "payment_method": null,
                            "type_of_wallet": null,
                            "transaction_id": null,
                            "amount_paid": "0.00",
                            "amount_remaining": "299.00",
                            "status": "processing",
                            "requires_approval": 0,
                            "address_id": 316,
                            "client_id": 196,
                            "company_id": 1,
                            "created_by": 1,
                            "updated_by": 1,
                            "created_at": "18-08-21",
                            "updated_at": "2021-08-18T04:34:30.000000Z"
                        },
                        {
                            "id": 91,
                            "order_number": "#00058",
                            "total": "1499.00",
                            "payment_when": "later",
                            "payment_method": null,
                            "type_of_wallet": null,
                            "transaction_id": null,
                            "amount_paid": "0.00",
                            "amount_remaining": "1499.00",
                            "status": "processing",
                            "requires_approval": 0,
                            "address_id": 316,
                            "client_id": 196,
                            "company_id": 1,
                            "created_by": 1,
                            "updated_by": 1,
                            "created_at": "18-08-21",
                            "updated_at": "2021-08-18T04:34:49.000000Z"
                        }
                    ]
                }
            },
            {
                "id": 90,
                "created_at": "18-08-21",
                "total": "299.00",
                "amount_paid": "0.00",
                "order_number": "#00057",
                "amount_remaining": "299.00",
                "status": "processing",
                "client_id": 196,
                "client": {
                    "id": 196,
                    "first_name": "Walid",
                    "last_name": "ksroo",
                    "mobile": "0101384880",
                    "email": "ksroo@gmail.com",
                    "debts": 42446,
                    "orders": [
                        {
                            "id": 88,
                            "order_number": "#00055",
                            "total": "648.00",
                            "payment_when": "later",
                            "payment_method": null,
                            "type_of_wallet": null,
                            "transaction_id": null,
                            "amount_paid": "0.00",
                            "amount_remaining": "648.00",
                            "status": "processing",
                            "requires_approval": 0,
                            "address_id": 316,
                            "client_id": 196,
                            "company_id": 1,
                            "created_by": 1,
                            "updated_by": 1,
                            "created_at": "18-08-21",
                            "updated_at": "2021-08-18T04:32:58.000000Z"
                        },
                        {
                            "id": 89,
                            "order_number": "#00056",
                            "total": "40000.00",
                            "payment_when": "later",
                            "payment_method": null,
                            "type_of_wallet": null,
                            "transaction_id": null,
                            "amount_paid": "0.00",
                            "amount_remaining": "40000.00",
                            "status": "processing",
                            "requires_approval": 0,
                            "address_id": 316,
                            "client_id": 196,
                            "company_id": 1,
                            "created_by": 1,
                            "updated_by": 1,
                            "created_at": "18-08-21",
                            "updated_at": "2021-08-18T04:34:10.000000Z"
                        },
                        {
                            "id": 90,
                            "order_number": "#00057",
                            "total": "299.00",
                            "payment_when": "later",
                            "payment_method": null,
                            "type_of_wallet": null,
                            "transaction_id": null,
                            "amount_paid": "0.00",
                            "amount_remaining": "299.00",
                            "status": "processing",
                            "requires_approval": 0,
                            "address_id": 316,
                            "client_id": 196,
                            "company_id": 1,
                            "created_by": 1,
                            "updated_by": 1,
                            "created_at": "18-08-21",
                            "updated_at": "2021-08-18T04:34:30.000000Z"
                        },
                        {
                            "id": 91,
                            "order_number": "#00058",
                            "total": "1499.00",
                            "payment_when": "later",
                            "payment_method": null,
                            "type_of_wallet": null,
                            "transaction_id": null,
                            "amount_paid": "0.00",
                            "amount_remaining": "1499.00",
                            "status": "processing",
                            "requires_approval": 0,
                            "address_id": 316,
                            "client_id": 196,
                            "company_id": 1,
                            "created_by": 1,
                            "updated_by": 1,
                            "created_at": "18-08-21",
                            "updated_at": "2021-08-18T04:34:49.000000Z"
                        }
                    ]
                }
            },
            {
                "id": 89,
                "created_at": "18-08-21",
                "total": "40000.00",
                "amount_paid": "0.00",
                "order_number": "#00056",
                "amount_remaining": "40000.00",
                "status": "processing",
                "client_id": 196,
                "client": {
                    "id": 196,
                    "first_name": "Walid",
                    "last_name": "ksroo",
                    "mobile": "0101384880",
                    "email": "ksroo@gmail.com",
                    "debts": 42446,
                    "orders": [
                        {
                            "id": 88,
                            "order_number": "#00055",
                            "total": "648.00",
                            "payment_when": "later",
                            "payment_method": null,
                            "type_of_wallet": null,
                            "transaction_id": null,
                            "amount_paid": "0.00",
                            "amount_remaining": "648.00",
                            "status": "processing",
                            "requires_approval": 0,
                            "address_id": 316,
                            "client_id": 196,
                            "company_id": 1,
                            "created_by": 1,
                            "updated_by": 1,
                            "created_at": "18-08-21",
                            "updated_at": "2021-08-18T04:32:58.000000Z"
                        },
                        {
                            "id": 89,
                            "order_number": "#00056",
                            "total": "40000.00",
                            "payment_when": "later",
                            "payment_method": null,
                            "type_of_wallet": null,
                            "transaction_id": null,
                            "amount_paid": "0.00",
                            "amount_remaining": "40000.00",
                            "status": "processing",
                            "requires_approval": 0,
                            "address_id": 316,
                            "client_id": 196,
                            "company_id": 1,
                            "created_by": 1,
                            "updated_by": 1,
                            "created_at": "18-08-21",
                            "updated_at": "2021-08-18T04:34:10.000000Z"
                        },
                        {
                            "id": 90,
                            "order_number": "#00057",
                            "total": "299.00",
                            "payment_when": "later",
                            "payment_method": null,
                            "type_of_wallet": null,
                            "transaction_id": null,
                            "amount_paid": "0.00",
                            "amount_remaining": "299.00",
                            "status": "processing",
                            "requires_approval": 0,
                            "address_id": 316,
                            "client_id": 196,
                            "company_id": 1,
                            "created_by": 1,
                            "updated_by": 1,
                            "created_at": "18-08-21",
                            "updated_at": "2021-08-18T04:34:30.000000Z"
                        },
                        {
                            "id": 91,
                            "order_number": "#00058",
                            "total": "1499.00",
                            "payment_when": "later",
                            "payment_method": null,
                            "type_of_wallet": null,
                            "transaction_id": null,
                            "amount_paid": "0.00",
                            "amount_remaining": "1499.00",
                            "status": "processing",
                            "requires_approval": 0,
                            "address_id": 316,
                            "client_id": 196,
                            "company_id": 1,
                            "created_by": 1,
                            "updated_by": 1,
                            "created_at": "18-08-21",
                            "updated_at": "2021-08-18T04:34:49.000000Z"
                        }
                    ]
                }
            },
            {
                "id": 88,
                "created_at": "18-08-21",
                "total": "648.00",
                "amount_paid": "0.00",
                "order_number": "#00055",
                "amount_remaining": "648.00",
                "status": "processing",
                "client_id": 196,
                "client": {
                    "id": 196,
                    "first_name": "Walid",
                    "last_name": "ksroo",
                    "mobile": "0101384880",
                    "email": "ksroo@gmail.com",
                    "debts": 42446,
                    "orders": [
                        {
                            "id": 88,
                            "order_number": "#00055",
                            "total": "648.00",
                            "payment_when": "later",
                            "payment_method": null,
                            "type_of_wallet": null,
                            "transaction_id": null,
                            "amount_paid": "0.00",
                            "amount_remaining": "648.00",
                            "status": "processing",
                            "requires_approval": 0,
                            "address_id": 316,
                            "client_id": 196,
                            "company_id": 1,
                            "created_by": 1,
                            "updated_by": 1,
                            "created_at": "18-08-21",
                            "updated_at": "2021-08-18T04:32:58.000000Z"
                        },
                        {
                            "id": 89,
                            "order_number": "#00056",
                            "total": "40000.00",
                            "payment_when": "later",
                            "payment_method": null,
                            "type_of_wallet": null,
                            "transaction_id": null,
                            "amount_paid": "0.00",
                            "amount_remaining": "40000.00",
                            "status": "processing",
                            "requires_approval": 0,
                            "address_id": 316,
                            "client_id": 196,
                            "company_id": 1,
                            "created_by": 1,
                            "updated_by": 1,
                            "created_at": "18-08-21",
                            "updated_at": "2021-08-18T04:34:10.000000Z"
                        },
                        {
                            "id": 90,
                            "order_number": "#00057",
                            "total": "299.00",
                            "payment_when": "later",
                            "payment_method": null,
                            "type_of_wallet": null,
                            "transaction_id": null,
                            "amount_paid": "0.00",
                            "amount_remaining": "299.00",
                            "status": "processing",
                            "requires_approval": 0,
                            "address_id": 316,
                            "client_id": 196,
                            "company_id": 1,
                            "created_by": 1,
                            "updated_by": 1,
                            "created_at": "18-08-21",
                            "updated_at": "2021-08-18T04:34:30.000000Z"
                        },
                        {
                            "id": 91,
                            "order_number": "#00058",
                            "total": "1499.00",
                            "payment_when": "later",
                            "payment_method": null,
                            "type_of_wallet": null,
                            "transaction_id": null,
                            "amount_paid": "0.00",
                            "amount_remaining": "1499.00",
                            "status": "processing",
                            "requires_approval": 0,
                            "address_id": 316,
                            "client_id": 196,
                            "company_id": 1,
                            "created_by": 1,
                            "updated_by": 1,
                            "created_at": "18-08-21",
                            "updated_at": "2021-08-18T04:34:49.000000Z"
                        }
                    ]
                }
            },
            {
                "id": 87,
                "created_at": "17-08-21",
                "total": "598.00",
                "amount_paid": "0.00",
                "order_number": "#00054",
                "amount_remaining": "598.00",
                "status": "processing",
                "client_id": 176,
                "client": {
                    "id": 176,
                    "first_name": "Sachin",
                    "last_name": "Jaiswal",
                    "mobile": "8098908098",
                    "email": "sachin@mailinator.com",
                    "debts": 598,
                    "orders": [
                        {
                            "id": 87,
                            "order_number": "#00054",
                            "total": "598.00",
                            "payment_when": "later",
                            "payment_method": null,
                            "type_of_wallet": null,
                            "transaction_id": null,
                            "amount_paid": "0.00",
                            "amount_remaining": "598.00",
                            "status": "processing",
                            "requires_approval": 0,
                            "address_id": 286,
                            "client_id": 176,
                            "company_id": 1,
                            "created_by": 21,
                            "updated_by": 21,
                            "created_at": "17-08-21",
                            "updated_at": "2021-08-17T15:23:21.000000Z"
                        }
                    ]
                }
            }
        ],
        "first_page_url": "http://csv.jithvar.com/api/v1/orders/all?page=1",
        "from": 1,
        "last_page": 1,
        "last_page_url": "http://csv.jithvar.com/api/v1/orders/all?page=1",
        "links": [
            {
                "url": null,
                "label": "&laquo; Previous",
                "active": false
            },
            {
                "url": "http://csv.jithvar.com/api/v1/orders/all?page=1",
                "label": "1",
                "active": true
            },
            {
                "url": null,
                "label": "Next &raquo;",
                "active": false
            }
        ],
        "next_page_url": null,
        "path": "http://csv.jithvar.com/api/v1/orders/all",
        "per_page": 10,
        "prev_page_url": null,
        "to": 5,
        "total": 5
    },
    "draw": 0
}
```

# Orders By Debt

**Endpoint:**  
`/api/v1/orders/debts`

**Method:**  `POST`

**Headers:**
- `Authorization: Bearer {token}`
- `Accept: application/json`

**Payload:**
```json
{
    "tableColumns": [
        "created_at",
        "order_number",
        "client",
        "total",
        "amount_remaining",
        "actions"
    ],
    "draw": 0,
    "length": 10,
    "search": "",
    "column": 0,
    "field": "",
    "relationship": false,
    "relationship_field": "",
    "dir": "desc"
}
```
**Details of payload:**
- `draw`: If you are sending many requests while searching, increment this by one after every request and then compare the one returned by the API
- `length`: Length of paginated page
- `search`: Search string
- `field`: Field to be searched ['created_at', 'order_number', 'client', 'total', 'amount_paid', 'amount_remaining']
- `relationship`: is the field being searched a relation to another table [set to true if search field is client, otherwise set to false]
- `relationship_field`: If the field being searched is a relationship field, enter the field in another table that will be returned [set to 'first_name' if search field is client, otherwise set to '']
- `dir`: Direction to order by, either asc or desc

**Example Payload : Search for client name**
```json
{
    "tableColumns": [
        "created_at",
        "order_number",
        "client",
        "total",
        "amount_remaining",
        "actions"
    ],
    "draw": 0,
    "length": 10,
    "search": "wali",
    "column": 0,
    "field": "client",
    "relationship": true,
    "relationship_field": "first_name",
    "dir": "desc"
}
```

**Example Payload : Search by debt amount**
```json
{
    "tableColumns": [
        "created_at",
        "order_number",
        "client",
        "total",
        "amount_remaining",
        "actions"
    ],
    "draw": 0,
    "length": 10,
    "search": "1417",
    "column": 0,
    "field": "amount_remaining",
    "relationship": false,
    "relationship_field": "",
    "dir": "desc"
}
```

**Sample response:**
```json
{
    "orders": {
        "current_page": 1,
        "data": [
            {
                "id": 91,
                "created_at": "18-08-21",
                "order_number": "#00058",
                "total": "1499.00",
                "amount_paid": "82.00",
                "amount_remaining": "1417.00",
                "client_id": 196,
                "status": "processing",
                "client": {
                    "id": 196,
                    "first_name": "Walid",
                    "last_name": "ksroo",
                    "mobile": "0101384880",
                    "email": "ksroo@gmail.com",
                    "debts": 42364,
                    "orders": [
                        {
                            "id": 88,
                            "order_number": "#00055",
                            "total": "648.00",
                            "payment_when": "later",
                            "payment_method": null,
                            "type_of_wallet": null,
                            "transaction_id": null,
                            "amount_paid": "0.00",
                            "amount_remaining": "648.00",
                            "status": "processing",
                            "requires_approval": 0,
                            "address_id": 316,
                            "client_id": 196,
                            "company_id": 1,
                            "created_by": 1,
                            "updated_by": 1,
                            "created_at": "18-08-21",
                            "updated_at": "2021-08-18T04:32:58.000000Z"
                        },
                        {
                            "id": 89,
                            "order_number": "#00056",
                            "total": "40000.00",
                            "payment_when": "later",
                            "payment_method": null,
                            "type_of_wallet": null,
                            "transaction_id": null,
                            "amount_paid": "0.00",
                            "amount_remaining": "40000.00",
                            "status": "processing",
                            "requires_approval": 0,
                            "address_id": 316,
                            "client_id": 196,
                            "company_id": 1,
                            "created_by": 1,
                            "updated_by": 1,
                            "created_at": "18-08-21",
                            "updated_at": "2021-08-18T04:34:10.000000Z"
                        },
                        {
                            "id": 90,
                            "order_number": "#00057",
                            "total": "299.00",
                            "payment_when": "later",
                            "payment_method": null,
                            "type_of_wallet": null,
                            "transaction_id": null,
                            "amount_paid": "0.00",
                            "amount_remaining": "299.00",
                            "status": "processing",
                            "requires_approval": 0,
                            "address_id": 316,
                            "client_id": 196,
                            "company_id": 1,
                            "created_by": 1,
                            "updated_by": 1,
                            "created_at": "18-08-21",
                            "updated_at": "2021-08-18T04:34:30.000000Z"
                        },
                        {
                            "id": 91,
                            "order_number": "#00058",
                            "total": "1499.00",
                            "payment_when": "later",
                            "payment_method": "wallet",
                            "type_of_wallet": "smilepay",
                            "transaction_id": "sf",
                            "amount_paid": "82.00",
                            "amount_remaining": "1417.00",
                            "status": "processing",
                            "requires_approval": 0,
                            "address_id": 316,
                            "client_id": 196,
                            "company_id": 1,
                            "created_by": 1,
                            "updated_by": 21,
                            "created_at": "18-08-21",
                            "updated_at": "2021-08-18T12:06:01.000000Z"
                        }
                    ]
                }
            },
            {
                "id": 90,
                "created_at": "18-08-21",
                "order_number": "#00057",
                "total": "299.00",
                "amount_paid": "0.00",
                "amount_remaining": "299.00",
                "client_id": 196,
                "status": "processing",
                "client": {
                    "id": 196,
                    "first_name": "Walid",
                    "last_name": "ksroo",
                    "mobile": "0101384880",
                    "email": "ksroo@gmail.com",
                    "debts": 42364,
                    "orders": [
                        {
                            "id": 88,
                            "order_number": "#00055",
                            "total": "648.00",
                            "payment_when": "later",
                            "payment_method": null,
                            "type_of_wallet": null,
                            "transaction_id": null,
                            "amount_paid": "0.00",
                            "amount_remaining": "648.00",
                            "status": "processing",
                            "requires_approval": 0,
                            "address_id": 316,
                            "client_id": 196,
                            "company_id": 1,
                            "created_by": 1,
                            "updated_by": 1,
                            "created_at": "18-08-21",
                            "updated_at": "2021-08-18T04:32:58.000000Z"
                        },
                        {
                            "id": 89,
                            "order_number": "#00056",
                            "total": "40000.00",
                            "payment_when": "later",
                            "payment_method": null,
                            "type_of_wallet": null,
                            "transaction_id": null,
                            "amount_paid": "0.00",
                            "amount_remaining": "40000.00",
                            "status": "processing",
                            "requires_approval": 0,
                            "address_id": 316,
                            "client_id": 196,
                            "company_id": 1,
                            "created_by": 1,
                            "updated_by": 1,
                            "created_at": "18-08-21",
                            "updated_at": "2021-08-18T04:34:10.000000Z"
                        },
                        {
                            "id": 90,
                            "order_number": "#00057",
                            "total": "299.00",
                            "payment_when": "later",
                            "payment_method": null,
                            "type_of_wallet": null,
                            "transaction_id": null,
                            "amount_paid": "0.00",
                            "amount_remaining": "299.00",
                            "status": "processing",
                            "requires_approval": 0,
                            "address_id": 316,
                            "client_id": 196,
                            "company_id": 1,
                            "created_by": 1,
                            "updated_by": 1,
                            "created_at": "18-08-21",
                            "updated_at": "2021-08-18T04:34:30.000000Z"
                        },
                        {
                            "id": 91,
                            "order_number": "#00058",
                            "total": "1499.00",
                            "payment_when": "later",
                            "payment_method": "wallet",
                            "type_of_wallet": "smilepay",
                            "transaction_id": "sf",
                            "amount_paid": "82.00",
                            "amount_remaining": "1417.00",
                            "status": "processing",
                            "requires_approval": 0,
                            "address_id": 316,
                            "client_id": 196,
                            "company_id": 1,
                            "created_by": 1,
                            "updated_by": 21,
                            "created_at": "18-08-21",
                            "updated_at": "2021-08-18T12:06:01.000000Z"
                        }
                    ]
                }
            }
        ],
        "first_page_url": "http://csv.jithvar.com/api/v1/orders/debts?page=1",
        "from": 1,
        "last_page": 3,
        "last_page_url": "http://csv.jithvar.com/api/v1/orders/debts?page=3",
        "links": [
            {
                "url": null,
                "label": "&laquo; Previous",
                "active": false
            },
            {
                "url": "http://csv.jithvar.com/api/v1/orders/debts?page=1",
                "label": "1",
                "active": true
            },
            {
                "url": "http://csv.jithvar.com/api/v1/orders/debts?page=2",
                "label": "2",
                "active": false
            },
            {
                "url": "http://csv.jithvar.com/api/v1/orders/debts?page=3",
                "label": "3",
                "active": false
            },
            {
                "url": "http://csv.jithvar.com/api/v1/orders/debts?page=2",
                "label": "Next &raquo;",
                "active": false
            }
        ],
        "next_page_url": "http://csv.jithvar.com/api/v1/orders/debts?page=2",
        "path": "http://csv.jithvar.com/api/v1/orders/debts",
        "per_page": 2,
        "prev_page_url": null,
        "to": 2,
        "total": 5
    },
    "draw": 0
}
```

# Customer By Debt

**Endpoint:**  
`/api/v1/clients/debts`

**Method:**  `POST`

**Headers:**
- `Authorization: Bearer {token}`
- `Accept: application/json`

**Payload:**
```json
{
  "draw": 0,
  "length": 10,
  "search": "",
  "column": 0,
  "field": "",
  "relationship": false,
  "relationship_field": "",
  "dir": "asc"
}
```
**Details of payload:**
- `draw`: If you are sending many requests while searching, increment this by one after every request and then compare the one returned by the API
- `length`: Length of paginated page
- `search`: Search string
- `field`: Field to be searched ['created_at', 'order_number', 'client', 'total', 'amount_paid', 'amount_remaining']
- `relationship`: is the field being searched a relation to another table [set to true if search field is client, otherwise set to false]
- `relationship_field`: If the field being searched is a relationship field, enter the field in another table that will be returned [set to 'first_name' if search field is client, otherwise set to '']
- `dir`: Direction to order by, either asc or desc

**Example Payload : Search for customer name**
```json
{
    "draw": 0,
    "length": 10,
    "search": "felix",
    "column": 0,
    "field": "first_name",
    "relationship": false,
    "relationship_field": "",
    "dir": "asc"
}
```

**Sample response:**
```json
{
    "orders": {
        "current_page": 1,
        "data": [
            {
                "id": 91,
                "created_at": "18-08-21",
                "total": "1499.00",
                "amount_paid": "0.00",
                "order_number": "#00058",
                "amount_remaining": "1499.00",
                "status": "processing",
                "client_id": 196,
                "client": {
                    "id": 196,
                    "first_name": "Walid",
                    "last_name": "ksroo",
                    "mobile": "0101384880",
                    "email": "ksroo@gmail.com",
                    "debts": 42446,
                    "orders": [
                        {
                            "id": 88,
                            "order_number": "#00055",
                            "total": "648.00",
                            "payment_when": "later",
                            "payment_method": null,
                            "type_of_wallet": null,
                            "transaction_id": null,
                            "amount_paid": "0.00",
                            "amount_remaining": "648.00",
                            "status": "processing",
                            "requires_approval": 0,
                            "address_id": 316,
                            "client_id": 196,
                            "company_id": 1,
                            "created_by": 1,
                            "updated_by": 1,
                            "created_at": "18-08-21",
                            "updated_at": "2021-08-18T04:32:58.000000Z"
                        },
                        {
                            "id": 89,
                            "order_number": "#00056",
                            "total": "40000.00",
                            "payment_when": "later",
                            "payment_method": null,
                            "type_of_wallet": null,
                            "transaction_id": null,
                            "amount_paid": "0.00",
                            "amount_remaining": "40000.00",
                            "status": "processing",
                            "requires_approval": 0,
                            "address_id": 316,
                            "client_id": 196,
                            "company_id": 1,
                            "created_by": 1,
                            "updated_by": 1,
                            "created_at": "18-08-21",
                            "updated_at": "2021-08-18T04:34:10.000000Z"
                        },
                        {
                            "id": 90,
                            "order_number": "#00057",
                            "total": "299.00",
                            "payment_when": "later",
                            "payment_method": null,
                            "type_of_wallet": null,
                            "transaction_id": null,
                            "amount_paid": "0.00",
                            "amount_remaining": "299.00",
                            "status": "processing",
                            "requires_approval": 0,
                            "address_id": 316,
                            "client_id": 196,
                            "company_id": 1,
                            "created_by": 1,
                            "updated_by": 1,
                            "created_at": "18-08-21",
                            "updated_at": "2021-08-18T04:34:30.000000Z"
                        },
                        {
                            "id": 91,
                            "order_number": "#00058",
                            "total": "1499.00",
                            "payment_when": "later",
                            "payment_method": null,
                            "type_of_wallet": null,
                            "transaction_id": null,
                            "amount_paid": "0.00",
                            "amount_remaining": "1499.00",
                            "status": "processing",
                            "requires_approval": 0,
                            "address_id": 316,
                            "client_id": 196,
                            "company_id": 1,
                            "created_by": 1,
                            "updated_by": 1,
                            "created_at": "18-08-21",
                            "updated_at": "2021-08-18T04:34:49.000000Z"
                        }
                    ]
                }
            },
            {
                "id": 90,
                "created_at": "18-08-21",
                "total": "299.00",
                "amount_paid": "0.00",
                "order_number": "#00057",
                "amount_remaining": "299.00",
                "status": "processing",
                "client_id": 196,
                "client": {
                    "id": 196,
                    "first_name": "Walid",
                    "last_name": "ksroo",
                    "mobile": "0101384880",
                    "email": "ksroo@gmail.com",
                    "debts": 42446,
                    "orders": [
                        {
                            "id": 88,
                            "order_number": "#00055",
                            "total": "648.00",
                            "payment_when": "later",
                            "payment_method": null,
                            "type_of_wallet": null,
                            "transaction_id": null,
                            "amount_paid": "0.00",
                            "amount_remaining": "648.00",
                            "status": "processing",
                            "requires_approval": 0,
                            "address_id": 316,
                            "client_id": 196,
                            "company_id": 1,
                            "created_by": 1,
                            "updated_by": 1,
                            "created_at": "18-08-21",
                            "updated_at": "2021-08-18T04:32:58.000000Z"
                        },
                        {
                            "id": 89,
                            "order_number": "#00056",
                            "total": "40000.00",
                            "payment_when": "later",
                            "payment_method": null,
                            "type_of_wallet": null,
                            "transaction_id": null,
                            "amount_paid": "0.00",
                            "amount_remaining": "40000.00",
                            "status": "processing",
                            "requires_approval": 0,
                            "address_id": 316,
                            "client_id": 196,
                            "company_id": 1,
                            "created_by": 1,
                            "updated_by": 1,
                            "created_at": "18-08-21",
                            "updated_at": "2021-08-18T04:34:10.000000Z"
                        },
                        {
                            "id": 90,
                            "order_number": "#00057",
                            "total": "299.00",
                            "payment_when": "later",
                            "payment_method": null,
                            "type_of_wallet": null,
                            "transaction_id": null,
                            "amount_paid": "0.00",
                            "amount_remaining": "299.00",
                            "status": "processing",
                            "requires_approval": 0,
                            "address_id": 316,
                            "client_id": 196,
                            "company_id": 1,
                            "created_by": 1,
                            "updated_by": 1,
                            "created_at": "18-08-21",
                            "updated_at": "2021-08-18T04:34:30.000000Z"
                        },
                        {
                            "id": 91,
                            "order_number": "#00058",
                            "total": "1499.00",
                            "payment_when": "later",
                            "payment_method": null,
                            "type_of_wallet": null,
                            "transaction_id": null,
                            "amount_paid": "0.00",
                            "amount_remaining": "1499.00",
                            "status": "processing",
                            "requires_approval": 0,
                            "address_id": 316,
                            "client_id": 196,
                            "company_id": 1,
                            "created_by": 1,
                            "updated_by": 1,
                            "created_at": "18-08-21",
                            "updated_at": "2021-08-18T04:34:49.000000Z"
                        }
                    ]
                }
            },
            {
                "id": 89,
                "created_at": "18-08-21",
                "total": "40000.00",
                "amount_paid": "0.00",
                "order_number": "#00056",
                "amount_remaining": "40000.00",
                "status": "processing",
                "client_id": 196,
                "client": {
                    "id": 196,
                    "first_name": "Walid",
                    "last_name": "ksroo",
                    "mobile": "0101384880",
                    "email": "ksroo@gmail.com",
                    "debts": 42446,
                    "orders": [
                        {
                            "id": 88,
                            "order_number": "#00055",
                            "total": "648.00",
                            "payment_when": "later",
                            "payment_method": null,
                            "type_of_wallet": null,
                            "transaction_id": null,
                            "amount_paid": "0.00",
                            "amount_remaining": "648.00",
                            "status": "processing",
                            "requires_approval": 0,
                            "address_id": 316,
                            "client_id": 196,
                            "company_id": 1,
                            "created_by": 1,
                            "updated_by": 1,
                            "created_at": "18-08-21",
                            "updated_at": "2021-08-18T04:32:58.000000Z"
                        },
                        {
                            "id": 89,
                            "order_number": "#00056",
                            "total": "40000.00",
                            "payment_when": "later",
                            "payment_method": null,
                            "type_of_wallet": null,
                            "transaction_id": null,
                            "amount_paid": "0.00",
                            "amount_remaining": "40000.00",
                            "status": "processing",
                            "requires_approval": 0,
                            "address_id": 316,
                            "client_id": 196,
                            "company_id": 1,
                            "created_by": 1,
                            "updated_by": 1,
                            "created_at": "18-08-21",
                            "updated_at": "2021-08-18T04:34:10.000000Z"
                        },
                        {
                            "id": 90,
                            "order_number": "#00057",
                            "total": "299.00",
                            "payment_when": "later",
                            "payment_method": null,
                            "type_of_wallet": null,
                            "transaction_id": null,
                            "amount_paid": "0.00",
                            "amount_remaining": "299.00",
                            "status": "processing",
                            "requires_approval": 0,
                            "address_id": 316,
                            "client_id": 196,
                            "company_id": 1,
                            "created_by": 1,
                            "updated_by": 1,
                            "created_at": "18-08-21",
                            "updated_at": "2021-08-18T04:34:30.000000Z"
                        },
                        {
                            "id": 91,
                            "order_number": "#00058",
                            "total": "1499.00",
                            "payment_when": "later",
                            "payment_method": null,
                            "type_of_wallet": null,
                            "transaction_id": null,
                            "amount_paid": "0.00",
                            "amount_remaining": "1499.00",
                            "status": "processing",
                            "requires_approval": 0,
                            "address_id": 316,
                            "client_id": 196,
                            "company_id": 1,
                            "created_by": 1,
                            "updated_by": 1,
                            "created_at": "18-08-21",
                            "updated_at": "2021-08-18T04:34:49.000000Z"
                        }
                    ]
                }
            },
            {
                "id": 88,
                "created_at": "18-08-21",
                "total": "648.00",
                "amount_paid": "0.00",
                "order_number": "#00055",
                "amount_remaining": "648.00",
                "status": "processing",
                "client_id": 196,
                "client": {
                    "id": 196,
                    "first_name": "Walid",
                    "last_name": "ksroo",
                    "mobile": "0101384880",
                    "email": "ksroo@gmail.com",
                    "debts": 42446,
                    "orders": [
                        {
                            "id": 88,
                            "order_number": "#00055",
                            "total": "648.00",
                            "payment_when": "later",
                            "payment_method": null,
                            "type_of_wallet": null,
                            "transaction_id": null,
                            "amount_paid": "0.00",
                            "amount_remaining": "648.00",
                            "status": "processing",
                            "requires_approval": 0,
                            "address_id": 316,
                            "client_id": 196,
                            "company_id": 1,
                            "created_by": 1,
                            "updated_by": 1,
                            "created_at": "18-08-21",
                            "updated_at": "2021-08-18T04:32:58.000000Z"
                        },
                        {
                            "id": 89,
                            "order_number": "#00056",
                            "total": "40000.00",
                            "payment_when": "later",
                            "payment_method": null,
                            "type_of_wallet": null,
                            "transaction_id": null,
                            "amount_paid": "0.00",
                            "amount_remaining": "40000.00",
                            "status": "processing",
                            "requires_approval": 0,
                            "address_id": 316,
                            "client_id": 196,
                            "company_id": 1,
                            "created_by": 1,
                            "updated_by": 1,
                            "created_at": "18-08-21",
                            "updated_at": "2021-08-18T04:34:10.000000Z"
                        },
                        {
                            "id": 90,
                            "order_number": "#00057",
                            "total": "299.00",
                            "payment_when": "later",
                            "payment_method": null,
                            "type_of_wallet": null,
                            "transaction_id": null,
                            "amount_paid": "0.00",
                            "amount_remaining": "299.00",
                            "status": "processing",
                            "requires_approval": 0,
                            "address_id": 316,
                            "client_id": 196,
                            "company_id": 1,
                            "created_by": 1,
                            "updated_by": 1,
                            "created_at": "18-08-21",
                            "updated_at": "2021-08-18T04:34:30.000000Z"
                        },
                        {
                            "id": 91,
                            "order_number": "#00058",
                            "total": "1499.00",
                            "payment_when": "later",
                            "payment_method": null,
                            "type_of_wallet": null,
                            "transaction_id": null,
                            "amount_paid": "0.00",
                            "amount_remaining": "1499.00",
                            "status": "processing",
                            "requires_approval": 0,
                            "address_id": 316,
                            "client_id": 196,
                            "company_id": 1,
                            "created_by": 1,
                            "updated_by": 1,
                            "created_at": "18-08-21",
                            "updated_at": "2021-08-18T04:34:49.000000Z"
                        }
                    ]
                }
            },
            {
                "id": 87,
                "created_at": "17-08-21",
                "total": "598.00",
                "amount_paid": "0.00",
                "order_number": "#00054",
                "amount_remaining": "598.00",
                "status": "processing",
                "client_id": 176,
                "client": {
                    "id": 176,
                    "first_name": "Sachin",
                    "last_name": "Jaiswal",
                    "mobile": "8098908098",
                    "email": "sachin@mailinator.com",
                    "debts": 598,
                    "orders": [
                        {
                            "id": 87,
                            "order_number": "#00054",
                            "total": "598.00",
                            "payment_when": "later",
                            "payment_method": null,
                            "type_of_wallet": null,
                            "transaction_id": null,
                            "amount_paid": "0.00",
                            "amount_remaining": "598.00",
                            "status": "processing",
                            "requires_approval": 0,
                            "address_id": 286,
                            "client_id": 176,
                            "company_id": 1,
                            "created_by": 21,
                            "updated_by": 21,
                            "created_at": "17-08-21",
                            "updated_at": "2021-08-17T15:23:21.000000Z"
                        }
                    ]
                }
            }
        ],
        "first_page_url": "http://csv.jithvar.com/api/v1/orders/all?page=1",
        "from": 1,
        "last_page": 1,
        "last_page_url": "http://csv.jithvar.com/api/v1/orders/all?page=1",
        "links": [
            {
                "url": null,
                "label": "&laquo; Previous",
                "active": false
            },
            {
                "url": "http://csv.jithvar.com/api/v1/orders/all?page=1",
                "label": "1",
                "active": true
            },
            {
                "url": null,
                "label": "Next &raquo;",
                "active": false
            }
        ],
        "next_page_url": null,
        "path": "http://csv.jithvar.com/api/v1/orders/all",
        "per_page": 10,
        "prev_page_url": null,
        "to": 5,
        "total": 5
    },
    "draw": 0
}
```


# Sales Report

**Endpoint:**  
`/api/v1/payments/reports/collection`

**Method:**  `POST`

**Headers:**
- `Authorization: Bearer {token}`
- `Accept: application/json`

**Payload:**
```json
{
  "tableColumns": [
    "created_at",
    "payment_method",
    "id",
    "client",
    "amount_remaining",
    "status",
    "reason_denied",
    "actions"
  ],
  "draw": 0,
  "length": 10,
  "column": 0,
  "dir": "desc",
  "created_at": "",
  "name": "",
  "amount_paid": "",
  "to": "",
  "from": "",
  "orderNumber": "",
  "payment_method": "",
  "status": ""
}
```
**Details of payload:**
- `draw`: If you are sending many requests while searching, increment this by one after every request and then compare the one returned by the API
- `length`: Length of paginated page
- `search`: Search string
- `field`: Field to be searched ['created_at', 'order_number', 'client', 'total', 'amount_paid', 'amount_remaining']
- `relationship`: is the field being searched a relation to another table [set to true if search field is client, otherwise set to false]
- `relationship_field`: If the field being searched is a relationship field, enter the field in another table that will be returned [set to 'first_name' if search field is client, otherwise set to '']
- `dir`: Direction to order by, either asc or desc

**Example Payload : Search to and from**
```json
{
    "tableColumns": [
        "created_at",
        "payment_method",
        "id",
        "client",
        "amount_remaining",
        "status",
        "reason_denied",
        "actions"
    ],
    "draw": 0,
    "length": 10,
    "column": 0,
    "dir": "desc",
    "created_at": "",
    "name": "",
    "amount_paid": "",
    "to": "2021-08-12",
    "from": "2021-08-18",
    "orderNumber": "",
    "payment_method": "",
    "status": ""
}
```

**Example Payload : Search specific date**
```json
{
    "tableColumns": [
        "created_at",
        "payment_method",
        "id",
        "client",
        "amount_remaining",
        "status",
        "reason_denied",
        "actions"
    ],
    "draw": 0,
    "length": 10,
    "column": 0,
    "dir": "desc",
    "created_at": "2021-08-18",
    "name": "",
    "amount_paid": "",
    "to": "",
    "from": "",
    "orderNumber": "",
    "payment_method": "",
    "status": ""
}
```

**Example Payload : Search order status**
```json
{
    "tableColumns": [
        "created_at",
        "payment_method",
        "id",
        "client",
        "amount_remaining",
        "status",
        "reason_denied",
        "actions"
    ],
    "draw": 0,
    "length": 10,
    "column": 0,
    "dir": "desc",
    "created_at": "",
    "name": "",
    "amount_paid": "",
    "to": "",
    "from": "",
    "orderNumber": "",
    "payment_method": "",
    "status": "pending"
}
```



**Sample response:**
```json
{
    "collections": {
        "current_page": 1,
        "data": [
            {
                "id": 80,
                "created_at": "18-08-21",
                "amount_paid": "41.00",
                "amount_remaining": "1417.00",
                "payment_method": "wallet",
                "order_id": 91,
                "status": "Approved",
                "reason_denied": null,
                "order": {
                    "id": 91,
                    "order_number": "#00058",
                    "total": "1499.00",
                    "payment_when": "later",
                    "payment_method": "wallet",
                    "type_of_wallet": "smilepay",
                    "transaction_id": "sf",
                    "amount_paid": "82.00",
                    "amount_remaining": "1417.00",
                    "status": "processing",
                    "requires_approval": 0,
                    "address_id": 316,
                    "client_id": 196,
                    "company_id": 1,
                    "created_by": 1,
                    "updated_by": 21,
                    "created_at": "18-08-21",
                    "updated_at": "2021-08-18T12:06:01.000000Z",
                    "client": {
                        "id": 196,
                        "first_name": "Walid",
                        "last_name": "ksroo",
                        "mobile": "0101384880",
                        "email": "ksroo@gmail.com",
                        "debts": 42364,
                        "orders": [
                            {
                                "id": 88,
                                "order_number": "#00055",
                                "total": "648.00",
                                "payment_when": "later",
                                "payment_method": null,
                                "type_of_wallet": null,
                                "transaction_id": null,
                                "amount_paid": "0.00",
                                "amount_remaining": "648.00",
                                "status": "processing",
                                "requires_approval": 0,
                                "address_id": 316,
                                "client_id": 196,
                                "company_id": 1,
                                "created_by": 1,
                                "updated_by": 1,
                                "created_at": "18-08-21",
                                "updated_at": "2021-08-18T04:32:58.000000Z"
                            },
                            {
                                "id": 89,
                                "order_number": "#00056",
                                "total": "40000.00",
                                "payment_when": "later",
                                "payment_method": null,
                                "type_of_wallet": null,
                                "transaction_id": null,
                                "amount_paid": "0.00",
                                "amount_remaining": "40000.00",
                                "status": "processing",
                                "requires_approval": 0,
                                "address_id": 316,
                                "client_id": 196,
                                "company_id": 1,
                                "created_by": 1,
                                "updated_by": 1,
                                "created_at": "18-08-21",
                                "updated_at": "2021-08-18T04:34:10.000000Z"
                            },
                            {
                                "id": 90,
                                "order_number": "#00057",
                                "total": "299.00",
                                "payment_when": "later",
                                "payment_method": null,
                                "type_of_wallet": null,
                                "transaction_id": null,
                                "amount_paid": "0.00",
                                "amount_remaining": "299.00",
                                "status": "processing",
                                "requires_approval": 0,
                                "address_id": 316,
                                "client_id": 196,
                                "company_id": 1,
                                "created_by": 1,
                                "updated_by": 1,
                                "created_at": "18-08-21",
                                "updated_at": "2021-08-18T04:34:30.000000Z"
                            },
                            {
                                "id": 91,
                                "order_number": "#00058",
                                "total": "1499.00",
                                "payment_when": "later",
                                "payment_method": "wallet",
                                "type_of_wallet": "smilepay",
                                "transaction_id": "sf",
                                "amount_paid": "82.00",
                                "amount_remaining": "1417.00",
                                "status": "processing",
                                "requires_approval": 0,
                                "address_id": 316,
                                "client_id": 196,
                                "company_id": 1,
                                "created_by": 1,
                                "updated_by": 21,
                                "created_at": "18-08-21",
                                "updated_at": "2021-08-18T12:06:01.000000Z"
                            }
                        ]
                    }
                }
            },
            {
                "id": 79,
                "created_at": "18-08-21",
                "amount_paid": "41.00",
                "amount_remaining": "1458.00",
                "payment_method": "wallet",
                "order_id": 91,
                "status": "Approved",
                "reason_denied": null,
                "order": {
                    "id": 91,
                    "order_number": "#00058",
                    "total": "1499.00",
                    "payment_when": "later",
                    "payment_method": "wallet",
                    "type_of_wallet": "smilepay",
                    "transaction_id": "sf",
                    "amount_paid": "82.00",
                    "amount_remaining": "1417.00",
                    "status": "processing",
                    "requires_approval": 0,
                    "address_id": 316,
                    "client_id": 196,
                    "company_id": 1,
                    "created_by": 1,
                    "updated_by": 21,
                    "created_at": "18-08-21",
                    "updated_at": "2021-08-18T12:06:01.000000Z",
                    "client": {
                        "id": 196,
                        "first_name": "Walid",
                        "last_name": "ksroo",
                        "mobile": "0101384880",
                        "email": "ksroo@gmail.com",
                        "debts": 42364,
                        "orders": [
                            {
                                "id": 88,
                                "order_number": "#00055",
                                "total": "648.00",
                                "payment_when": "later",
                                "payment_method": null,
                                "type_of_wallet": null,
                                "transaction_id": null,
                                "amount_paid": "0.00",
                                "amount_remaining": "648.00",
                                "status": "processing",
                                "requires_approval": 0,
                                "address_id": 316,
                                "client_id": 196,
                                "company_id": 1,
                                "created_by": 1,
                                "updated_by": 1,
                                "created_at": "18-08-21",
                                "updated_at": "2021-08-18T04:32:58.000000Z"
                            },
                            {
                                "id": 89,
                                "order_number": "#00056",
                                "total": "40000.00",
                                "payment_when": "later",
                                "payment_method": null,
                                "type_of_wallet": null,
                                "transaction_id": null,
                                "amount_paid": "0.00",
                                "amount_remaining": "40000.00",
                                "status": "processing",
                                "requires_approval": 0,
                                "address_id": 316,
                                "client_id": 196,
                                "company_id": 1,
                                "created_by": 1,
                                "updated_by": 1,
                                "created_at": "18-08-21",
                                "updated_at": "2021-08-18T04:34:10.000000Z"
                            },
                            {
                                "id": 90,
                                "order_number": "#00057",
                                "total": "299.00",
                                "payment_when": "later",
                                "payment_method": null,
                                "type_of_wallet": null,
                                "transaction_id": null,
                                "amount_paid": "0.00",
                                "amount_remaining": "299.00",
                                "status": "processing",
                                "requires_approval": 0,
                                "address_id": 316,
                                "client_id": 196,
                                "company_id": 1,
                                "created_by": 1,
                                "updated_by": 1,
                                "created_at": "18-08-21",
                                "updated_at": "2021-08-18T04:34:30.000000Z"
                            },
                            {
                                "id": 91,
                                "order_number": "#00058",
                                "total": "1499.00",
                                "payment_when": "later",
                                "payment_method": "wallet",
                                "type_of_wallet": "smilepay",
                                "transaction_id": "sf",
                                "amount_paid": "82.00",
                                "amount_remaining": "1417.00",
                                "status": "processing",
                                "requires_approval": 0,
                                "address_id": 316,
                                "client_id": 196,
                                "company_id": 1,
                                "created_by": 1,
                                "updated_by": 21,
                                "created_at": "18-08-21",
                                "updated_at": "2021-08-18T12:06:01.000000Z"
                            }
                        ]
                    }
                }
            }
        ],
        "first_page_url": "http://csv.jithvar.com/api/v1/payments/reports/collection?page=1",
        "from": 1,
        "last_page": 1,
        "last_page_url": "http://csv.jithvar.com/api/v1/payments/reports/collection?page=1",
        "links": [
            {
                "url": null,
                "label": "&laquo; Previous",
                "active": false
            },
            {
                "url": "http://csv.jithvar.com/api/v1/payments/reports/collection?page=1",
                "label": "1",
                "active": true
            },
            {
                "url": null,
                "label": "Next &raquo;",
                "active": false
            }
        ],
        "next_page_url": null,
        "path": "http://csv.jithvar.com/api/v1/payments/reports/collection",
        "per_page": 10,
        "prev_page_url": null,
        "to": 2,
        "total": 2
    },
    "draw": 0
}
```