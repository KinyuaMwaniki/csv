# Search By name or phone

**Endpoint:**  

`/api/v1/users/search-update-credit`

**Method:**  `POST`

**Headers:**

- `Authorization: Bearer {token}`
- `Accept: application/json`

**Payload:**

```json
{
    "draw": 0,
    "search": "Doe"
}
```

**Details of payload:**

- `draw`: If you are sending many requests while searching, increment this by one after every request and then compare the one returned by the API
- `search`: Search string

**Example Payload : Search for a name**

```json
{
    "draw": 0,
    "search": "Doe"
}
```

**Example Payload : Search for a phone**

```json
{
    "draw": 0,
    "search": "0742424245"
}
```

**Sample response:**
```json
{
    "users": [
        {
            "id": 30,
            "title": "Mr",
            "first_name": "John",
            "last_name": "Doe",
            "email": "nationsunday@mailinator.com",
            "username": "jdoe@doe.com",
            "phone": "074-242-4245",
            "role": {
                "id": 7,
                "name": "Commercial"
            },
            "credit": "20000.00",
            "credit_limit_start_date": null,
            "credit_limit_end_date": null,
            "company": {
                "id": 1,
                "name": "Baumbach-Kunze",
                "industry": "Apparel",
                "mobile": "+14404881961",
                "fax": "+17708161134",
                "email": "buckridge.josie@example.net",
                "address": "51889 Nettie Run",
                "address_2": "99099 Alford Locks",
                "city": "Brandyntown",
                "state": null,
                "country": {
                    "id": 133,
                    "iso": "MH",
                    "name": "MARSHALL ISLANDS",
                    "nicename": "Marshall Islands",
                    "iso3": "MHL",
                    "numcode": 584,
                    "phonecode": "692",
                    "status": 1,
                    "created_at": ""
                },
                "zip_code": "22124-3496",
                "status": 1,
                "creator": null,
                "updater": null
            },
            "photo": null,
            "locale": "en",
            "email_verified_at": "2021-08-30T08:02:54.000000Z",
            "active": 1,
            "force_pw_change": 0
        },
        {
            "id": 32,
            "title": "Mrs",
            "first_name": "Jane",
            "last_name": "Doe",
            "email": "janedoe@mailinator.com",
            "username": "mrsjanedoe",
            "phone": "042-313-4214",
            "role": {
                "id": 7,
                "name": "Commercial"
            },
            "credit": "2000.00",
            "credit_limit_start_date": "2021-09-03",
            "credit_limit_end_date": "2021-09-28",
            "company": {
                "id": 1,
                "name": "Baumbach-Kunze",
                "industry": "Apparel",
                "mobile": "+14404881961",
                "fax": "+17708161134",
                "email": "buckridge.josie@example.net",
                "address": "51889 Nettie Run",
                "address_2": "99099 Alford Locks",
                "city": "Brandyntown",
                "state": null,
                "country": {
                    "id": 133,
                    "iso": "MH",
                    "name": "MARSHALL ISLANDS",
                    "nicename": "Marshall Islands",
                    "iso3": "MHL",
                    "numcode": 584,
                    "phonecode": "692",
                    "status": 1,
                    "created_at": ""
                },
                "zip_code": "22124-3496",
                "status": 1,
                "creator": null,
                "updater": null
            },
            "photo": null,
            "locale": "en",
            "email_verified_at": null,
            "active": 0,
            "force_pw_change": 0
        }
    ],
    "draw": 0
}
```