# Paginated Deposits

**Endpoint:**  
`/api/v1/paginated-collections`

**Method:**  `POST`

**Headers:**
- `Authorization: Bearer {token}`
- `Accept: application/json`

**Payload:**
```json
{
  "draw": 0,
  "length": 10,
  "search": "",
  "column": 0,
  "dir": "asc"
}
```
**Details of payload:**
- `draw`: If you are sending many requests while searching, increment this by one after every request and then compare the one returned by the API
- `length`: Length of paginated page
- `search`: Search string
- `column`: Column to order the records by

**Example Payload : Search for any column**
```json
{
    "draw": 0,
    "length": 10,
    "search": "4132",
    "column": 0,
    "dir": "asc"
}
```

**Example Payload : Order by Date**
```json
{
    "draw": 0,
    "length": 10,
    "search": "",
    "column": 0,
    "dir": "desc"
}
```

**Example Payload : Order by Amount**
```json
{
    "draw": 0,
    "length": 10,
    "search": "",
    "column": 1,
    "dir": "asc"
}
```

**Sample response:**
```json
{
    "collections": {
        "current_page": 1,
        "data": [
            {
                "id": 24,
                "date": "2021-08-01",
                "amount": "4132.00",
                "approved": 0,
                "denied": 1,
                "reason_denied": "What about this reason",
                "status": "Pending",
                "photo": {
                    "id": 195,
                    "reference_id": 24,
                    "reference_type": "App\\Models\\UserCash",
                    "name": "6",
                    "force_download": 1,
                    "file_path": "user_cash/6_1628576555.jpeg"
                }
            }
        ],
        "first_page_url": "http://csv.test:8080/api/v1/paginated-collections?page=1",
        "from": 1,
        "last_page": 1,
        "last_page_url": "http://csv.test:8080/api/v1/paginated-collections?page=1",
        "links": [
            {
                "url": null,
                "label": "&laquo; Previous",
                "active": false
            },
            {
                "url": "http://csv.test:8080/api/v1/paginated-collections?page=1",
                "label": "1",
                "active": true
            },
            {
                "url": null,
                "label": "Next &raquo;",
                "active": false
            }
        ],
        "next_page_url": null,
        "path": "http://csv.test:8080/api/v1/paginated-collections",
        "per_page": 10,
        "prev_page_url": null,
        "to": 1,
        "total": 1
    },
    "draw": 0
}
```