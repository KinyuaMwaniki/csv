# Create

**Endpoint:**  
`/api/v1/user-cash`

**Method:** `POST`

**Headers:**

- `Authorization: Bearer {token}`
- `Accept: application/json`

**Payload:**

-   `amount`: '1000',
-   `date`: '2021-08-20'
-   `amount`: 'file',

**Validation Rules:**

-   `amount`: 'required',
-   `date`: 'required''
-   `amount`: 'required', 'mimes:jpeg,jpg,png,gif', 'max:3000,

**Sample Success Response:**

```json
{
    "id": 27,
    "date": "2021-08-20",
    "amount": "1000",
    "created_at": "20-08-21",
    "updated_at": "20-08-21"
}
```