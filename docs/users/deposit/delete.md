# Delete

**Endpoint:**  
`/api/v1/user-cash/1`

**Method:**  `DELETE`

**Headers:**
- `Authorization: Bearer {token}`
- `Accept: application/json`

**Success Response Status:**
204