# Login

**Endpoint:**  
`/api/v1/login`

**Method:** `POST`

**Headers:**

-   `Authorization: Bearer {token}`
-   `Accept: application/json`

**Payload:**

```json
{
    "email": "admin@crm.test",
    "password": "password2"
}
```

**Validation Rules:**

-   `email`: required
-   `password`: required

**Sample Success Response:**

```json
{
    "token": "19|OyqlJhpUXstOZK7Yylh0Nq2bIucd1keLB3MqA1s2",

    "user": {
        "id": 1,

        "title": "Mr",

        "first_name": "Crm",

        "last_name": "Administrator",

        "email": "admin@crm.test",

        "username": "admin",

        "phone": "+2551234566",

        "role": {
            "id": 1,

            "name": "Administrator"
        },

        "company": {
            "id": 1,

            "name": "Baumbach-Kunze",

            "industry": "Apparel",

            "mobile": "+14404881961",

            "fax": "+17708161134",

            "email": "buckridge.josie@example.net",

            "address": "51889 Nettie Run",

            "address_2": "99099 Alford Locks",

            "city": "Brandyntown",

            "state": null,

            "zip_code": "22124-3496",

            "status": 1,

            "creator": null,

            "updater": null
        },

        "photo": {
            "id": 25,

            "original_name": null,

            "name": "Costa Rican Frog",

            "force_download": 0,

            "file_path": "user_photos/Costa Rican Frog_1625728878.jpg",

            "created_at": "08-07-2021"
        },

        "email_verified_at": "2021-07-06T15:27:51.000000Z",

        "active": 1,

        "force_pw_change": 0
    }
}
```

**Detail of Success Response:**

-   `token`: Detail: Contains a JWT token for authentication

# Get User Details

**Endpoint:**  
`/api/v1/user/info`

**Method:** `GET`

**Headers:**

-   `Authorization: Bearer {token}`
-   `Accept: application/json`

**Payload:** N/A

**Validation Rules:** N/A

**Sample Success Response:**

```json
{
    "user": {
        "id": 1,
        "title": "Mr",
        "first_name": "Crm",
        "last_name": "Administrator",
        "email": "admin@crm.test",
        "username": "admin",
        "phone": "+2551234566",
        "user_role": 1,
        "company_id": 1,
        "photo": null,
        "email_verified_at": "2021-07-06T15:27:51.000000Z",
        "active": 1,
        "force_pw_change": 0,
        "created_at": "2021-07-06T15:27:51.000000Z",
        "updated_at": "2021-07-12T06:48:34.000000Z"
    }
}
```

**Detail of Success Response:**

-   `token`: Detail: Contains a JWT token for authentication

# Request OTP (Forgot Password)

**Endpoint:**  
`/api/v1/forgot-password`

**Method:** `POST`

**Headers:**

-   `Accept: application/json`

**Payload:**

```json
{
    "email": "admin@crm.test"
}
```

**Validation Rules:**

-   `email`: required

**Sample Success Response:**

```json
{
    "message": "Successfully reset password, check your email"
}
```

**Sample User Not Found Response:**

```json
{
    "message": "User Not found"
}
```

**Detail User Not Found Response:**

-   `status`: 404

# Change Password with OTP

**Endpoint:**  
`/api/v1/change-password-otp`

**Method:** `POST`

**Headers:**

-   `Accept: application/json`

**Payload:**

```json
{
    "email": "admin@crm.test",
    "otp": "cRLqi0AW",
    "password": "password2",
    "password_confirmation": "password2"
}
```

**Validation Rules:**

-   `email`: required
-   `otp`: required, 8 characters
-   `password`: required, confirmed, min: 8 characters

**Sample Success Response:**
```json
{
    "token": "64|khRDZbrLyJCNDGREiXotSogKvURNEapz2v6j3FXJ"
}
```

**Sample User Not Found Response:**

```json
{
    "message": "User Not found"
}
```

**Detail User Not Found Response:**
-   `status`: 404

# Change password

**Endpoint:**  
`/api/v1/change-password`

**Method:** `POST`

**Headers:**

-   `Authorization: Bearer {token}`
-   `Accept: application/json`

**Payload:**

```json
{
    "password": "password2",
    "password_confirmation": "password2"
}
```

**Validation Rules:**

-   `password`: required
-   `password_confirmation`: required

**Sample Success Response:**

```json
{
    "token": "22|Rsl3EDXSS9cd1duSPk9tJyZoKaI635geHTG6vwci"
}
```

**Detail of Success Response:**

-   `token`: Detail: Contains a JWT token for authentication
