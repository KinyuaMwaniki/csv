# Get Numbers

**Endpoint:**  

`/api/v1/dashboard`

**Method:**  `GET`

**Headers:**

- `Authorization: Bearer {token}`
- `Accept: application/json`

**Sample response:**

```json
{
    "clientsCount": 8,
    "clientsThisMonth": 1,
    "month": "08",
    "totalDebt": 1397.48,
    "totalPayment": 1322.82
}
```