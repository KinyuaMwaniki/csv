# Monthly Debt Collection

**Endpoint:**  
`/api/v1/charts/collections/monthly`

**Method:**  `POST`

**Headers:**

- `Authorization: Bearer {token}`
- `Accept: application/json`

**Payload:**
```json
{
    "from": "2021-01-01",
    "to": "2021-08-30"
}
```

**Sample response:**
```json
{
    "monthlyCollections": [
        {
            "total": "0.00",
            "months": "January 2021",
            "monthKey": "01"
        },
        {
            "total": "0.00",
            "months": "February 2021",
            "monthKey": "02"
        },
        {
            "total": "0.00",
            "months": "March 2021",
            "monthKey": "03"
        },
        {
            "total": "0.00",
            "months": "April 2021",
            "monthKey": "04"
        },
        {
            "total": "0.00",
            "months": "May 2021",
            "monthKey": "05"
        },
        {
            "total": "0.00",
            "months": "June 2021",
            "monthKey": "06"
        },
        {
            "total": "0.00",
            "months": "July 2021",
            "monthKey": "07"
        },
        {
            "total": "90422.00",
            "months": "August 2021",
            "monthKey": "08"
        }
    ]
}
```