# Daily Debt Collection

**Endpoint:**  
`/api/v1/charts/collections/daily`

**Method:**  `POST`

**Headers:**

- `Authorization: Bearer {token}`
- `Accept: application/json`

**Payload:**
```json
{
    "from": "2021-07-26",
    "to": "2021-08-26"
}
```

**Sample response:**
```json
{
    "dailyCollections": [
        {
            "date": "2021-08-19",
            "total": "41.00"
        },
        {
            "date": "2021-08-13",
            "total": "41.00"
        },
        {
            "date": "2021-08-11",
            "total": "72.00"
        },
        {
            "date": "2021-08-10",
            "total": "223.47"
        },
        {
            "date": "2021-08-09",
            "total": "53.00"
        },
        {
            "date": "2021-08-07",
            "total": "151.00"
        },
        {
            "date": "2021-08-06",
            "total": "141.00"
        },
        {
            "date": "2021-08-05",
            "total": "2.00"
        }
    ]
}
```