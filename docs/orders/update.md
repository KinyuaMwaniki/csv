# Update

**Endpoint:**  

`/api/v1/orders/{{id}}`

**Method:**  `PUT`

**Headers:**

- `Authorization: Bearer {token}`
- `Accept: application/json`

**Payload:**

```json
{
    "id": "155",
    "total": 75.36,
    "payment_method": "wallet",
    "type_of_wallet": "smilepay",
    "transaction_id": "0001",
    "amount_paid": 2,
    "amount_remaining": 73.36,
    "address_id": 278,
    "client_id": 44,
    "cart": [
        {
            "id": 250,
            "quantity": 1,
            "product_id": 14
        },
        {
            "id": 251,
            "quantity": 1,
            "product_id": 6
        }
    ]
}
```
	
**Validation rules:**

- `total`: 'required', 'numeric', 'min:0'
- `payment_method`: 'required'
- `type_of_wallet`: 'required_if:payment_method,wallet'
- `transaction_id`: 'required_if:payment_method,wallet'
- `amount_paid`: 'numeric', 'required', 'min:0'
- `amount_remaining`: 'numeric', 'required', 'min:0'
- `address_id`: 'required'
- `client_id`: 'required'
- `cart`: 'required'


**Sample success response:**

```json
{
    "id": 155,
    "order_number": "#00065",
    "total": 75.36,
    "payment_method": "wallet",
    "type_of_wallet": "smilepay",
    "transaction_id": "0001",
    "amount_paid": 4,
    "amount_remaining": 71.36,
    "status": "processing",
    "address": {
        "id": 278,
        "street_address": "",
        "zip_code": "",
        "locality": "",
        "city": "Las Vegas",
        "state": "NV",
        "country": "United States",
        "is_default": 1,
        "is_billing": 0,
        "company": null
    },
    "client": {
        "id": 44,
        "first_name": "Felix",
        "last_name": "Mwaniki",
        "name": "Felix Mwaniki",
        "photo": {
            "id": 87,
            "original_name": null,
            "name": "Costa Rican Frog",
            "force_download": 0,
            "file_path": "client_photos/Costa Rican Frog_1626163371.jpg",
            "created_at": "13-07-21"
        },
        "mobile": "4444444444",
        "email": "333@333.333",
        "shipping_addresses": [
            {
                "id": 278,
                "client_id": 44,
                "street_address": "",
                "zip_code": "",
                "locality": "",
                "city": "Las Vegas",
                "state": "NV",
                "country": "United States",
                "is_default": 1,
                "is_billing": 0,
                "company_id": 1
            },
            {
                "id": 279,
                "client_id": 44,
                "street_address": "",
                "zip_code": "78597",
                "locality": "",
                "city": "South Padre Island",
                "state": "TX",
                "country": "United States",
                "is_default": 0,
                "is_billing": 0,
                "company_id": 1
            }
        ]
    },
    "products": [
        {
            "id": 250,
            "order": {
                "id": 155
            },
            "product": {
                "id": 14,
                "name": "Impedit proident masf asdfassd werrwer asfsdfsfs werwer  asfsdfswe werq",
                "description": null,
                "model": "Aut animi enim illu",
                "sku": "Dolor fugit commodi",
                "price": "17.11",
                "currency": {
                    "id": 11,
                    "name": "Kenya Shillings",
                    "code": "KES",
                    "symbol": "KSH",
                    "status": 1,
                    "created_at": "2021-07-12 14:04:13"
                },
                "quantity": 73,
                "minimum": 68,
                "subtract": 0,
                "stock_status": "2-3 Days",
                "stock_status_id": 1,
                "shipping": 0,
                "date_available": "2004-09-24",
                "status": 0,
                "sort_order": 71,
                "manufacturer": null,
                "categories": [],
                "attributes": [],
                "photos": [],
                "creator": {
                    "id": 1,
                    "title": null,
                    "first_name": "Crm",
                    "last_name": "Administrator",
                    "email": null,
                    "username": null,
                    "phone": null,
                    "role": null,
                    "credit": null,
                    "company": null,
                    "photo": {
                        "id": 25,
                        "original_name": null,
                        "name": "Costa Rican Frog",
                        "force_download": 0,
                        "file_path": "user_photos/Costa Rican Frog_1625728878.jpg",
                        "created_at": "08-07-21"
                    },
                    "locale": null,
                    "email_verified_at": null,
                    "active": 0,
                    "force_pw_change": 0
                },
                "updater": {
                    "id": 1,
                    "title": null,
                    "first_name": "Crm",
                    "last_name": "Administrator",
                    "email": null,
                    "username": null,
                    "phone": null,
                    "role": null,
                    "credit": null,
                    "company": null,
                    "photo": {
                        "id": 25,
                        "original_name": null,
                        "name": "Costa Rican Frog",
                        "force_download": 0,
                        "file_path": "user_photos/Costa Rican Frog_1625728878.jpg",
                        "created_at": "08-07-21"
                    },
                    "locale": null,
                    "email_verified_at": null,
                    "active": 0,
                    "force_pw_change": 0
                },
                "created_at": "13-07-21",
                "updated_at": "07-08-21",
                "updated": true
            },
            "quantity": 1,
            "currency": null,
            "price": "17.11",
            "total": "17.11",
            "company": {
                "id": 1,
                "name": "Baumbach-Kunze",
                "industry": null,
                "mobile": null,
                "fax": null,
                "email": null,
                "address": null,
                "address_2": null,
                "city": null,
                "state": null,
                "country": null,
                "zip_code": null,
                "status": null,
                "creator": null,
                "updater": null
            },
            "created_by": {
                "id": 1,
                "title": null,
                "first_name": "Crm",
                "last_name": "Administrator",
                "email": null,
                "username": null,
                "phone": null,
                "role": null,
                "credit": null,
                "company": null,
                "photo": {
                    "id": 25,
                    "original_name": null,
                    "name": "Costa Rican Frog",
                    "force_download": 0,
                    "file_path": "user_photos/Costa Rican Frog_1625728878.jpg",
                    "created_at": "08-07-21"
                },
                "locale": null,
                "email_verified_at": null,
                "active": 0,
                "force_pw_change": 0
            },
            "updated_by": null,
            "created_at": "26-08-21",
            "updated_at": "26-08-21"
        },
        {
            "id": 251,
            "order": {
                "id": 155
            },
            "product": {
                "id": 6,
                "name": "Dolor officiis ipsum",
                "description": "<p>Descripton</p>",
                "model": "Atque reiciendis rer",
                "sku": "Architecto occaecat",
                "price": "58.25",
                "currency": {
                    "id": 4,
                    "name": "Algerian Dinar",
                    "code": "DZD",
                    "symbol": null,
                    "status": 1,
                    "created_at": "2021-07-06 18:27:51"
                },
                "quantity": 36,
                "minimum": 40,
                "subtract": 1,
                "stock_status": "In Stock",
                "stock_status_id": 2,
                "shipping": 0,
                "date_available": "1983-10-11",
                "status": 0,
                "sort_order": 44,
                "manufacturer": {
                    "id": 3,
                    "name": "Felix",
                    "created_at": "12-07-21",
                    "updated_at": "12-07-21"
                },
                "categories": [
                    {
                        "id": 1,
                        "parent_id": null,
                        "name": "Women's Clothes",
                        "code": "001",
                        "slug": "1-womens-clothers",
                        "status": 1,
                        "company_id": 1,
                        "created_by": 1,
                        "updated_by": 1,
                        "created_at": "2021-07-06T18:29:38.000000Z",
                        "updated_at": "2021-07-09T14:50:55.000000Z",
                        "full_name": "Women's Clothes",
                        "pivot": {
                            "product_id": 6,
                            "category_id": 1,
                            "created_at": "2021-08-13T10:18:04.000000Z",
                            "updated_at": "2021-08-13T10:18:04.000000Z"
                        },
                        "parent_category": null
                    }
                ],
                "attributes": [
                    {
                        "id": 1,
                        "name": "Weight",
                        "company_id": 1,
                        "created_at": "2021-07-06T18:28:26.000000Z",
                        "updated_at": "2021-07-06T18:28:26.000000Z",
                        "pivot": {
                            "product_id": 6,
                            "attribute_id": 1,
                            "value": "3",
                            "unit_id": 2,
                            "unit_name": "kg",
                            "id": 26,
                            "created_at": "2021-08-13T10:18:04.000000Z",
                            "updated_at": "2021-08-13T10:18:04.000000Z"
                        }
                    },
                    {
                        "id": 1,
                        "name": "Weight",
                        "company_id": 1,
                        "created_at": "2021-07-06T18:28:26.000000Z",
                        "updated_at": "2021-07-06T18:28:26.000000Z",
                        "pivot": {
                            "product_id": 6,
                            "attribute_id": 1,
                            "value": "4",
                            "unit_id": 1,
                            "unit_name": "g",
                            "id": 27,
                            "created_at": "2021-08-13T10:18:04.000000Z",
                            "updated_at": "2021-08-13T10:18:04.000000Z"
                        }
                    },
                    {
                        "id": 3,
                        "name": "Height",
                        "company_id": 1,
                        "created_at": "2021-07-12T14:20:50.000000Z",
                        "updated_at": "2021-07-12T14:20:50.000000Z",
                        "pivot": {
                            "product_id": 6,
                            "attribute_id": 3,
                            "value": "5",
                            "unit_id": 7,
                            "unit_name": "Cm",
                            "id": 28,
                            "created_at": "2021-08-13T10:18:04.000000Z",
                            "updated_at": "2021-08-13T10:18:04.000000Z"
                        }
                    },
                    {
                        "id": 3,
                        "name": "Height",
                        "company_id": 1,
                        "created_at": "2021-07-12T14:20:50.000000Z",
                        "updated_at": "2021-07-12T14:20:50.000000Z",
                        "pivot": {
                            "product_id": 6,
                            "attribute_id": 3,
                            "value": "4",
                            "unit_id": 9,
                            "unit_name": "Feet",
                            "id": 29,
                            "created_at": "2021-08-13T10:18:04.000000Z",
                            "updated_at": "2021-08-13T10:18:04.000000Z"
                        }
                    },
                    {
                        "id": 4,
                        "name": "Color",
                        "company_id": 1,
                        "created_at": "2021-08-13T10:17:40.000000Z",
                        "updated_at": "2021-08-13T10:17:40.000000Z",
                        "pivot": {
                            "product_id": 6,
                            "attribute_id": 4,
                            "value": "red",
                            "unit_id": null,
                            "unit_name": null,
                            "id": 30,
                            "created_at": "2021-08-13T10:18:04.000000Z",
                            "updated_at": "2021-08-13T10:18:04.000000Z"
                        }
                    },
                    {
                        "id": 4,
                        "name": "Color",
                        "company_id": 1,
                        "created_at": "2021-08-13T10:17:40.000000Z",
                        "updated_at": "2021-08-13T10:17:40.000000Z",
                        "pivot": {
                            "product_id": 6,
                            "attribute_id": 4,
                            "value": "yellow",
                            "unit_id": null,
                            "unit_name": null,
                            "id": 31,
                            "created_at": "2021-08-13T10:18:04.000000Z",
                            "updated_at": "2021-08-13T10:18:04.000000Z"
                        }
                    }
                ],
                "photos": [
                    {
                        "id": 39,
                        "original_name": null,
                        "name": "kenyan_fist",
                        "force_download": 0,
                        "file_path": "product_images/kenyan_fist_1625823590.jpg",
                        "created_at": "09-07-21"
                    },
                    {
                        "id": 110,
                        "original_name": null,
                        "name": "ew",
                        "force_download": 0,
                        "file_path": "product_images/ew_1626181076.jpg",
                        "created_at": "13-07-21"
                    }
                ],
                "creator": {
                    "id": 1,
                    "title": null,
                    "first_name": "Crm",
                    "last_name": "Administrator",
                    "email": null,
                    "username": null,
                    "phone": null,
                    "role": null,
                    "credit": null,
                    "company": null,
                    "photo": {
                        "id": 25,
                        "original_name": null,
                        "name": "Costa Rican Frog",
                        "force_download": 0,
                        "file_path": "user_photos/Costa Rican Frog_1625728878.jpg",
                        "created_at": "08-07-21"
                    },
                    "locale": null,
                    "email_verified_at": null,
                    "active": 0,
                    "force_pw_change": 0
                },
                "updater": {
                    "id": 1,
                    "title": null,
                    "first_name": "Crm",
                    "last_name": "Administrator",
                    "email": null,
                    "username": null,
                    "phone": null,
                    "role": null,
                    "credit": null,
                    "company": null,
                    "photo": {
                        "id": 25,
                        "original_name": null,
                        "name": "Costa Rican Frog",
                        "force_download": 0,
                        "file_path": "user_photos/Costa Rican Frog_1625728878.jpg",
                        "created_at": "08-07-21"
                    },
                    "locale": null,
                    "email_verified_at": null,
                    "active": 0,
                    "force_pw_change": 0
                },
                "created_at": "13-07-21",
                "updated_at": "28-07-21",
                "updated": true
            },
            "quantity": 1,
            "currency": null,
            "price": "58.25",
            "total": "58.25",
            "company": {
                "id": 1,
                "name": "Baumbach-Kunze",
                "industry": null,
                "mobile": null,
                "fax": null,
                "email": null,
                "address": null,
                "address_2": null,
                "city": null,
                "state": null,
                "country": null,
                "zip_code": null,
                "status": null,
                "creator": null,
                "updater": null
            },
            "created_by": {
                "id": 1,
                "title": null,
                "first_name": "Crm",
                "last_name": "Administrator",
                "email": null,
                "username": null,
                "phone": null,
                "role": null,
                "credit": null,
                "company": null,
                "photo": {
                    "id": 25,
                    "original_name": null,
                    "name": "Costa Rican Frog",
                    "force_download": 0,
                    "file_path": "user_photos/Costa Rican Frog_1625728878.jpg",
                    "created_at": "08-07-21"
                },
                "locale": null,
                "email_verified_at": null,
                "active": 0,
                "force_pw_change": 0
            },
            "updated_by": null,
            "created_at": "26-08-21",
            "updated_at": "26-08-21"
        }
    ],
    "company": {
        "id": 1,
        "name": "Baumbach-Kunze",
        "industry": "Apparel",
        "mobile": "+14404881961",
        "fax": "+17708161134",
        "email": "buckridge.josie@example.net",
        "address": "51889 Nettie Run",
        "address_2": "99099 Alford Locks",
        "city": "Brandyntown",
        "state": null,
        "country": {
            "id": 133,
            "iso": "MH",
            "name": "MARSHALL ISLANDS",
            "nicename": "Marshall Islands",
            "iso3": "MHL",
            "numcode": 584,
            "phonecode": "692",
            "status": 1,
            "created_at": ""
        },
        "zip_code": "22124-3496",
        "status": 1,
        "creator": null,
        "updater": null
    },
    "created_by": {
        "id": 1,
        "title": null,
        "first_name": "Crm",
        "last_name": "Administrator",
        "email": null,
        "username": null,
        "phone": null,
        "role": null,
        "credit": null,
        "company": null,
        "photo": {
            "id": 25,
            "original_name": null,
            "name": "Costa Rican Frog",
            "force_download": 0,
            "file_path": "user_photos/Costa Rican Frog_1625728878.jpg",
            "created_at": "08-07-21"
        },
        "locale": null,
        "email_verified_at": null,
        "active": 0,
        "force_pw_change": 0
    },
    "updated_by": null,
    "created_at": "26-08-21",
    "updated_at": "28-08-21"
}
```