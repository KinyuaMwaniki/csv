# Get Invoice Pdf Link

**Endpoint:**  

`/api/v1/orders/pdf`

**Method:**  `POST`

**Headers:**

- `Authorization: Bearer {token}`
- `Accept: application/json`

**Payload:**

```json
{
    "id": 153
}
```
**Details of payload:**

- `id`: Invoice ID

**Sample response:**

```json
{
    "url": "/storage/invoices/invoice_1629535020.pdf"
}
```