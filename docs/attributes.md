# Search Add Money

**Endpoint:**  
`/api/v1/clients/paginated-attribute-types`

**Method:**  `POST`

**Headers:**
- `Authorization: Bearer {token}`
- `Accept: application/json`

**Payload:**
```json
{
    "draw": 0,
    "length": 10,
    "search": "",
    "column": 0,
    "dir": "asc"
}
```
**Details of payload:**
- `draw`: If you sending many requests while searching, increment this by one after every request and then compare the one returned by the API
- `length`: Length of paginated page
- `search`: Search string
- `column`: Colunm index to search. The columns array are ['name', 'units', 'actions'];
- `dir`: Direction to order by, either asc or desc

**Sample response:**
```json
{
    "attribute_types": {
        "current_page": 1,
        "data": [
            {
                "id": 3,
                "name": "Height",
                "units": [
                    {
                        "id": 7,
                        "name": "Cm",
                        "company_id": 1,
                        "attribute_type_id": 3,
                        "created_at": "2021-07-12T11:22:42.000000Z",
                        "updated_at": "2021-07-12T11:22:42.000000Z"
                    },
                    {
                        "id": 8,
                        "name": "M",
                        "company_id": 1,
                        "attribute_type_id": 3,
                        "created_at": "2021-07-12T11:22:42.000000Z",
                        "updated_at": "2021-07-12T11:22:42.000000Z"
                    },
                    {
                        "id": 9,
                        "name": "Feet",
                        "company_id": 1,
                        "attribute_type_id": 3,
                        "created_at": "2021-07-12T11:22:42.000000Z",
                        "updated_at": "2021-07-12T11:22:42.000000Z"
                    }
                ]
            },
            {
                "id": 1,
                "name": "Weight",
                "units": [
                    {
                        "id": 1,
                        "name": "g",
                        "company_id": 1,
                        "attribute_type_id": 1,
                        "created_at": "2021-07-06T15:28:26.000000Z",
                        "updated_at": "2021-07-06T15:28:26.000000Z"
                    },
                    {
                        "id": 2,
                        "name": "kg",
                        "company_id": 1,
                        "attribute_type_id": 1,
                        "created_at": "2021-07-06T15:28:26.000000Z",
                        "updated_at": "2021-07-06T15:28:26.000000Z"
                    }
                ]
            }
        ],
        "first_page_url": "http://csv.test:8080/api/v1/paginated-attribute-types?page=1",
        "from": 1,
        "last_page": 1,
        "last_page_url": "http://csv.test:8080/api/v1/paginated-attribute-types?page=1",
        "links": [
            {
                "url": null,
                "label": "&laquo; Previous",
                "active": false
            },
            {
                "url": "http://csv.test:8080/api/v1/paginated-attribute-types?page=1",
                "label": "1",
                "active": true
            },
            {
                "url": null,
                "label": "Next &raquo;",
                "active": false
            }
        ],
        "next_page_url": null,
        "path": "http://csv.test:8080/api/v1/paginated-attribute-types",
        "per_page": 10,
        "prev_page_url": null,
        "to": 2,
        "total": 2
    },
    "draw": 0
}
```