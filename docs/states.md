# Create

**Endpoint:**  
`/api/v1/states`

**Method:**  `POST`

**Headers:**
- `Authorization: Bearer {token}`
- `Accept: application/json`

**Payload:**
```json
{
    "country_id": 100,
    "code": "SOs",
    "name": "Mombasa"    
}
```
	
**Details of Payload:**
- `country_id`: ID of country that the state belongs to. From Get Countries API
- `code`: Code of state 

**Validation Rules:**
- `name`: required, unique
- `country_id`: required
- `code`: required, max:4, unique

**Sample Success Response:**
```json
{
    "id": 30,
    "code": "SOb",
    "name": "Mombasad",
    "country": {
        "id": 100,
        "iso": "ID",
        "name": "INDONESIA",
        "nicename": "Indonesia",
        "iso3": "IDN",
        "numcode": 360,
        "phonecode": "62",
        "status": 1,
        "created_at": ""
    },
    "status": 0
}
```
**Success Response Status:**
201

# Update

**Endpoint:**  
`/api/v1/states/30`

**Method:**  `PUT`

**Headers:**
- `Authorization: Bearer {token}`
- `Accept: application/json`

**Payload:**
```json
{
    "country_id": 100,
    "code": "SOb",
    "name": "Mombasa - ed"    
}
```
	
**Details of payload:**
- `country_id`: ID of country that the state belongs to. From Get Countries API
- `code`: Code of state 

**Validation rules:**
- `name`: unique
- `code`: max:4, unique

**Sample success response:**
```json
{
    "id": 30,
    "code": "SOb",
    "name": "Mombasa - ed",
    "country": {
        "id": 100,
        "iso": "ID",
        "name": "INDONESIA",
        "nicename": "Indonesia",
        "iso3": "IDN",
        "numcode": 360,
        "phonecode": "62",
        "status": 1,
        "created_at": ""
    },
    "status": 1
}
```

# Update Status

**Endpoint:**  
`/api/v1/states/30`

**Method:**  `PUT`

**Headers:**
- `Authorization: Bearer {token}`
- `Accept: application/json`

**Payload:**
```json
{
    "status": 0
}
```

**Details of payload:**
- `status`: New status of state 

**Sample success response:**
```json
{
    "id": 30,
    "code": "SOb",
    "name": "Mombasa - ed",
    "country": {
        "id": 100,
        "iso": "ID",
        "name": "INDONESIA",
        "nicename": "Indonesia",
        "iso3": "IDN",
        "numcode": 360,
        "phonecode": "62",
        "status": 1,
        "created_at": ""
    },
    "status": 1
}
```

# Get Paginated States

**Endpoint:**  
`/api/v1/paginated-states?page=1`

**Method:**  `POST`

**Headers:**
- `Authorization: Bearer {token}`
- `Accept: application/json`

**Payload:**
```json
{
    "draw": 0,
    "length": 10,
    "search": "",
    "column": 1,
    "dir": "asc"
}
```

**Details of payload:**
- `draw`: We use this variable while searching since many requests are sent as user types search string. This variable is returned by the endpoint. We use it to confirm that the reponse is for the current string
- `length`: Length of page
- `search`: String to search
- `column`: Column to order by, from 0 index ['country', 'code', 'name', 'status', 'actions']
- `dir`: Direction to order by

**Sample success response:**
```json
{
    "states": {
        "current_page": 1,
        "data": [
            {
                "id": 2,
                "country_id": 99,
                "code": "AR",
                "name": "Arunachal Pradesh",
                "status": 1,
                "country": {
                    "id": 99,
                    "iso": "IN",
                    "name": "INDIA",
                    "nicename": "India",
                    "iso3": "IND",
                    "numcode": 356,
                    "phonecode": 91,
                    "status": 1,
                    "created_at": null,
                    "updated_at": null
                }
            },
            {
                "id": 3,
                "country_id": 99,
                "code": "AS",
                "name": "Assam",
                "status": 1,
                "country": {
                    "id": 99,
                    "iso": "IN",
                    "name": "INDIA",
                    "nicename": "India",
                    "iso3": "IND",
                    "numcode": 356,
                    "phonecode": 91,
                    "status": 1,
                    "created_at": null,
                    "updated_at": null
                }
            },
            {
                "id": 4,
                "country_id": 99,
                "code": "BR",
                "name": "Bihar",
                "status": 1,
                "country": {
                    "id": 99,
                    "iso": "IN",
                    "name": "INDIA",
                    "nicename": "India",
                    "iso3": "IND",
                    "numcode": 356,
                    "phonecode": 91,
                    "status": 1,
                    "created_at": null,
                    "updated_at": null
                }
            },
            {
                "id": 5,
                "country_id": 99,
                "code": "CG",
                "name": "Chhattisgarh",
                "status": 1,
                "country": {
                    "id": 99,
                    "iso": "IN",
                    "name": "INDIA",
                    "nicename": "India",
                    "iso3": "IND",
                    "numcode": 356,
                    "phonecode": 91,
                    "status": 1,
                    "created_at": null,
                    "updated_at": null
                }
            },
            {
                "id": 6,
                "country_id": 99,
                "code": "GA",
                "name": "Goa",
                "status": 1,
                "country": {
                    "id": 99,
                    "iso": "IN",
                    "name": "INDIA",
                    "nicename": "India",
                    "iso3": "IND",
                    "numcode": 356,
                    "phonecode": 91,
                    "status": 1,
                    "created_at": null,
                    "updated_at": null
                }
            },
            {
                "id": 7,
                "country_id": 99,
                "code": "GJ",
                "name": "Gujarat",
                "status": 1,
                "country": {
                    "id": 99,
                    "iso": "IN",
                    "name": "INDIA",
                    "nicename": "India",
                    "iso3": "IND",
                    "numcode": 356,
                    "phonecode": 91,
                    "status": 1,
                    "created_at": null,
                    "updated_at": null
                }
            },
            {
                "id": 9,
                "country_id": 99,
                "code": "HP",
                "name": "Himachal Pradesh",
                "status": 1,
                "country": {
                    "id": 99,
                    "iso": "IN",
                    "name": "INDIA",
                    "nicename": "India",
                    "iso3": "IND",
                    "numcode": 356,
                    "phonecode": 91,
                    "status": 1,
                    "created_at": null,
                    "updated_at": null
                }
            },
            {
                "id": 8,
                "country_id": 99,
                "code": "HR",
                "name": "Haryana",
                "status": 1,
                "country": {
                    "id": 99,
                    "iso": "IN",
                    "name": "INDIA",
                    "nicename": "India",
                    "iso3": "IND",
                    "numcode": 356,
                    "phonecode": 91,
                    "status": 1,
                    "created_at": null,
                    "updated_at": null
                }
            },
            {
                "id": 10,
                "country_id": 99,
                "code": "JH",
                "name": "Jharkhand",
                "status": 1,
                "country": {
                    "id": 99,
                    "iso": "IN",
                    "name": "INDIA",
                    "nicename": "India",
                    "iso3": "IND",
                    "numcode": 356,
                    "phonecode": 91,
                    "status": 1,
                    "created_at": null,
                    "updated_at": null
                }
            },
            {
                "id": 11,
                "country_id": 99,
                "code": "KA",
                "name": "Karnataka",
                "status": 1,
                "country": {
                    "id": 99,
                    "iso": "IN",
                    "name": "INDIA",
                    "nicename": "India",
                    "iso3": "IND",
                    "numcode": 356,
                    "phonecode": 91,
                    "status": 1,
                    "created_at": null,
                    "updated_at": null
                }
            }
        ],
        "first_page_url": "http://csv.test:8080/api/v1/paginated-states?page=1",
        "from": 1,
        "last_page": 3,
        "last_page_url": "http://csv.test:8080/api/v1/paginated-states?page=3",
        "links": [
            {
                "url": null,
                "label": "&laquo; Previous",
                "active": false
            },
            {
                "url": "http://csv.test:8080/api/v1/paginated-states?page=1",
                "label": "1",
                "active": true
            },
            {
                "url": "http://csv.test:8080/api/v1/paginated-states?page=2",
                "label": "2",
                "active": false
            },
            {
                "url": "http://csv.test:8080/api/v1/paginated-states?page=3",
                "label": "3",
                "active": false
            },
            {
                "url": "http://csv.test:8080/api/v1/paginated-states?page=2",
                "label": "Next &raquo;",
                "active": false
            }
        ],
        "next_page_url": "http://csv.test:8080/api/v1/paginated-states?page=2",
        "path": "http://csv.test:8080/api/v1/paginated-states",
        "per_page": 10,
        "prev_page_url": null,
        "to": 10,
        "total": 29
    },
    "draw": 0
}
```

# Delete

**Endpoint:**  
`/api/v1/states/1`

**Method:**  `DELETE`

**Headers:**
- `Authorization: Bearer {token}`
- `Accept: application/json`

**Success Response Status:**
204

