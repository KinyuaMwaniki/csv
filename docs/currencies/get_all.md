### Get All

**Endpoint:**  

`api/v1/currencies`

**Method:**  `GET`

**Headers:**

- `Accept: application/json`
- `Authorization: Bearer {token}`

**Sample success response:**
```json
{
    "currencies": [
        {
            "id": 2,
            "name": "Euro",
            "code": "EUR"
        },
        {
            "id": 5,
            "name": "US Dollar",
            "code": "USD"
        },
        {
            "id": 12,
            "name": "Franc CFA",
            "code": "FCFA"
        },
        {
            "id": 13,
            "name": "Franc CFA AO",
            "code": "FCFA AO"
        }
    ]
}
```