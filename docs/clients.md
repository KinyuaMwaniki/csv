# Check if email is unique in the company

**Endpoint:**  
`/api/v1/clients/check-email`

**Method:**  `POST`

**Headers:**
- `Authorization: Bearer {token}`
- `Accept: application/json`

**Payload:**
```json
{
    "email": "mail@mail.com"
}
```

**Validation Rules:**
- `email`: required, valid email, unique within the company_id

**Sample response:**
```json
{
    "email": [
        "The email must be a valid email address."
    ]
}
```

# Search Add Money

**Endpoint:**  
`/api/v1/clients/search-add-money`

**Method:**  `POST`

**Headers:**
- `Authorization: Bearer {token}`
- `Accept: application/json`

**Payload:**
```json
{
    "draw": 0,
    "search": "7222222414"
}
```
**Details of payload:**
- `draw`: If you sending many requests while searching, increment this by one after every request and then compare the one returned by the API
- `search`: Mobile number to search

**Sample response:**
```json
{
    "clients": [
        {
            "id": 23,
            "first_name": "Felix",
            "last_name": "unique",
            "mobile": "7222222414",
            "email": "em@em.com",
            "street_address": "streed ed",
            "zip_code": "Zim ed",
            "locality": "Kahawa South",
            "city": "Naromoru",
            "state": "Nairobi County",
            "country": "Kenya",
            "company_id": 1,
            "status": 1,
            "is_premium": 0,
            "created_by": 12,
            "updated_by": 1,
            "created_at": "2021-07-08T13:22:54.000000Z",
            "updated_at": "2021-07-27T07:53:47.000000Z",
            "debts": 1584.7099999999996,
            "orders": [
                {
                    "id": 1,
                    "total": "112.36",
                    "payment_method": "wallet",
                    "type_of_wallet": "smilepay",
                    "transaction_id": "asfsaaf",
                    "amount_paid": "0.00",
                    "amount_remaining": "0.00",
                    "status": "processing",
                    "address_id": 106,
                    "client_id": 23,
                    "company_id": 1,
                    "created_by": 1,
                    "updated_by": 1,
                    "created_at": "2021-07-19",
                    "updated_at": "2021-07-19T11:53:47.000000Z"
                },
                {
                    "id": 3,
                    "total": "112.36",
                    "payment_method": "cash",
                    "type_of_wallet": null,
                    "transaction_id": null,
                    "amount_paid": "41.00",
                    "amount_remaining": "71.36",
                    "status": "processing",
                    "address_id": 106,
                    "client_id": 23,
                    "company_id": 1,
                    "created_by": 1,
                    "updated_by": 1,
                    "created_at": "2021-07-19",
                    "updated_at": "2021-07-19T11:56:29.000000Z"
                },
                {
                    "id": 4,
                    "total": "112.36",
                    "payment_method": "wallet",
                    "type_of_wallet": "smilepay",
                    "transaction_id": "asddfsasfds",
                    "amount_paid": "0.00",
                    "amount_remaining": "0.00",
                    "status": "processing",
                    "address_id": 106,
                    "client_id": 23,
                    "company_id": 1,
                    "created_by": 1,
                    "updated_by": 1,
                    "created_at": "2021-07-19",
                    "updated_at": "2021-07-19T11:56:40.000000Z"
                },
                {
                    "id": 5,
                    "total": "112.36",
                    "payment_method": "cash",
                    "type_of_wallet": null,
                    "transaction_id": null,
                    "amount_paid": "41.00",
                    "amount_remaining": "71.36",
                    "status": "dispatched",
                    "address_id": 106,
                    "client_id": 23,
                    "company_id": 1,
                    "created_by": 1,
                    "updated_by": 1,
                    "created_at": "2021-07-19",
                    "updated_at": "2021-07-28T08:39:09.000000Z"
                },
                {
                    "id": 6,
                    "total": "112.36",
                    "payment_method": "wallet",
                    "type_of_wallet": "smilepay",
                    "transaction_id": "asfasdffdsf",
                    "amount_paid": null,
                    "amount_remaining": null,
                    "status": "processing",
                    "address_id": 106,
                    "client_id": 23,
                    "company_id": 1,
                    "created_by": 1,
                    "updated_by": 1,
                    "created_at": "2021-07-19",
                    "updated_at": "2021-07-19T11:58:29.000000Z"
                },
                {
                    "id": 7,
                    "total": "112.36",
                    "payment_method": "wallet",
                    "type_of_wallet": "orangePay",
                    "transaction_id": "asfasdffdsf",
                    "amount_paid": null,
                    "amount_remaining": null,
                    "status": "in transit",
                    "address_id": 106,
                    "client_id": 23,
                    "company_id": 1,
                    "created_by": 1,
                    "updated_by": 1,
                    "created_at": "2021-07-19",
                    "updated_at": "2021-07-27T09:21:11.000000Z"
                },
                {
                    "id": 8,
                    "total": "112.36",
                    "payment_method": "cash",
                    "type_of_wallet": null,
                    "transaction_id": null,
                    "amount_paid": "41.00",
                    "amount_remaining": "71.36",
                    "status": "processing",
                    "address_id": 106,
                    "client_id": 23,
                    "company_id": 1,
                    "created_by": 1,
                    "updated_by": 1,
                    "created_at": "2021-07-19",
                    "updated_at": "2021-07-19T11:58:52.000000Z"
                },
                {
                    "id": 9,
                    "total": "112.36",
                    "payment_method": "cash",
                    "type_of_wallet": null,
                    "transaction_id": null,
                    "amount_paid": "0.00",
                    "amount_remaining": "112.36",
                    "status": "processing",
                    "address_id": 106,
                    "client_id": 23,
                    "company_id": 1,
                    "created_by": 1,
                    "updated_by": 1,
                    "created_at": "2021-07-19",
                    "updated_at": "2021-07-19T12:03:05.000000Z"
                },
                {
                    "id": 10,
                    "total": "112.36",
                    "payment_method": "cash",
                    "type_of_wallet": null,
                    "transaction_id": null,
                    "amount_paid": "41.00",
                    "amount_remaining": "71.36",
                    "status": "processing",
                    "address_id": 106,
                    "client_id": 23,
                    "company_id": 1,
                    "created_by": 1,
                    "updated_by": 1,
                    "created_at": "2021-07-19",
                    "updated_at": "2021-07-19T12:03:10.000000Z"
                },
                {
                    "id": 11,
                    "total": "112.36",
                    "payment_method": "cash",
                    "type_of_wallet": null,
                    "transaction_id": null,
                    "amount_paid": "112.36",
                    "amount_remaining": "0.00",
                    "status": "processing",
                    "address_id": 106,
                    "client_id": 23,
                    "company_id": 1,
                    "created_by": 1,
                    "updated_by": 1,
                    "created_at": "2021-07-19",
                    "updated_at": "2021-07-19T12:03:20.000000Z"
                },
                {
                    "id": 12,
                    "total": "112.36",
                    "payment_method": "wallet",
                    "type_of_wallet": "smilepay",
                    "transaction_id": "asfasdf",
                    "amount_paid": null,
                    "amount_remaining": null,
                    "status": "processing",
                    "address_id": 106,
                    "client_id": 23,
                    "company_id": 1,
                    "created_by": 1,
                    "updated_by": 1,
                    "created_at": "2021-07-19",
                    "updated_at": "2021-07-19T12:03:33.000000Z"
                },
                {
                    "id": 13,
                    "total": "112.36",
                    "payment_method": "wallet",
                    "type_of_wallet": "smilepay",
                    "transaction_id": "sfasfafsad",
                    "amount_paid": null,
                    "amount_remaining": null,
                    "status": "processing",
                    "address_id": 106,
                    "client_id": 23,
                    "company_id": 1,
                    "created_by": 1,
                    "updated_by": 1,
                    "created_at": "2021-07-19",
                    "updated_at": "2021-07-19T12:11:40.000000Z"
                },
                {
                    "id": 17,
                    "total": "112.36",
                    "payment_method": "wallet",
                    "type_of_wallet": "smilepay",
                    "transaction_id": "afadaaDAS",
                    "amount_paid": null,
                    "amount_remaining": null,
                    "status": "processing",
                    "address_id": 106,
                    "client_id": 23,
                    "company_id": 1,
                    "created_by": 1,
                    "updated_by": 1,
                    "created_at": "2021-07-19",
                    "updated_at": "2021-07-19T12:24:00.000000Z"
                },
                {
                    "id": 18,
                    "total": "112.36",
                    "payment_method": "wallet",
                    "type_of_wallet": "smilepay",
                    "transaction_id": "afadaaDAS",
                    "amount_paid": null,
                    "amount_remaining": null,
                    "status": "processing",
                    "address_id": 106,
                    "client_id": 23,
                    "company_id": 1,
                    "created_by": 1,
                    "updated_by": 1,
                    "created_at": "2021-07-19",
                    "updated_at": "2021-07-19T12:33:24.000000Z"
                },
                {
                    "id": 19,
                    "total": "112.36",
                    "payment_method": "wallet",
                    "type_of_wallet": "smilepay",
                    "transaction_id": "afadaaDAS",
                    "amount_paid": null,
                    "amount_remaining": null,
                    "status": "processing",
                    "address_id": 106,
                    "client_id": 23,
                    "company_id": 1,
                    "created_by": 1,
                    "updated_by": 1,
                    "created_at": "2021-07-19",
                    "updated_at": "2021-07-19T12:34:00.000000Z"
                },
                {
                    "id": 20,
                    "total": "112.36",
                    "payment_method": "wallet",
                    "type_of_wallet": "smilepay",
                    "transaction_id": "afadaaDAS",
                    "amount_paid": null,
                    "amount_remaining": null,
                    "status": "processing",
                    "address_id": 106,
                    "client_id": 23,
                    "company_id": 1,
                    "created_by": 1,
                    "updated_by": 1,
                    "created_at": "2021-07-19",
                    "updated_at": "2021-07-19T12:34:16.000000Z"
                },
                {
                    "id": 21,
                    "total": "112.36",
                    "payment_method": "wallet",
                    "type_of_wallet": "smilepay",
                    "transaction_id": "afadaaDAS",
                    "amount_paid": null,
                    "amount_remaining": null,
                    "status": "processing",
                    "address_id": 106,
                    "client_id": 23,
                    "company_id": 1,
                    "created_by": 1,
                    "updated_by": 1,
                    "created_at": "2021-07-19",
                    "updated_at": "2021-07-19T12:34:45.000000Z"
                },
                {
                    "id": 22,
                    "total": "112.36",
                    "payment_method": "wallet",
                    "type_of_wallet": "smilepay",
                    "transaction_id": "afadaaDAS",
                    "amount_paid": null,
                    "amount_remaining": null,
                    "status": "processing",
                    "address_id": 106,
                    "client_id": 23,
                    "company_id": 1,
                    "created_by": 1,
                    "updated_by": 1,
                    "created_at": "2021-07-19",
                    "updated_at": "2021-07-19T12:40:54.000000Z"
                },
                {
                    "id": 23,
                    "total": "112.36",
                    "payment_method": "wallet",
                    "type_of_wallet": "smilepay",
                    "transaction_id": "afadaaDAS",
                    "amount_paid": null,
                    "amount_remaining": null,
                    "status": "processing",
                    "address_id": 106,
                    "client_id": 23,
                    "company_id": 1,
                    "created_by": 1,
                    "updated_by": 1,
                    "created_at": "2021-07-19",
                    "updated_at": "2021-07-19T12:41:13.000000Z"
                },
                {
                    "id": 24,
                    "total": "112.36",
                    "payment_method": "wallet",
                    "type_of_wallet": "smilepay",
                    "transaction_id": "afadaaDAS",
                    "amount_paid": null,
                    "amount_remaining": null,
                    "status": "processing",
                    "address_id": 106,
                    "client_id": 23,
                    "company_id": 1,
                    "created_by": 1,
                    "updated_by": 1,
                    "created_at": "2021-07-19",
                    "updated_at": "2021-07-19T12:41:37.000000Z"
                },
                {
                    "id": 25,
                    "total": "112.36",
                    "payment_method": "wallet",
                    "type_of_wallet": "smilepay",
                    "transaction_id": "asfasfdsaf",
                    "amount_paid": null,
                    "amount_remaining": null,
                    "status": "processing",
                    "address_id": 106,
                    "client_id": 23,
                    "company_id": 1,
                    "created_by": 1,
                    "updated_by": 1,
                    "created_at": "2021-07-19",
                    "updated_at": "2021-07-19T12:45:03.000000Z"
                },
                {
                    "id": 26,
                    "total": "392.00",
                    "payment_method": "cash",
                    "type_of_wallet": null,
                    "transaction_id": null,
                    "amount_paid": "41.00",
                    "amount_remaining": "351.00",
                    "status": "delivered",
                    "address_id": 104,
                    "client_id": 23,
                    "company_id": 1,
                    "created_by": 1,
                    "updated_by": 1,
                    "created_at": "2021-07-19",
                    "updated_at": "2021-07-27T09:11:12.000000Z"
                },
                {
                    "id": 28,
                    "total": "177.61",
                    "payment_method": "wallet",
                    "type_of_wallet": "smilepay",
                    "transaction_id": "trans001",
                    "amount_paid": "41.00",
                    "amount_remaining": "136.61",
                    "status": "processing",
                    "address_id": 106,
                    "client_id": 23,
                    "company_id": 1,
                    "created_by": 1,
                    "updated_by": 1,
                    "created_at": "2021-07-21",
                    "updated_at": "2021-07-28T07:33:43.000000Z"
                },
                {
                    "id": 29,
                    "total": "75.36",
                    "payment_method": "cash",
                    "type_of_wallet": null,
                    "transaction_id": null,
                    "amount_paid": "41.00",
                    "amount_remaining": "34.36",
                    "status": "processing",
                    "address_id": 106,
                    "client_id": 23,
                    "company_id": 1,
                    "created_by": 1,
                    "updated_by": 1,
                    "created_at": "2021-07-21",
                    "updated_at": "2021-07-21T13:12:39.000000Z"
                },
                {
                    "id": 30,
                    "total": "105.36",
                    "payment_method": "wallet",
                    "type_of_wallet": "smilepay",
                    "transaction_id": "sfassasdf",
                    "amount_paid": null,
                    "amount_remaining": null,
                    "status": "processing",
                    "address_id": 106,
                    "client_id": 23,
                    "company_id": 1,
                    "created_by": 1,
                    "updated_by": 1,
                    "created_at": "2021-07-21",
                    "updated_at": "2021-07-21T13:15:14.000000Z"
                },
                {
                    "id": 31,
                    "total": "75.36",
                    "payment_method": "wallet",
                    "type_of_wallet": "smilepay",
                    "transaction_id": "sfsfsa",
                    "amount_paid": null,
                    "amount_remaining": null,
                    "status": "processing",
                    "address_id": 106,
                    "client_id": 23,
                    "company_id": 1,
                    "created_by": 1,
                    "updated_by": 1,
                    "created_at": "2021-07-21",
                    "updated_at": "2021-07-21T13:16:54.000000Z"
                },
                {
                    "id": 32,
                    "total": "67.21",
                    "payment_method": "cash",
                    "type_of_wallet": null,
                    "transaction_id": null,
                    "amount_paid": "21.00",
                    "amount_remaining": "46.21",
                    "status": "processing",
                    "address_id": 106,
                    "client_id": 23,
                    "company_id": 1,
                    "created_by": 1,
                    "updated_by": 1,
                    "created_at": "2021-07-22",
                    "updated_at": "2021-07-22T06:42:37.000000Z"
                },
                {
                    "id": 33,
                    "total": "107.21",
                    "payment_method": "cash",
                    "type_of_wallet": null,
                    "transaction_id": null,
                    "amount_paid": "41.00",
                    "amount_remaining": "66.21",
                    "status": "processing",
                    "address_id": 106,
                    "client_id": 23,
                    "company_id": 1,
                    "created_by": 1,
                    "updated_by": 1,
                    "created_at": "2021-07-22",
                    "updated_at": "2021-07-22T06:44:41.000000Z"
                },
                {
                    "id": 34,
                    "total": "67.21",
                    "payment_method": "cash",
                    "type_of_wallet": null,
                    "transaction_id": null,
                    "amount_paid": "21.00",
                    "amount_remaining": "46.21",
                    "status": "processing",
                    "address_id": 106,
                    "client_id": 23,
                    "company_id": 1,
                    "created_by": 1,
                    "updated_by": 1,
                    "created_at": "2021-07-22",
                    "updated_at": "2021-07-22T06:51:31.000000Z"
                },
                {
                    "id": 35,
                    "total": "114.42",
                    "payment_method": "wallet",
                    "type_of_wallet": "smilepay",
                    "transaction_id": "Trans101",
                    "amount_paid": "0.00",
                    "amount_remaining": "41.00",
                    "status": "processing",
                    "address_id": 106,
                    "client_id": 23,
                    "company_id": 1,
                    "created_by": 1,
                    "updated_by": 1,
                    "created_at": "2021-07-22",
                    "updated_at": "2021-07-22T07:00:02.000000Z"
                },
                {
                    "id": 37,
                    "total": "120.00",
                    "payment_method": "cash",
                    "type_of_wallet": "smilepay",
                    "transaction_id": null,
                    "amount_paid": "120.00",
                    "amount_remaining": "0.00",
                    "status": "processing",
                    "address_id": 106,
                    "client_id": 23,
                    "company_id": 1,
                    "created_by": 1,
                    "updated_by": 1,
                    "created_at": "2021-07-22",
                    "updated_at": "2021-07-26T12:11:31.000000Z"
                },
                {
                    "id": 38,
                    "total": "133.61",
                    "payment_method": "wallet",
                    "type_of_wallet": "smilepay",
                    "transaction_id": "41sfsaffas",
                    "amount_paid": "60.00",
                    "amount_remaining": "73.61",
                    "status": "processing",
                    "address_id": 106,
                    "client_id": 23,
                    "company_id": 1,
                    "created_by": 1,
                    "updated_by": 1,
                    "created_at": "2021-07-26",
                    "updated_at": "2021-07-27T14:05:48.000000Z"
                },
                {
                    "id": 39,
                    "total": "75.36",
                    "payment_method": "cash",
                    "type_of_wallet": null,
                    "transaction_id": null,
                    "amount_paid": "80.00",
                    "amount_remaining": "-4.64",
                    "status": "processing",
                    "address_id": 106,
                    "client_id": 23,
                    "company_id": 1,
                    "created_by": 1,
                    "updated_by": 1,
                    "created_at": "2021-07-26",
                    "updated_at": "2021-07-26T12:41:42.000000Z"
                },
                {
                    "id": 40,
                    "total": "58.25",
                    "payment_method": "cash",
                    "type_of_wallet": null,
                    "transaction_id": null,
                    "amount_paid": "60.00",
                    "amount_remaining": "-1.75",
                    "status": "processing",
                    "address_id": 106,
                    "client_id": 23,
                    "company_id": 1,
                    "created_by": 1,
                    "updated_by": 1,
                    "created_at": "2021-07-26",
                    "updated_at": "2021-07-26T12:43:34.000000Z"
                },
                {
                    "id": 41,
                    "total": "58.25",
                    "payment_method": "cash",
                    "type_of_wallet": null,
                    "transaction_id": null,
                    "amount_paid": "60.00",
                    "amount_remaining": "-1.75",
                    "status": "processing",
                    "address_id": 106,
                    "client_id": 23,
                    "company_id": 1,
                    "created_by": 1,
                    "updated_by": 1,
                    "created_at": "2021-07-26",
                    "updated_at": "2021-07-26T12:44:26.000000Z"
                },
                {
                    "id": 45,
                    "total": "75.36",
                    "payment_method": "wallet",
                    "type_of_wallet": "smilepay",
                    "transaction_id": "Transaction001",
                    "amount_paid": "41.00",
                    "amount_remaining": "34.36",
                    "status": "in transit",
                    "address_id": 104,
                    "client_id": 23,
                    "company_id": 1,
                    "created_by": 1,
                    "updated_by": 1,
                    "created_at": "2021-07-28",
                    "updated_at": "2021-07-28T15:52:19.000000Z"
                },
                {
                    "id": 46,
                    "total": "75.36",
                    "payment_method": "wallet",
                    "type_of_wallet": "smilepay",
                    "transaction_id": "sf",
                    "amount_paid": "75.36",
                    "amount_remaining": "0.00",
                    "status": "delivered",
                    "address_id": 104,
                    "client_id": 23,
                    "company_id": 1,
                    "created_by": 1,
                    "updated_by": 1,
                    "created_at": "2021-07-28",
                    "updated_at": "2021-07-30T04:49:07.000000Z"
                },
                {
                    "id": 47,
                    "total": "75.36",
                    "payment_method": "wallet",
                    "type_of_wallet": "smilepay",
                    "transaction_id": "Transaction001",
                    "amount_paid": "41.00",
                    "amount_remaining": "34.36",
                    "status": "processing",
                    "address_id": 104,
                    "client_id": 23,
                    "company_id": 1,
                    "created_by": 1,
                    "updated_by": 1,
                    "created_at": "2021-07-29",
                    "updated_at": "2021-07-29T13:33:03.000000Z"
                },
                {
                    "id": 48,
                    "total": "354.12",
                    "payment_method": "wallet",
                    "type_of_wallet": "smilepay",
                    "transaction_id": "TransactionId",
                    "amount_paid": "23.00",
                    "amount_remaining": "331.12",
                    "status": "dispatched",
                    "address_id": 106,
                    "client_id": 23,
                    "company_id": 1,
                    "created_by": 1,
                    "updated_by": 1,
                    "created_at": "2021-07-29",
                    "updated_at": "2021-07-30T04:46:28.000000Z"
                }
            ]
        }
    ],
    "draw": 0
}
```

# Add Money

**Endpoint:**  
`/api/v1/client-balances`

**Method:**  `POST`

**Headers:**
- `Authorization: Bearer {token}`
- `Accept: application/json`

**Payload:**
```json
{
    "client_id": 23,
    "amount": 41
}
```

**Validation Rules:**
- `client_id` : 'required'
- `amount` : 'required', 'numeric', 'min:0'

**Sample response:**
23

**Details of response:**
Response returns client id