<?php

use App\Models\User;
use App\Models\Order;
use App\Models\UserCash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Resources\UserCollection;
use App\Http\Controllers\Api\V1\AuthController;
use App\Http\Controllers\Api\V1\LeadsController;
use App\Http\Controllers\Api\V1\RolesController;
use App\Http\Controllers\Api\V1\UsersController;
use App\Http\Controllers\Api\V1\BrandsController;
use App\Http\Controllers\Api\V1\ChartsController;
use App\Http\Controllers\Api\V1\OrdersController;
use App\Http\Controllers\Api\V1\StatesController;
use App\Http\Controllers\Api\V1\ClientsController;
use App\Http\Controllers\Api\V1\MastersController;
use App\Http\Controllers\Api\V1\PaymentsController;
use App\Http\Controllers\Api\V1\ProductsController;
use App\Http\Controllers\Api\V1\CompaniesController;
use App\Http\Controllers\Api\V1\CountriesController;
use App\Http\Controllers\Api\V1\DashboardController;
use App\Http\Controllers\Api\V1\CategoriesController;
use App\Http\Controllers\Api\V1\CurrenciesController;
use App\Http\Controllers\Api\V1\UserCashesController;
use App\Http\Controllers\Api\V1\AttributeTypesController;
use App\Http\Controllers\Api\V1\ClientBalancesController;
use App\Http\Controllers\Api\V2\AuthController as MobileAuthController;
use App\Http\Controllers\Api\V2\ClientsController as MobileClientsController;
use App\Http\Controllers\Api\V2\UserCashesController as MobileUserCashesController;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => '/v2'], function () {
    Route::post('login', [MobileAuthController::class, 'login']);
    Route::post('/user-cash', [MobileUserCashesController::class, 'store']);
});

Route::group(['prefix' => '/v2'], function () {
    Route::group(['middleware' => ['auth:sanctum']], function () {
        Route::post('/user-cash', [MobileUserCashesController::class, 'store']);
        Route::put('/user-cash/{userCash}', [MobileUserCashesController::class, 'update']);
        Route::post('search-clients', [MobileClientsController::class, 'searchClients']);
        Route::post('paginated-clients', [MobileClientsController::class, 'indexPaginated']);
    });
});

Route::group(['prefix' => '/v1'], function () {
    Route::post('login', [AuthController::class, 'login']);
    Route::post('forgot-password', [AuthController::class, 'forgotPassword']);
    Route::post('change-password-otp', [AuthController::class, 'changePasswordWithOTP']);
    Route::group(['middleware' => ['auth:sanctum']], function () {
        Route::post('logout', [AuthController::class, 'logout']);
        Route::post('change-password', [AuthController::class, 'changePassword']);
        Route::get('user/info', [AuthController::class, 'getUserInfo']);
        Route::apiResource('dashboard', DashboardController::class);
        Route::post('paginated-countries', [CountriesController::class, 'indexPaginated']);
        Route::apiResource('countries', CountriesController::class);
        Route::post('paginated-roles', [RolesController::class, 'indexPaginated']);
        Route::apiResource('roles', RolesController::class);
        Route::post('paginated-users', [UsersController::class, 'indexPaginated']);
        Route::group(['prefix' => 'users'], function () {
            Route::post('/search-update-credit', [UsersController::class, 'searchUpdateCredit']);
        });
        Route::apiResource('users', UsersController::class);
        Route::get('masters/type/{type}', [MastersController::class, 'indexByType']);
        Route::post('paginated-masters', [MastersController::class, 'indexPaginated']);
        Route::apiResource('masters', MastersController::class);
        Route::post('paginated-leads', [LeadsController::class, 'indexPaginated']);
        Route::post('leads/import', [LeadsController::class, 'import']);
        Route::get('leads/export', [LeadsController::class, 'export']);
        Route::post('leads/delete-checked', [LeadsController::class, 'deleteChecked']);
        Route::apiResource('leads', LeadsController::class);
        Route::post('paginated-states', [StatesController::class, 'indexPaginated']);
        Route::apiResource('states', StatesController::class);
        Route::post('paginated-companies', [CompaniesController::class, 'indexPaginated']);
        // Route::put('companies/user/update-activity-period', [CompaniesController::class, 'updateActivityPeriod']);
        Route::put('companies/user/update', [CompaniesController::class, 'updateUserCompany']);
        Route::get('companies/user', [CompaniesController::class, 'showUserCompany']);
        Route::apiResource('companies', CompaniesController::class);
        Route::post('paginated-categories', [CategoriesController::class, 'indexPaginated']);
        Route::put('categories/status/{category}', [CategoriesController::class, 'updateStatus']);
        Route::apiResource('categories', CategoriesController::class);
        Route::post('paginated-brands', [BrandsController::class, 'indexPaginated']);
        Route::apiResource('brands', BrandsController::class);
        Route::post('paginated-attribute-types', [AttributeTypesController::class, 'indexPaginated']);
        Route::apiResource('attribute-types', AttributeTypesController::class);
        Route::post('paginated-products', [ProductsController::class, 'indexPaginated']);
        Route::post('catalog-products', [ProductsController::class, 'catalogPaginated']);
        Route::put('products/status/{product}', [ProductsController::class, 'updateStatus']);
        Route::apiResource('products', ProductsController::class);
        Route::post('paginated-currencies', [CurrenciesController::class, 'indexPaginated']);
        Route::get('currencies/company', [CurrenciesController::class, 'showCompanyCurrency']);
        Route::apiResource('currencies', CurrenciesController::class);
        Route::post('paginated-clients', [ClientsController::class, 'indexPaginated']);
        Route::post('search-clients', [ClientsController::class, 'searchClients']);
        Route::group(['prefix' => 'clients'], function () {
            Route::post('/search-add-money', [ClientsController::class, 'searchAddMoney']);
            Route::post('/debts', [ClientsController::class, 'debtsPaginated']);
            Route::post('/check-email', [ClientsController::class, 'checkEmail']);
            Route::put('/status/{client}', [ClientsController::class, 'updateStatus']);
            Route::put('/level/{client}', [ClientsController::class, 'updateLevel']);
        });
        Route::apiResource('clients', ClientsController::class);
        Route::apiResource('client-balances', ClientBalancesController::class);
        Route::post('paginated-orders', [OrdersController::class, 'indexPaginated']);
        Route::group(['prefix' => 'orders'], function () {
            Route::post('/all', [OrdersController::class, 'allPaginated']);
            Route::put('/approve/{order}', [OrdersController::class, 'approve']);
            Route::post('/export', [OrdersController::class, 'export']);
            Route::put('/payment/{order}', [OrdersController::class, 'updatePayment']);
            Route::put('/update-status/{order}', [OrdersController::class, 'updateStatus']);
            Route::post('/debts', [OrdersController::class, 'debtsPaginated']);
            Route::post('/sales-report', [OrdersController::class, 'salesReport']);
            Route::post('/download', [OrdersController::class, 'downloadInvoice']);
            Route::post('/pdf', [OrdersController::class, 'getPdf']);
        });
        Route::apiResource('orders', OrdersController::class);
        Route::group(['prefix' => 'charts'], function () {
            Route::post('/collections/daily', [ChartsController::class, 'getDailyCollection']);
            Route::post('/collections/monthly', [ChartsController::class, 'getMonthlyCollection']);
        });
        Route::post('paginated-collections', [UserCashesController::class, 'indexPaginated']);
        Route::group(['prefix' => 'user-cash'], function () {
            Route::put('/approve/{userCash}', [UserCashesController::class, 'approve']);
            Route::put('/deny/{userCash}', [UserCashesController::class, 'deny']);
        });
        Route::post('/payments/export/collection', [PaymentsController::class, 'export']);
        Route::put('/payments/approve/{payment}', [PaymentsController::class, 'approve']);
        Route::put('/payments/deny/{payment}', [PaymentsController::class, 'deny']);
        Route::post('/payments/reports/collection', [PaymentsController::class, 'collectionReport']);
        Route::apiResource('user-cash', UserCashesController::class);
    });
});

Route::get('test', function () {
    return "success";
});

