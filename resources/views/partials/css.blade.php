    <link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/dark.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/skins.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/animated.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/sidemenu.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/web-fonts/icons.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/web-fonts/font-awesome/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/web-fonts/plugin.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/p-scrollbar/p-scrollbar.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/switcher/css/switcher.css') }}">
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatable/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/skins.css') }}">
    <link href="{{ asset('assets/plugins/notify/css/notifIt.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/accordion/accordion.css') }}" rel="stylesheet" />
