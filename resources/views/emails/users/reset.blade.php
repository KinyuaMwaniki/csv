@component('mail::message')
# Dear {!! $user->first_name !!} {!! $user->last_name !!}

Your OTP is {{ $OTP }}. It is valid for 3 hours.

{{-- Please login via the link below or Follow:

@component('mail::button', ['url' => url('/')])
Login
@endcomponent --}}

Thanks,<br>
{{ config('app.name') }}
@endcomponent
