<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Invoice</title>

    <link rel="stylesheet" href="{{ asset('assets/css/invoice.css') }}">

    <style></style>

</head>

<body>
    <div class="row roboto-text">
        <div class="col-md-12">
            <div class="card" style="height: 100vh;">
                <div class="card-body" style="display: flex;flex-flow: column;height: 100%;">
                    <div class="invoice-header text-right d-block mb-5">
                        <h1 class="invoice-title font-weight-bold text-uppercase mb-1">{{ __('invoice.invoice') }}
                        </h1>
                    </div>
                    <div class="row mt-4">

                        <div class="bill-to"> <label
                                class="font-weight-bold">{{ __('invoice.billedTo') }}</label>
                            <div class="billed-to">
                                @if (isset($order->client))
                                    @isset($order->client->company_name)
                                        <h6>
                                            {{ $order->client->company_name }}
                                        </h6>
                                    @endisset
                                    @empty($order->client->company_name)
                                        <h6>
                                            {{ $order->client->first_name }} {{ $order->client->last_name }}
                                        </h6>
                                    @endempty
                                @endif

                                @if (isset($order->address))
                                    <p>
                                        @if ($order->address->zip_code !== '')
                                            <span>
                                                {{ isset($order->address->zip_code) ? $order->address->zip_code : '' }},
                                            </span>
                                        @endif
                                        @if ($order->address->street_address !== '')
                                            <span>
                                                {{ isset($order->address->street_address) ? $order->address->street_address : '' }},
                                            </span>
                                        @endif
                                        @if ($order->address->locality !== '')
                                            <span>
                                                {{ isset($order->address->locality) ? $order->address->locality : '' }},
                                            </span>
                                        @endif
                                        @if ($order->address->city !== '')
                                            <span>
                                                {{ isset($order->address->city) ? $order->address->city : '' }},
                                            </span>
                                        @endif
                                        @if ($order->address->country !== '')
                                            <span>
                                                {{ isset($order->address->country) ? $order->address->country : '' }}
                                            </span>
                                        @endif
                                    </p>

                                    @if (isset($order->client))
                                        <p>
                                            @isset($order->client->mobile)
                                                <span>
                                                    {{ __('invoice.telNo') }}: {{ $order->client->mobile }}
                                                </span>
                                            @endisset
                                            @isset($order->client->email)
                                                <br />
                                                <span v-if="order.client.email">
                                                    {{ __('invoice.email') }}: {{ $order->client->email }}
                                                </span>
                                            @endisset
                                        </p>
                                    @endif
                                @endif
                            </div>
                        </div>
                        <div class="bill-from">
                            <div class="billed-from" style="text-align: right">
                                <p class="font-weight-bold">{{ __('invoice.billedFrom') }}</p>
                                @isset($order->company)
                                    <h6>{{ $order->company->name }}</h6>
                                    <p>
                                        @isset($order->company->zip_code)
                                            {{ $order->company->zip_code }},
                                        @endisset
                                        @isset($order->company->address)
                                            {{ $order->company->address }} ,
                                        @endisset
                                        @isset($order->company->city)
                                            {{ $order->company->city }},
                                        @endisset
                                        @isset($order->company->country)
                                            {{ $order->company->country }},
                                        @endisset
                                        @isset($order->company->state)
                                            {{ $order->company->state }},
                                        @endisset
                                        <br />
                                        <span>
                                            {{ __('invoice.telNo') }}: {{ $order->company->mobile }}
                                        </span>
                                        <br />
                                        <span>
                                            {{ __('invoice.email') }}: {{ $order->company->email }}
                                        </span>
                                    </p>
                                @endisset
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive mt-4" style="flex: 1 1 auto;">
                        <table class="table table-bordered border text-nowrap mb-0" style="height: 100%;">
                            <thead>
                                <tr>
                                    <th class="wd-20p">{{ __('invoice.product') }}</th>
                                    <th class="tx-center">{{ __('invoice.quantity') }}</th>
                                    <th class="tx-right">{{ __('invoice.unitPrice') }}</th>
                                    <th class="tx-right">{{ __('invoice.amount') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($order->products as $product)
                                    <tr>
                                        <td class="font-weight-bold">
                                            <p> {{ $product->product->name }}</p>
                                            <p>
                                                @foreach ($product->product_attributes as $attribute)
                                                    @if ($attribute['name'] === 'Color' || $attribute['name'] === 'color' || $attribute['name'] === 'Colour' || $attribute['name'] === 'colour')
                                                        @php
                                                            $color = substr($attribute['value'], -6);
                                                            $color = '#' . $color;
                                                        @endphp

                                                        <small class="d-block">
                                                            <span style="padding-right:5px">{{ $attribute['name'] }}
                                                            </span>
                                                            <span
                                                                style="background:<?= $color ?>;height:10px;width:30px;display:inline-block">
                                                            </span>
                                                        </small>
                                                    @else
                                                        <small class="d-block">
                                                            {{ $attribute['name'] }}: {{ $attribute['value'] }}
                                                        </small>

                                                    @endif
                                                @endforeach
                                            </p>
                                        </td>
                                        <td class="text-right">{{ $product->quantity }}</td>
                                        <td class="text-right">
                                            {{ $order->company->currency->symbol }} 
                                            {{ $product->product->price }}
                                        </td>
                                        <td class="text-right">
                                            {{ $order->company->currency->symbol }} 
                                            {{ $product->total }}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td class="text-uppercase font-weight-semibold" colspan="3">
                                        {{ __('invoice.totalAmount') }}</td>
                                    <td class="text-right" colspan="3">
                                        <h4 class="text-primary font-weight-bold">
                                            {{ $order->company->currency->symbol }} {{ $order->total }}</h4>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-uppercase font-weight-semibold" colspan="3">
                                        {{ __('invoice.paidAmount') }}</td>
                                    <td class="text-right" colspan="3">
                                        <h4 class="text-primary font-weight-bold">
                                            {{ $order->company->currency->symbol }} {{ $order->amount_paid }}</h4>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-uppercase font-weight-semibold" colspan="3">
                                        {{ __('invoice.remainingAmount') }}</td>
                                    <td class="text-right" colspan="3">
                                        <h4 class="text-primary font-weight-bold">
                                            {{ $order->company->currency->symbol }} {{ $order->amount_remaining }}
                                        </h4>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        window.onload = function() {
            window.print();
        }
    </script>
</body>

</html>
