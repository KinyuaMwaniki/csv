<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <meta charset="UTF-8">
    <meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0'>
    <meta content="CSV"
        name="description">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta content="Jithvar" name="author">
    <meta name="keywords"
        content="Ecommerce, Administration, Customer relationship" />
    <title>CRM</title>
    @include('partials.css')
</head>

<body id="vue-app" class="app sidebar-mini light-mode color-sidebar">
    
    @include('partials.scripts')
</body>

</html>
