<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'invoice' => "Invoice",
    'billedTo' => "Billed To",
    "telNo"=> "Tel No",
    "billedFrom"=> "Billed From",
    "email"=> "E-mail",
    "product" => "Product",
    "quantity" => "Quantity",
    "unitPrice" => "Unit Price",
    "amount" => "Amount",
    "totalAmount" => "Total Amount",
    "remainingAmount" => "Remaining Amount",
    "paidAmount" => "Paid Amount",
    "paymentDate" => "Payment Date",
    "paymentMethod" => "Payment Method",
    "orderNumber" => "Order #",
    "clientName" => "Client Name",
    "status" => "Status",
    "createdAt" => "Creatd At"
];