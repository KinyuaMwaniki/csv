<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'invoice' => "Facture d'achat",
    'billedTo' => "Facturé à",
    "telNo" => "Numéro de téléphone",
    "billedFrom" => "Facturé à partir de",
    "email" => "E-mail",
    "product" => "Produit",
    "quantity" => "Quantité",
    "unitPrice" => "Prix unitaire",
    "amount" => "Montant",
    "totalAmount" => "Montant total",
    "remainingAmount" => "Montant restant",
    "paidAmount" => "Montant payé",
    "paymentDate" => "Date de paiement",
    "paymentMethod" => "Mode de paiement",
    "orderNumber" => "Numéro de commande",
    "clientName" => "Nom du client",
    "status" => "Status",
    "createdAt" => "Créé à"
];
