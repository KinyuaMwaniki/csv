export default {
    setMasters(state, payload) {
        state.masters = payload;
    },
    setTypes(state, payload) {
        state.types = payload;
    }
};
