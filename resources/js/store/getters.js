export default {
    token(state) {
        return state.token;
    },
    user(state) {
        return state.user;
    },
    locale(state) {
        return state.locale;
    },
    loggedIn(state) {
        return state.token != null;
    },
    isAdmin(state) {
        return state.user.role.name === "Administrator";
    },
    isCommercial(state) {
        return state.user.role.name === "Commercial";
    },
    isAuthenticated(state) {
        return (
            !!state.token &&
            !!state.user &&
            state.user.active === 1 &&
            state.user.email_verified_at
        );
    },
    email(state) {
        return state.email;
    }
};
