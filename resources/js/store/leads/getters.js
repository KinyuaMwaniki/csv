export default {
    masters(state) {
        return state.masters;
    },
    types(state) {
        return state.types;
    }
};
