export default {
    clients(state) {
        return state.clients;
    },
    orderUpdated(state) {
        return state.orderUpdated;
    }
};
