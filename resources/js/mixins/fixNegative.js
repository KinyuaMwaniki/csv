export default {
    methods: {
        fixNegative() {
            for (const key in this.form) {
                if (this.form[key] === -1) {
                    this.form[key] = null;
                }
            }
        },
    },
};
