export default {
    methods: {
        catchNotFoundError(err) {
            if (!!err.response && err.response.status === 404) {
                this.showError("Not Found");
            }
        }
    }
};
