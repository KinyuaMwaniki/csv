export default {
    created() {
        this.checkToken();
    },
    methods: {
        async checkToken() {
            try {
                const response = await this.$store.dispatch("getUser");
            } catch (err) {
                if (
                    err.response &&
                    err.response.status &&
                    err.response.status === 401
                ) {
                    this.logout();
                }
            }
        },
        async logout() {
            try {
                const response = await this.$store.dispatch("logOut");
                this.$router.replace("/auth");
                this.$store.dispatch("destroyToken");
            } catch (err) {
                console.log("Logout Error");
                this.$router.replace("/auth");
                this.$store.dispatch("destroyToken");
            }
        },
    },
};
