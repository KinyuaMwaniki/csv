export default {
    methods: {
        notify(message) {
            notif({
                msg: message,
                type: "success",
            });
        },
    },
};
