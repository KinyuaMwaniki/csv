export default {
    methods: {
        catchForbiddenError(err) {
            if (!!err.response && err.response.status === 403) {
                this.showError("Unauthorized");
            }
        }
    }
};
