export default {
    methods: {
        clearForm() {
            for (const key in this.form) {
                this.form[key] = null;
            }
        },
    },
};
