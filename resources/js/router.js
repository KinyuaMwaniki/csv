import { createRouter, createWebHistory } from "vue-router";

import Auth from "./components/auth/Login.vue";
import ChangePasswordOTP from "./components/auth/ChangePasswordOTP.vue";
import Forgot from "./components/auth/ForgotPassword.vue";
import ChangePassword from "./components/auth/ChangePassword.vue";

import Dashboard from "./components/Dashboard.vue";

// import CountriesIndex from "./components/countries/Index.vue";
// import CountriesCreate from "./components/countries/Create.vue";
// import CountriesShow from "./components/countries/Show.vue";
// import CountriesEdit from "./components/countries/Edit.vue";

// import StatesIndex from "./components/states/Index.vue";
import CurrenciesIndex from "./components/currencies/Index.vue";

import UserRoles from "./components/access/roles/Index.vue";
import RolePermissions from "./components/access/permissions/Index.vue";
import Users from "./components/users/Index.vue";
import UsersDeposit from "./components/users/Deposit.vue";
import UsersUpdateCredit from "./components/users/UpdateCredit.vue";
import UsersCreate from "./components/users/Create.vue";
import UsersEdit from "./components/users/Edit.vue";
import UsersShow from "./components/users/Show.vue";
import Profile from "./components/users/Profile.vue";

import CategoriesIndex from "./components/categories/Index.vue";
import CategoriesCreate from "./components/categories/Create.vue";
import CategoriesShow from "./components/categories/Show.vue";
import CategoriesEdit from "./components/categories/Edit.vue";

import ProductsIndex from "./components/products/Index.vue";
import ProductsCreate from "./components/products/Create.vue";
import ProductsShow from "./components/products/Show.vue";
import ProductsEdit from "./components/products/Edit.vue";

import CatalogIndex from "./components/products_catalog/Index.vue";
import CatalogCart from "./components/products_catalog/Cart.vue";
import CatalogPreview from "./components/products_catalog/Preview.vue";
import CatalogInvoice from "./components/products_catalog/Invoice.vue";
import CatalogPayment from "./components/products_catalog/Payment.vue";
import UpdateOrder from "./components/products_catalog/UpdateOrder.vue";

import InvoicesIndex from "./components/orders/InvoicesIndex.vue";
import OrdersIndex from "./components/orders/OrdersIndex.vue";
import OrdersByDebt from "./components/orders/OrdersByDebt.vue";
import CustomersByDebt from "./components/orders/CustomersByDebt.vue";
import PendingPayments from "./components/orders/PendingPayments.vue";

import ClientsIndex from "./components/clients/Index.vue";
import ClientsCreate from "./components/clients/Create.vue";
import ClientsShow from "./components/clients/Show.vue";
import ClientsEdit from "./components/clients/Edit.vue";

import SalesReport from "./components/orders/reports/Sales.vue";
import CreditSpendingSearch from "./components/orders/reports/CreditSpendingSearch.vue";
import CollectionReport from "./components/orders/reports/Collection.vue";

// import ActivityPeriodShow from "./components/activity_period/Show.vue";
// import ActivityPeriodEdit from "./components/activity_period/Edit.vue";
import BrandsIndex from "./components/brands/Index.vue";
import AttributeTypeIndex from "./components/attribute_types/Index.vue";
import CompaniesIndex from "./components/companies/Index.vue";
import CompaniesEdit from "./components/companies/Edit.vue";
import CompaniesShow from "./components/companies/Show.vue";

import NotFound from "./components/NotFound.vue";

import store from "./store/index.js";

const router = createRouter({
    history: createWebHistory(),
    routes: [
        {
            path: "/auth",
            component: Auth,
            meta: {
                requiresUnAuth: true
            }
        },
        {
            path: "/forgot",
            component: Forgot,
            meta: {
                requiresUnAuth: true
            }
        },
        {
            path: "/change-password-otp",
            component: ChangePasswordOTP,
            meta: {
                requiresUnAuth: true
            }
        },
        {
            path: "/change-password/:token",
            props: true,
            component: ChangePassword,
            meta: {
                requiredDestroyToken: true
            }
        },
        {
            path: "/",
            redirect: "/dashboard",
            meta: {
                requiresAuth: true
            }
        },
        {
            path: "/dashboard",
            component: Dashboard,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: "/:notFound(.*)",
            component: NotFound
        },
        // {
        //     path: "/countries",
        //     component: CountriesIndex,
        //     meta: {
        //         requiresAuth: true,
        //         requiresAdmin: true
        //     }
        // },
        // {
        //     path: "/countries/create",
        //     component: CountriesCreate,
        //     meta: {
        //         requiresAuth: true,
        //         requiresAdmin: true
        //     }
        // },
        // {
        //     path: "/countries/:id/edit",
        //     component: CountriesEdit,
        //     props: true,
        //     meta: {
        //         requiresAuth: true,
        //         requiresAdmin: true
        //     }
        // },
        // {
        //     path: "/countries/:id",
        //     component: CountriesShow,
        //     props: true,
        //     meta: {
        //         requiresAuth: true,
        //         requiresAdmin: true
        //     }
        // },
        {
            path: "/roles",
            component: UserRoles,
            meta: {
                requiresAuth: true,
                requiresAdmin: true
            }
        },
        {
            path: "/roles/:id",
            component: RolePermissions,
            props: true,
            meta: {
                requiresAuth: true,
                requiredAdmin: true
            }
        },
        {
            path: "/users",
            component: Users,
            meta: {
                requiresAuth: true,
                requiresAdmin: true
            }
        },
        {
            path: "/users/update-credit",
            component: UsersUpdateCredit,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: "/users/deposit",
            component: UsersDeposit,
            meta: {
                requiresAuth: true
            }
        },

        {
            path: "/users/:id/profile",
            component: Profile,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: "/users/create",
            component: UsersCreate,
            meta: {
                requiresAuth: true,
                requiresAdmin: true
            }
        },
        {
            path: "/users/:id/edit",
            component: UsersEdit,
            props: true,
            meta: {
                requiresAuth: true,
                requiresAdmin: true
            }
        },
        // {
        //     path: "/states",
        //     component: StatesIndex,
        //     meta: {
        //         requiresAuth: true,
        //         requiresAdmin: true
        //     }
        // },
        {
            path: "/categories",
            component: CategoriesIndex,
            meta: {
                requiresAuth: true,
                requiresAdmin: true
            }
        },
        {
            path: "/categories/create",
            component: CategoriesCreate,
            meta: {
                requiresAuth: true,
                requiresAdmin: true
            }
        },
        {
            path: "/categories/:id",
            component: CategoriesShow,
            props: true,
            meta: {
                requiresAuth: true,
                requiresAdmin: true
            }
        },
        {
            path: "/categories/:id/edit",
            component: CategoriesEdit,
            props: true,
            meta: {
                requiresAuth: true,
                requiresAdmin: true
            }
        },
        {
            path: "/users/:id",
            component: UsersShow,
            props: true,
            meta: {
                requiresAuth: true,
                requiresAdmin: true
            }
        },
        {
            path: "/brands",
            component: BrandsIndex,
            meta: {
                requiresAuth: true,
                requiresAdmin: true
            }
        },
        {
            path: "/attributes",
            component: AttributeTypeIndex,
            meta: {
                requiresAuth: true,
                requiresAdmin: true
            }
        },
        {
            path: "/products",
            component: ProductsIndex,
            meta: {
                requiresAuth: true,
                requiresAdmin: true
            }
        },
        {
            path: "/products/create",
            component: ProductsCreate,
            meta: {
                requiresAuth: true,
                requiresAdmin: true
            }
        },
        {
            path: "/products/:id",
            component: ProductsShow,
            props: true,
            meta: {
                requiresAuth: true,
                requiresAdmin: true
            }
        },
        {
            path: "/products/:id/edit",
            component: ProductsEdit,
            props: true,
            meta: {
                requiresAuth: true,
                requiresAdmin: true
            }
        },
        {
            path: "/products-catalog",
            component: CatalogIndex,
            name: "catalog",
            meta: {
                requiresAuth: true
            }
        },
        {
            path: "/catalog/cart",
            component: CatalogCart,
            name: "cart",
            meta: {
                requiresAuth: true
            }
        },
        {
            path: "/catalog/order-preview/:id?",
            component: CatalogPreview,
            props: true,
            name: "orderPreview",
            meta: {
                requiresAuth: true
            }
        },
        {
            path: "/catalog/invoice/:id?",
            component: CatalogInvoice,
            props: true,
            name: "order.invoice",
            meta: {
                requiresAuth: true
            }
        },
        {
            path: "/catalog/order/edit/:id",
            component: UpdateOrder,
            props: true,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: "/invoices/:id?",
            component: InvoicesIndex,
            props: true,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: "/orders/index",
            component: OrdersIndex,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: "/order-by-debt",
            component: OrdersByDebt,
            meta: {
                requiresAuth: true
            }
        },
        // {
        //     path: "/pending-orders",
        //     component: PendingOrders,
        //     meta: {
        //         requiresAuth: true,
        //         requiresAdmin: true
        //     }
        // },
        {
            path: "/pending-payments",
            component: PendingPayments,
            meta: {
                requiresAuth: true,
                requiresAdmin: true
            }
        },
        {
            path: "/catalog/payment/:id",
            component: CatalogPayment,
            props: true,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: "/customer-by-debt",
            component: CustomersByDebt,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: "/currencies",
            component: CurrenciesIndex,
            meta: {
                requiresAuth: true,
                requiresAdmin: true
            }
        },
        {
            path: "/clients",
            component: ClientsIndex,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: "/clients/create/:from?",
            component: ClientsCreate,
            props: true,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: "/clients/:id",
            component: ClientsShow,
            props: true,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: "/clients/:id/edit/:from?",
            component: ClientsEdit,
            props: true,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: "/reports/credit-spending-search",
            component: CreditSpendingSearch,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: "/reports/sales",
            component: SalesReport,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: "/reports/collection",
            component: CollectionReport,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: "/companies",
            component: CompaniesIndex,
            meta: {
                requiresAuth: true
            }
        },
        // {
        //     path: "/activity-period/show",
        //     component: ActivityPeriodShow,
        //     meta: {
        //         requiresAuth: true
        //     }
        // },
        // {
        //     path: "/activity-period/edit",
        //     component: ActivityPeriodEdit,
        //     meta: {
        //         requiresAuth: true
        //     }
        // },
        {
            path: "/companies/edit",
            component: CompaniesEdit,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: "/companies/show",
            component: CompaniesShow,
            meta: {
                requiresAuth: true
            }
        }
    ]
});

router.beforeEach(function(to, _, next) {
    if (to.meta.requiresAuth && !store.getters.isAuthenticated) {
        store.dispatch("destroyUser");
        next("/auth");
    } else if (to.meta.requiresUnAuth && store.getters.isAuthenticated) {
        next("/");
    } else if (to.meta.requiredDestroyToken && store.getters.isAuthenticated) {
        store.dispatch("destroyToken");
        store.dispatch("destroyUser");
        next();
    } else if (to.meta.requiresAdmin && !store.getters.isAdmin) {
        next("/dashboard");
    } else {
        next();
    }
});

export default router;
